#!/bin/sh

# Non-Interactive
export DEBIAN_FRONTEND=noninteractive

# Workable System
apt-get update -y
apt-get -o Dpkg::Options::="--force-confnew" --force-yes -fuy upgrade
apt-get install -q -y build-essential vim make git

mkdir setup
cd setup

# Apache 
    # Install Latest PCRE
    wget http://iweb.dl.sourceforge.net/project/pcre/pcre/8.32/pcre-8.32.tar.gz
    tar xzvf pcre-8.32.tar.gz
    cd pcre-8.32
        ./configure
        make
        make install
    cd ..

    # Download Apache HTTPD+APN
    wget http://apache.parentingamerica.com//httpd/httpd-2.4.6.tar.gz
    tar xzvf httpd-2.4.6.tar.gz
    cd httpd-2.4.6
        wget http://apache.sunsite.ualberta.ca//apr/apr-1.4.8.tar.gz
        wget http://apache.sunsite.ualberta.ca//apr/apr-util-1.5.2.tar.gz
        tar xzvf apr-1.4.8.tar.gz
        tar xzvf apr-util-1.5.2.tar.gz
        mv apr-1.4.8 srclib/apr
        mv apr-util-1.5.2 srclib/apr-util
        ./configure --prefix=/usr/local/apache --with-included-apr --enable-so
        make
        make install
    cd ..

# MySQL
apt-get install -q -y mysql-server libmysqlclient-dev

# PHP
    # PHP Dependencies
    apt-get install -q -y libxml2-dev libcurl4-openssl-dev pkg-config libmcrypt-dev


    # GD
    apt-get install -q -y libpng-dev libjpeg-dev cmake-curses-gui cmake
    git clone https://bitbucket.org/libgd/gd-libgd.git
    cd gd-libgd
        mkdir build
        cd build 
            cmake ..
            make
            make install
    cd ../..

    # Download PHP
    wget http://ar2.php.net/distributions/php-5.3.23.tar.gz
    tar xzvf php-5.3.23.tar.gz
    cd php-5.3.23
        ./configure \
            --with-curl \
            --with-mhash \
            --with-intl \
            --with-gd \
            --with-pear \
            --with-apxs2=/usr/local/apache/bin/apxs \
            --with-openssl \
            --with-pdo-mysql \
            --with-jpeg-dir=/usr/lib/x86_64-linux-gnu/ \
            --enable-xdebug \
            --enable-zip \
            --enable-mbstring \
            --with-zlib \
            --with-mcrypt \
            --enable-pcntl \
            --enable-calendar \
            --enable-sockets

        # Compile and Install
        make && make install
    cd ..


cd ..
rm -fr setup
rm /etc/ld.so.cache
/sbin/ldconfig

# Configure PHP
echo "
    error_reporting = -1
    date.timezone = 'America/Vancouver'
    magic_globals = off
    magic_quotes_gpc = off
" | tee -a /usr/local/lib/php.ini

echo "
    <FilesMatch \.php$>
        SetHandler application/x-httpd-php
    </FilesMatch>
    <Directory /vagrant>
        Options all
        AllowOverride All
        order deny,allow
        allow from all
        Require all granted
    </Directory>
    Include conf/extra/httpd-vhosts.conf
    LoadModule rewrite_module modules/mod_rewrite.so
" | tee -a /usr/local/apache/conf/httpd.conf

echo "
    <VirtualHost *:80>
        ServerName www.orcamax.dev
        ServerAlias *.orcamax.dev
        DocumentRoot /vagrant/web
    </VirtualHost>
" | tee /usr/local/apache/conf/extra/httpd-vhosts.conf

