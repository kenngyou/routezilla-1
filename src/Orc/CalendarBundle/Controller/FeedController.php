<?php

namespace Orc\CalendarBundle\Controller;

use Orc\BookingBundle\Entity\Worker;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FeedController extends Controller
{
    public function feedAction(Worker $worker)
    {
        $response = new Response();

        if ($this->container->getParameter('kernel.environment') == 'dev') {
            $response->headers->set('Content-Type', 'text/plain');
        } else {
           $response->headers->set('Content-Type', 'text/calendar; charset: utf-8');
        }

        $response->headers->set('Cache-Control', 'max-age=10');
        $response->setContent(str_replace("\n", "\r\n", $this->get('templating')->render('OrcCalendarBundle:Feed:feed.ical.twig', array(
            'worker' => $worker,
            'user' => $worker->getUser(),
            'bookings' => $worker->getCrew() ? $this->get('orc_booking.repository.booking')->findUpcomingByCrew($worker->getCrew(), null, new \DateTime('today')) : array()
        ))));

        return $response;
    }
}
