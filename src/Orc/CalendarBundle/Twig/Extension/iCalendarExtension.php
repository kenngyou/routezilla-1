<?php

namespace Orc\CalendarBundle\Twig\Extension;

class iCalendarExtension extends \Twig_Extension
{
    protected $dateFormat;
    protected $wrapLength;
    protected $textReplacements = array(
        '\\' => '\\\\',
        '"'  => '\"',
        "\r" => '',
        "\n" => '\n ',
        ';'  => '\;',
        ','  => '\,'
    );

    public function __construct($dateFormat, $wrapLength)
    {
        $this->dateFormat = $dateFormat;
        $this->wrapLength = $wrapLength;
    }

    public function getFilters()
    {
        return array(
            'ical_date' => new \Twig_Filter_Method($this, 'iCalDate'),
            'ical_text' => new \Twig_Filter_Method($this, 'iCalText')
        );
    }

    /**
     * Converts a \DateTime into something iCal can read
     * @param    \DateTime        input date
     * @return   string           iCalendar ready date string
     */
    public function icalDate(\DateTime $date)
    {
        $date = clone $date;
        $date->setTimezone(new \DateTimeZone('GMT'));
        return $date->format($this->dateFormat);
    }

    /**
     * Escape text for iCalendar use
     * @param    string        Unsafe text
     * @return   string        iCalendar safe text
     */
    public function icalText($text)
    {
        $text = str_replace(array_keys($this->textReplacements), array_values($this->textReplacements), $text);
        $text = wordwrap($text, $this->wrapLength, "\n ", true);

        return $text;
    }

    public function getName()
    {
        return 'orc_calendar_ical';
    }
}
