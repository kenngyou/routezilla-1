<?php

namespace Orc\CalendarBundle\Request\ParamConverter;

use Orc\UserBundle\Entity\UserManager;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

class TokenParamConverter implements ParamConverterInterface
{
    protected $repository;

    public function __construct(EntityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function apply(Request $request, ConfigurationInterface $configuration)
    {
        if (!$token = $request->attributes->get('workerToken')) {
            return;
        }

        if (!$worker = $this->repository->findOneByToken($token)) {
            throw new AccessDeniedHttpException('Invalid user specified');
        }

        $request->attributes->set('id', $worker->getUser()->getId());
    }

    public function supports(ConfigurationInterface $configuration)
    {
        return true;
    }
}
