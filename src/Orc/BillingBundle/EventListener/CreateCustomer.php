<?php

namespace Orc\BillingBundle\EventListener;

use Orc\BillingBundle\Service\StripeCustomer;
use Orc\BillingBundle\Entity\StripeCustomer as Customer;
use Orc\BillingBundle\Repository\PlanRepository;
use Orc\SaasBundle\Event\ClientEvent;
use Doctrine\ORM\EntityManager;

/**
 * Responsible for creating customers in Stripe on registration
 */
class CreateCustomer
{
    /**
     * @param EntityManager $em
     * @param StripeCustomer $customerService stripe customer service
     * @param PlanRepository $planRepository for accessing plan to subscribe to
     */
    public function __construct(EntityManager $em, StripeCustomer $customerService, PlanRepository $planRepository)
    {
        $this->em = $em;
        $this->customerService = $customerService;
        $this->planRepository = $planRepository;
    }

    /**
     * Creates a new stripe account for a new Client
     * Updates the client to contain the Stripe Customer ID
     *
     * @param    ClientEvent
     */
    public function onCreateClient(ClientEvent $event)
    {
        $client = $event->getClient();
        $plan = $this->planRepository->findDefault();

        $end = new \DateTime('+1 month');
        $end->setTimezone(new \DateTimeZone('UTC'));

        $customer = $this->customerService->create(array(
            'description' => $client->getName(),
            'email'       => $client->getUser()->getEmail(),
            'plan'        => $plan->getId(),
            'trial_end'   => $end->format('U')
        ));

        $client->setStripeId($customer['id']);
        $client->setPlan($plan);

        $stripeCustomer = new Customer();
        $stripeCustomer->setId($customer['id']);
        $stripeCustomer->setLastUpdated(new \DateTime());
        $stripeCustomer->setTrialEndDate($end);
        $stripeCustomer->setClient($client);
        $client->setCustomer($stripeCustomer);

        $this->em->persist($client);
        $this->em->persist($stripeCustomer);
        $this->em->flush();
    }
}
