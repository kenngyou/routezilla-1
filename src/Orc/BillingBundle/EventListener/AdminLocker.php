<?php

namespace Orc\BillingBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class AdminLocker
{
    /**
     * @param string $prefix url prefix to block
     * @param array $whitelistedUrlsPatterns regex to match urls to block
     * @param UrlGeneratorInterface $urlGenerator
     */
    public function __construct($prefix, array $whitelist = array(), UrlGeneratorInterface $urlGenerator)
    {
        $this->prefix = $prefix;
        $this->whitelist = $whitelist;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * Block access to $prefix excluding $whitelist
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if (!$this->isBlockedRequest($request)) {
            return;
        }

        if ($this->isExpiredClient($request) or $this->isExceedingClient($request) or $this->isCancelledClient($request) or $this->isEndedTrial($request)) {
            if ($request->getPathInfo() == '/') {
                $event->setResponse(new RedirectResponse($this->urlGenerator->generate('offline')));
            } else {
                $event->setResponse(new RedirectResponse($this->urlGenerator->generate('billing')));
            }

            $response = $event->getResponse();
            $response->setPrivate();
            $response->setMaxAge(0);
            $response->setSharedMaxAge(0);
            $response->headers->addCacheControlDirective('must-revalidate', true);
            $response->headers->addCacheControlDirective('no-store', true);
        }
    }

    /**
     * Determine whether or not a client is expired in this request
     * @param Request $request (used to grab client)
     * @return boolean true if expired
     */
    protected function isExpiredClient(Request $request)
    {
        if (!$client = $request->attributes->get('client')) {
            return false;
        }

        return $client->isExpired();
    }

    /**
     * Determine whether or not the user is stuck on an expired trial
     * @param Request $request
     * @return boolean true if expired
     */
    protected function isEndedTrial(Request $request)
    {
        if (!$client = $request->attributes->get('client')) {
            return false;
        }

        if ($client->getCustomer() and $client->getCustomer()->getTrialEndDate()) {
            if ($client->getCustomer()->getTrialEndDate() < new \DateTime()) {
                return true;
            }

            return false;
        }
    }

    /**
     * Determine whether or not a client has exceeded their plan limit
     * @param Request $request
     * @return boolean true if exceeding
     */
    protected function isExceedingClient(Request $request)
    {
        if (!$client = $request->attributes->get('client')) {
            return false;
        }

        if (!$client->getPlan() or !$max = $client->getPlan()->getMaxCrews()) {
            return false;
        }

        if ($client->getCustomer() and $client->getCustomer()->getTrialEndDate()) {
            return false;
        }

        return $client->getCrews() > $max;
    }

    /**
     * Determine whether or not the client is cancelled
     * @param Request $request
     * @return boolean true if cancelled
     */
    protected function isCancelledClient(Request $request)
    {
        if (!$client = $request->attributes->get('client')) {
            return false;
        }

        return !$client->getPlan();
    }

    /**
     * Determine whether or not a request is blocked
     * @param Request $request
     * @return boolean true if blocked
     */
    protected function isBlockedRequest(Request $request)
    {
        $url = $request->getPathInfo();
        foreach ($this->whitelist as $safeUrl) {
            if (strpos($url, $safeUrl) !== false) {
                return false;
            }
        }

        return strpos($url, $this->prefix) !== false || $url == '/';
    }
}
