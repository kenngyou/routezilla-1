<?php

namespace Orc\BillingBundle\Request\ParamConverter;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Orc\BillingBundle\Request\Webhook;
use Orc\BillingBundle\Service\StripeEvent;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

class WebhookConverter implements ParamConverterInterface
{
    protected $eventService;

    public function __construct(StripeEvent $eventService)
    {
        $this->eventService = $eventService;
    }

    public function apply(Request $request, ConfigurationInterface  $configuration)
    {
        if (!$raw = json_decode($request->getContent())) {
            throw new \RuntimeException('Could not parse webhook JSON');
        }

        if ($raw->livemode) {
            try {
                $this->eventService->view($raw->id);
            } catch (\Stripe_InvalidRequestError $e) {
                throw new NotFoundHttpException();
            }
        }

        $request->attributes->set('webhook', new Webhook($raw->type, $raw->data));
    }

    public function supports(ConfigurationInterface $configuration)
    {
        return $configuration->getClass() == 'Orc\BillingBundle\Request\Webhook';
    }
}
