<?php

namespace Orc\BillingBundle\Tests\Request\ParamConverter;

use Symfony\Component\HttpFoundation\Request;
use Orc\BillingBundle\Request\ParamConverter\WebhookConverter;

class WebhookConverterTest extends \PHPUnit_Framework_TestCase
{
    public function testConvertsContentToWebhookAttribute()
    {
        $input = (object)array(
            'id' => 'event_x',
            'type' => 'event.from',
            'data' => array('args'),
            'livemode' => true
        );

        $eventService = $this->getMockBuilder('Orc\BillingBundle\Service\StripeEvent')->disableOriginalConstructor()->getMock();
        $eventService
            ->expects($this->once())
            ->method('view')
            ->with($input->id)
            ->will($this->returnValue($input))
        ;

        $request = $this->getMock('Symfony\Component\HttpFoundation\Request');
        $request
            ->expects($this->once())
            ->method('getContent')
            ->will($this->returnValue(json_encode($input)))
        ;
        $request->attributes = $this->getMock('Symfony\Component\HttpFoundation\ParameterBag');
        $request->attributes
            ->expects($this->once())
            ->method('set')
            ->with('webhook', $this->callback(function($webhook) {
                return 'event.from' == $webhook->getType() && array('args') == $webhook->getData();
            }))
        ;

        $converter = new WebhookConverter($eventService);
        $converter->apply($request, $this->getMock('Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface'));
    }

    public function testThrowsExceptionOnBadJson()
    {
        $request = $this->getMock('Symfony\Component\HttpFoundation\Request');
        $request
            ->expects($this->once())
            ->method('getContent')
            ->will($this->returnValue('df;#$lkj43$<'))
        ;

        $eventService = $this->getMockBuilder('Orc\BillingBundle\Service\StripeEvent')->disableOriginalConstructor()->getMock();

        $this->setExpectedException('RuntimeException');

        $converter = new WebhookConverter($eventService);
        $converter->apply($request, $this->getMock('Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface'));
    }

    public function testInvalidEventThrows404()
    {
        $input = (object)array(
            'id' => 'event_x',
            'type' => 'event.from',
            'data' => array('args'),
            'livemode' => true
        );

        $eventService = $this->getMockBuilder('Orc\BillingBundle\Service\StripeEvent')->disableOriginalConstructor()->getMock();
        $eventService
            ->expects($this->once())
            ->method('view')
            ->with($input->id)
            ->will($this->throwException(new \Stripe_InvalidRequestError('blah', 'blah')))
        ;

        $request = $this->getMock('Symfony\Component\HttpFoundation\Request');
        $request
            ->expects($this->once())
            ->method('getContent')
            ->will($this->returnValue(json_encode($input)))
        ;

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');

        $converter = new WebhookConverter($eventService);
        $converter->apply($request, $this->getMock('Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface'));
    }

    public function testInvalidEventInTestModeIsIgnored()
    {
        $input = (object)array(
            'id' => 'event_x',
            'type' => 'event.from',
            'data' => array('args'),
            'livemode' => false
        );

        $eventService = $this->getMockBuilder('Orc\BillingBundle\Service\StripeEvent')->disableOriginalConstructor()->getMock();

        $request = $this->getMock('Symfony\Component\HttpFoundation\Request');
        $request
            ->expects($this->once())
            ->method('getContent')
            ->will($this->returnValue(json_encode($input)))
        ;
        $request->attributes = $this->getMock('Symfony\Component\HttpFoundation\ParameterBag');
        $request->attributes
            ->expects($this->once())
            ->method('set')
            ->with('webhook', $this->callback(function($webhook) {
                return 'event.from' == $webhook->getType() && array('args') == $webhook->getData();
            }))
        ;

        $converter = new WebhookConverter($eventService);
        $converter->apply($request, $this->getMock('Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface'));
    }
}
