<?php

namespace Orc\BillingBundle\Tests\Sync;

use Orc\BillingBundle\Sync\SyncPlans;
use Orc\BillingBundle\Entity\Plan;

class SyncPlansTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesNewPlans()
    {
        $planRepository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $planRepository
            ->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue($existingPlans = array()))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripePlan')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($newPlans = array(
                array(
                    'id' => 'the_id',
                    'name' => 'Fancy Pants',
                    'price' => 500,
                    'interval' => 'month'
                )
            )))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($this->isInstanceOf('Orc\BillingBundle\Entity\Plan'))
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $planSync = new SyncPlans($em, $planRepository, $stripe);
        $planSync->sync();
        $messages= $planSync->getMessages();

        $this->assertContainsMessage('created', $planSync);
    }

    public function testUpdatesExistingPlans()
    {
        $plan = new Plan();
        $plan->setId('hello_world');
        $plan->setPrice(500);

        $planRepository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $planRepository
            ->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue($existingPlans = array($plan)))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripePlan')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($newPlans = array(
                array(
                    'id' => 'hello_world',
                    'name' => 'New Name',
                    'price' => 250,
                    'trial_period_days' => 30,
                    'interval' => 'month'
                )
            )))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($this->isInstanceOf('Orc\BillingBundle\Entity\Plan'))
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $planSync = new SyncPlans($em, $planRepository, $stripe);
        $planSync->sync();

        $this->assertEquals(250, $plan->getPrice());
        $this->assertEquals('New Name', $plan->getName());
        $this->assertEquals(30, $plan->getTrialLength());
        $this->assertContainsMessage('updated', $planSync);
    }

    public function testDeletesRemovedPlans()
    {
        $plan = new Plan();

        $planRepository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $planRepository
            ->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue($existingPlans = array($plan)))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripePlan')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($newPlans = array()))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($plan)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $planSync = new SyncPlans($em, $planRepository, $stripe);
        $planSync->sync();

        $this->assertContainsMessage('deleted', $planSync);
        $this->assertTrue($plan->getDeleted());
    }

    public function testExtractsCrewsFromName()
    {
        $plan = new Plan();
        $plan->setId('max_5_yo');

        $planRepository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $planRepository
            ->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue($existingPlans = array($plan)))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripePlan')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($newPlans = array(
                array(
                    'id' => 'max_5_yo',
                    'name' => 'Max 5 (5)',
                    'price' => 10,
                    'interval' => 'month'
                )
            )))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($plan)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $planSync = new SyncPlans($em, $planRepository, $stripe);
        $planSync->sync();


        $this->assertEquals(5, $plan->getMaxCrews());
    }

    public function testUnlimitedCrewsIfNotMatching()
    {
        $plan = new Plan();
        $plan->setId('nomax');
        $plan->setMaxCrews(100);

        $planRepository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $planRepository
            ->expects($this->once())
            ->method('findAll')
            ->will($this->returnValue($existingPlans = array($plan)))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripePlan')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('fetchAll')
            ->will($this->returnValue($newPlans = array(
                array(
                    'id' => 'nomax',
                    'name' => 'No Maximum',
                    'price' => 10,
                    'interval' => 'month'
                )
            )))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($plan)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $planSync = new SyncPlans($em, $planRepository, $stripe);
        $planSync->sync();


        $this->assertNull($plan->getMaxCrews());
    }

    protected function assertContainsMessage($keyword, $sync)
    {
        $found = false;
        foreach ($sync->getMessages() as $message) {
            if (strpos($message, $keyword) !== false) {
                $found = true;
            }
        }

        if (!$found) {
            $this->fail('No sync messages contain "' . $keyword . '"');
        }
    }
}
