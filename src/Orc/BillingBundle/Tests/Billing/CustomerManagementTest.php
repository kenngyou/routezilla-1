<?php

namespace Orc\BillingBundle\Tests\Billing;

use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\Billing\CustomerManagement;

class CustomerManagementTest extends \PHPUnit_Framework_TestCase
{
    public function testSubscribe()
    {
        $card = "asdf";
        $plan = new Plan();
        $plan->setId($planId = 'theplan');
        $trialEnd = "trial end";

        $client = new Client();
        $client->setStripeId($id = "adsfas");

        $customer = $this->getMockBuilder('Stripe_Customer')->disableOriginalConstructor()->getMock();
        $customer
            ->expects($this->any())
            ->method('offsetGet')
            ->with('subscription')
            ->will($this->returnValue(array('trial_end' => $trialEnd)))
        ;
        $customer
            ->expects($this->once())
            ->method('updateSubscription')
            ->with(array(
                'card' => $card,
                'plan' => $planId,
                'trial_end' => $trialEnd,
                'coupon' => null
            ));

        $stripeCustomer = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripeCustomer
            ->expects($this->once())
            ->method('view')
            ->with($id)
            ->will($this->returnValue($customer))
        ;
        $stripeCharge = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock();

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $service = $this->getManager($stripeCustomer, $stripeCharge, null, $em);
        $service->subscribe($client, $plan, $card);

        $this->assertSame($plan, $client->getPlan());
        $this->assertEquals(Client::STATUS_ACTIVE, $client->getStatus());
        $this->assertFalse($client->getSkipWizard());
    }

    public function testChangeCard()
    {
        $client = new Client();
        $client->setStripeId($id = "adsfas");
        $card = "Asdfsa";

        $stripeCustomer = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripeCustomer
            ->expects($this->once())
            ->method('update')
            ->with($id, array('card' => $card))
        ;
        $stripeCustomer
            ->expects($this->once())
            ->method('flushCache')
            ->with($id)
        ;
        $stripeCharge = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock();



        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();


        $service = $this->getManager($stripeCustomer, $stripeCharge, null, $em);
        $service->changeCard($client, $card);
    }

    public function testChangePlan()
    {
        $trialEnd = 500;
        $plan = new Plan();
        $plan->setId($planId = 'theplan');

        $client = new Client();
        $client->setStripeId($id = "adsfas");

        $customer = $this->getMockBuilder('Stripe_Customer')->disableOriginalConstructor()->getMock();
        $customer
            ->expects($this->any())
            ->method('offsetGet')
            ->with('subscription')
            ->will($this->returnValue(array('trial_end' => $trialEnd)))
        ;
        $customer
            ->expects($this->once())
            ->method('updateSubscription')
            ->with(array(
                'plan' => $planId,
                'trial_end' => $trialEnd,
                'coupon' => null
            ));

        $stripeCustomer = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripeCustomer
            ->expects($this->once())
            ->method('view')
            ->with($id)
            ->will($this->returnValue($customer))
        ;
        $stripeCustomer
            ->expects($this->once())
            ->method('flushCache')
            ->with($id)
        ;
        $stripeCharge = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock();


        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $service = $this->getManager($stripeCustomer, $stripeCharge, null, $em);
        $service->changePlan($client, $plan, null);

        $this->assertSame($plan, $client->getPlan());
    }

    public function testSquareUp()
    {
        $card = "!23212dfsdf";
        $client = new Client();
        $client->setStripeId($id = "adsfas");

        $customer = $this->getMockBuilder('Stripe_Customer')->disableOriginalConstructor()->getMock();
        $customer
            ->expects($this->once())
            ->method('offsetGet')
            ->with('account_balance')
            ->will($this->returnValue($balance = 500))
        ;

        $stripeCustomer = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripeCustomer
            ->expects($this->once())
            ->method('view')
            ->with($id)
            ->will($this->returnValue($customer))
        ;
        $stripeCustomer
            ->expects($this->once())
            ->method('flushCache')
            ->with($id)
        ;
        $stripeCharge = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock();

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $service = $this->getManager($stripeCustomer, $stripeCharge, null, $em);
        $service->squareUp($client, $card);

        $this->assertEquals(Client::STATUS_ACTIVE, $client->getStatus());
    }

    public function testCancel()
    {
        $client = new Client();
        $client->setStripeId($id = "adsfas");

        $customer = $this->getMockBuilder('Stripe_Customer')->disableOriginalConstructor()->getMock();
        $customer
            ->expects($this->once())
            ->method('cancelSubscription')
        ;

        $stripeCustomer = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripeCustomer
            ->expects($this->once())
            ->method('view')
            ->with($id)
            ->will($this->returnValue($customer))
        ;
        $stripeCustomer
            ->expects($this->once())
            ->method('flushCache')
            ->with($id)
        ;
        $stripeCharge = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock();

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $service = $this->getManager($stripeCustomer, $stripeCharge, null, $em);
        $service->cancel($client);

        $this->assertNull($client->getPlan());
        $this->assertEquals(Client::STATUS_CANCELLED, $client->getStatus());
    }

    protected function getManager($stripeCustomer = null, $stripeCharge = null, $stripeCoupon = null, $em = null)
    {
        return new CustomerManagement(
            $stripeCustomer ?: $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock(),
            $stripeCharge   ?: $this->getMockBuilder('Orc\BillingBundle\Service\StripeCharge')->disableOriginalConstructor()->getMock(),
            $stripeCoupon   ?: $this->getMockBuilder('Orc\BillingBundle\Service\StripeCoupon')->disableOriginalConstructor()->getMock(),
            $em             ?: $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock()
        );
    }
}
