<?php

namespace Orc\BillingBundle\Tests\WebhookListener;

use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\WebhookListener\UpdateCustomerPlan;

class UpdateCustomerPlanTest extends \PHPUnit_Framework_TestCase
{
    public function testIgnoresNoPlanChange()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());
        $plan->setId(6);

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId = 'cus_5'))
        ;
        $event
            ->expects($this->once())
            ->method('getPlanId')
            ->will($this->returnValue(6))
        ;

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId = 'cus_5')
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $listener = new UpdateCustomerPlan($repository, $em);
        $listener->onSubscriptionUpdate($event);
    }

    public function testUpdatesPlan()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());
        $plan->setId(6);

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId = 'cus_5'))
        ;
        $event
            ->expects($this->once())
            ->method('getPlanId')
            ->will($this->returnValue(5))
        ;

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId = 'cus_5')
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('getReference')
            ->with('Orc\BillingBundle\Entity\Plan', 5)
            ->will($this->returnValue($newPlan = new Plan()))
        ;
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $listener = new UpdateCustomerPlan($repository, $em);
        $listener->onSubscriptionUpdate($event);

        $this->assertSame($newPlan, $client->getPlan());
    }
}
