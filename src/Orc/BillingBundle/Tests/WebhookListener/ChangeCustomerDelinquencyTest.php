<?php

namespace Orc\BillingBundle\Tests\WebhookListener;

use DateTime;
use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\StripeCustomer;
use Orc\BillingBundle\WebhookListener\ChangeCustomerDelinquency;

class ChangeCustomerDelinquencyTest extends \PHPUnit_Framework_TestCase
{
    public function testSuccessDoesNothingIfCannotFind()
    {
        $id = 'cus_5';
        $client = new Client();

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($id)
            ->will($this->returnValue(null))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\ChargeEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($id))
        ;

        $listener = new ChangeCustomerDelinquency($repository, $em);
        $listener->onChargeSuccess($event);
    }

    public function testSuccessClearsDelinquencyANdTrial()
    {
        $id = 'cus_5';
        $client = new Client();
        $client->setCustomer($customer = new StripeCustomer());
        $client->setDeliquentSince(new DateTime());
        $customer->setTrialEndDate(new DateTime());

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($id)
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->at(0))
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->at(1))
            ->method('persist')
            ->with($customer)
        ;
        $em
            ->expects($this->at(2))
            ->method('flush')
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\ChargeEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($id))
        ;

        $listener = new ChangeCustomerDelinquency($repository, $em);
        $listener->onChargeSuccess($event);

        $this->assertNull($client->getDeliquentSince());
        $this->assertNull($customer->getTrialEndDate());
    }

    public function testFailedChargeIgnoresBadClient()
    {
        $id = 'asdf';

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($id)
            ->will($this->returnValue(null))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\ChargeEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($id))
        ;

        $listener = new ChangeCustomerDelinquency($repository, $em);
        $listener->onChargeFailure($event);
    }

    public function testFailedChargesSetDeliquency()
    {
        $id = 'cus_5';
        $client = new Client();
        $client->setDeliquentSince(null);

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($id)
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->at(0))
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->at(1))
            ->method('flush')
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\ChargeEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($id))
        ;

        $listener = new ChangeCustomerDelinquency($repository, $em);
        $listener->onChargeFailure($event);

        $this->assertEquals(new DateTime(), $client->getDeliquentSince());
    }

    public function testOnEndingTrialAddsDeliquency()
    {
        $id = 'cus_5';
        $client = new Client();
        $client->setDeliquentSince(null);

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($id)
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->at(0))
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->at(1))
            ->method('flush')
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getWebhookData')
            ->will($this->returnValue((object)array(
                'status' => 'past due'
            )));
        ;
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($id))
        ;

        $listener = new ChangeCustomerDelinquency($repository, $em);
        $listener->onSubscriptionUpdate($event);

        $this->assertEquals(new DateTime(), $client->getDeliquentSince());
    }
}
