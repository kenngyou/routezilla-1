<?php

namespace Orc\BillingBundle\Tests\WebhookListener;

use Orc\BillingBundle\WebhookListener\ChangeSubscriptionStatus;
use DateTime;
use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\StripeCustomer;
use Orc\BillingBundle\EventListener\SubscriptionChangeListener;

class ChangeSubscriptionStatusTest extends \PHPUnit_Framework_TestCase
{
    public function testUpdateConvertsExpiredToActive()
    {
        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId = 'cuS_5'))
        ;
        $event
            ->expects($this->once())
            ->method('getWebhookData')
            ->will($this->returnValue($data = (object)array(
                'object' => (object)array(
                    'trial_end' => time()
                )
            )))
        ;

        $client = new Client();
        $client->setStatus(Client::STATUS_EXPIRED);
        $client->setCustomer($customer = new StripeCustomer());

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId)
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->at(0))
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->at(1))
            ->method('persist')
            ->with($customer)
        ;
        $em
            ->expects($this->at(2))
            ->method('flush')
        ;

        $listener = new ChangeSubscriptionStatus($repository, $em);
        $listener->onSubscriptionUpdate($event);

        $this->assertEquals(Client::STATUS_ACTIVE, $client->getStatus());
    }

    public function testUpdateGrabsLatestTrialDate()
    {
        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId = 'cuS_5'))
        ;
        $event
            ->expects($this->once())
            ->method('getWebhookData')
            ->will($this->returnValue($data = (object)array(
                'object' => (object)array(
                    'trial_end' => $time = time()
                )
            )))
        ;

        $client = new Client();
        $client->setStatus(Client::STATUS_EXPIRED);
        $client->setCustomer($customer = new StripeCustomer());
        $customer->setTrialEndDate(new \DateTime('-100 days'));

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId)
            ->will($this->returnValue($client))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();

        $listener = new ChangeSubscriptionStatus($repository, $em);
        $listener->onSubscriptionUpdate($event);

        $this->assertEquals(DateTime::createFromFormat('U', $time), $customer->getTrialEndDate());
    }

    public function testEndChangeStatusToExpired()
    {
        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId = 'cus_5')
            ->will($this->returnValue($client = new Client()))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->once())
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->once())
            ->method('flush')
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\SubscriptionEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId))
        ;

        $listener = new ChangeSubscriptionStatus($repository, $em);
        $listener->onSubscriptionEnd($event);

        $this->assertEquals(Client::STATUS_EXPIRED, $client->getStatus());

    }
}
