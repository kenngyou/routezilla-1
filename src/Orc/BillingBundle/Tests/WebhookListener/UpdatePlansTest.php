<?php

namespace Orc\BillingBundle\Tests\WebhookListener;

use Orc\BillingBundle\WebhookListener\UpdatePlans;

class UpdatePlansTest extends \PHPUnit_Framework_TestCase
{
    public function testAlwaysSyncs()
    {
        $sync = $this->getMockBuilder('Orc\BillingBundle\Sync\SyncPlans')->disableOriginalConstructor()->getMock();
        $sync
            ->expects($this->once())
            ->method('sync')
        ;
        $sync
            ->expects($this->once())
            ->method("getMessages")
            ->will($this->returnValue(array('a', 'b', 'c')))
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\WebhookEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('addMessage')
            ->with('updating plans: a,b,c')
        ;

        $updater = new UpdatePlans($sync);
        $updater->onPlanChange($event);
    }
}
