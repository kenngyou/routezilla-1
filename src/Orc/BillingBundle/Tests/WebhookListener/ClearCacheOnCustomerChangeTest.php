<?php

namespace Orc\BillingBundle\Tests\WebhookListener;

use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\WebhookListener\ClearCacheOnCustomerChange;

class ClearCacheOnCustomerChangeTest extends \PHPUnit_Framework_TestCase
{
    public function testClearsCache()
    {
        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findOneByStripeId')
            ->with($customerId = 'cus_5')
            ->will($this->returnValue(new Client()))
        ;

        $event = $this->getMockBuilder('Orc\BillingBundle\Event\CustomerEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue($customerId))
        ;

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('flushCache')
            ->with($customerId)
        ;

        $listener = new ClearCacheOnCustomerChange($repository, $stripe);
        $listener->onCustomerUpdate($event);
    }

    public function testIgnoresIfNotFound()
    {
        $event = $this->getMockBuilder('Orc\BillingBundle\Event\CustomerEvent')->disableOriginalConstructor()->getMock();
        $event
            ->expects($this->once())
            ->method('getCustomerId')
            ->will($this->returnValue(null))
        ;

        $repository = $this->getMockBuilder('Orc\SaasBundle\Repository\ClientRepository')->disableOriginalConstructor()->getMock();
        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();

        $listener = new ClearCacheOnCustomerChange($repository, $stripe);
        $listener->onCustomerUpdate($event);
    }
}
