<?php

namespace Orc\BillingBundle\Tests\Controller;

use Orc\BillingBundle\Request\Webhook;
use Orc\BillingBundle\Controller\WebhooksController;

class WebhooksControllerTest extends \PHPUnit_Framework_TestCase
{
    public function testDefaultsToWebhookEvent()
    {
        $dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with('type', $this->callback(function($obj) { return get_class($obj) === 'Orc\BillingBundle\Event\WebhookEvent'; }))
        ;

        $controller = new WebhooksController($dispatcher);
        $controller->receiveAction(new Webhook('type', array()));
    }

    public function testUseMappedClassINstead()
    {
        $mapping = array(
            'customer.' => 'Orc\BillingBundle\Event\CustomerEvent'
        );

        $dispatcher = $this->getMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
        $dispatcher
            ->expects($this->once())
            ->method('dispatch')
            ->with(
                'customer.updated',
                $this->callback(function($obj) {
                    return get_class($obj) === 'Orc\BillingBundle\Event\CustomerEvent';
                })
            )
        ;

        $controller = new WebhooksController($dispatcher, $mapping);
        $controller->receiveAction(new Webhook('customer.updated', array()));
    }
}
