<?php

namespace Orc\BillingBundle\Tests\EventListener;

use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\Entity\StripeCustomer;
use Orc\BillingBundle\EventListener\AdminLocker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class AdminLockerTest extends \PHPUnit_Framework_TestCase
{
    public function testAlwaysAllowUnblockedLocations()
    {
        $client = new Client();
        $client->setStatus(Client::STATUS_EXPIRED);

        $request = Request::create('/dashboard/billing');
        $request->attributes->set('client', $client);

        $locker = new AdminLocker(
            '/dashboard',
            array('/dashboard/billing'),
            $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface')
        );

        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $this->assertNull($event->getResponse());
    }

    public function testAlwaysAllowWhitelistedLocations()
    {
        $client = new Client();
        $client->setStatus(Client::STATUS_EXPIRED);

        $request = Request::create('/dashboard');
        $request->attributes->set('client', $client);

        $locker = new AdminLocker(
            '/dashboard',
            array('/dashboard'),
            $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface')
        );

        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $this->assertNull($event->getResponse());
    }

    public function testBlocksExpiredClientsInBlockedLocations()
    {
        $client = new Client();
        $client->setStatus(Client::STATUS_EXPIRED);

        $request = Request::create('/dashboard');
        $request->attributes->set('client', $client);

        $urlGenerator = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface');
        $urlGenerator
            ->expects($this->once())
            ->method('generate')
            ->with('billing')
            ->will($this->returnValue($url = '/billing'))
        ;

        $locker = new AdminLocker(
            '/dashboard',
            array(),
            $urlGenerator
        );
        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $response = $event->getResponse();

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $response);
        $this->assertEquals($url, $response->getTargetUrl());
    }

    public function testBlocksExceededClientsInBlockedLocations()
    {
        $client = new Client();
        $client->setCrews(5);
        $client->setPlan($plan = new Plan());
        $plan->setMaxCrews(3);

        $request = Request::create('/dashboard');
        $request->attributes->set('client', $client);

        $urlGenerator = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface');
        $urlGenerator
            ->expects($this->once())
            ->method('generate')
            ->with('billing')
            ->will($this->returnValue($url = '/billing'))
        ;

        $locker = new AdminLocker('/dashboard', array(), $urlGenerator);
        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $response = $event->getResponse();

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $response);
        $this->assertEquals($url, $response->getTargetUrl());
    }

    public function testBlocksCustomersPastTrialWithoutCard()
    {
        $client = new Client();
        $client->setCrews(5);
        $client->setStatus(Client::STATUS_STEP_5);
        $client->setPlan($plan = new Plan());
        $client->setCustomer($customer = new StripeCustomer());
        $customer->setTrialEndDate(new \DateTime('-1 day'));

        $plan->setMaxCrews(3);
        $plan->setTrialLength(30);

        $request = Request::create('/dashboard');
        $request->attributes->set('client', $client);

        $urlGenerator = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface');
        $urlGenerator
            ->expects($this->once())
            ->method('generate')
            ->with('billing')
            ->will($this->returnValue($url = '/billing'))
        ;

        $locker = new AdminLocker('/dashboard', array(), $urlGenerator);
        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $response = $event->getResponse();

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $response);
        $this->assertEquals($url, $response->getTargetUrl());
    }

    public function testAllowsWhileTrialing()
    {
        $client = new Client();
        $client->setCrews(5);
        $client->setStatus(Client::STATUS_STEP_5);
        $client->setPlan($plan = new Plan());
        $client->setCustomer($customer = new StripeCustomer());
        $customer->setTrialEndDate(new \DateTime());


        $plan->setMaxCrews(3);
        $plan->setTrialLength(30);

        $request = Request::create('/dashboard');
        $request->attributes->set('client', $client);

        $locker = new AdminLocker('/dashboard', array(), $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface'));
        $locker->onKernelRequest(
            $event = new GetResponseEvent(
                $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
                $request,
                'asdf'
            )
        );

        $response = $event->getResponse();

        $this->assertNull($response);
    }
}
