<?php

namespace Orc\BillingBundle\Tests\EventListener;

use Orc\SaasBundle\Entity\Client;
use Orc\UserBundle\Entity\User;
use Orc\SaasBundle\Event\ClientEvent;
use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\EventListener\CreateCustomer;

class CreateCustomerTest extends \PHPUnit_Framework_TestCase
{
    public function testCreatesClient()
    {
        $plan = new Plan();
        $plan->setId('trial');
        $client = new Client();
        $client->setName('Tester');
        $client->setUser($user = new User());
        $user->setEmail('test@er.com');

        $repository = $this->getMockBuilder('Orc\BillingBundle\Repository\PlanRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findDefault')
            ->will($this->returnValue($plan))
        ;

        $date = new \DateTime('+1 month');
        $date->setTimezone(new \DateTimeZone('UTC'));

        $stripe = $this->getMockBuilder('Orc\BillingBundle\Service\StripeCustomer')->disableOriginalConstructor()->getMock();
        $stripe
            ->expects($this->once())
            ->method('create')
            ->with(array(
                'description' => 'Tester',
                'email' => 'test@er.com',
                'plan' => 'trial',
                'trial_end' => $date->format('U')
            ))
            ->will($this->returnValue($customer = array(
                'id' => 'stripe_id'
            )))
        ;

        $em = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $em
            ->expects($this->at(0))
            ->method('persist')
            ->with($client)
        ;
        $em
            ->expects($this->at(1))
            ->method('persist')
            ->with($this->isInstanceOf('Orc\BillingBundle\Entity\StripeCustomer'))
        ;
        $em
            ->expects($this->at(2))
            ->method('flush')
        ;

        $listener = new CreateCustomer($em, $stripe, $repository);
        $listener->onCreateClient(new ClientEvent($client));

        $customer = $client->getCustomer();

        $this->assertSame($plan, $client->getPlan());
        $this->assertEquals('stripe_id', $customer->getId());
        $this->assertEquals($customer->getLastUpdated(), new \DateTime());
        $this->assertEquals($customer->getTrialEndDate(), $date);
    }
}
