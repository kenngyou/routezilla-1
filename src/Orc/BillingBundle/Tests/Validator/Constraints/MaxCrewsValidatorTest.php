<?php

namespace Orc\BillingBundle\Tests\Validator\Constraints;

use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\Crew;
use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\Validator\Constraints\MaxCrewsValidator;
use Orc\BillingBundle\Validator\Constraints\MaxCrews as Constraint;

class MaxCrewsValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testPassesWithUnlimitedCrews()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\CrewRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('getTenant')
            ->will($this->returnValue($client))
        ;

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolation')
        ;

        $validator = new MaxCrewsValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid($this->generateCrew(), new Constraint());

        $this->assertTrue($result);
    }

    public function testPassesWithLessCrewsThanLimit()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());
        $plan->setMaxCrews(1);

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\CrewRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('getTenant')
            ->will($this->returnValue($client))
        ;
        $repository
            ->expects($this->once())
            ->method('findActive')
            ->with($client)
            ->will($this->returnValue(array()))
        ;

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolation')
        ;

        $validator = new MaxCrewsValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid(new Crew(), new Constraint());

        $this->assertTrue($result);
    }

    public function testFailsWithMoreCrewsThanLimit()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());
        $plan->setMaxCrews(1);

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\CrewRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('getTenant')
            ->will($this->returnValue($client))
        ;
        $repository
            ->expects($this->once())
            ->method('findActive')
            ->with($client)
            ->will($this->returnValue(array($this->generateCrew())))
        ;

        $constraint = new Constraint();

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->once())
            ->method('addViolation')
            ->with($constraint->message)
        ;

        $validator = new MaxCrewsValidator($repository);
        $validator->initialize($context);

        $result = $validator->isValid($this->generateCrew(), $constraint);
        $this->assertFalse($result);
    }

    public function testPassesIfTrialing()
    {
        $client = new Client();
        $client->setPlan($plan = new Plan());
        $client->setStatus(Client::STATUS_STEP_5);
        $plan->setMaxCrews(1);
        $plan->setTrialLength(30);

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\CrewRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('getTenant')
            ->will($this->returnValue($client))
        ;

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolation')
        ;

        $validator = new MaxCrewsValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid(new Crew(), new Constraint());

        $this->assertTrue($result);
    }

    protected function generateCrew()
    {
        static $counter = 0;
        $crew = new Crew();
        $crew->setId(++$counter);

        return $crew;
    }
}
