<?php

namespace Orc\BillingBundle\Tests\Validator\Constraints;

use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Booking;
use Orc\BillingBundle\Validator\Constraints\NotScheduledValidator;
use Orc\BillingBundle\Validator\Constraints\NotScheduled as Constraint;

class NotScheduledValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testPassesIfNewCrew()
    {
        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolationAtSubPath')
        ;

        $validator = new NotScheduledValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid(new Crew(), new Constraint());

        $this->assertTrue($result);
    }

    public function testPassesIfKeepingActive()
    {
        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolationAtSubPath')
        ;

        $crew = new Crew();
        $crew->setActive(true);
        $crew->setClient(new Client());

        $validator = new NotScheduledValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid($crew, new Constraint());

        $this->assertTrue($result);
    }

    public function testPassesIfDeactivatingUnscheduledCrew()
    {
        $crew = new Crew();
        $crew->setClient(new Client());
        $crew->setActive(false);

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findUpcomingByCrew')
            ->with($crew, $this->isInstanceOf('DateTime'))
            ->will($this->returnValue(array()))
        ;

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->never())
            ->method('addViolationAtSubPath')
        ;

        $validator = new NotScheduledValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid($crew, new Constraint());

        $this->assertTrue($result);
    }

    public function testFailsIfDeactivatingScheduledCrew()
    {
        $crew = new Crew();
        $crew->setClient(new Client());
        $crew->setActive(false);

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findUpcomingByCrew')
            ->with($crew, $this->isInstanceOf('DateTime'))
            ->will($this->returnValue(array(new Booking())))
        ;

        $constraint = new Constraint();

        $context = $this->getMockBuilder('Symfony\Component\Validator\ExecutionContext')->disableOriginalConstructor()->getMock();
        $context
            ->expects($this->once())
            ->method('addViolationAtSubPath')
            ->with('active', $constraint->message)
        ;

        $validator = new NotScheduledValidator($repository);
        $validator->initialize($context);
        $result = $validator->isValid($crew, $constraint);

        $this->assertFalse($result);
    }
}
