<?php

namespace Orc\BillingBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PlanRepository extends EntityRepository
{
    public function findDefault()
    {
        $query = '
            SELECT plan
              FROM OrcBillingBundle:Plan plan
             WHERE plan.initial = 1
               AND plan.deleted = 0
        ';

        $results = $this->getEntityManager()->createQuery($query) ->getResult();
        if (empty($results[0])) {
            throw new \Exception('Could not find default plan');
        }

        return $results[0];
    }

    public function findAll()
    {
        return $this->findBy(array(), array('monthlyPrice' => 'asc'));
    }

    public function getBuilderForLivePlans()
    {
        return $this->createQueryBuilder('plan')
            ->select('plan')
            ->andWhere('plan.deleted = 0')
            ->orderBy('plan.monthlyPrice');
    }
}
