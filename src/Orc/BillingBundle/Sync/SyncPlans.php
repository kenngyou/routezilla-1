<?php

namespace Orc\BillingBundle\Sync;

use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\Repository\PlanRepository;
use Orc\BillingBundle\Service\StripePlan;
use Doctrine\ORM\EntityManager;

/**
 * Responsible for synchronizing plans between Stripe and our local application
 * Plan titles can optionally embed the max crew count by wrapping a number in parenthesis:
 *
 *  "Gold (5)" Would signify a "Gold" membership that allows up to 5 crews.
 *  "Enterprise" would signify a "Enterprise" plan with no limit.
 */
class SyncPlans
{
    /**
     * @param EntityManager $em doctrine.orm.entity_manager
     * @param PlanRepository $planRepository data access for repositories
     * @param StripePlan $stripe service class for stripe plans
     */
    public function __construct(EntityManager $em, PlanRepository $planRepository, StripePlan $stripe)
    {
        $this->em = $em;
        $this->planRepository = $planRepository;
        $this->stripe = $stripe;
    }

    /**
     * Downloads the latest plans from stripe, updating the local collection
     */
    public function sync()
    {
        $this->messages = array();
        $detected = array();
        $existing = $this->planRepository->findAll();

        foreach ($this->stripe->fetchAll() as $plan) {
            $detected[] = $plan;

            if ($existingPlan = $this->findPlan($plan, $existing)) {
                $this->updatePlan($existingPlan, $plan);
            } else {
                $this->createPlan($plan);
            }
        }

        foreach ($this->findMissing($existing, $detected) as $missingPlan) {
            $this->deletePlan($missingPlan);
        }

        $this->em->flush();
    }

    /**
     * Returns the messages from the sync command
     * @return array messages
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Update the plan locally
     * @param Plan $plan the existing plan
     * @param object $changes changes from Stripe
     */
    protected function updatePlan(Plan $plan, $changes)
    {
        $this->applyChanges($plan, $changes);
        $this->em->persist($plan);
        $this->messages[] = sprintf('Plan %s has been updated', $plan->getName());
    }

    /**
     * Create the plan locally
     * @param object $new plan info
     */
    protected function createPlan($new)
    {
        $plan = new Plan();
        $this->applyChanges($plan, $new);
        $this->em->persist($plan);

        $this->messages[] = sprintf('Plan %s has been created', $plan->getName());
    }

    /**
     * Apply setters and getters
     * @param Plan $plan entity to change
     * @param object|array $changes changes to apply to entity
     */
    protected function applyChanges(Plan $plan, $changes)
    {
        if (isset($changes['id'])) {
            $plan->setId($changes['id']);
        }

        if (strpos($changes['name'], '*') !== false) {
            $plan->setInitial(true);
        } else {
            $plan->setInitial(false);
        }

        if (isset($changes['name'])) {
            $matches = null;
            if (preg_match('/\(([0-9]+)\)/', $changes['name'], $matches)) {
                $plan->setMaxCrews((int)$matches[1]);
            } else {
                $plan->setMaxCrews(null);
            }
            $plan->setName(preg_replace('/\(([0-9]+)\)/', '', str_replace('*', '', $changes['name'])));
        }

        if (isset($changes['price'])) {
            $plan->setPrice($changes['price']);
        }

        if (isset($changes['amount'])) {
            $plan->setPrice($changes['amount'] ? round($changes['amount'] / 100) : 0);
        }

        if (isset($changes['trial_period_days'])) {
            $plan->setTrialLength((int)$changes['trial_period_days']);
        }

        $plan->setInterval($changes['interval']);
        if ($plan->getInterval() == 'month') {
            $plan->setMonthlyPrice($plan->getPrice());
        } elseif ($plan->getInterval() == 'year') {
            $plan->setMonthlyPrice($plan->getPrice() / 12);
        }
    }

    /**
     * Delete the plan locally
     * @param Plan $plan
     */
    protected function deletePlan(Plan $plan)
    {
        $plan->setDeleted(true);
        $this->em->persist($plan);
        $this->messages[] = sprintf('Plan %s has been soft-deleted', $plan->getName());
    }

    /**
     * Find the plan from an array of plans
     * @param object|array $plan plan from Stripe
     * @param Plan[]
     * @return Plan|null
     */
    protected function findPlan($plan, $existing)
    {
        foreach ($existing as $existingPlan) {
            if ($existingPlan->getId() == $plan['id']) {
                return $existingPlan;
            }
        }
    }

    /**
     * Find the plans missing from the response (assumed deleted)
     * @param Plan[] $existing plans that already exist
     * @param array $detected plans from stripe
     */
    protected function findMissing(array $existing, array $detected)
    {
        $missing = array();
        foreach ($existing as $existingPlan) {
            $found = false;
            foreach ($detected as $detectedPlan) {
                if ($detectedPlan['id'] == $existingPlan->getId()) {
                    $found = true;
                }
            }

            if (!$found) {
                $missing[] = $existingPlan;
            }
        }

        return $missing;
    }
}
