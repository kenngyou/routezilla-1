<?php

namespace Orc\BillingBundle\Mailer;

use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Templating\EngineInterface;

/**
 * Responsible for sending overdue notices to clients
 */
class AccountOverdueMailer
{
    const FIRST_WARNING = 30;
    const LAST_WARNING = 60;

    /**
     * @var array valid warnings
     */
    protected $warnings = array(self::FIRST_WARNING, self::LAST_WARNING);

    /**
     * @param Swift_Mailer $mailer
     * @param EngineInterface $templating template engine
     * @param string $fromName
     * @param string $fromAddress
     */
    public function __construct(\Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
    }

    /**
     * Sends an overdue warning to a Client
     *
     * @param Client $client recipient
     * @param integer $warning one of the *_WARNING constants
     */
    public function send(Client $client, $warning)
    {
        if (!in_array($warning, $this->warnings)) {
            throw new \InvalidArgumentException('$warning must be a valid warning');
        }

        $message = \Swift_Message::newInstance()
            ->setTo(array($client->getEmail() => $client->getName()))
            ->setFrom($this->from)
            ->setSubject('Routezilla - Account Overdue')
            ->setBody(
                $this->templating->render("OrcBillingBundle:Email:overdue_$warning.html.twig", array('client' => $client)),
                'text/html'
            )
        ;

        $this->mailer->send($message);
    }
}
