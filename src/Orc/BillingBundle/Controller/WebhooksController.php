<?php

namespace Orc\BillingBundle\Controller;

use Orc\BillingBundle\Request\Webhook;
use Orc\BillingBundle\Event\WebhookEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Monolog\Logger;

class WebhooksController
{
    protected $dispatcher;
    protected $mapping;
    protected $logger;

    public function __construct(EventDispatcherInterface $dispatcher, array $mapping = array(), Logger $logger = null)
    {
        $this->dispatcher = $dispatcher;
        $this->mapping = $mapping;
        $this->logger = $logger;
    }

    /**
     * Responds to webhook requests
     *
     * @param Webhook $webhook
     * @return Response
     */
    public function receiveAction(Webhook $webhook)
    {
        $this->dispatcher->dispatch(
            $webhook->getType(),
            $event = $this->createEvent($webhook)
        );

        $messages = $event->getMessages();

        if ($this->logger) {
            $this->logger->addDebug(sprintf(
                "webhook received [%s]: %s\n\n",
                $webhook->getType(),
                json_encode($webhook->getData())
            ));
        }

        return new Response($messages ? implode(', ', $messages) : 'no action taken');
    }

    /**
     * Create a new Event object
     *
     * @param Webhook $webhook
     * @return WebhookEvent
     */
    protected function createEvent(Webhook $webhook)
    {
        foreach ($this->mapping as $prefix => $class) {
            if (strpos($webhook->getType(), $prefix) === 0) {
                return new $class($webhook);
            }
        }

        return new WebhookEvent($webhook);
    }
}
