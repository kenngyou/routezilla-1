<?php

namespace Orc\BillingBundle\Controller;

use Orc\BillingBundle\Form\Type\CardType;
use Orc\BillingBundle\Form\Type\PlanType;
use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\SaasEvents;
use Orc\SaasBundle\Event\ClientEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BillingController extends Controller
{
    /**
     * Account information summary
     */
    public function accountAction()
    {
        $customerService = $this->get('orc_billing.stripe.customer');

        $em = $this->get('doctrine.orm.entity_manager');
        $client = $this->get('synd_multitenant.tenant');
        $customer = $client->getStripeId() ? $customerService->view($client->getStripeId()) : null;

        $stripeCustomer = $client->getCustomer();
        $stripeCustomer->updateFromStripe($customer['subscription']);
        $em->persist($stripeCustomer);
        $em->flush();

        $form = $this->createForm('upgrade', array('plan' => $client->getPlan()));


        return $this->render('OrcBillingBundle:Billing:account.html.twig', array(
            'form' => $form->createView(),
            'client' => $client,
            'customer' => $customer,
            'existing' => $client->getStripeId() ? $customerService->fetchCardDetails($client->getStripeId()) : null,
            'api_public_key' => $this->container->getParameter('orc_billing.stripe.api_public_key'),
            'crews' => $this->get('orc_booking.repository.crew')->findActive()
        ));
    }

    /**
     * Update credit card information on file
     */
    public function subscribeAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');
        $form = $this->createForm('upgrade');

        if ($client->getStatus() == Client::STATUS_ACTIVE) {
            $existing = $customerService->fetchCardDetails($client->getStripeId());
        }

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                if (!$customerService->validateToken($data['card']['token'])) {
                    $this->get('session')->getFlashBag()->add('error', 'Invalid card specified');
                    return $this->redirect($this->generateUrl('billing'));
                }

                try {
                    $billingService = $this->get('orc_billing.service.customer');
                    $billingService->subscribe($client, $data['plan'], $data['card']['token'], $data['coupon']);
                } catch (\Stripe_CardError $e) {
                    $this->get('session')->getFlashBag()->add('error', $e->getMessage());
                    return $this->redirect($this->generateUrl('billing'));
                }

                $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_CONVERT, new ClientEvent($client));
                return $this->redirect($this->generateUrl('billing'));
            } else {
                $messages = array();
                foreach ($form->getErrors() as $error) {
                    $messages[] = $error->getMessage();
                }
                $this->get('session')->getFlashBag()->add('error', implode(',', $messages));
            }
        }

        return $this->redirect($this->generateUrl('billing'));
    }

    /**
     * Change the user's plan
     */
    public function changePlanAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');
        $form = $this->createForm(new PlanType());

        $existing = $customerService->fetchCardDetails($client->getStripeId());
        if (!$existing) {
            $this->get('session')->getFlashBag()->add('error', 'Subscribe to a plan first.');
            return $this->redirect($this->generateUrl('billing'));
        }

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                $billingService = $this->get('orc_billing.service.customer');
                $billingService->changePlan($client, $data['plan'], $data['coupon']);

                $this->get('session')->getFlashBag()->add('success', 'Congratulations, you have successfully changed your plan!');
                $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_CHANGE_PLAN, new ClientEvent($client));
                return $this->redirect($this->generateUrl('billing'));
            }

        }

        return $this->redirect($this->generateUrl('billing'));
    }

    /**
     * Change the user's card on file
     */
    public function changeCardAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');
        $form = $this->createForm(new CardType());

        $existing = $customerService->fetchCardDetails($client->getStripeId());
        if (!$existing) {
            $this->get('session')->getFlashBag()->add('error', 'Subscribe to a plan first.');
            return $this->redirect($this->generateUrl('billing'));
        }
        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                if (!$customerService->validateToken($data['token'])) {
                    $this->get('session')->getFlashBag()->add('error', 'Invalid card specified');
                    return $this->redirect($this->generateUrl('billing_subscribe'));
                }

                try {
                    $billingService = $this->get('orc_billing.service.customer');
                    $billingService->changeCard($client, $data['token']);
                } catch (\Stripe_CardError $e) {
                    $this->get('session')->getFlashBag()->add('error', $e->getMessage());
                    return $this->redirect($this->generateUrl('billing'));
                }

                $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_CHANGE_CARD, new ClientEvent($client));
                return $this->redirect($this->generateUrl('billing'));
            }
        }

        return $this->redirect($this->generateUrl('billing'));
    }

    /**
     * Pay amount owing
     */
    public function squareUpAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');
        $chargeService = $this->get('orc_billing.stripe.charge');

        $customer = $customerService->view($client->getStripeId());

        if ($customer->account_balance <= 0) {
            $this->get('session')->getFlashBag()->add('info', 'You have no balance owing.');
            return $this->redirect($this->generateUrl('billing'));
        }

        $form = $this->createForm(new CardType());

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();
                if (!$customerService->validateToken($data['token'])) {
                    $this->get('session')->getFlashBag()->add('error', 'Invalid card specified');
                    return $this->redirect($this->generateUrl('billing'));
                }

                try {
                    $billingService = $this->get('orc_billing.service.customer');
                    $billingService->squareUp($client, $data['token']);
                } catch (\Stripe_CardError $e) {
                    $this->get('session')->getFlashBag()->add('error', 'Charge failed: ' . $error->getMessage());
                    return $this->redirect($this->generateUrl('billing'));
                }

                $this->get('session')->getFlashBag()->add('success', 'Account paid in full!');
                return $this->redirect($this->generateUrl('billing'));
            }
        }
        return $this->redirect($this->generateUrl('billing'));
    }

    /**
     * Cancel subscription
     */
    public function cancelAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');
        $form = $this->createFormBuilder()->getForm();

        if (!$customerService->fetchCardDetails($client->getStripeId())) {
            return $this->redirect($this->generateUrl('billing'));
        }

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $billingService = $this->get('orc_billing.service.customer');
                $billingService->cancel($client);

                return $this->redirect($this->generateUrl('billing'));
            }
        }

        return $this->redirect($this->generateUrl('billing'));
    }

    public function chargesAction()
    {
        $client = $this->get('synd_multitenant.tenant');
        $customerService = $this->get('orc_billing.stripe.customer');

        return $this->render('OrcBillingBundle:Billing:charges.html.twig', array(
            'client' => $client,
            'charges' => $customerService->viewCharges($client->getStripeId())
        ));
    }
}
