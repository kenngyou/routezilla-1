<?php

namespace Orc\BillingBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class MockStripePass implements CompilerPassInterface
{
    /**
     * Replace all stripe services with mocks for testing
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->getParameter('kernel.environment') == 'test') {
            foreach ($container->getServiceIds() as $serviceId) {
                if (strpos($serviceId, 'orc_billing.stripe.') === 0) {
                    $definition = $container->getDefinition($serviceId);
                    $definition->setClass(str_replace('\Stripe', '\Mock\Mock', $definition->getClass()));
                }
            }
        }
    }
}
