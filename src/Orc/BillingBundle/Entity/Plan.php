<?php

namespace Orc\BillingBundle\Entity;

use Orc\SaasBundle\Entity\Client;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Orc\BillingBundle\Repository\PlanRepository")
 */
class Plan
{
    /**
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $maxCrews;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $trialLength;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $deleted = false;

    /**
     * @ORM\Column(type="string", name="intervalText")
     */
    protected $interval;

    /**
     * @ORM\Column(type="integer")
     */
    protected $monthlyPrice;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $initial = false;


    /**
     * Set id
     *
     * @param string $id
     * @return Plan
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Plan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Plan
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Plan
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set maxCrews
     *
     * @param integer $maxCrews
     * @return Plan
     */
    public function setMaxCrews($maxCrews)
    {
        $this->maxCrews = $maxCrews;

        return $this;
    }

    /**
     * Get maxCrews
     *
     * @return integer
     */
    public function getMaxCrews()
    {
        return $this->maxCrews;
    }

    /**
     * Set trialLength
     *
     * @param integer $trialLength
     * @return Plan
     */
    public function setTrialLength($trialLength)
    {
        $this->trialLength = $trialLength;

        return $this;
    }

    /**
     * Get trialLength
     *
     * @return integer
     */
    public function getTrialLength()
    {
        return $this->trialLength;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Generate a friendly version for forms
     * @return string name and price
     */
    public function getNameAndPrice()
    {
        return sprintf('%d,%d,%s,%s', $this->price, $this->maxCrews, $this->name, $this->interval);
    }

    /**
     * Set deleted
     *
     * @param boolean $deleted
     * @return Plan
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return boolean
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set interval
     *
     * @param string $interval
     * @return Plan
     */
    public function setInterval($interval)
    {
        $this->interval = $interval;

        return $this;
    }

    /**
     * Get interval
     *
     * @return string
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * Set monthlyPrice
     *
     * @param integer $monthlyPrice
     * @return Plan
     */
    public function setMonthlyPrice($monthlyPrice)
    {
        $this->monthlyPrice = $monthlyPrice;

        return $this;
    }

    /**
     * Get monthlyPrice
     *
     * @return integer
     */
    public function getMonthlyPrice()
    {
        return $this->monthlyPrice;
    }

    /**
     * Set initial
     *
     * @param boolean $initial
     * @return Plan
     */
    public function setInitial($initial)
    {
        $this->initial = $initial;

        return $this;
    }

    /**
     * Get initial
     *
     * @return boolean
     */
    public function getInitial()
    {
        return $this->initial;
    }
}
