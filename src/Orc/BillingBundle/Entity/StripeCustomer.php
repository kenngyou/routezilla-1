<?php

namespace Orc\BillingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="stripe_customer")
 */
class StripeCustomer
{
    /**
     * @ORM\ID
     * @ORM\Column(type="string")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $trialEndDate;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $lastUpdated;

    public function updateFromStripe($subscription)
    {
        if ($subscription && $subscription->trial_end) {
            $this->trialEndDate = \DateTime::createFromFormat('U', $subscription->trial_end);
        } else {
            $this->trialEndDate = null;
        }

        $this->lastUpdated = new \DateTime();
    }

    /**
     * Set id
     *
     * @param string $id
     * @return StripeCustomer
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trialEndDate
     *
     * @param \DateTime $trialEndDate
     * @return StripeCustomer
     */
    public function setTrialEndDate($trialEndDate)
    {
        $this->trialEndDate = $trialEndDate;

        return $this;
    }

    /**
     * Get trialEndDate
     *
     * @return \DateTime
     */
    public function getTrialEndDate()
    {
        return $this->trialEndDate;
    }

    /**
     * Set lastUpdated
     *
     * @param \DateTime $lastUpdated
     * @return StripeCustomer
     */
    public function setLastUpdated($lastUpdated)
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    /**
     * Get lastUpdated
     *
     * @return \DateTime
     */
    public function getLastUpdated()
    {
        return $this->lastUpdated;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return StripeCustomer
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
