<?php

namespace Orc\BillingBundle\Event;

use Orc\BillingBundle\Request\Webhook;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\EventDispatcher\Event;

class WebhookEvent extends Event
{
    protected $webhook;
    protected $response;
    protected $messages;

    public function __construct(Webhook $webhook)
    {
        $this->webhook = $webhook;
        $this->messages = array();
    }

    public function getWebhook()
    {
        return $this->webhook;
    }

    public function getWebhookData()
    {
        return $this->webhook->getData();
    }

    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    public function getResponse()
    {
        return $this->response;
    }

    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    public function getMessages()
    {
        return $this->messages;
    }
}
