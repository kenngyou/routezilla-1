<?php

namespace Orc\BillingBundle\Event;

class SubscriptionEvent extends WebhookEvent
{
    public function getCustomerId()
    {
        $data = $this->webhook->getData();
        return $data->object->customer;
    }

    public function getCustomer()
    {
        $data = $this->webhook->getData();
        return $data->object->customer;
    }

    public function getPlanId()
    {
        $data = $this->webhook->getData();
        return $data->object->plan->id;
    }
}
