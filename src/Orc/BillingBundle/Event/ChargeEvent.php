<?php

namespace Orc\BillingBundle\Event;

class ChargeEvent extends WebhookEvent
{
    public function getCustomerId()
    {
        $data = $this->webhook->getData();
        return $data->object->customer;
    }
}
