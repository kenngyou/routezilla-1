<?php

namespace Orc\BillingBundle\Event;

class CustomerEvent extends WebhookEvent
{
    public function getCustomerId()
    {
        $data = $this->webhook->getData();
        return $data->object->id;
    }

    public function getCustomer()
    {
        $data = $this->webhook->getData();
        return $data->object;
    }
}
