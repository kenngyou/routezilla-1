<?php

namespace Orc\BillingBundle;

use Orc\BillingBundle\DependencyInjection\Compiler\MockStripePass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrcBillingBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new MockStripePass());
    }
}
