<?php

namespace Orc\BillingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PlanType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plan', 'entity', array(
                'class' => 'Orc\BillingBundle\Entity\Plan',
                'property' => 'nameAndPrice',
                'expanded' => true,
                'query_builder' => function($repository) {
                    return $repository->getBuilderForLivePlans();
                }

            ))
            ->add('token', 'hidden', array(
                'required' => false
            ))
            ->add('card', 'hidden', array(
                'required' => false
            ))
            ->add('coupon', null, array(
                'required' => false
            ))
        ;
    }

    public function getName()
    {
        return 'upgrade';
    }
}
