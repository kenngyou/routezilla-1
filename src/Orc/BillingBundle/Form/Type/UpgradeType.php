<?php

namespace Orc\BillingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UpgradeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plan', 'entity', array(
                'class' => 'Orc\BillingBundle\Entity\Plan',
                'property' => 'nameAndPrice',
                 'query_builder' => function($repository) {
                    return $repository->getBuilderForLivePlans();
                },
               'expanded' => true
            ))
            ->add('card', 'card')
            ->add('coupon', null, array(
                'required' => false
            ))
        ;
    }

    public function getName()
    {
        return 'upgrade';
    }
}
