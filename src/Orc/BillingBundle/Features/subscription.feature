Feature: Subscription Billing
  Background:
    Given I'm logged in as a new client

  Scenario: Standard Subscribe
     When I follow "Billing"
      And I press the "Main Plan" button
      And I submit with a valid credit card
     Then I should see "Something Fancy"
      And I should have a balance of 100
      And I should be subscribed to plan Y

  Scenario: Delinquent Account
    Given something
     When event happens
     Then another event should be triggered

  Scenario: Failed Subscribe

  Scenario: Change Plan within Trial

  Scenario: Change Plan after Trial

  Scenario: Cancel

  Scenario: Subcribe after Cancellation

  Scenario: Card Change

  Scenario: Failed Card Change

  Scenario: Square Up

  Scenario: Failed Square Up
