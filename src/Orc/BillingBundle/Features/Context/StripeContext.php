<?php

namespace Orc\BillingBundle\Features\Context;

class StripeContext extends BehatContext implements KernelAwareInterface
{
    /**
     * Assert the Client has a balance on Stripe
     * @param Client $client
     * @param integer $balance
     */
    public function assertBalance(Client $client, $balance)
    {
        assertEquals($balance * 100, $this->fetchCustomer($client)->balance);
    }

    public function assertPlanMatch(Client $client, Plan $plan)
    {
        assertEquals($plan->getId(), $this->fetchCustomer($client)->subscription->plan);
    }

    /**
     * Simulates a Stripe webhook of $type containing $data
     * @param string $type event name
     * @param array $data event data
     */
    public function triggerWebhook($type, $data, $assertCode = 200)
    {
        $webhook = Request::create('http://admin.orcamax.dev/_stripe_webhook', 'POST', array(), array(), array(), array(), json_encode(array(
            'id' => "made up",
            'type' => $type,
            'data' => $data,
            'livemode' => false
        )));

        $response = $this->kernel->handle($webhook);
        assertEquals($assertCode, $response->getStatusCode());

        return $response;
    }

    /**
     * Fetches remote Stripe information about a Client
     * @param Client $client
     * @return Stripe_Customer
     */
    private function fetchCustomer(Client $client)
    {
        $stripe = $this->kernel->getContainer()->get('orc_billing.service.customer');
        return $stripe->view($client->getStripeId());
    }
}
