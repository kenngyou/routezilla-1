<?php

namespace Orc\BillingBundle\Features\Context;

use Symfony\Component\HttpFoundation\Request;
use Orc\UserBundle\Features\Context\UserContext;
use Orc\UserBundle\Entity\User;
use Orc\BookingBundle\Entity\DateTime;
use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Entity\Site;
use Orc\BookingBundle\Features\Context\ScheduleContext;
use Orc\SaasBundle\Features\Context\SaasContext;
use Orc\SaasBundle\Features\Context\MinkContext;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Step;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Feature context.
 */
class FeatureContext extends BehatContext
                  implements KernelAwareInterface
{
    private $kernel;
    private $parameters;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
        $this->useContext('saas', new SaasContext($parameters));
        $this->useContext('schedule', new ScheduleContext($parameters));
        $this->useContext('mink', new MinkContext());
        $this->useContext('user', new UserContext($parameters));
    }

    /**
     * @Given /Stripe ends the subscription for "([^"]*)"/
     */
    public function stripeSubscriptionEnds($clientDomain)
    {
        $this->sendWebhook('customer.subscription.deleted', array(
            'customer' => 'test_sadf'
        ));
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function iHaveAClient($date = null)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $um = $this->kernel->getContainer()->get('fos_user.user_manager');

        $site = new Site();
        $site->setDomain('test.orcamax.dev');
        $site->setClient($client = new Client());

        $client->setSite($site);
        $client->setName('Testmaster');
        $client->setDateCreated($date ?: new DateTime());
        $client->setStatus(Client::STATUS_STEP_5);

        $em->persist($client);
        $em->flush();

        $client->setUser($user = $um->createUser());

        $user->setClient($client);
        $user->setEmail('admin@site.com');
        $user->setPlainPassword('whatevs');
        $user->setEnabled(true);
        $user->addRole('ROLE_CLIENT');
        $user->addRole('ROLE_STAFF');

        $em->persist($client);
        $em->persist($user);
        $em->flush();
    }

    /**
     * @Given /^I signup as a new client$/
     */
    public function iSignupAsANewClient()
    {
        $this->iHaveAClient();

        $this->getMainContext()->getSubcontext('mink')->visit('http://test.orcamax.dev/dashboard');
        $this->getMainContext()->getSubcontext('mink')->fillField('username', 'admin@site.com');
        $this->getMainContext()->getSubcontext('mink')->fillField('password', 'whatevs');
        $this->getMainContext()->getSubcontext('mink')->pressButton('_submit');
    }

    /**
     * @Given /^I signed up as a client (\d+) days ago$/
     */
    public function iSignedUpAsClientXDaysAgo($days)
    {
        $this->iHaveAClient(new DateTime(sprintf('-%d days', $days)));

        $this->getMainContext()->getSubcontext('mink')->visit('http://test.orcamax.dev/dashboard');
        $this->getMainContext()->getSubcontext('mink')->fillField('username', 'admin@site.com');
        $this->getMainContext()->getSubcontext('mink')->fillField('password', 'whatevs');
        $this->getMainContext()->getSubcontext('mink')->pressButton('_submit');
    }

    /**
     * @Given /^I\'m on a new client site$/
     */
    public function iMOnANewClientSite()
    {
        $this->iHaveAClient();
        $this->getMainContext()->getSubcontext('mink')->visit('http://test.orcamax.dev/');
    }

    /**
     * @Given /^I\'m on a client site created (\d+) days ago$/
     */
    public function iMOnAClientSiteCreatedDaysAgo($days)
    {
        $this->iHaveAClient(new DateTime(sprintf('-%d days', $days)));
        $this->getMainContext()->getSubcontext('mink')->visit('http://test.orcamax.dev/');
    }

}
