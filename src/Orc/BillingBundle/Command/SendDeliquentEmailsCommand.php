<?php

namespace Orc\BillingBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class SendDeliquentEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:mail:deliquent')
            ->setDescription('Sends the deliquent emails to customers')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('orc_billing.mailer.deliquent');
        $repository = $this->getContainer()->get('orc_saas.repository.client');

        foreach (array(30, 60) as $days) {
            foreach ($repository->findDeliquent($days) as $client) {
                $mailer->send($client, $days);
                $output->writeln(sprintf(
                    "Mail send to <info>%s</info> for being <info>%d</info> days late.",
                    $client->getName(),
                    $days
                ));
            }
        }
    }
}
