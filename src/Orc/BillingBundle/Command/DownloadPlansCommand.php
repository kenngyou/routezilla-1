<?php

namespace Orc\BillingBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DownloadPlansCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:stripe:download-plans')
            ->setDescription('Downloads or syncs the latest information from Stripe.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $synchronizer = $this->getContainer()->get('orc_billing.sync_plans');
        $synchronizer->sync();

        foreach ($synchronizer->getMessages() as $message) {
            $output->writeln($message);
        }
    }
}
