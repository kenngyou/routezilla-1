<?php

namespace Orc\BillingBundle\Command;

use Orc\BillingBundle\Entity\StripeCustomer as Customer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ImportClientsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:billing:import-clients')
            ->setDescription('Import clients into Stripe')
            ->setDefinition(array(
                new InputOption('file', 'f', InputOption::VALUE_OPTIONAL, 'File to include'),
            ))
        ;
    }

    /**
     * Import clients into Stripe
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $clientRepository = $this->getContainer()->get('orc_saas.repository.client');
        $customerService = $this->getContainer()->get('orc_billing.stripe.customer');

        $trialEnd = new \DateTime('+1 month');
        $trialEnd->setTimezone(new \DateTimeZone('UTC'));

        $clients = $this->getClients($input->getOption('file') ?: 'php://stdin');
        $plan = $this->getContainer()->get('orc_billing.repository.plan')->findDefault();

        foreach ($clients as $rawClient) {
            if (!$rawClient['name'] or !$rawClient['email']) {
                $output->writeln("<error>Invalid client: " . print_r($rawClient, true) . '</error>');
                continue;
            }

            if (!$client = $clientRepository->findOneByEmail($rawClient['email'])) {
                $output->writeln("<error>Could not find $rawClient[email]</error>");
                continue;
            }

            $customer = $customerService->create(array(
                'description' => $rawClient['name'],
                'email'       => $rawClient['email'],
                'plan'        => $plan->getId(),
                'trial_end'   => $trialEnd->format('U')
            ));


            $client->setPlan($plan);
            $client->setStripeId($customer['id']);

            $stripeCustomer = new Customer();
            $stripeCustomer->setId($customer['id']);
            $stripeCustomer->setLastUpdated(new \DateTime());
            $stripeCustomer->setTrialEndDate($trialEnd);

            $em->persist($client);
            $em->persist($stripeCustomer);

            $output->writeln("Persisting <info>$rawClient[email]</info>");
        }

        $em->flush();
    }

    /**
     * Extracts client information from a file
     * @param string filename or stream
     * @return array clients
     */
    protected function getClients($filename)
    {
        $clients = array();

        $keys = null;
        $handle = fopen($filename, 'r');
        while (false !== $data = fgetcsv($handle, 1000)) {
            if (!$keys) {
                $keys = $data;
                continue;
            }

            $clients[] = array_combine($keys, $data);
        }

        return $clients;
    }
}
