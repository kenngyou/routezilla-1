<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Event;

class StripeEvent
{
    protected $apiKey;

    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    public function view($id)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_Event::retrieve($id);
    }
}
