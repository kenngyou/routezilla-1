<?php

namespace Orc\BillingBundle\Service\Mock;

use Orc\BillingBundle\Service\StripeCustomer;

class MockCustomer extends StripeCustomer
{
    public function create(array $data)
    {
        return array('id' => 'cus_' . uniqid());
    }

    public function view($id, $cache = true)
    {
        return json_decode(json_encode(array(
            'id' => $id,
            'subscription' => array(

            )
        )), false);
    }

    public function fetchCardDetails($id)
    {
        return json_decode(json_encode(array(
            'name' => 'Fake Name',
            'last4' => 1234,
            'exp_month' => 4,
            'exp_year' => 2013,
            'type' => 'visa'
        )), false);
    }
}
