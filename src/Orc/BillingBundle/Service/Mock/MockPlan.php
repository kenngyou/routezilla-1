<?php

namespace Orc\BillingBundle\Service\Mock;

use Orc\BillingBundle\Service\StripePlan;

class MockPlan extends StripePlan
{
    public function fetchAll()
    {
        return array(
            array(
                'id' => 'defaultplan',
                'name' => 'Main Plan (5)*',
                'amount' => 1000,
                'trial_period_days' => 30,
                'interval' => 'month'
            ),
            array(
                'id' => 'upgradedplan',
                'name' => 'Upgraded Plan',
                'amount' => 5000,
                'interval' => 'month'
            )
        );
    }

}
