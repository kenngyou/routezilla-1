<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Coupon;
use Stripe_InvalidRequestError;

class StripeCoupon
{
    public function __construct($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * Verify a coupon code's validity
     * @param string $couponCode
     * @return boolean true if valid
     */
    public function verify($couponCode)
    {
        try {
            Stripe::setApiKey($this->apiKey);
            Stripe_Coupon::retrieve($couponCode);
            return true;
        } catch (Stripe_InvalidRequestError $e) {
            return false;
        }
    }
}
