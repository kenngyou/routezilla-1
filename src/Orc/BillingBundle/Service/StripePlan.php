<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Plan;

class StripePlan
{
    public function __construct($apiKey, $currency)
    {
        $this->apiKey = $apiKey;
        $this->currency = $currency;
    }

    public function fetchAll()
    {
        Stripe::setApiKey($this->apiKey);
        $out = Stripe_Plan::all()->__toArray(true);
        return $out['data'];
        return Stripe_Plan::all()->all();
    }
}
