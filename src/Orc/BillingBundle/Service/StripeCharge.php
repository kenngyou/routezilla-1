<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Charge;

class StripeCharge
{
    public function __construct($apiKey, $currency)
    {
        $this->apiKey = $apiKey;
        $this->currency = $currency;
    }

    public function charge($amount, $customer)
    {
        return Stripe_Charge::create(array(
            'amount' => $amount,
            'customer' => $customer,
            'currency' => $this->currency,
            'description' => 'Routezilla balance owing'
        ));
    }
}
