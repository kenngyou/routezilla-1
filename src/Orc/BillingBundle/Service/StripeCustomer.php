<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Customer;
use Stripe_Charge;
use Stripe_Token;
use Doctrine\Common\Cache\Cache;

class StripeCustomer
{
    public function __construct($apiKey, Cache $cache, $ttl)
    {
        $this->apiKey = $apiKey;
        $this->cache = $cache;
        $this->ttl = $ttl;
    }

    public function view($id, $cache = true)
    {
        if ($cache and false !== $cached = $this->cache->fetch($cacheId = "stripe.customer.$id")) {
            return unserialize($cached);
        }

        Stripe::setApiKey($this->apiKey);
        $customer = Stripe_Customer::retrieve($id);
        $this->cache->save($cacheId, serialize($customer), $this->ttl);

        return $customer;
    }

    public function fetchCardDetails($id)
    {
        $customer = $this->view($id);

        $cards = $customer['cards']->__toArray();
        foreach ($cards['data'] as $card) {
            if ($card['id'] == $customer['default_card']) {
                return $card;
            }
        }
    }

    public function validateToken($token)
    {
        Stripe::setApiKey($this->apiKey);

        try {
            $token = Stripe_Token::retrieve($token ?: "invalid");
            return (bool)$token->id;

        } catch (\Stripe_InvalidRequestError $e) {
            return false;
        }
    }

    public function create(array $data)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_Customer::create($data);
    }

    public function update($id, array $data)
    {
        Stripe::setApiKey($this->apiKey);
        $customer = Stripe_Customer::retrieve($id);

        foreach ($data as $key => $value) {
            if (is_array($customer[$key])) {
                foreach ($data[$key] as $subKey => $subValue) {
                    $customer[$subKey][$key] = $subValue;
                }
            } else {
                $customer[$key] = $value;
            }
        }

        $this->cache->delete("stripe.customer.$id");
        return $customer->save();
    }

    public function viewCharges($id)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_Charge::all(array(
            'customer' => $id
        ));
    }

    public function flushCache($id)
    {
        $this->cache->delete("stripe.customer.$id");
    }
}
