<?php

namespace Orc\BillingBundle\Service;

use Stripe;
use Stripe_Invoice;
use Stripe_InvoiceItem;

class StripeInvoice
{
    public function __construct($apiKey, $currency)
    {
        $this->apiKey = $apiKey;
        $this->currency = $currency;
    }

    public function fetchAll($customerId)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_Invoice::all(array(
            'customer' => $customerId
        ));
    }

    public function fetch($invoiceId)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_Invoice::retrieve($invoiceId);
    }

    public function add($customerId, $amount, $description)
    {
        Stripe::setApiKey($this->apiKey);
        return Stripe_InvoiceItem::create(array(
            'customer' => $customerId,
            'amount' => round($amount * 100),
            'currency' => $this->currency,
            'description' => $description
        ));
    }
}
