<?php

namespace Orc\BillingBundle\Validator\Constraints;

use Orc\BookingBundle\Repository\BookingRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class NotScheduledValidator extends ConstraintValidator
{
    /**
     * @param BookingRepository $bookingRepository
     */
    public function __construct(BookingRepository $bookingRepository)
    {
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * Determine whether or not the user can deactivate a crew
     * @param Crew $crew new/udpated crew being validated
     * @param Constraint $constraint
     * @return boolean true if not being deactivated with pending bookings
     */
    public function isValid($crew, Constraint $constraint)
    {
        if (!$crew->getClient()) {
            return true;
        }

        if ($crew->getActive()) {
            return true;
        }

        $date = new \DateTime('+1 year');
        if (!$this->bookingRepository->findUpcomingByCrew($crew, $date)) {
            return true;
        }

        $this->context->addViolationAtSubPath('active', $constraint->message);
        return false;
    }
}
