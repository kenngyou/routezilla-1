<?php

namespace Orc\BillingBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class MaxCrews extends Constraint
{
    public $message = "You are over your crew limit for your Plan. You must either delete crews to the allowable crew number or upgrade your plan.";

    public function validatedBy()
    {
        return 'maxcrews_validator';
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}
