<?php

namespace Orc\BillingBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotScheduled extends Constraint
{
    public $message = 'Cannot deactivate crew while it has scheduled bookings.';

    public function validatedBy()
    {
        return 'notscheduled_validator';
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}
