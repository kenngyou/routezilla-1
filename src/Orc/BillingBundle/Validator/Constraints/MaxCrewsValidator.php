<?php

namespace Orc\BillingBundle\Validator\Constraints;

use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Repository\CrewRepository;
use Orc\SaasBundle\Entity\Client;
use Synd\MultiTenantBundle\Event\TenantEvent;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MaxCrewsValidator extends ConstraintValidator
{
    /**
     * @param CrewRepository $crewRepository
     */
    public function __construct(CrewRepository $crewRepository)
    {
        $this->crewRepository = $crewRepository;
    }

    /**
     * Determine whether or not the user can create the crew (does it surpass plan limit)
     * @param Crew $crew new/udpated crew being avlidated
     * @param Constraint $constraint
     * @return boolean true if within limitations
     */
    public function isValid($crew, Constraint $constraint)
    {
        $client = $this->crewRepository->getTenant();

        if ($crew->getActive() and $limit = $this->getLimit($client)) {
            $crews = $this->getTotalCrews($crew, $client);
            if ($crews > $limit) {
                $this->context->addViolation($constraint->message);
                return false;
            }
        }

        return true;
    }

    /**
     * Get the maximum number of crews the user is allowed
     * @return integer|null number of crews, or null if not limited
     */
    protected function getLimit(Client $client)
    {
        if ($client->isTrialing()) {
            return null;
        }

        return $client->getPlan() ? $client->getPlan()->getMaxCrews() : null;
    }

    /**
     * Count the total number of crews the user has, including the new one
     * @param Crew $crew crew being updated
     * @param Client $client
     * @return integer total number of active crews
     */
    protected function getTotalCrews(Crew $crew, Client $client)
    {
        $crews = $this->crewRepository->findActive($client);
        $crews[] = $crew;

        $ids = array();
        foreach ($crews as $crew) {
            if ($crew->isActive()) {
                $ids[] = $crew->getId();
            }
        }

        return count(array_unique($ids));
    }
}
