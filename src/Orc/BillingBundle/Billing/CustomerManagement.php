<?php

namespace Orc\BillingBundle\Billing;

use Orc\BillingBundle\Entity\Plan;
use Orc\BillingBundle\Service\StripeCustomer;
use Orc\BillingBundle\Service\StripeCharge;
use Orc\BillingBundle\Service\StripeCoupon;
use Orc\SaasBundle\Entity\Client;
use Doctrine\ORM\EntityManager;

/**
 * Domain-level class for managing customer subscriptions
 * Persists changes both in application and on Stripe
 */
class CustomerManagement
{
    /**
     * @param StripeCustomer $stripeCustomer stripe.customer service
     * @param StripeCharge $stripeCharge stripe.customer service
     * @param StripeCoupon $stripeCoupon stripe.coupon service
     * @param EntityManager $em doctrine entity manager
     */
    public function __construct(StripeCustomer $stripeCustomer, StripeCharge $stripeCharge, StripeCoupon $stripeCoupon, EntityManager $em)
    {
        $this->stripeCustomer = $stripeCustomer;
        $this->stripeCharge= $stripeCharge;
        $this->stripeCoupon = $stripeCoupon;
        $this->em = $em;
    }

    /**
     * Upgrade a Client to a paid plan for the first time
     * @param Client $client
     * @param Plan $plan
     * @param string $token card representation from Stripe
     * @param string $couponCode
     */
    public function subscribe(Client $client, Plan $plan, $token, $couponCode = '')
    {
        $customer = $this->stripeCustomer->view($id = $client->getStripeId());
        $customer->updateSubscription(array(
            'card' => $token,
            'plan' => $plan->getId(),
            'trial_end' => $client->hasTrialed() ? 'now' : $customer['subscription']['trial_end'],
            'coupon' => $this->getCoupon($couponCode)
        ));

        $client->setPlan($plan);
        $client->setStatus(Client::STATUS_ACTIVE);
        $client->setSkipWizard(false);

        $this->em->persist($client);
        $this->em->flush();

        $this->stripeCustomer->flushCache($id);
    }

    /**
     * Changes a Client's card information on Stripe
     * @param Client $client
     * @param string $token new card representation
     */
    public function changeCard(Client $client, $token)
    {
        $this->stripeCustomer->update(
            $id = $client->getStripeId(),
            array('card' => $token)
        );
        $this->stripeCustomer->flushCache($id);
    }

    /**
     * Changes a Client's plan, while maintaining any active trial
     * @param Client $client
     * @param Plan $plan
     * @param string $couponCode
     */
    public function changePlan(Client $client, Plan $plan, $couponCode)
    {
        $customer = $this->stripeCustomer->view($id = $client->getStripeId());

        if ($client->hasTrialed()) {
            $trialEnd = 'now';
        } else {
            $trialEnd = $customer['subscription']['trial_end'];
            if ($trialEnd <= time()) {
                $trialEnd = 'now';
            }
        }


        $customer = $this->stripeCustomer->view($id = $client->getStripeId());
        $customer->updateSubscription(array(
            'plan' => $plan->getId(),
            'trial_end' => $trialEnd,
            'coupon' => $this->getCoupon($couponCode)
        ));

        $client->setPlan($plan);
        if ($client->isExpired()) {
            $client->setStatus(Client::STATUS_ACTIVE);
        }
        $this->em->persist($client);

        $this->em->flush();
        $this->stripeCustomer->flushCache($id);
    }

    /**
     * Pay off a Client's outstanding balance
     * @param Client $client
     * @param strin $token new card token
     * @throws \ErrorException
     */
    public function squareUp(Client $client, $token)
    {
        $customer = $this->stripeCustomer->view($id = $client->getStripeId());
        $this->stripeCustomer->update($id, array('card' => $token));

        try {
            $this->stripeCharge->charge($customer['account_balance'], $id);
            $this->stripeCustomer->update($id, array('account_balance' => 0));
        } catch (\Stripe_CardError $e) {
            $this->stripeCustomer->flushCache($id);
            throw $e;
        }

        $client->setStatus(Client::STATUS_ACTIVE);
        $this->em->persist($client);
        $this->em->flush();
        $this->stripeCustomer->flushCache($id);

    }

    /**
     * Cancels a Client's subscription
     * @param Client $client
     */
    public function cancel(Client $client)
    {
        $customer = $this->stripeCustomer->view($id = $client->getStripeId());
        $customer->cancelSubscription();

        $client->setPlan(null);
        $client->setStatus(Client::STATUS_CANCELLED);

        $this->em->persist($client);
        $this->em->flush();

        $this->stripeCustomer->flushCache($id);
    }

    /**
     * Return a verified coupon code, or nothing
     * @param string $code
     * @return string|null code if valid, null otherwise
     */
    protected function getCoupon($code)
    {
        return $code && $this->stripeCoupon->verify($code) ? $code : null;
    }
}
