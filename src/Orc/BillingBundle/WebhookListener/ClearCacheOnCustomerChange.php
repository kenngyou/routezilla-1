<?php

namespace Orc\BillingBundle\WebhookListener;

use Orc\BillingBundle\Event\ChargeEvent;
use Orc\BillingBundle\Event\CustomerEvent;
use Orc\BillingBundle\Event\SubscriptionEvent;
use Orc\BillingBundle\Service\StripeCustomer;
use Orc\SaasBundle\Repository\ClientRepository;
use Symfony\Component\HttpFoundation\Response;

class ClearCacheOnCustomerChange
{
    protected $repository;
    protected $stripe;

    public function __construct(ClientRepository $repository, StripeCustomer $stripe)
    {
        $this->repository = $repository;
        $this->stripe = $stripe;
    }

    public function onCustomerUpdate(CustomerEvent $event)
    {
        if ($id = $event->getCustomerId() and $this->repository->findOneByStripeId($id)) {
            $this->stripe->flushCache($id);
            $event->addMessage('cleared cache');
        }
    }

    public function onSubscriptionUpdate(SubscriptionEvent $event)
    {
        if ($id = $event->getCustomerId() and $this->repository->findOneByStripeId($id)) {
            $this->stripe->flushCache($id);
            $event->addMessage('cleared cache');
        }
    }

    public function onChargeSuccess(ChargeEvent $event)
    {
        if ($id = $event->getCustomerId() and $this->repository->findOneByStripeId($id)) {
            $this->stripe->flushCache($id);
            $event->addMessage('cleared cache');
        }
    }
}
