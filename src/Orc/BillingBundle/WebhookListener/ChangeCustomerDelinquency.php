<?php

namespace Orc\BillingBundle\WebhookListener;

use Orc\BillingBundle\Event\ChargeEvent;
use Orc\BillingBundle\Event\SubscriptionEvent;
use Orc\SaasBundle\Repository\ClientRepository;
use Doctrine\ORM\EntityManager;

class ChangeCustomerDelinquency
{
    protected $repository;
    protected $em;

    public function __construct(ClientRepository $repository, EntityManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * Removes delinquency and trial status on successful payment
     *
     * @param ChargeEvent $event
     */
    public function onChargeSuccess(ChargeEvent $event)
    {
        if (!$client = $this->repository->findOneByStripeId($event->getCustomerId())) {
            return;
        }

        if ($client->getDeliquentSince()) {
            $event->addMessage('removed deliquency');
        }
        $client->setDeliquentSince(null);
        if ($client->getCustomer()->getTrialEndDate()) {
            $event->addMessage('removed trial');
        }
        $client->getCustomer()->setTrialEndDate(null);

        $this->em->persist($client);
        $this->em->persist($client->getCustomer());
        $this->em->flush();
    }

    /**
     * Sets deliquency on failed payment
     *
     * @param ChargeEvent $event
     */
    public function onChargeFailure(ChargeEvent $event)
    {
        if (!$client = $this->repository->findOneByStripeId($event->getCustomerId())) {
            return;
        }

        if (!$client->getDeliquentSince()) {
            $event->addMessage('added deliquency');
        }
        $client->setDeliquentSince(new \DateTime);

        $this->em->persist($client);
        $this->em->persist($client->getCustomer());
        $this->em->flush();
    }

    /**
     * Sets deliquency when subscription switches to past due
     *
     * @param SubscriptionEvent $event
     */
    public function onSubscriptionUpdate(SubscriptionEvent $event)
    {
        if (!$client = $this->repository->findOneByStripeId($event->getCustomerId())) {
            return;
        }

        if ($event->getWebhookData()->status == 'past due') {
            if (!$client->getDeliquentSince()) {
                $event->addMessage('added deliquency');
            }

            $client->setDeliquentSince(new \DateTime);

            $this->em->persist($client);
            $this->em->persist($client->getCustomer());
            $this->em->flush();
        }
    }
}
