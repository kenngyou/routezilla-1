<?php

namespace Orc\BillingBundle\WebhookListener;

use Orc\BillingBundle\Event\SubscriptionEvent;
use Orc\SaasBundle\Repository\ClientRepository;
use Doctrine\ORM\EntityManager;

class UpdateCustomerPlan
{
    protected $clientRepository;
    protected $em;

    public function __construct(ClientRepository $clientRepository, EntityManager $em)
    {
        $this->clientRepository = $clientRepository;
        $this->em = $em;
    }

    /**
     * Updates a Client's Plan if it changed remotely
     *
     * @param SubscriptionEvent $event
     */
    public function onSubscriptionUpdate(SubscriptionEvent $event)
    {
        $customerId = $event->getCustomerId();
        $planId = $event->getPlanId();

        if (!$client = $this->clientRepository->findOneByStripeId($customerId)) {
            return;
        }

        if ($planId != $client->getPlan()->getId()) {
            $client->setplan($this->em->getReference(get_class($client->getPlan()), $planId));
            $this->em->persist($client);
            $this->em->flush();
            $event->addMessage('udpated customer plan');
        }
    }
}
