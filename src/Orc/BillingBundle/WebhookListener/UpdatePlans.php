<?php

namespace Orc\BillingBundle\WebhookListener;

use Orc\BillingBundle\Sync\SyncPlans;
use Orc\BillingBundle\Event\WebhookEvent;
use Symfony\Component\HttpFoundation\Response;

class UpdatePlans
{
    public function __construct(SyncPlans $sync)
    {
        $this->sync = $sync;
    }

    public function onPlanChange(WebhookEvent $event)
    {
        $this->sync->sync();
        $event->addMessage('updating plans: ' . implode(',', $this->sync->getMessages()));
    }
}
