<?php

namespace Orc\BillingBundle\WebhookListener;

use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Repository\ClientRepository;
use Orc\BillingBundle\Event\SubscriptionEvent;
use Doctrine\ORM\EntityManager;

class ChangeSubscriptionStatus
{
    protected $repository;
    protected $em;

    public function __construct(ClientRepository $repository, EntityManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * Updates a subscription, re-instating it if expired
     * Also grabs latest trial dates from Stripe
     *
     * @param SubscriptionEvent $event
     */
    public function onSubscriptionUpdate(SubscriptionEvent $event)
    {
        $customerId = $event->getCustomerId();
        if (!$client = $this->repository->findOneByStripeId($customerId)) {
            return;
        }

        if ($client->getStatus() == Client::STATUS_EXPIRED) {
            $event->addMessage('re-activated client');
            $client->setStatus(Client::STATUS_ACTIVE);
        }

        $client->getCustomer()->updateFromStripe($event->getWebhookData()->object);

        $this->em->persist($client);
        $this->em->persist($client->getCustomer());
        $this->em->flush();
    }

    /**
     * Ends a subscription if it ended on Stripe
     *
     * @param SubscriptionEvent $event
     */
    public function onSubscriptionEnd(SubscriptionEvent $event)
    {
        if (!$client = $this->repository->findOneByStripeId($event->getCustomerId())) {
            return;
        }

        if ($client->getStatus() !== Client::STATUS_EXPIRED) {
            $event->addMessage('expired client');
        }
        $client->setStatus(Client::STATUS_EXPIRED);

        $this->em->persist($client);
        $this->em->flush();
    }
}
