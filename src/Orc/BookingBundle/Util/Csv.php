<?php

namespace Orc\BookingBundle\Util;

class Csv
{
    /**
     * Convert a multi-dimensional, associative array to CSV data
     * @param  array $data the array of data
     * @return string       CSV text
     */
    public static function arrayToCsv(array $data)
    {
        if (!count($data)) {
            return '';
        }

        $fh = fopen('php://temp', 'rw');
        fputcsv($fh, array_keys(current($data)));

        foreach ($data as $row ) {
            fputcsv($fh, $row);
        }
        rewind($fh);
        $csv = stream_get_contents($fh);
        fclose($fh);

        return $csv;
    }
}
