<?php

namespace Orc\BookingBundle\Util;

class GetDotted
{
    public static function get($obj, $key, $fallback = '')
    {
        $value = $obj;
        $parts = explode('.', $key);
        while (count($parts)) {
            $nextKey = array_shift($parts);
            $value = self::getValue($value, $nextKey);
            if ($value === null) return $fallback;
        }

        return $value;
    }

    private static function getValue($obj, $singleKey)
    {
        $getterName = 'get' . ucfirst($singleKey);
        if (method_exists($obj, $getterName)) {
            return call_user_func(array($obj, $getterName));
        }

        if (!is_array($obj)) {
            return null;
        }

        return $obj[$singleKey];
    }
}
