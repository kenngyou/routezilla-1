<?php

namespace Orc\BookingBundle\Util;

/**
 * Cleans text to be safe for URLs
 */
class Slugify
{
    static $find = array(
        "'",
        '(',
        ')',
        '&amp;',
        '&lt;',
        '&gt;'
    );

    static $replace = array(
        '',
        ' ',
        ' ',
        ' ',
        ' ',
        ' '
    );

    static public function generate($text, $char = '-')
    {
        if (!is_string($text) and !is_numeric($text) and !is_bool($text)) {
            throw new \InvalidArgumentException('$text cannot be converted to string');
        }

        $text = str_replace(self::$find, self::$replace, $text);
        $text = preg_replace('/[^(a-zA-Z0-9)]+/', $char, strtolower($text));
        return trim($text, $char);
    }
}
