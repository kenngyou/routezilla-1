<?php

namespace Orc\BookingBundle\Util;

class ColorGenerator
{
    const COLOR_DARK = '000000';
    const COLOR_LIGHT = 'FFFFFF';

    static public function generate()
    {
        return self::rgbToHex(mt_rand(0, 255), mt_rand(0, 255), mt_rand(0, 255));
    }

    static public function getForeground($background, $dark = self::COLOR_DARK, $light = self::COLOR_LIGHT)
    {
        $background = str_replace('#', '', $background);

        $r = hexdec(substr($background, 0, 2));
        $g = hexdec(substr($background, 2, 2));
        $b = hexdec(substr($background, 4, 2));

        $luminance = ($r * 0.229) + ($g * 0.587) + ($b * 0.114);
        if (1 - $luminance > 0.5) {
            return $dark;
        }

        return $light;
    }

    static protected function rgbToHex($r, $g, $b)
    {
        return sprintf('%02x', $r) . sprintf('%02x', $g) . sprintf('%02x', $b);
    }
}
