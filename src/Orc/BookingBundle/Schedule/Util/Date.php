<?php

namespace Orc\BookingBundle\Schedule\Util;

class Date
{

    public static function diff(array $datesA, array $datesB)
    {
        $out = array();

        foreach ($datesA as $date) {
            if (!in_array($date, $datesB)) {
                $out[] = $date;
            }
        }

        return $out;
    }

    /**
     * Finds the timeslots which are found in both arrays
     *
     * @param    DateTime[]
     * @param    DateTime[]
     * @return   DateTime[]
     */
    static public function intersect(array $datesA, array $datesB)
    {
        $out = array();

        foreach ($datesA as $a => $dateA) {
            foreach ($datesB as $b => $dateB) {
                if ($dateA == $dateB) {
                    $out[] = $dateA;
                }
            }
        }

        return $out;
    }

    /**
     * Gets the dates in $slots between $start and $end
     *
     * @param    DateTime        range start
     * @param    DateTime        range end
     * @param    DateTime        Slots
     * @param    integer         Minutes between slots
     * @return   DateTime[]      Found dates
     */
    static public function getBetween($start, $end, array $slots, $interval)
    {
        $found = array();

        foreach ($slots as $slot) {
            if (Date::areOverlapping($start, $end, $slot, Date::getNext($slot, $interval))) {
                $found[] = $slot;
            }
        }

        return $found;
    }

    /**
     * Checks to see if two date ranges overlap
     * @param    DateTime (A start)
     * @param    DateTime (A end)
     * @param    DateTime (B start)
     * @param    DateTime (B end)
     * @return   booelan    true if they overlap
     */
    static public function areOverlapping($a1, $a2, $b1, $b2)
    {
        return $a1 < $b2 && $a2 > $b1;
    }

    /**
     * Clones and modifies a date
     * @param    DateTime
     * @param    integer         Minutes to add
     * @return   DateTime        Modified date
     */
    static public function getNext(\DateTime $date, $minutes)
    {
        $out = clone $date;
        $out->modify(sprintf('+%d minutes', $minutes));

        return $out;
    }
}
