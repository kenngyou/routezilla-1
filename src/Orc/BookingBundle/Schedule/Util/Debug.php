<?php

namespace Orc\BookingBundle\Schedule\Util;

class Debug
{
    static public function dumpMatchingDay(array $slots, $day)
    {
        foreach ($slots as $slot) {
            if ($slot->format('Y-m-d') == $day) {
                var_dump($slot->format('H:ia'));
            }
        }
    }
}
