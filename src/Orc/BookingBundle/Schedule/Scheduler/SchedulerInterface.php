<?php

namespace Orc\BookingBundle\Schedule\Scheduler;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Crew;
use Orc\SaasBundle\Entity\Client;

interface SchedulerInterface
{
    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    function setRange(\DateTime $start, \DateTime $end);

    /**
     * Returns all available time slots for Client
     * This is the primary query used for Scheduling
     *
     * @param    Client
     * @return   DateTime[]
     */
    function getAvailableTimeSlots(Client $client, Booking $booking = null);

    /**
     * Returns the first available Crew which can service Booking, if any
     *
     * @param    Booking
     * @param    Client
     * @return   Crew
     */
    function getAvailableCrew(Booking $booking, Client $client);

    /**
     * Checks to see whether a crew is available at a given time
     *
     * @param    Crew|null      Crew to test, or null for assigned
     * @param    DateTime
     * @param    Booking|null
     * @param    Client
     * @return   boolean        TRUE if available
     */
    function isCrewAvailableAt(Crew $crew = null, \DateTime $slot, $ignoreBooking = null, Client $client);
}
