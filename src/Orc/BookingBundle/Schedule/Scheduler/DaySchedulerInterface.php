<?php

namespace Orc\BookingBundle\Schedule\Scheduler;

use Orc\BookingBundle\Schedule\Scheduler;
use Orc\SaasBundle\Entity\Client;

interface DaySchedulerInterface
{
    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    function setRange(\DateTime $start, \DateTime $end);

    /**
     * Returns all available days for Client
     *
     * @param    Client
     * @param    Booking
     * @return   DateTime[]
     */
    function getAvailableDays(Client $client, $booking = null);

    /**
     * Check to see if a given day is available
     *
     * @param    Client
     * @param    DateTime  (the day to check)
     * @param    Booking|null
     * @return   boolean        True if available (for booking)?
     */
    public function isDayAvailable(Client $client, \DateTime $day, $booking = null);
}
