<?php


namespace Orc\BookingBundle\Schedule\Scheduler\UnassignableScheduler;

use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Crew;
use Orc\SaasBundle\Entity\Client;

class UnassignableScheduler implements SchedulerInterface
{
    protected $interval;
    protected $calculator;
    protected $start;
    protected $end;

    public function __construct($interval, EndTimeCalculator $calculator)
    {
        $this->interval = $interval;
        $this->calculator = $calculator;
    }

    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    public function setRange(\DateTime $start, \DateTime $end)
    {
        $today = new DateTime('today');

        if ($start < $today) {
            $start = $today;
        }

        $this->start = clone $start;
        $this->end = clone $end;
    }

    /**
     * Generates ALL time slots for a short time span
     *
     * {@inheritDoc}
     */
    public function getAvailableTimeSlots(Client $client, Booking $booking = null)
    {
        if (!$booking) {
            throw new \LogicException('UnassignableScheduler requires a Booking');
        }

        $minutes = $this->calculator->getTimeRequired($booking);

        $slots = array();

        for ($i = clone $this->start; $i < $this->end; $i->modify(sprintf('+%d minutes', $this->interval))) {
            $test = clone $i;
            if ($test->modify(sprintf('+%d minutes', $minutes)) >= $this->end) {
                continue;
            }

            $slots[] = clone $i;
        }

        return $slots;
    }

    /**
     * {@inheritDoc}
     * @throws    \LogicException  always
     */
    public function getAvailableCrew(Booking $booking, Client $client)
    {
        throw new \LogicException('Cannot get available crew for unassignable');
    }

    /**
     * {@inheritDoc}
     * @throws    \LogicException  always
     */
    function isCrewAvailableAt(Crew $crew = null, \DateTime $slot, $ignoreBooking = null, Client $client)
    {
        throw new \LogicException('Cannot get available crew for unassignable');
    }
}
