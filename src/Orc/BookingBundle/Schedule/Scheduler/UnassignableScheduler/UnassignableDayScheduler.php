<?php

namespace Orc\BookingBundle\Schedule\Scheduler\UnassignableScheduler;

use Orc\BookingBundle\Schedule\Scheduler\DaySchedulerInterface;
use Orc\BookingBundle\Entity\DateTime;
use Orc\SaasBundle\Entity\Client;

class UnassignableDayScheduler implements DaySchedulerInterface
{
    protected $calendarDateFormat;
    protected $start;
    protected $end;

    public function __construct($calendarDateFormat)
    {
        $this->calendarDateFormat = $calendarDateFormat;
    }

    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    public function setRange(\DateTime $start, \DateTime $end)
    {
        $today = new DateTime('today');

        if ($start < $today) {
            $start = $today;
        }

        $this->start = clone $start;
        $this->end = clone $end;
    }

    /**
     * Generates all the days regardless of the generator / scheduler
     *
     * {@inheritDoc}
     */
    public function getAvailableDays(Client $client, $booking = null)
    {
        $days = array();

        for ($i = clone $this->start; $i < $this->end; $i->modify('+1 day')) {
            $days[] = clone $i;
        }

        return $days;
    }

    /**
     * {@inheritDoc}
     */
    public function isDayAvailable(Client $client, \DateTime $day, $booking = null)
    {
        $day = $day->format($this->calendarDateFormat);
        foreach ($this->getAvailableDays($client, $booking) as $testDay) {
            if ($day == $testDay->format($this->calendarDateFormat)) {
                return true;
            }
        }

        return false;
    }
}
