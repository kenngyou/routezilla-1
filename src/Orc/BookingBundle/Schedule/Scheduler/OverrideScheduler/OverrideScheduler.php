<?php

namespace Orc\BookingBundle\Schedule\Scheduler\OverrideScheduler;

use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\BookingBundle\Schedule\Util\Date;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Crew;
use Orc\SaasBundle\Entity\Client;
use Doctrine\ORM\EntityRepository;

class OverrideScheduler implements SchedulerInterface
{
    protected $interval;
    protected $bookingRepository;

    protected $start;

    public function __construct($interval, EntityRepository $bookingRepository)
    {
        $this->interval = $interval;
        $this->bookingRepository = $bookingRepository;
    }

    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    public function setRange(\DateTime $start, \DateTime $end)
    {
        throw new \LogicException('Override scheduler only works on a single day');
    }

    /**
     * {@inheritDoc}
     * @throws    \LogicException  always
     */
    public function getAvailableTimeSlots(Client $client, Booking $booking = null)
    {
        throw new \LogicException('Override scheduler cannot generate timeslots');
    }

    /**
     * {@inheritDoc}
     * @throws    \LogicException  always
     */
    public function getAvailableCrew(Booking $booking, Client $client)
    {
        throw new \LogicException('Cannot get available crew for unassignable');
    }

    /**
     * A simplified crew availability check which only checks for conflicts in existing bookings
     * All other checks are ignored
     *
     * {@inheritDoc}
     */
    public function isCrewAvailableAt(Crew $crew = null, \DateTime $slot, $booking = null, Client $client)
    {
        if (!$crew) {
            return true;
        }

        $start = clone $slot;
        $start->setTime(0, 0);
        $end = clone $start;
        $end->modify('+2 days');

        $slots = $this->getTimeSlots($start, $end);

        $requiredSlots = array();
        foreach ($slots as $slot) {
            if (Date::areOverlapping($slot, Date::getNext($slot, $this->interval), $booking->getDateStart(), $booking->getDateEnd())) {
                $requiredSlots[] = $slot;
            }
        }

        if (!$requiredSlots) {
            return false;
        }
        if (reset($requiredSlots)->format('Y-m-d') != end($requiredSlots)->format('Y-m-d')) {
            return false;
        }

        return !Date::intersect($requiredSlots, $this->getConflictsByCrew($crew, $booking, $slots));
    }

    /**
     * Simplified version of TimeSlotGenerator
     * Generates X minute increments from the past up until the end of the day we are checking
     * @param    DateTime    starting point
     * @return   DateTime[]  Slots
     */
    protected function getTimeSlots(\DateTime $date, \DateTime $end)
    {
        $slots = array();

        for ($i = clone $date; $i < $end; $i->modify(sprintf('+%d minutes', $this->interval))) {
            $slots[] = clone $i;
        }

        return $slots;
    }

    /**
     * Creates conflicts based on the bookings that overlap times that we need
     * @param    Crew            The crew we are checking for availability
     * @param    Booking         The booking we are trying to book
     * @param    DateTime[]      The timeslots we have to work with
     * @return   DateTime[]      The conflicting timeslots
     */
    protected function getConflictsByCrew(Crew $crew, Booking $booking, array $slots)
    {
        $conflicting = array();

        foreach ($this->bookingRepository->findConflictingWith($crew, $booking) as $upcomingBooking) {
            $conflicting = array_merge(
                $conflicting,
                Date::getBetween($upcomingBooking->getDateStart(), $upcomingBooking->getDateEnd(), $slots, $this->interval)
            );
        }

        return $conflicting;
    }
}
