<?php

namespace Orc\BookingBundle\Schedule\Scheduler\StrictScheduler;

use Orc\BookingBundle\Schedule\Scheduler\DaySchedulerInterface;
use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\SaasBundle\Entity\Client;

class DayScheduler implements DaySchedulerInterface
{
    protected $scheduler;
    protected $calendarDateFormat;
    protected $publicInterval;

    public function __construct(SchedulerInterface $scheduler, $calendarDateFormat, $publicInterval)
    {
        $this->scheduler = $scheduler;
        $this->calendarDateFormat = $calendarDateFormat;
        $this->publicInterval = $publicInterval;
    }

    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    public function setRange(\DateTime $start, \DateTime $end)
    {
        $this->scheduler->setRange($start, $end);
    }

    /**
     * Returns all available days for Client
     * This reduces the timeslots into days rather than %interval% timeslots
     *
     * @param    Client
     * @param    Booking
     * @return   DateTime[]
     */
    public function getAvailableDays(Client $client, $booking = null)
    {
        $out = array();

        foreach ($this->scheduler->getAvailableTimeSlots($client, $booking) as $timeSlot) {
            if ($timeSlot->format('i') % $this->publicInterval) {
                continue;
            }
            $out[$timeSlot->format($this->calendarDateFormat)] = $timeSlot;
        }
        return $out;
    }

    /**
     * Check to see if a given day is available
     * @param    Client
     * @param    DateTime  (the day to check)
     * @param    Booking|null
     * @return   boolean        True if available (for booking)?
     */
    public function isDayAvailable(Client $client, \DateTime $day, $booking = null)
    {
        $available = $this->getAvailableDays($client, $booking);
        return isset($available[$day->format($this->calendarDateFormat)]);
    }
}
