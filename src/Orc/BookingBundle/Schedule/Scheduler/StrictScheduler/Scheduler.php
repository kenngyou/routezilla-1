<?php

namespace Orc\BookingBundle\Schedule\Scheduler\StrictScheduler;

use Orc\BookingBundle\Schedule\Util\Date;
use Orc\BookingBundle\Schedule\Util\Debug;
use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\BookingBundle\Schedule\Exception\CrewUnavailableException;
use Orc\BookingBundle\Schedule\TimeSlotGenerator;
use Orc\BookingBundle\Booking\DistanceCalculator;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\CrewRadius;
use Orc\SaasBundle\Entity\Client;
use Doctrine\ORM\EntityRepository;

/**
 * The Scheduler Component
 *
 * This class works by generating a Client's potentially available time slots
 * through the TimeSlotGenerator, and then removing all of the conflicting / unbookable
 * timeslots through a series of checks:
 *
 * The "potentially" available timeslots are the default hours of operation, expanded, with schedule
 * exceptions (adjustments) added in automatically.  Blackouts are not processed yet.
 *
 * Conflicts include:
 *     - Global blackouts
 *     - Crew conflicts (these tests are run per crew!):
 *         - existing bookings
 *         - Crew blackouts
 *         - Crew radius *
 *         - Crew's supported services *
 *         - Crew's assigned regions *
 *
 * Items marked * require a booking for comparison; some tests require a Location, Region of Service
 * to determine availability.
 */
class Scheduler implements SchedulerInterface
{
    protected $generator;
    protected $calculator;
    protected $distanceCalculator;
    protected $crewRepository;
    protected $crewRadiusRepository;
    protected $bookingRepository;
    protected $blackoutRepository;
    protected $interval;
    protected $start;
    protected $end;

    protected $ignoreBookings = false;

    public function __construct(
        TimeSlotGenerator $generator,
        EndTimeCalculator $calculator,
        DistanceCalculator $distanceCalculator,
        EntityRepository $crewRepository,
        EntityRepository $crewRadiusRepository,
        EntityRepository $bookingRepository,
        EntityRepository $blackoutRepository,
        $interval
    )
    {
        $this->generator = $generator;
        $this->calculator = $calculator;
        $this->distanceCalculator = $distanceCalculator;
        $this->crewRepository = $crewRepository;
        $this->crewRadiusRepository = $crewRadiusRepository;
        $this->bookingRepository = $bookingRepository;
        $this->blackoutRepository = $blackoutRepository;
        $this->interval = $interval;
    }

    /**
     * Determines the range to query
     * @param    \DateTime    Start day
     * @param    \DateTime    End day
     */
    public function setRange(\DateTime $start, \DateTime $end, $current = true)
    {
        $today = new DateTime('today');

        if ($current and $start < $today) {
            $start = $today;
        }

        $this->start = clone $start;
        $this->end = clone $end;
    }

    /**
     * Returns all available time slots for Client
     * This is the primary query used for Scheduling
     *
     * @param    Client
     * @return   DateTime[]
     */
    public function getAvailableTimeSlots(Client $client, Booking $booking = null)
    {
        $slots = $this->generator->getTimeSlots($client, $this->start, $this->end);
        $conflicting = $booking ? $this->getConflictsByBooking($booking, $slots) : array();

        $crewConflicts = array();

        foreach ($crews = $this->crewRepository->findByClient($client) as $crew) {
            try {
                $crewConflicts[$crew->getName()] = array_merge($conflicting, $this->getConflictsByCrew($crew, $booking, $slots));
            } catch (CrewUnavailableException $e) {

            }
        }

        if (!$crewConflicts) {
            return array();
        }

        $slots = Date::diff($slots, $conflicting);
        return $this->mergeCrewAvailability($slots, $crewConflicts, $booking);
    }

    /**
     * Returns the first available Crew which can service Booking, if any
     * @param    Booking
     * @param    Client
     * @return   Crew
     */
    public function getAvailableCrew(Booking $booking, Client $client)
    {
        $slots = $this->generator->getTimeSlots($client, $this->start, $this->end);

        // find required slots for crew and global checks
        $broken = false;
        $requiredSlots = $requiredFirstSlots = array();
        foreach ($slots as $slot) {
            if (Date::areOverlapping($slot, Date::getNext($slot, $this->interval), $booking->getDateStart(), $booking->getDateEnd())) {
                $requiredSlots[] = $slot;

                if ($broken) {
                    continue;
                }

                $broken = true;
                $requiredFirstSlots[] = $slot;

            }
        }

        // Are there any crew-wide conflicts?
        if (Date::intersect($requiredFirstSlots, $this->getConflictsByBooking($booking, $slots))) {
            return;
        }

        // Find first crew which don't have any of their own conflicts
        $availableCrews = array();
        foreach ($this->crewRepository->findByClient($client) as $crew) {
            try {
                $crewConflicting = $this->getConflictsByCrew($crew, $booking, $slots);
            } catch (CrewUnavailableException $e) {
                continue;
            }

            if (!Date::intersect($requiredSlots, $crewConflicting)) {
                return $crew;
            }
        }
    }

    /**
     * Count the number of available slots for a Client within $days
     * @param    Client
     * @param    integer        Number of days to check
     * @return   array          Counts per day
     */
    public function countAvailableSlots(Client $client)
    {
        $crews = $this->crewRepository->findAll();
        $slots = $this->generator->getTimeSlots($client, $this->start, $this->end);

        $this->ignoreBookings = true;

        $total = 0;

        foreach ($crews as $crew) {
            $crewTotal = count($slots);

            try {
                $crewTotal -= count($this->getConflictsByCrew($crew, null, $slots));
            } catch (CrewUnavailableException $e) {
                $crewTotal = 0;
            }

            $total += $crewTotal;
        }

        $this->ignoreBookings = false;

        return $total;
    }

    /**
     * Checks to see whether a crew is available at a given time
     *
     * @param    Crew|null      Crew to test, or null for assigned
     * @param    DateTime
     * @param    Booking|null
     * @param    Client
     * @return   boolean        TRUE if available
     */
    public function isCrewAvailableAt(Crew $crew = null, \DateTime $slot, $ignoreBooking = null, Client $client)
    {
        $slots = $this->generator->getTimeSlots($client, $this->start, $this->end);
        $conflicts = $ignoreBooking ? $this->getConflictsByBooking($ignoreBooking, $slots) : array();
        $crewConflicts = array();

        if ($crew) {
            try {
                $crewConflicts = $this->getConflictsByCrew($crew, $ignoreBooking, $slots);
            } catch (CrewUnavailableException $e) {
                return false;
            }
        } else {
            return true;
        }

        // find required slots for crew and global checks
        $broken = false;
        $requiredSlots = $requiredFirstSlots = array();
        foreach ($slots as $slot) {
            if (Date::areOverlapping($slot, Date::getNext($slot, $this->interval), $ignoreBooking->getDateStart(), $ignoreBooking->getDateEnd())) {
                $requiredSlots[] = $slot;

                if ($broken) {
                    continue;
                }

                $broken = true;
                $requiredFirstSlots[] = $slot;

            }
        }

        if (!$requiredSlots) {
            return false;
        }
        $endCheck = clone end($requiredSlots);
        $endCheck->modify(sprintf('+%d minutes', $this->interval));
        if ($ignoreBooking->getDateEnd() > $endCheck) {
            return false;
        }

        // Are there any crew-wide conflicts?
        if (Date::intersect($requiredFirstSlots, $conflicts)) {
            return false;
        }

        // Conflicts on required slots?
        if (Date::intersect($requiredSlots, $crewConflicts)) {
            return false;
        }

        foreach ($requiredSlots as $slot) {
            if (!in_array($slot, $slots)) {
                return false;
            }
        }

        return true;
    }


    /**
     * Finds all the conflicts for a Booking based on Blackouts
     * Adds conflicts for bookings which may take too long
     *
     * @param    Booking
     * @return   DateTime[]        Conflicting slots
     */
    protected function getConflictsByBooking(Booking $booking, array $slots)
    {
        $conflicting = array();

        foreach ($this->blackoutRepository->findAffectingBooking($booking, $this->start, $this->end) as $blackout) {
            foreach ($blackout->getEffectiveRanges() as $range) {
                foreach ($slots as $slot) {
                    if ($slot >= $range[0] and $slot <= $range[1]) {
                        $conflicting[$slot->format('U')] = $slot;
                    }
                }
            }
        }

        return array_values($conflicting);
    }

    /**
     * Returns all conflicting time slots for Crew
     *
     * @param    Crew
     * @param    Booking|null (if attempting to book a Booking)
     *
     * @return   DateTime[]
     * @throws   CrewUnavailableException  when crew can't service request
     */
    protected function getConflictsByCrew(Crew $crew, Booking $ignoreBooking = null, array $slots)
    {
        $conflicting = array();

        // Can the crew handle the booking?
        if ($ignoreBooking) {
            if (!$crew->getServices() or !$crew->getServices()->contains($ignoreBooking->getService())) {
                throw new CrewUnavailableException();
            }
            if (!$crew->getRegions() or !$crew->getRegions()->contains($ignoreBooking->getRegion())) {
                throw new CrewUnavailableException();
            }
            if (!$ignoreBooking->getService()->getIsActive()) {
                throw new CrewUnavailableException();
            }
        }

        if (!$crew->getActive()) {
            throw new CrewUnavailableException();
        }
        if ($crew->isDeleted()) {
            throw new CrewUnavailableException();
        }


        // Is the crew off?
        foreach ($this->blackoutRepository->findAffectingCrew($crew, $this->start, $this->end, $ignoreBooking) as $blackout) {
            $end = clone $blackout->getDateEnd();
            $end->modify('+1 day');

            $conflicting = array_merge(
                $conflicting,
                Date::getBetween($blackout->getDateStart(), $end, $slots, $this->interval)
            );
        }

        // Is the request within the crew's radius for the day?
        if ($ignoreBooking) {
            $radiuses = $this->crewRadiusRepository->findByCrewWithinRange($crew, $this->start, $this->end);
            $dayRadiusCheck = array();

            foreach ($slots as $slot) {
                $key = $slot->format('Y-m-d');
                if (!isset($daysChecked[$key])) {
                    if (!isset($radiuses[$key])) {
                       $dayRadiusCheck[$key] = true;
                    } else {
                        $dayRadiusCheck[$key] = $this->isInRange($ignoreBooking, $radiuses[$key]);
                    }
                }

                if (!$dayRadiusCheck[$key]) {
                    $conflicting[] = $slot;
                }
            }
        }

        // Is the crew already booked?
        if (!$this->ignoreBookings) {
            foreach ($this->bookingRepository->findUpcomingByCrew($crew) as $booking) {
                if ($booking == $ignoreBooking or $booking->getId() == $ignoreBooking->getId()) {
                    continue;
                }

                $conflicting = array_merge(
                    $conflicting,
                    Date::getBetween($booking->getDateStart(), $booking->getDateEnd(), $slots, $this->interval)
                );
            }
        }

        return $conflicting;
    }

    /*
     * Checks to see if two bookings are in allowable range from each other
     * @param    Booking
     * @param    Booking
     * @return   boolean    True if out of range
     */
    protected function isInRange(Booking $booking, CrewRadius $radius)
    {
        $distance = $this->distanceCalculator->getDistance(
            $booking->getLocation(),
            $radius->getLocation(),
            $radius->getCrew()->getClient()->getUnits() == 'km'
                ? DistanceCalculator::EARTH_RADIUS_KM
                : DistanceCalculator::EARTH_RADIUS_MILES
        );

        return $distance <= $radius->getRadius();
    }

    /**
     * Removes a set of conficting timeslots from a set of potential slots
     *
     * @param    DateTime[]    Timeslots
     * @param    array         Crew conflicts, indexed by crew ID
     *
     * @return   DateTime[]    $potential timeslots with $conflicting ones removed
     */
    protected function mergeCrewAvailability($slots, $crewConflicts, Booking $booking = null)
    {
        $conflictsPerSlot = array();

        // first pass: ensure enough room for booking
        if ($booking) {
            $fullCrewConflicts = array();

            if ($booking->getDateEnd()) {
                $minutes = $booking->getTimeRequired();
            } else {
                $minutes = $this->calculator->getTimeRequired($booking);
            }

            foreach ($crewConflicts as $crewId => $conflicts) {
                $myConflicts = array();

                $crewSlots = Date::diff($slots, $conflicts);
                $crewDaySlots = $this->groupSlotsByDay($crewSlots);

                foreach ($crewDaySlots as $day => $daySlots) {
                    foreach ($daySlots as $slot) {
                        $end = clone $slot;
                        $end->modify('+ ' . $minutes . ' minutes');

                        $required = array();
                        for ($test = clone $slot; $test < $end; $test->modify(sprintf('+%d minutes', $this->interval))) {
                            $required[] = clone $test;
                        }

                        // a not in b
                        if (Date::diff($required, $daySlots)) {
                            $myConflicts[] = clone $slot;
                        }

                    }
                }

                $fullCrewConflicts[$crewId] = array_merge($conflicts, $myConflicts);
            }

            $crewConflicts = $fullCrewConflicts;
        }

        // second pass: merge resultset to flat list
        foreach ($crewConflicts as $crewId => $conflicts) {
            $duplicates = array();

            foreach ($conflicts as $conflictingSlot) {
                $id = $conflictingSlot->format('m/d/Y h:ia');
                if (in_array($id, $duplicates)) {
                    continue;
                }
                $duplicates[] = $id;

                if (!isset($conflictsPerSlot[$id])) {
                    $conflictsPerSlot[$id] = 1;
                } else {
                    $conflictsPerSlot[$id]++;
                }

                if ($conflictsPerSlot[$id] >= count($crewConflicts)) {
                    if (false !== $found = array_search($conflictingSlot, $slots)) {
                        unset($slots[$found]);
                    }

                }
            }
        }


        return $slots;
    }

    private function groupSlotsByDay(array $slots)
    {
        $byDay = array();
        foreach ($slots as $slot) {
            $id = $slot->format('Y-m-d');

            if (!isset($byDay[$id])) {
                $byDay[$id] = array();
            }
            $byDay[$id][] = $slot;
        }

        return $byDay;
    }

    protected function getCacheKey(Client $client, Booking $booking = null) {
        $parts = array($client->getId());

        if ($booking) {
            if ($booking->getLocation()) {
                $parts[] = $booking->getLocation()->getLatitude() . ',' . $booking->getLocation()->getLongitude();
            }

            $parts[] = $booking->getRegion()  ? $booking->getRegion()->getId() : 'noregion';
            $parts[] = $booking->getService() ? $booking->getService()->getId() : 'noservice';

            $answers = array();
            foreach ($booking->getFields() as $field) {
                $answers[] = $field->getAnswer();
            }

            $parts[] = $answers ? md5(implode(',', $answers)) : 'noanswers';
        }

        return implode(',', $parts);
    }
}
