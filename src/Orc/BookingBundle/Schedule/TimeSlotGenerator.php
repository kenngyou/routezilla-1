<?php

namespace Orc\BookingBundle\Schedule;

use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\DateTime;

class TimeSlotGenerator
{
    /**
     * @var    string        Date::modify() compatible increment length
     */
    protected $interval;

    public function __construct($interval)
    {
        $this->interval = sprintf('+%d minutes', $interval);
    }

    /**
     * Generates all upcoming timeslots
     * @param    Client
     * @param    DateTime        Date in future to generate timeslots until
     * @return   DateTime[]
     */
    public function getTimeSlots(Client $client, \DateTime $startDate, \DateTime $endDate)
    {
        $defaults = $client->getHoursOfOperation();
        $exceptions = $this->indexExceptions($client->getHoursChanges());

        $slots = array();

        // foreach day from starting point until $timeAhead
        for ($i = clone $startDate; $i < $endDate; $i->modify('+1 day')) {

            $todaysHours = $this->getToday($i, $defaults, $exceptions);
            if ($todaysHours->getOff()) {
                continue;
            }

            $start = $todaysHours->getStartTime();
            $end = $todaysHours->getEndTime();

            $start->setDate($i->format('Y'), $i->format('m'), $i->format('d'));
            $end->setDate($i->format('Y'), $i->format('m'), $i->format('d'));

            // foreach interval from starttime to endtime
            for ($i->setTime($start->format('h'), $start->format('i')); $i < $end; $i->modify($this->interval)) {
                $slots[] = clone $i;
            }
        }

        return $slots;
    }

    protected function getToday($today, $defaults, $exceptions)
    {
        return isset($exceptions[$today->format('Y-m-d')])
            ? $exceptions[$today->format('Y-m-d')]
            : $defaults[$today->format('w')];
    }


    protected function indexExceptions($exceptions)
    {
        $out = array();
        foreach ($exceptions as $exception) {
            $out[$exception->getDay()->format('Y-m-d')] = $exception;
        }

        return $out;
    }
}
