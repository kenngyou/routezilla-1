<?php

namespace Orc\BookingBundle\Schedule;

use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Orc\SaasBundle\Entity\Client;

class CrewEnquirer
{
    const STATUS_AVAILABLE = 'available';
    const STATUS_DISCOURAGED = 'discouraged';
    const STATUS_UNAVAILABLE = 'unavailable';
    const STATUS_PASTDATE = 'pastdate';

    protected $scheduler;
    protected $overrider;

    public function __construct(SchedulerInterface $scheduler, SchedulerInterface $overrideScheduler, EndTimeCalculator $calculator)
    {
        $this->scheduler = $scheduler;
        $this->overrider = $overrideScheduler;
        $this->calculator = $calculator;
    }

    /**
     * Gets the crew's availability status for a given time, assuming context of booking $booking
     * @param    Crew          Crew to check status of
     * @param    DateTime      Slot to check status
     * @param    Booking       Booking we are working with (service, region, etc.)
     * @param    Client        Tenant
     * @return   string        One of the STATUS_ constants
     */
    public function getCrewStatus(Crew $crew, \DateTime $slot, Booking $booking, Client $client)
    {

        $start = clone $slot;
        $end   = clone $slot;

        $start->setTime(0, 0);
        $start->modify('-1 week');

        $end->setTime(0, 0);
        $end->modify('+1 week');

        $this->scheduler->setRange($start, $end);

        $booking = clone $booking;
        if (!$booking->getDateEnd()) {
            $booking->setDateEnd($this->calculator->getEndTime($booking));
        }

        if ($booking->getDateStart() < new DateTime()) {
            return self::STATUS_PASTDATE;
        }

        if (!$this->overrider->isCrewAvailableAt($crew, $slot, $booking, $client)) {
            return self::STATUS_UNAVAILABLE;
        }

        if (!$this->scheduler->isCrewAvailableAt($crew, $slot, $booking, $client)) {
            return self::STATUS_DISCOURAGED;
        }

        return self::STATUS_AVAILABLE;
    }
}
