Feature: Manage Services
  In order for customers to post booknigs
  As an administrator
  I need to be able to manage services
  
  Background:
    Given I'm logged in as a new client
  
  Scenario: Create Service
    Given I follow "Services"
      And I follow "Add Service"
     When I fill in "Name of Service" with "Cleaning"
      And I fill in "Description" with "Cleaning stuff up"
      And I fill in "Time Required" with "60"
      And I press "Add Service"
     Then I should see "Service added successfully"
      And I should see "Cleaning"
      And I should see "Cleaning stuff up"
      
  Scenario: Edit Service
    Given I create a service "Cleaning" described as "Cleaning stuff up" which takes "120" minutes
     When I follow "Services"
      And I follow "Edit Cleaning"
     Then I should see "Edit"
     When I fill in "Name of Service" with "Housecleaning"
      And I press "Save Changes"
     Then I should see "Housecleaning"
  
  
  Scenario: Delete Service
    Given I create a service "TCB" described as "Taking Care of Business" which takes "120" minutes
     When I follow "Services"
      And I follow "Delete TCB"
     Then I should see "Confirm Deletion"
      And I press "Remove"
     Then I should not see "TCB"
