Feature: Scheduling Blackouts
  As a Client
  In order to manage my schedule
  I need to be able to black out days for various crews/regions/services.
  
  Background:
    Given today is "January 1, 2012"
     And I work standard hours
      
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Burnaby   | [[49.258571,-123.0222786],[49.2578988,-122.9817665],[49.2446768,-122.9690636],[49.2379523,-122.9347313],[49.1996058,-122.9357612],[49.1818801,-122.9790199],[49.2014005,-123.0236519]] |
      | Richmond  | [[49.1960163,-123.1331719],[49.1767182,-123.1523979],[49.1762694,-123.1784905],[49.1744738,-123.2100762],[49.1214753,-123.2066429],[49.1106890,-123.1510246],[49.1088910,-123.1153191],[49.1282155,-123.0672539],[49.1443884,-123.0439079],[49.1659440,-122.9766167],[49.1825533,-122.9834831],[49.1991571,-123.0198753],[49.2049896,-123.0651939],[49.1964650,-123.0954064],[49.2005031,-123.1166924]] |
      
     And I offer the following services:
      | Name        | Time   | Fields |
      | Landscaping | 60     |        |
      | Plumbing    | 60     |        |
      | Estimating  | 60     |        |
      | Intense     | 420    |        |
      | Sleeping    | 60     |        |
      
     And I have the following crews:
      | Name        | Regions                              | Services                                 |
      | Landscapers | ["Vancouver", "Burnaby", "Richmond"] | ["Landscaping", "Estimating", "Intense"] |
      | Plumbers    | ["Vancouver", "Burnaby", "Richmond"] | ["Plumbing", "Estimating", "Intense"]    |
      
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  | 
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  | 
          
  Scenario: Can't Book on Blacked out day
    Given I have "['Dec 25 2012']" blacked out Crew "" Region "" Service ""
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Plumbing"
     Then Date "Sunday Dec 25 2012" should have no openings
      And Date "Monday Dec 26 2012" should have openings
     
  Scenario: Can't Book Single-Crew Service on Crew Blackout
    Given I have "['Sep 14 2012']" blacked out Crew "Landscapers" Region "" Service ""
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Landscaping"
     Then Date "Friday Sep 14 2012" should have no openings
      
  Scenario: Can't Book When all Blackout criteria matches
    Given I have "['Dec 27 2012']" blacked out Crew "Landscapers" Region "Vancouver" Service "Estimating"
      And Crew "Plumbers" is booked from "Dec 27 2012 9:00" to "Dec 27 2012 17:00" at "Downtown Vancouver"
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Estimating"
     Then Date "Dec 27 2012" should have no openings
       
  Scenario: Can Book When not all Blackout criteria matches
    Given I have "['Dec 27 2012']" blacked out Crew "Landscapers" Region "Vancouver" Service "Estimating"
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Estimating"
     Then Date "Dec 27 2012" should have openings
    
  Scenario: Can Book on day after Blackout
    Given I have "['January 1 2012', 'July 31 2012']" blacked out Crew "" Region "" Service ""
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Plumbing"
     Then Time "August 1 2012 9:00" should be available
     
  Scenario: With Blackout, allow Querying Twice
    Given I have "['January 1 2012', 'July 31 2012']" blacked out Crew "" Region "" Service ""
     When I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Plumbing"
      And Time "August 1 2012 9:00" should be available
     Then Date "August 1 2012" should have openings
