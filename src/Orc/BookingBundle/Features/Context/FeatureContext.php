<?php

namespace Orc\BookingBundle\Features\Context;

use Orc\BookingBundle\Features\Context\ScheduleContext;
use Orc\SaasBundle\Features\Context\SaasContext;
use Orc\SaasBundle\Features\Context\MinkContext;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Step;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Symfony\Component\HttpKernel\KernelInterface;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class FeatureContext extends BehatContext //MinkContext if you want to test web
                  implements KernelAwareInterface
{
    private $kernel;
    private $parameters;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
        $this->useContext('saas', new SaasContext($parameters));
        $this->useContext('schedule', new ScheduleContext($parameters));
        $this->useContext('mink', new MinkContext());
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /I create a service "([^"]*)" described as "([^"]*)" which takes "([^"]*)" minutes$/
     */
    public function iCreateAService($name, $des, $minutes)
    {
        return array(
            new Step\When('I follow "Services"'),
            new Step\When('I follow "Add Service"'),
            new Step\When('I fill in "Name of Service" with "' . $name . '"'),
            new Step\When('I fill in "Description" with "' . $des . '"'),
            new Step\When('I fill in "Time Required" with "' . $minutes . '"'),
            new Step\When('I press "Add Service"')
        );
    }

    /**
     * @Given /I add the staff "(officeadmin|crewmember)" "([^"]+)"$/
     */
    public function iCreateAStaffMember($type, $name)
    {
        $username = 'a' . md5($name);
        $email = $username . '@site.com';

        return array(
            new Step\When('I follow "Staff"'),
            new Step\When('I follow "Add Staff Member"'),
            new Step\When('I fill in "Name" with "' . $name . '"'),
            new Step\When('I fill in "Type" with "' . $type . '"'),
            new Step\When('I fill in "Email" with "' . $email . '"'),
            new Step\When('I fill in "Password" with "letmein"'),
            new Step\When('I fill in "Re-type" with "letmein"'),
            new Step\When('I press "Save Staff Info"')
        );
    }

    /**
     * @Given /I create a new crew "([^"]+)" with:$/
     */
    public function iCreateANewCrew($name, TableNode $users)
    {
        $steps = array(
            new Step\When('I follow "Crews"'),
            new Step\When('I follow "Create New Crew"'),
            new Step\When('I fill in "Name" with "' . $name . '"')
        );

        foreach ($users->getRows() as $row) {
            $steps[] = new Step\When('I additionally select "' . $row[0] . '" from "Members"');
        }

        return array_merge($steps, array(
            new Step\When('I press "Save New Crew"')
        ));
    }

    /**
     * @BeforeScenario
     * @AfterScenario
     */
    public function cleanup()
    {
        $db = $this->kernel->getContainer()->get('database_connection');
        $db->executeQuery('SET foreign_key_checks = 0');

        $types = array(
            'OrcBookingBundle:Service',
            'OrcBookingBundle:ServiceField',
            'OrcBookingBundle:Worker',
            'OrcBookingBundle:Crew'
        );

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($types as $type) {
            $em->createQuery('DELETE ' . $type)->execute();
        }

        $em->flush();
        $db->executeQuery('SET foreign_key_checks = 1');
    }
}
