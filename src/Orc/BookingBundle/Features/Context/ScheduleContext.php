<?php

namespace Orc\BookingBundle\Features\Context;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\HoursOfOperation;
use Orc\BookingBundle\Entity\Region;
use Orc\BookingBundle\Entity\Boundary;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\ServiceField;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Worker;
use Orc\BookingBundle\Entity\Blackout;
use Orc\BookingBundle\Entity\BlackoutConfiguration;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\SaasBundle\Entity\Client;
use Orc\UserBundle\Entity\User;
use Orc\BookingBundle\Schedule\Util\Date;
use Orc\BookingBundle\Form\Handler\BookingHandler;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Step;
use Behat\Mink\Exception\ElementNotFoundException;

require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';

class ScheduleContext extends BehatContext implements KernelAwareInterface
{
    private $kernel;
    private $parameters;
    private $em;

    protected $client;
    protected $locations;
    protected $booking;
    protected $override;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
        $this->locations = array();
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Transform /^Date "([^"]*)"$/
     * @Transform /^Time "([^"]*)"$/
     */
    public function getDate($date)
    {
        return new DateTime($date);
    }


    /**
     * @Transform /^Location "([^"]*)"$/
     */
    public function getLocation($locationName)
    {
        if (!isset($this->locations[$locationName])) {
            throw new \Exception("$locationName is not a valid Location");
        }

        $location = new Location();
        $location->setLatitude($this->locations[$locationName]['Latitude']);
        $location->setLongitude($this->locations[$locationName]['Longitude']);
        $location->setCity($this->locations[$locationName]['Region']);

        return $location;
    }

    /**
     * @Transform /^Crew "([^"]*)"$/
     */
    public function getCrew($crewName)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('OrcBookingBundle:Crew');

        if (!$crew = $repository->findOneByName($crewName)) {
            throw new \Exception("$crewName is not a Crew");
        }

        return $crew;
    }

    /**
     * @Transform /^Service "([^"]*)"$/
     */
    public function getService($serviceName)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('OrcBookingBundle:Service');

        if (!$service = $repository->findOneByName($serviceName)) {
            throw new \Exception("$serviceName is not a Service");
        }

        return $service;
    }

    /**
     * @Transform /^Region "([^"]*)"$/
     */
    public function getRegion($regionName)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $repository = $em->getRepository('OrcBookingBundle:Region');

        if (!$region = $repository->findOneByName($regionName)) {
            throw new \Exception("$regionName is not a Region");
        }

        return $region;
    }

    /**
     * @Given /^I work standard hours$/
     * @Given /^Client works standard hours$/
     */
    public function iWorkStandardHours()
    {
        $saas = $this->getMainContext()->getSubcontext('saas');
        $saas->iHaveASite('test.orcamax.dev');
        $client = $this->client = $saas->getClient();
        $client->setUser($user = new User());

        $user->setEmail('helloworld@site.com');
        $user->setPassword('meh');

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->setTenant($client);
        $em->setMultiTenantRepositoryClass('Synd\\MultiTenantBundle\\ORM\\Repository\\MultiTenantRepository');

        $em->persist($client);
        $em->persist($user);
        $em->flush();
    }

    /**
     * @Given /^I work the following hours:$/
     * @Given /^Client works the following hours:$/
     */
    public function iWorkTheFollowingHours(TableNode $table)
    {
        $saas = $this->getMainContext()->getSubcontext('saas');
        $saas->iHaveASite('test.orcamax.dev');
        $client = $this->client = $saas->getClient();

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->setTenant($client);
        $em->setMultiTenantRepositoryClass('Synd\\MultiTenantBundle\\ORM\\Repository\\MultiTenantRepository');

        foreach ($client->getHoursOfOperation() as $hours) {
            $client->removeHoursOfOperation($hours);
            $em->remove($hours);
        }

        $em->flush();

        foreach ($table->getHash() as $i => $day) {
            if ($day['Start'] and $day['End']) {
                $start = new DateTime('today ' . $day['Start']);
                $end = new DateTime('today ' . $day['End']);
                $off = false;
            } else {
                $start = new DateTime('today');
                $end = new DateTime('today');
                $off = true;
            }

            $hoursOfOperation = new HoursOfOperation();
            $hoursOfOperation->setDay($i);
            $hoursOfOperation->setStartTime($start);
            $hoursOfOperation->setEndTime($end);
            $hoursOfOperation->setOff($off);

            $client->addHoursOfOperation($hoursOfOperation);
        }

        $em->persist($client);
        $em->flush();
    }

    /**
     * @Given /^I operate in the following regions:$/
     * @Given /^Client operates in the following regions:$/
     */
    public function iOperateInTheFollowingRegions(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($table->getHash() as $regionInfo) {
            $region = new Region();
            $region->setName($regionInfo['Name']);

            foreach (json_decode($regionInfo['Boundaries']) as $order => $coordinates) {
                $location = new Location();
                $location->setLatitude((float)$coordinates[0]);
                $location->setLongitude((float)$coordinates[1]);

                $boundary = new Boundary();
                $boundary->setOrder($order);
                $boundary->setLocation($location);
                $boundary->setRegion($region);

                $region->addBoundarie($boundary);
            }

            $em->persist($region);
        }

        $em->flush();
    }

    /**
     * @Given /^I offer the following services:$/
     * @Given /^Client offers the following services:$/
     */
    public function iOfferTheFollowingServices(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($table->getHash() as $serviceInfo) {
            $service = new Service();
            $service->setName($serviceInfo['Name']);
            $service->setBaseTime($serviceInfo['Time']);
            $service->setDescription('oh hai');

            if (!empty($serviceInfo['Disabled'])) {
                $service->setIsActive(false);
            }

            if ($fields = json_decode($serviceInfo['Fields'])) {
                foreach ($fields as $question => $minutes) {
                    $field = new ServiceField();
                    $field->setType(ServiceField::TYPE_NUMERIC);
                    $field->setQuestion($question);
                    $field->setAddMinutes($minutes);

                    $service->addServiceField($field);
                }
            }

            $em->persist($service);
        }

        $em->flush();
    }

    /**
     * @Given /^I have the crew "([^"]*)" servicing "([^"]*)" with "([^"]*)"$/
     */
    public function iHaveACrew($crewName, $regionNames, $serviceNames)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        if (!$regions = @json_decode($regionNames)) {
            $regions = array($regionNames);
        }

        if (!$services = @json_decode($serviceNames)) {
            $services = array($serviceNames);
        }

        $crew = new Crew();
        $crew->setName($crewName);

        foreach ($regions as $region) {
            $crew->addRegion($this->getRegion($region));
        }
        foreach ($services as $service) {
            $crew->addService($this->getService($service));
        }

        $em->persist($crew);
        $em->flush();
    }

    /**
     * @Given /^I have the following crews:$/
     * @Given /^Client has the following crews:$/
     */
    public function iHaveTheFollowingCrews(TableNode $table)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($table->getHash() as $crewInfo) {
            $crew = new Crew();
            $crew->setName($crewInfo['Name']);

            foreach (json_decode($crewInfo['Regions']) as $regionName) {
                $crew->addRegion($this->getRegion($regionName));
            }
            foreach (json_decode($crewInfo['Services']) as $serviceName) {
                $crew->addService($this->getService($serviceName));
            }

            $em->persist($crew);
        }

        $em->flush();
    }

    /**
     * @Given /^Client has "([^"]*)" blacked out for Crew "([^"]*)" Region "([^"]*)" Service "([^"]*)"$/
     * @Given /^I have "([^"]*)" blacked out Crew "([^"]*)" Region "([^"]*)" Service "([^"]*)"$/
     */
    public function createBlackout($dateJson, $forCrew, $forRegion, $forService)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        $dates = json_decode(str_replace("'", '"', $dateJson));
        if (!isset($dates[1])) {
            $dates[1] = $dates[0];
        }

        $blackout = new Blackout();
        $blackout->setName('whatevs');
        $blackout->setDateStart($this->getDate($dates[0]));
        $blackout->setDateEnd($this->getDate($dates[1]));

        if ($forCrew) {
            $blackout->setCrew($this->getCrew($forCrew));
        }
        if ($forRegion) {
            $blackout->setRegion($this->getRegion($forRegion));
        }
        if ($forService) {
            $blackout->setService($this->getService($forService));
        }

        $em->persist($blackout);
        $em->flush();
    }

    /**
     * @Given /^I have the following blackouts:$/
     * @Given /^Client has the following blackouts:$/
     */
    public function iHaveTheFollowingBlackouts(TableNode $table)
    {
        foreach ($table->getHash() as $blackoutInfo) {
            $this->createBlackout($blackoutInfo['Date'], $blackoutInfo['Crew'], $blackoutInfo['Region'], $blackoutInfo['Service']);
        }
    }

    /**
     * @Given /^using the following named locations:$/
     */
    public function usingTheFollowingNamedLocations(TableNode $table)
    {
        $alternateFormat = '%s, %s, %s, %s';

        foreach ($table->getHash() as $locationInfo) {
            $alternate = sprintf($alternateFormat, $locationInfo['Street'], $locationInfo['City'], $locationInfo['Province'], $locationInfo['Code']);

            $this->locations[$alternate] = $locationInfo;
            $this->locations[$locationInfo['Name']] = $locationInfo;
        }

        file_put_contents(
            sprintf('%s/cache/test/locations.php', $this->kernel->getRootDir()),
            '<?php return ' . var_export($this->locations, true) . ';'
        );
    }


    /**
     * @AfterScenario
     */
    public function removeLocationCache()
    {
        $filename = sprintf('%s/app/cache/test/locations.php', $this->kernel->getRootDir());

        if (file_exists($filename)) {
            unlink($filename);
        }
    }

    /**
     * @Given /^today is "([^"]*)"$/
     */
    public function todayIs($date)
    {
        DateTime::setDefault($date);
    }

    /**
     * @Given /^I am overriding the schedule$/
     */
    public function iAmOverriding()
    {
        $this->override = true;
    }

    /**
     * @Given /^(Crew "([^"]*)") is booked from "([^"]*)" to "([^"]*)" at "([^"]*)"$/
     */
    public function crewIsBooked(Crew $crew, $cn, $start, $end, $location)
    {
        $booking = new Booking();
        $booking->setLocation($location = $this->getLocation($location));
        $booking->setCrew($crew);
        $booking->setRegion($this->getRegion($location->getCity())); // XXX
        $booking->setService($this->getService('Landscaping'));
        $booking->setDateStart(new DateTime($start));
        $booking->setDateEnd(new DateTime($end));
        $booking->setStatus(BookingStatus::STATUS_ASSIGNED);

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        $booking->setDateStart(new DateTime($start));
        $booking->setDateEnd(new DateTime($end));
        $em->persist($booking);
        $em->flush();
    }

    /**
     * @Given /^I reassign the last "([^"]*)" booking to (Crew "([^"]*)")$/
     */
    public function changeLastBookingToCrew($n, Crew $crew)
    {
        $booking = $this->kernel->getContainer()->get('orc_booking.repository.booking')->findLast($n);
        $booking->setCrew($crew);

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();
    }


    /**
     * @Given /^I am booking from (Location "([^"]*)")$/
     */
    public function iAmBookingFrom(Location $location)
    {
        $booking = new Booking();
        $booking->setLocation($location);
        $booking->setRegion($this->getRegion($location->getCity())); // XXX

        $this->booking = $booking;
    }

    /**
     * @Given /^I am requesting (Service "([^"]*)")$/
     */
    public function iAmRequesting(Service $service)
    {
        $this->booking->setService($service);
    }

    /**
     * @Given /^I am requesting (Time "([^"]*)")$/
     */
    public function iAmRequestingTime(\DateTime $date)
    {
        $calculator = $this->kernel->getContainer()->get('orc_booking.booking.endtimecalculator');

        $this->booking->setDateStart($date);
        $this->booking->setDateEnd($calculator->getEndTime($this->booking));
    }

    protected function isTimeAvailable(\DateTime $time)
    {
        $start = clone $time;
        $start->setTime(0, 0);
        $start->modify('-1 day');
        $end = clone $time;
        $end->setTime(0, 0);
        $end->modify('+1 day');

        $scheduler = $this->kernel->getContainer()->get('orc_booking.schedule.scheduler');
        $scheduler->setRange($start, $end);

        $available = $scheduler->getAvailableTimeSlots($this->client, $this->booking);
        $slots = Date::intersect($available, array($time));

        return count($slots) >= 1;
    }

    /**
     * @Then /^no times should be available$/
     */
    public function noTimesShouldBeAvailable()
    {
        $scheduler = $this->kernel->getContainer()->get('orc_booking.schedule.scheduler');
        $scheduler->setRange(new DateTime('today'), new DateTime('+1 year'));
        $available = $scheduler->getAvailableTimeSlots($this->client, $this->booking);
        assertCount(0, $available);
    }

    /**
     * @Then /^(Time "([^"]*)") should be available$/
     */
    public function shouldBeAvailable(DateTime $time)
    {
        assertTrue($this->isTimeAvailable($time));
    }

    /**
     * @Then /^(Time "([^"]*)") should be unavailable$/
     */
    public function shouldBeUnavailable(DateTime $time)
    {
        assertFalse($this->isTimeAvailable($time));
    }

    protected function hasOpenings(\DateTime $day)
    {
        $end = clone $day;
        $end->modify('+1 day');

        $scheduler = $this->kernel->getContainer()->get('orc_booking.schedule.scheduler.day');
        $scheduler->setRange($day, $end);

        return $scheduler->isDayAvailable($this->client, $day, $this->booking);
    }

    /**
     * @Then /^(Date "([^"]*)") should have openings$/
     */
    public function shouldHaveOpenings(DateTime $day)
    {
        assertTrue($this->hasOpenings($day));
    }

    /**
     * @Then /^(Date "([^"]*)") should have no openings$/
     */
    public function shouldHaveNoOpenings(DateTime $day)
    {
        assertFalse($this->hasOpenings($day));
    }

    /**
     * @Then /^(Crew "([^"]*)") should be assigned$/
     */
    public function crewShouldBeAssigned(Crew $crew)
    {
        $scheduler = $this->kernel->getContainer()->get('orc_booking.schedule.scheduler');

        $start = clone $this->booking->getDateStart();
        $start->setTime(0, 0);
        $start->modify('-1 day');

        $end = clone $this->booking->getDateStart();
        $end->setTime(0, 0);
        $end->modify('+1 day');

        $scheduler->setRange($start, $end);

        assertEquals($crew, $scheduler->getAvailableCrew($this->booking, $this->client));
    }

    /**
     * @Then /^(Crew "([^"]*)") should be "([^"]*)"$/
     */
    public function assertCrewStatus(Crew $crew, $cn, $expectedStatus)
    {
        $validStatuses = array(
            'available',
            'discouraged',
            'unavailable'
        );

        if (!in_array($expectedStatus, $validStatuses)) {
            throw new \Exception("'$expectedStatus' is not a valid schedule query status");
        }

        $enquirer = $this->kernel->getContainer()->get('orc_booking.schedule.scheduler.enquirer');
        assertEquals($expectedStatus, $enquirer->getCrewStatus($crew, $this->booking->getDateStart(), $this->booking, $this->client));
    }

    /**
     * @Given /^I\'m a Customer on Client\'s booking site$/
     */
    public function iAmOnClientSite()
    {
        return new Step\Then(sprintf('I am on "http://%s"', $this->client->getSite()->getDomain()));
    }

    /**
     * @Given /^I\'m logged in as an administrator$/
     */
    public function iAmAdminLoggedIn()
    {
        $userManager = $this->kernel->getContainer()->get('fos_user.user_manager');
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        $user = $userManager->createUser();
        $user->setEmail('admin@site.com');
        $user->setPlainPassword('letmein');
        $user->setEnabled(true);
        $user->setClient($this->client);
        $user->addRole('ROLE_CLIENT');
        $user->addRole('ROLE_STAFF');

        $userManager->updateUser($user);

        $this->client->setStatus(Client::STATUS_STEP_5);
        $this->client->setUser($user);

        $em->persist($this->client);
        $em->flush();

        return array(
            new Step\Then(sprintf('I am on "http://%s/dashboard"', $this->client->getSite()->getDomain())),
            new Step\Then(sprintf('I fill in "Email" with "%s"', 'admin@site.com')),
            new Step\Then(sprintf('I fill in "Password" with "%s"', 'letmein')),
            new Step\Then('I press "Login"')
        );
    }

    /**
     * @Given /^I place a booking for (Crew "([^"]*)") at (Time "([^"]*)")$/
     */
    public function iPlaceBooking(Crew $crew, $cn, \DateTime $date)
    {
        return new Step\Then(sprintf(
            'I go to "/dashboard/bookings/create?crew=%d&date=%s"',
            $crew->getId(),
            $date->format('Y-m-d g:ia')
        ));
    }


    /**
     * @Given /^I pick (Date "([^"]*)")$/
     */
    public function iPickDay(DateTime $day)
    {
        $mink = $this->getMainContext()->getSubcontext('mink');

        $matches = null;
        if (preg_match('#dashboard/bookings/(check|check-day)/([0-9]+)#', $mink->getSession()->getCurrentUrl(), $matches)) {
            return new Step\Then(sprintf('I go to "/dashboard/bookings/check-day/%d/%s"', $matches[2], $day->format('Y-m-d')));
        }

        return new Step\Then(sprintf('I go to "/step3/%s"', $day->format('Y-m-d')));
    }

    protected function getLastBooking()
    {
        return $this->kernel->getContainer()->get('orc_booking.repository.booking')->findLast();
    }

    /**
     * @Given /^I edit the last Booking$/
     */
    public function iEditLastBooking()
    {
        $lastBooking = $this->getLastBooking();
        $this->booking = clone $lastBooking;
        $this->booking->setLocation(clone $lastBooking->getLocation());

        return new Step\Then(sprintf(
            'I go to "/dashboard/bookings/edit/%d"',
            $this->getLastBooking()->getId()
        ));
    }

    /**
     * @Then /^I should see the booking marker in another position$/
     */
    public function assertBookingMoved()
    {
        $booking = $this->getLastBooking();
        $newLocation = $booking->getLocation();
        $oldLocation = $this->booking->getLocation();

        assertNotEquals($oldLocation->getLatitude(), $newLocation->getLatitude());
        assertNotEquals($oldLocation->getLongitude(), $newLocation->getLongitude());
    }

    /**
     * @Then /^that Booking should be assigned to (Crew "([^"]*)")$/
     */
    public function assertBookingCrew(Crew $crew)
    {
        assertEquals($crew, $this->getLastBooking()->getCrew());
        $this->assertBookingStatus("assigned");
    }

    /**
     * @Then /^that Booking should be status "([^"]*)"$/
     */
    public function assertBookingStatus($status)
    {
       $status = constant(sprintf('Orc\BookingBundle\Entity\BookingStatus::STATUS_%s', strtoupper($status)));
       $booking = $this->getLastBooking();

       assertEquals($status, $booking->getStatus());

       $noCrewStatuses = array(
           BookingStatus::STATUS_ATTEMPT,
           BookingStatus::STATUS_DRAFT,
           BookingStatus::STATUS_UNASSIGNABLE,
           BookingStatus::STATUS_CANCELLED
       );

       if (in_array($status, $noCrewStatuses)) {
           assertEmpty($booking->getCrew());
       } else {
           assertNotEmpty($booking->getCrew());
       }
    }

    /**
     * @Given /^I pick Time "([^"]*)"$/
     */
    public function iSelectTheTime($label)
    {
        $mink = $this->getMainContext()->getSubcontext('mink');

        $input = $mink->getSession()->getPage()->findField($label);
        if (null === $input) {
            throw new ElementNotFoundException(
                $mink->getSession(), 'form field', 'id|name|label|value', $label
            );
        }

        $value = $input->getAttribute('value');
        $mink->fillField($label, $value);
    }

    protected function getCrewRadius(Crew $crew, \DateTime $day)
    {
        $repository = $this->kernel->getContainer()->get('orc_booking.repository.crewradius');
        $results = $repository->findByCrewWithinRange($crew, $day, $day);
        return reset($results);
    }

    /**
     * @Then /^(Crew "([^"]*)") should be within (Location "([^"]*)") on (Date "([^"]*)")$/
     */
    public function assertCrewWithin(Crew $crew, $cn, Location $location, $ln, \DateTime $date)
    {
        if (!$radius = $this->getCrewRadius($crew, $date)) {
            throw new \Exception('Crew has no radius on that day');
        }

        assertEquals($this->round($location->getLatitude()), $this->round($radius->getLocation()->getLatitude()));
        assertEquals($this->round($location->getLongitude()), $this->round($radius->getLocation()->getLongitude()));
    }

    /**
     * @Then /^(Crew "([^"]*)") should be unrestricted on (Date "([^"]*)")$/
     */
    public function assertCrewUnrestrictedOnDate(Crew $crew, $cn, \DateTime $date)
    {
        if ($crew = $this->getCrewRadius($crew, $date)) {
            throw new \Exception('Crew has a radius that day');
        }
    }

    /**
     * @When /^(Crew "([^"]*)") is moved to (Location "([^"]*)") on (Date "([^"]*)")$/
     */
    public function crewIsMovedToLocationOnDate(Crew $crew, $cn, Location $location, $ln, \DateTime $date)
    {
        if (!$radius = $this->getCrewRadius($crew, $date)) {
            throw new \Exception('Crew has no radius on that day');
        }

        $oldLocation = $radius->getLocation();
        $radius->setLocation(null);
        $radius->setLocation($location);

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->remove($oldLocation);
        $em->persist($location);
        $em->persist($radius);
        $em->flush();
    }


    /**
     * @When /^the last Booking is moved to (Date "([^"]*)")$/
     */
    public function theLastBookingIsMovedTo(\DateTime $time)
    {
        $booking = $this->getLastBooking();
        $booking->setDateStart($time);

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();
     }

    /**
     * @When /^the second last Booking is moved to (Date "([^"]*)")$/
     */
    public function theSecondLastBookingIsMovedTo(\DateTime $time)
    {
        $booking = $this->getLastBooking(2);
        $booking->setDateStart($time);

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();
     }

     /**
      * @When /^I switch to the list view$/
      */
     public function iGoToListView()
     {
         return new Step\Then('I go to "/dashboard/bookings/list"');
     }

     /**
      * @When /^I switch to (Date "([^"]*)")$/
      */
     public function iGoToDayView(\DateTime $day)
     {
         return new Step\Then('I go to "/dashboard/bookings/day/' . $day->format('Y-m-d') . '"');
     }

     /**
      * @Then /^I should be viewing (Date "([^"]*)")$/
      */
     public function assertViewingDate(\DateTime $date)
     {
         return new Step\Then('I should be on "/dashboard/bookings/day/' . $date->format('Y-m-d') . '"');
     }

    /**
     * @AfterScenario
     * @BeforeScenario
     */
    public function cleanup()
    {
        $db = $this->kernel->getContainer()->get('database_connection');
        $db->executeQuery('SET foreign_key_checks = 0');

        $types = array(
            'OrcBookingBundle:Blackout',
            'OrcBookingBundle:BlackoutConfiguration',
            'OrcBookingBundle:Booking',
            'OrcBookingBundle:BookingServiceField',
            'OrcBookingBundle:Crew',
            'OrcBookingBundle:CrewRadius',
            'OrcBookingBundle:Customer',
            'OrcBookingBundle:CustomerBilling',
            'OrcBookingBundle:Worker',
            'OrcBookingBundle:Region',
            'OrcBookingBundle:Service',
            'OrcBookingBundle:ServiceField',
            'OrcBookingBundle:Boundary',
            'OrcBookingBundle:Location',
            'OrcBookingBundle:HoursOfOperation'
        );

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($types as $type) {
            $em->createQuery('DELETE ' . $type)->execute();
        }

        // XXX
        $db->executeQuery("DELETE FROM crew_region");
        $db->executeQuery("DELETE FROM crew_service");

        $em->flush();

        $db->executeQuery('SET foreign_key_checks = 1');
    }

    /**
     * @Given /^I schedule a blackout with:$/
     */
    public function iScheduleABlackoutWith(TableNode $table)
    {
        $options = $table->getRowsHash();
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        $blackout = new BlackoutConfiguration();
        $blackout->setName('whatevs');
        $blackout->setDateStart($this->getDate($options['start']));
        $blackout->setDateEnd($this->getDate($options['end']));
        $blackout->setFrequency($options['frequency']);
        $blackout->setFrequencyMultiplier($options['multiplier']);

        if ($options['daysOfWeek']) {
            $blackout->setDaysOfWeek(explode(',', $options['daysOfWeek']));
        }
        if ($options['daysOfMonth']) {
            $blackout->setDaysOfMonth(explode(',', $options['daysOfMonth']));
        }
        if ($options['months']) {
            $months = array_flip(array('wat', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'));
            $blackout->setMonthsOfYear(array_map(
                function($name) use ($months) {
                    return $months[$name];
                },
                explode(',', $options['months'])
            ));
        }

        if ($options['crew']) {
            $blackout->setCrew($this->getCrew($options['crew']));
        }
        if ($options['region']) {
            $blackout->setRegion($this->getRegion($options['region']));
        }
        if ($options['service']) {
            $blackout->setService($this->getService($options['service']));
        }

        $em->persist($blackout);
        $em->flush();

        $generator = new \Orc\BookingBundle\Booking\BlackoutGenerator;
        foreach ($generator->getBlackouts($blackout) as $new) {
            $new->setClient($this->client);
            $em->persist($new);
        }
        $em->flush();
    }

     protected function round($number, $precision = 6)
     {
        $numberpart = explode('.', $number);
        $numberpart[1] = substr_replace($numberpart[1], '.', $precision, 0);

        if($numberpart[0] >= 0) {
            $numberpart[1] = ceil($numberpart[1]);
        } else {
            $numberpart[1] = floor($numberpart[1]);
        }

        $ceil_number = array($numberpart[0],$numberpart[1]);
        return implode('.', $ceil_number);
     }
}
