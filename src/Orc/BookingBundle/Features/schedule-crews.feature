Feature: Scheduling Crews
  As a Client
  In order to take in bookings
  I need to make sure bookings are assigned to open crews
  
  Background:
    Given today is "January 1, 2012"
     And I work standard hours
      
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Burnaby   | [[49.258571,-123.0222786],[49.2578988,-122.9817665],[49.2446768,-122.9690636],[49.2379523,-122.9347313],[49.1996058,-122.9357612],[49.1818801,-122.9790199],[49.2014005,-123.0236519]] |
      | Richmond  | [[49.1960163,-123.1331719],[49.1767182,-123.1523979],[49.1762694,-123.1784905],[49.1744738,-123.2100762],[49.1214753,-123.2066429],[49.1106890,-123.1510246],[49.1088910,-123.1153191],[49.1282155,-123.0672539],[49.1443884,-123.0439079],[49.1659440,-122.9766167],[49.1825533,-122.9834831],[49.1991571,-123.0198753],[49.2049896,-123.0651939],[49.1964650,-123.0954064],[49.2005031,-123.1166924]] |
      
     And I offer the following services:
      | Name        | Time   | Fields           |
      | Landscaping | 60     | {"Rooms": 30 }   |
      | Plumbing    | 60     |                  |
      | Estimating  | 60     |                  |
      | Intense     | 240    |                  |
      | Sleeping    | 60     |                  |
      
     And I have the following crews:
      | Name        | Regions                              | Services                                 |
      | Landscapers | ["Vancouver", "Burnaby", "Richmond"] | ["Landscaping", "Estimating", "Intense"] |
      | Plumbers    | ["Vancouver", "Burnaby", "Richmond"] | ["Plumbing", "Estimating", "Intense"]    |
      | Nogooders   | ["Vancouver", "Burnaby", "Richmond"] | []                                       |
      
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  | 
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  | 
      | MEXICO       | In Mexico          | 1                  | 1                    |                    |           |         |          |         |
      | Burnaby      | Metrotown Burnaby  | 49.22592184601026  | -123.00267219543457  | 4700 Kingsway      | Burnaby   | V5H 4J5 | BC       | Canada  |
      | Burnaby      | Ironclad Games     | 49.22494439229017  | -122.98904120922089  | 6539 Royal Oak Ave | Burnaby   | V5H 2G1 | BC       | Canada  |
      | Burnaby      | Swangard Stadium   | 49.230833          | -123.021389          | 3883 Imperial St   | Burnaby   | V5S 3R2 | BC       | Canada  |
      | Richmond     | Richmond Centre    | 49.16529016314306  | -123.13601016998291  | 6760 Number 3 Rd   | Richmond  | V6Y 0A2 | BC       | Canada  |
      | Richmond     | Richmond Olympic   | 49.174634          | -123.151567          | 6111 River Rd      | Richmond  | V7C 0A2 | BC       | Canada  |
      | Richmond     | Steveston Dojo     | 49.125879729463485 | -123.17734569311142  | 4281 Moncton St    | Richmond  | V7E 3A8 | BC       | Canada  |

      
   Scenario: Can't Book when Crew is already Booked
     Given Crew "Landscapers" is booked from "August 1 2012 9:00" to "August 1 2012 10:00" at "Downtown Vancouver"
       And I am booking from Location "Downtown Vancouver"
       And I am requesting Service "Landscaping"
      Then Time "August 1 2012 9:00" should be unavailable
       But Time "August 1 2012 10:00" should be available
       
   Scenario: Can Book a redundant Crew/Service
     Given Crew "Landscapers" is booked from "August 1 2012 9:00" to "August 1 2012 10:00" at "Downtown Vancouver"
       And I am booking from Location "Downtown Vancouver"
       And I am requesting Service "Estimating"
      Then Time "August 1 2012 9:00" should be available
       And Time "August 1 2012 10:00" should be available
       
    Scenario: Can't Book when both Crews are already Booked
     Given Crew "Landscapers" is booked from "August 1 2012 9:00" to "August 1 2012 10:00" at "Downtown Vancouver"
       And Crew "Plumbers" is booked from "August 1 2012 9:00" to "August 1 2012 10:00" at "Downtown Vancouver"
       And I am booking from Location "Downtown Vancouver"
       And I am requesting Service "Estimating"
      Then Time "August 1 2012 9:00" should be unavailable
      
    Scenario: Use Redundant Crew on Long Booking
      Given Crew "Landscapers" is booked from "August 1 2012 10:00" to "August 1 2012 18:00" at "Downtown Vancouver"
        And I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Intense"
        And I am requesting Time "August 1 2012 9:00"
       Then Crew "Plumbers" should be assigned
       
    Scenario: Can't Book multi-hour Booking without all slots being open
      Given Crew "Landscapers" is booked from "August 1 2012 11:00" to "August 1 2012 13:00" at "Downtown Vancouver"
        And Crew "Plumbers" is booked from "August 1 2012 9:00" to "August 1 2012 17:00" at "Downtown Vancouver"
       When I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Intense"
      Then Time "August 1 2012 9:00" should be unavailable
      
    Scenario: Can't Book Service without Crews
      Given I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Sleeping"
       Then no times should be available
