Feature: Scheduling: Crew Radius Filtering
  As a Client
  In order to to cut back on driving costs
  I need to to limit Crews booking radius for each day
  
  Background:
    Given today is "January 1, 2012"
      And I work standard hours
      
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Burnaby   | [[49.258571,-123.0222786],[49.2578988,-122.9817665],[49.2446768,-122.9690636],[49.2379523,-122.9347313],[49.1996058,-122.9357612],[49.1818801,-122.9790199],[49.2014005,-123.0236519]] |
      
     And I offer the following services:
      | Name        | Time   | Fields   |
      | Landscaping | 60     |          |
      
     And I have the following crews:
      | Name        | Regions                   | Services        |
      | Landscapers | ["Vancouver", "Burnaby" ] | ["Landscaping"] |
      
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  | 
      | Burnaby      | Ironclad Games     | 49.22494439229017  | -122.98904120922089  | 6539 Royal Oak Ave | Burnaby   | V5H 2G1 | BC       | Canada  |


    Scenario: Can't Book out of Crew Radius
     Given Crew "Landscapers" is booked from "August 1 9:00 2012" to "August 1 10:00 2012" at "Downtown Vancouver"
       And I am booking from Location "Ironclad Games"
       And I am requesting Service "Landscaping"
      Then Date "August 1" should have no openings
      
    Scenario: Can Book within Crew Radius
      Given Crew "Landscapers" is booked from "August 1 9:00 2012" to "August 1 10:00 2012" at "Downtown Vancouver"
        And I am booking from Location "Whitecaps Office"
        And I am requesting Service "Landscaping"
       Then Date "August 1 2012" should have openings
       
    Scenario: Can't Book outside of moved Crew Radius
      Given Crew "Landscapers" is booked from "August 1 9:00 2012" to "August 1 10:00 2012" at "Downtown Vancouver"
        And Crew "Landscapers" is moved to Location "Ironclad Games" on Date "August 1 2012"
       When I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Landscaping"
       Then Date "August 1 2012" should have no openings
       
    Scenario: Can't Book out of Crew Radius Bug Fix
      Given I have the crew "Redundant" servicing "Vancouver" with "Landscaping"
       And Crew "Landscapers" is booked from "August 1 2012 9:00" to "August 1 2012 10:00" at "Downtown Vancouver"
       And Crew "Landscapers" is moved to Location "Ironclad Games" on Date "August 1 2012"
      When I am booking from Location "Downtown Vancouver"
       And I am requesting Service "Landscaping"
       And I am requesting Time "August 1 9:00 2012"
      Then Crew "Redundant" should be assigned
