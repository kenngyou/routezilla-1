Feature: Radius Management
  As a Crew
  In order to prevent extraneous traveling
  I should be restricted to a geographical area
  
  Background:
    Given today is "January 1, 2012"
     And I work standard hours
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
     And I offer the following services:
      | Name        | Time   | Fields | Disabled |
      | Estimating  | 60     |        |          |
      | Plumbing    | 60     |        |          |
      | Landscaping | 60     |        |          |
     And I have the following crews:
      | Name        | Regions       | Services                      |
      | Landscapers | ["Vancouver"] | ["Estimating", "Landscaping"] |
      | Plumbers    | ["Vancouver"] | ["Estimating", "Plumbing"]    |
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  | 
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  | 
      | Richmond     | Steveston Dojo     | 49.125879729463485 | -123.17734569311142  | 4281 Moncton St    | Richmond  | V7E 3A8 | BC       | Canada  |
    And I'm logged in as an administrator
    And I go to "/dashboard/bookings/day"
    
    
  ######## BOOKING RADIUS ######## 
    
    
  Scenario: Create Radius with first Booking
    Given Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
     Then Crew "Landscapers" should be within Location "Downtown Vancouver" on Date "August 1"

  Scenario: Don't create Radius with subsequent Booking
    Given Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
     When Crew "Landscapers" is booked from "August 1 10:00" to "August 1 11:00" at "Whitecaps Office"
     Then Crew "Landscapers" should be within Location "Downtown Vancouver" on Date "August 1"
  
  Scenario: Move Booking Radius when Booking is rescheduled as another first
    Given Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
     When the last Booking is moved to Date "August 2 9:00"
     Then Crew "Landscapers" should be unrestricted on Date "August 1"
      And Crew "Landscapers" should be within Location "Downtown Vancouver" on Date "August 2"
     
  Scenario: Remove Booking Radius when Booking is rescheduled in another Radius
    Given Crew "Landscapers" is booked from "August 2 9:00" to "August 2 10:00" at "Whitecaps Office"
      And Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
     When the last Booking is moved to Date "August 2 10:00"
     Then Crew "Landscapers" should be unrestricted on Date "August 1"
      And Crew "Landscapers" should be within Location "Whitecaps Office" on Date "August 2"
    
    
  ######## STANDALONE RADIUS ######## 
    
    
  Scenario: Move Radius away from Booking
    Given Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
     When Crew "Landscapers" is moved to Location "Whitecaps Office" on Date "August 1"
     Then Crew "Landscapers" should be within Location "Whitecaps Office" on Date "August 1"
     
  Scenario: Reset overridden Radius when Booking is rescheduled
     Given Crew "Landscapers" is booked from "August 1 9:00" to "August 1 10:00" at "Downtown Vancouver"
       And Crew "Landscapers" is moved to Location "Whitecaps Office" on Date "August 1"
      When the last Booking is moved to Date "August 2 9:00"
      Then Crew "Landscapers" should be within Location "Downtown Vancouver" on Date "August 2"


  ######## BUG FIXING ######## 
  
  Scenario: ROUT90 - No radius when a booking is reassigned
    Given I follow "Create New Booking"
      And I select "Landscaping" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
      And I press the "Check Availability" button
      And I should see "Check Availability"
      And I pick Date "September 12 2012"
      And I press the "Between 9:00am and 11:00am" option
      And I press "Choose Time"
      And I should see a "success" message containing "Booking updated successfully"
      And that Booking should be status "assigned"
      And I place a booking for Crew "Landscapers" at Time "September 12 2012 9:00"
      And I select "Landscaping" from "Service"
      And I fill in the following:
        | Street             | 4281 Moncton St |
        | City               | Richmond        |
        | ZIP or Postal Code | V7E 3A8         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Someone Else    |
        | Email              | someonelse@s.ca |
        | Phone              | 2531313222      |
     When I press the "Create Booking" button
     And Crew "Landscapers" should be within Location "Downtown Vancouver" on Date "Sep 12 2012"
     When I reassign the last "2" booking to Crew "Plumbers"
     Then Crew "Landscapers" should be within Location "Steveston Dojo" on Date "Sep 12 2012"
