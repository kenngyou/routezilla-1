Feature: Admin Booking Management
  In order to add bookings as efficiently as possible
  As the business owner
  I need the recommendation system to work around me

  Background:
    Given today is "January 1, 2012"
     And I work standard hours

     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Burnaby   | [[49.258571,-123.0222786],[49.2578988,-122.9817665],[49.2446768,-122.9690636],[49.2379523,-122.9347313],[49.1996058,-122.9357612],[49.1818801,-122.9790199],[49.2014005,-123.0236519]] |

     And I offer the following services:
      | Name        | Time   | Fields | Disabled |
      | Estimating  | 60     |        |          |

     And I have the following crews:
      | Name        | Regions       | Services       |
      | Landscapers | ["Vancouver"] | ["Estimating"] |

    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country |
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  |
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  |
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  |
      | MEXICO       | In Mexico          | 1                  | 1                    |                    |           |         |          |         |
      | Burnaby      | Metrotown Burnaby  | 49.22592184601026  | -123.00267219543457  | 4700 Kingsway      | Burnaby   | V5H 4J5 | BC       | Canada  |
      | Burnaby      | Ironclad Games     | 49.22494439229017  | -122.98904120922089  | 6539 Royal Oak Ave | Burnaby   | V5H 2G1 | BC       | Canada  |
      | Burnaby      | Swangard Stadium   | 49.230833          | -123.021389          | 3883 Imperial St   | Burnaby   | V5S 3R2 | BC       | Canada  |
      | Richmond     | Steveston Dojo     | 49.125879729463485 | -123.17734569311142  | 4281 Moncton St    | Richmond  | V7E 3A8 | BC       | Canada  |

    And I'm logged in as an administrator
    And I go to "/dashboard/bookings/day"




  ######## Editing Real Bookings ########


  Scenario: Minor data correction (same coordinates)
      Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
       When I select "Estimating" from "Service"
        And I fill in the following:
          | Street             | 541 Howe St     |
          | City               | Vancouver       |
          | ZIP or Postal Code | V6C 1K6         |
          | Province           | BC              |
          | Country            | Canada          |
          | Customer Name      | Mr. Smithers    |
          | Email              | mr@smithers.com |
          | Phone              | 2501112222      |
        And I press the "Create Booking" button
       When I edit the last Booking
        And I fill in "Customer Name" with "Smith"
        And I press the "Save Changes" button
       Then I should see a "success" message containing "Booking updated successfully"


  Scenario: Moderate address correction (new coordinates)
      Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
       When I select "Estimating" from "Service"
        And I fill in the following:
          | Street             | 541 Howe St     |
          | City               | Vancouver       |
          | ZIP or Postal Code | V6C 1K6         |
          | Province           | BC              |
          | Country            | Canada          |
          | Customer Name      | Mr. Smithers    |
          | Email              | mr@smithers.com |
          | Phone              | 2501112222      |
        And I press the "Create Booking" button
       When I edit the last Booking
        And I fill in the following:
          | Street             | 375 Water St    |
          | ZIP or Postal Code | V6B 5C6         |
        And I press the "Save Changes" button
       Then I should see a "success" message containing "Booking updated successfully"
        And I should see the booking marker in another position

  Scenario: Address change out of service area triggers junction
      Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
       When I select "Estimating" from "Service"
        And I fill in the following:
          | Street             | 541 Howe St     |
          | City               | Vancouver       |
          | ZIP or Postal Code | V6C 1K6         |
          | Province           | BC              |
          | Country            | Canada          |
          | Customer Name      | Mr. Smithers    |
          | Email              | mr@smithers.com |
          | Phone              | 2501112222      |
        And I press the "Create Booking" button
       When I edit the last Booking
        And I fill in the following:
          | Street             | 4281 Moncton St |
          | City               | Richmond        |
          | ZIP or Postal Code | V7E 3A8         |
        And I press the "Save Changes" button
       Then I should see "The requested location is not covered by our service area"


  Scenario: Address change discourages current crew assignment
      Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
       When I select "Estimating" from "Service"
        And I fill in the following:
          | Street             | 541 Howe St     |
          | City               | Vancouver       |
          | ZIP or Postal Code | V6C 1K6         |
          | Province           | BC              |
          | Country            | Canada          |
          | Customer Name      | Mr. Smithers    |
          | Email              | mr@smithers.com |
          | Phone              | 2501112222      |
        And I press the "Create Booking" button
       When I edit the last Booking
        And I fill in the following:
          | Street             | 4700 Kingsway   |
          | City               | Burnaby         |
          | ZIP or Postal Code | V5H 4J5         |
        And I press the "Save Changes" button
       Then I should see "Your booking placement is discouraged"


  Scenario: Cancelling a live booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
      And I press the "Create Booking" button
     When I edit the last Booking
      And I follow "Cancel Booking"
      And I press "Cancel"
     Then I should see a "success" message containing "Booking cancelled successfully"


  Scenario: Rescheduling a live booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
      And I press the "Create Booking" button
     When I edit the last Booking
      And I press the "Reschedule" button
     Then I should see "Thursday, 13 September"
     When I pick Date "August 1 2012"
      And I press the "Between 9:00am and 11:00am" option
      And I press "Choose Time"
     Then I should see a "success" message containing "Booking updated successfully"
      And that Booking should be status "assigned"



  ######## Editing Drafts / Cancelled ########




  Scenario: Scheduling a draft
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
      And I press the "Save as Draft" button
     When I edit the last Booking
      And I press the "Check Availability" button
     Then I should see "Thursday, 13 September"
     When I pick Date "August 1 2012"
      And I press the "Between 9:00am and 11:00am" option
      And I press "Choose Time"
     Then I should see a "success" message containing "Booking updated successfully"
      And that Booking should be status "assigned"

  Scenario: Deleting a draft
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
      And I press the "Save as Draft" button
     When I edit the last Booking
      And I follow "Delete Booking"
      And I press "Remove"
     Then I should see a "success" message containing "Booking removed successfully"

  Scenario: Drafting a cancelled Booking



  ######## Editing Attempts / Requests ########



  Scenario: Drafting an Attempt

  Scenario: Scheduling an Attempt

  Scenario: Scheduling an Attempt without all required information

  Scenario: Scheduling an attempt with out of Region
