Feature: Public Bookings
  In order to book an appointment
  As the Customer
  I need to be able to book an Appointment online

  Background:
    Given today is "January 1, 2012"
     And Client works standard hours

     And Client operates in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Burnaby   | [[49.2585710,-123.0222786],[49.2361590,-122.9443443],[49.2014005,-123.0236519],[49.2578988,-122.9817665],[49.1996058,-122.9426277],[49.2446768,-122.9690636],[49.1818801,-122.9790199]] |
      | Richmond  | [[49.1960163,-123.1331719],[49.1767182,-123.1523979],[49.1762694,-123.1784905],[49.1744738,-123.2100762],[49.1214753,-123.2066429],[49.1106890,-123.1510246],[49.1088910,-123.1153191],[49.1282155,-123.0672539],[49.1443884,-123.0439079],[49.1659440,-122.9766167],[49.1825533,-122.9834831],[49.1991571,-123.0198753],[49.2049896,-123.0651939],[49.1964650,-123.0954064],[49.2005031,-123.1166924]] |

     And Client offers the following services:
      | Name        | Time   | Fields           |
      | Landscaping | 60     | {"Rooms": 30 }   |
      | Plumbing    | 60     |                  |
      | Estimating  | 60     |                  |
      | Unmanaged   | 60     |                  |

     And Client has the following crews:
      | Name        | Regions                              | Services                      |
      | Landscapers | ["Vancouver", "Burnaby", "Richmond"] | ["Landscaping", "Estimating"] |
      | Plumbers    | ["Vancouver", "Burnaby", "Richmond"] | ["Plumbing", "Estimating"]    |

    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country |
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  |
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  |
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  |
      | MEXICO       | In Mexico          | 1                  | 1                    |                    |           |         |          |         |
      | Burnaby      | Metrotown Burnaby  | 49.22592184601026  | -123.00267219543457  | 4700 Kingsway      | Burnaby   | V5H 4J5 | BC       | Canada  |
      | Burnaby      | Ironclad Games     | 49.22494439229017  | -122.98904120922089  | 6539 Royal Oak Ave | Burnaby   | V5H 2G1 | BC       | Canada  |
      | Burnaby      | Swangard Stadium   | 49.230833          | -123.021389          | 3883 Imperial St   | Burnaby   | V5S 3R2 | BC       | Canada  |
      | Richmond     | Richmond Centre    | 49.16529016314306  | -123.13601016998291  | 6760 Number 3 Rd   | Richmond  | V6Y 0A2 | BC       | Canada  |
      | Richmond     | Richmond Olympic   | 49.174634          | -123.151567          | 6111 River Rd      | Richmond  | V7C 0A2 | BC       | Canada  |
      | Richmond     | Steveston Dojo     | 49.125879729463485 | -123.17734569311142  | 4281 Moncton St    | Richmond  | V7E 3A8 | BC       | Canada  |

  Scenario: Booking out of Service Area
    Given I'm a Customer on Client's booking site
     When I fill in "Address" with "In Mexico"
      And I press "Search"
     Then I should see "Outside of Service Area"

   Scenario: Booking an untrackable Location
     Given I'm a Customer on Client's booking site
      When I fill in "Address" with "Not Even Google Can Find This"
       And I press "Search"
      Then I should see "cannot find your location"

    Scenario: Changing Location during Booking
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
       Then I should be on "/step1-confirm"
        But follow "1. Location"
       Then I should be on "/"
        And fill in "Address" with "Mozilla Downtown"
        And press "Search"
       Then I should see "Please confirm your location"

    Scenario: Changing Date
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
        And press "Next Step"
        And I press the "Landscaping" option
        And press "Next Step"
        And I pick Date "October 1 2012"
        But go to "/step3"
        And I pick Date "August 2 2012"
       Then I should see "2 August 2012"

    Scenario: Changing Time
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
        And press "Next Step"
        And I press the "Landscaping" option
        And press "Next Step"
        And I pick Date "October 1"
        And I pick Time "Between 9:00am and 11:00am"
        And press "Choose Time"
       But go to "/step3/2012-10-01"
       When I pick Time "Between 1:00pm and 3:00pm"
        And press "Choose Time"
       Then I should see "Contact Information"

    Scenario: Standard Booking
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
        And press "Next Step"
       Then I should see "Location set successfully"
       When I press the "Estimating" option
        And fill in "Comments (optional)" with "watch out for my dog!"
        And press "Next Step"
       Then I should see "Click on the day you would like service"
       When I pick Date "October 1 2012"
       Then I should see "Choose a time for service"
       When I pick Time "Between 9:00am and 11:00am"
        And press "Choose Time"
       Then I should see "Contact Information"
       When I fill in the following:
       | Your Name | Mr. Developer Man    |
       | Email     | customer@company.com |
       | Phone     | 1800-FAKE-NUMBER     |
        And press "Complete Booking"
       Then I should see "Booking Confirmation"
        And I should see "Booked on Monday Oct 01"
        And I should see "Start time between 9:00am and 11:00am"


    Scenario: Invalid Email Address
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
        And press "Next Step"
       When I press the "Estimating" option
        And fill in "Comments (optional)" with "watch out for my dog!"
        And press "Next Step"
       When I pick Date "October 1"
       When I pick Time "Between 9:00am and 11:00am"
        And press "Choose Time"
       When I fill in the following:
       | Your Name | Mr. Developer Man    |
       | Email     | notrealobviously     |
       | Phone     | 1800-FAKE-NUMBER     |
        And press "Complete Booking"
       Then I should see "not a valid email"

    Scenario: Attempting to go back after Booking
      Given I'm a Customer on Client's booking site
       When I fill in "Address" with "Downtown Vancouver"
        And press "Search"
        And press "Next Step"
        And I press the "Estimating" option
        And press "Next Step"
        And I pick Date "October 1"
        And I pick Time "Between 9:00am and 11:00am"
        And press "Choose Time"
       Then I fill in the following:
       | Your Name | Mr. Developer Man    |
       | Email     | customer@company.com |
       | Phone     | 1800-FAKE-NUMBER     |
        And press "Complete Booking"
       Then I should see "Booking Confirmation"
       When I go to "/step3/"
       Then I should see "You must select a service first"
        And the "Address" field should contain ""
