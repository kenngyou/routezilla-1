Feature: Admin Booking Management
  In order to add bookings as efficiently as possible
  As the business owner
  I need the recommendation system to work around me

  Background:
    Given today is "January 1, 2012"
     And I work standard hours

     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      | Richmond  | [[49.1960163,-123.1331719],[49.1767182,-123.1523979],[49.1762694,-123.1784905],[49.1744738,-123.2100762],[49.1214753,-123.2066429],[49.1106890,-123.1510246],[49.1088910,-123.1153191],[49.1282155,-123.0672539],[49.1443884,-123.0439079],[49.1659440,-122.9766167],[49.1825533,-122.9834831],[49.1991571,-123.0198753],[49.2049896,-123.0651939],[49.1964650,-123.0954064],[49.2005031,-123.1166924]] |

     And I offer the following services:
      | Name        | Time   | Fields | Disabled |
      | Estimating  | 60     |        |          |

     And I have the following crews:
      | Name        | Regions       | Services       |
      | Landscapers | ["Vancouver"] | ["Estimating"] |

    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country |
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  |
      | Vancouver    | Whitecaps Office   | 49.284848          | -123.11105           | 375 Water St       | Vancouver | V6B 5C6 | BC       | Canada  |
      | Vancouver    | Mozilla Downtown   | 49.2824481056452   | -123.1092095375061   | 163 W Hastings St  | Vancouver | V6C 1K6 | BC       | Canada  |
      | MEXICO       | In Mexico          | 1                  | 1                    |                    |           |         |          |         |
      | Burnaby      | Metrotown Burnaby  | 49.22592184601026  | -123.00267219543457  | 4700 Kingsway      | Burnaby   | V5H 4J5 | BC       | Canada  |
      | Burnaby      | Ironclad Games     | 49.22494439229017  | -122.98904120922089  | 6539 Royal Oak Ave | Burnaby   | V5H 2G1 | BC       | Canada  |
      | Burnaby      | Swangard Stadium   | 49.230833          | -123.021389          | 3883 Imperial St   | Burnaby   | V5S 3R2 | BC       | Canada  |
      | Richmond     | Richmond Centre    | 49.16529016314306  | -123.13601016998291  | 6760 Number 3 Rd   | Richmond  | V6Y 0A2 | BC       | Canada  |
      | Richmond     | Richmond Olympic   | 49.174634          | -123.151567          | 6111 River Rd      | Richmond  | V7C 0A2 | BC       | Canada  |
      | Richmond     | Steveston Dojo     | 49.125879729463485 | -123.17734569311142  | 4281 Moncton St    | Richmond  | V7E 3A8 | BC       | Canada  |
    And I'm logged in as an administrator
    And I go to "/dashboard/bookings/day"




  ######## CREATE: Standard ########



  Scenario: Create normal Booking over the phone
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Check Availability" button
     Then I should see "Check Availability"
     When I pick Date "September 12 2012"
     When I press the "Between 9:00am and 11:00am" option
      And I press "Choose Time"
     Then I should see a "success" message containing "Booking updated successfully"
      And that Booking should be status "assigned"

  Scenario: Draft a Booking created over the phone
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Save as Draft" button
     Then I should see a "success" message containing "Booking added successfully as a draft"
      And that Booking should be status "draft"



  ######## CREATE: Error Conditions #######



  Scenario: Create Booking with bad address
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | ??????????????  |
        | City               | ??????????????  |
        | ZIP or Postal Code | ??????????????  |
        | Province           | ??????????????  |
        | Country            | ??????????????  |
        | Customer Name      | ??????????????  |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Check Availability" button
     Then I should see a "error" message containing "We couldn't find that address"

  Scenario: Create Booking without required fields
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
     When I press the "Check Availability" button
     Then I should see "This value should not be blank"



  ######## CREATE: STANDARD PROCESS OUT OF REGION ########



  Scenario: Create Booking out of Region
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 4700 Kingsway   |
        | City               | Burnaby         |
        | ZIP or Postal Code | V5H 4J5         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
    When I press the "Check Availability" button
    Then I should see a "error" message containing "Booking could not be completed"
    Then I should see "Not covered"

  Scenario: Cancel a Booking created out of Region
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 4700 Kingsway   |
        | City               | Burnaby         |
        | ZIP or Postal Code | V5H 4J5         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
    When I press the "Check Availability" button
     And I follow "Cancel Booking"
     And I press "Cancel"
    Then I should see a "success" message containing "Booking cancelled successfully"
     And that Booking should be status "cancelled"

  Scenario: Schedule a Booking created out of Region as Unassigned
    Given I follow "Create New Booking"
      And I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 4700 Kingsway   |
        | City               | Burnaby         |
        | ZIP or Postal Code | V5H 4J5         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
    When I press the "Check Availability" button
     And I follow "Schedule as Unassigned"
     Then I should see "Check Availability"
     When I pick Date "September 15 2012"
      And I press the "Between 2:00am and 4:00am" option
      And I press "Choose Time"
     Then I should see a "success" message containing "Booking updated successfully"
      And that Booking should be status "unassignable"



  ######## PLACE: Create Booking for Crew at Time #### ####



  Scenario: Place Booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 9:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Create Booking" button
     Then I should see a "success" message containing "Booking added successfully"
      And that Booking should be assigned to Crew "Landscapers"

  Scenario: Draft a placed Booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 9:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 9:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Save as Draft" button
     Then I should see a "success" message containing "Booking added successfully as a draft"
      And that Booking should be status "draft"

  Scenario: Discourage a placed Booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 2:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 2:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Create Booking" button
     Then I should see "Your booking placement is discouraged"
      And that Booking should be assigned to Crew "Landscapers"

  Scenario: Cancel a discouraged placed booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 2:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 2:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Create Booking" button
      And I follow "Cancel Booking"
      And I press "Cancel"
     Then I should see a "success" message containing "Booking cancelled successfully"

  Scenario: Reschedule a discouraged placed booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 2:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 2:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Create Booking" button
      And I follow "Check Availability"
      And I pick Date "September 13 2012"
      And I press the "Between 9:00am and 11:00am" option
      And I press "Choose Time"
     Then I should see a "success" message containing "Booking updated successfully"


  Scenario: Unassign on a discouraged placed booking
    Given I place a booking for Crew "Landscapers" at Time "September 13 2012 2:00"
     Then I should see "assigned to Landscapers on Thu, Sep 13th at 2:00am"
     When I select "Estimating" from "Service"
      And I fill in the following:
        | Street             | 541 Howe St     |
        | City               | Vancouver       |
        | ZIP or Postal Code | V6C 1K6         |
        | Province           | BC              |
        | Country            | Canada          |
        | Customer Name      | Mr. Smithers    |
        | Email              | mr@smithers.com |
        | Phone              | 2501112222      |
     When I press the "Create Booking" button
      And I follow "Unassign"
     Then I should see a "success" message containing "Booking unassigned successfully"


