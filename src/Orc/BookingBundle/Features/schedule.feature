Feature: Schedule
  As a Client
  In order to take bookings online
  I need to control which days are available for automated bookings
  
  Background:
    Given today is "January 1, 2012"
     And I work the following hours:
      | Day       | Start | End   |
      | Sunday    |       |       |
      | Monday    | 9:00  | 17:00 |
      | Tuesday   | 9:00  | 17:00 |
      | Wednesday | 8:00  | 18:00 |
      | Thursday  | 9:00  | 17:00 |
      | Friday    | 10:00 | 16:00 |
      | Saturday  |       |       |
      
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      
     And I offer the following services:
      | Name        | Time   | Fields | Disabled |
      | Estimating  | 60     |        |          |
      | Not Offered | 60     |        | yes      |
      
     And I have the following crews:
      | Name        | Regions       | Services                      |
      | Landscapers | ["Vancouver"] | ["Estimating", "Not Offered"] |
      
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 


  Scenario: Can book during default hours
    Given I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Estimating"
     Then Date "next Monday" should have openings
      And Time "next Monday 9am" should be available
      
  Scenario: Can't Book outside of default hours
    Given I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Estimating"
     Then Date "next Sunday" should have no openings
      And Time "next Monday 5am" should be unavailable
      
  Scenario: Can't Book when service is not available
    Given I am booking from Location "Downtown Vancouver"
      And I am requesting Service "Not Offered"
     Then no times should be available
