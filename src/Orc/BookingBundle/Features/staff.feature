Feature: Staff Management
  In order to run my business
  As the business owner
  I should be able to create, edit and delete staff members and crews
  
  Background:
    Given I'm logged in as a new client
    
    Scenario: Create Office Admin
      Given I follow "Staff"
        And I should see "Staff Members"
        And I follow "Add Staff Member"
       When I fill in the following:
       | Name         | John Smith    |
       | Type         | officeadmin   | 
       | Email        | john@site.com |
       | Password     | letmein       |
       | Re-type | letmein       | 
        And I press "Save Staff Info"
       Then I should see "Staff member added successfully."
        And I should see "John Smith"
       
    Scenario: Create Worker
      Given I follow "Staff"
        And I should see "Staff Members"
        And I follow "Add Staff Member"
       When I fill in the following:
       | Name         | Jimbo Man     |
       | Type         | crewmember    | 
       | Email        | jimb@site.com |
       | Password     | letmeink?     |
       | Re-type | letmeink?     | 
        And I press "Save Staff Info"
       Then I should see "Staff member added successfully."
        And I should see "Jimbo"
        
    Scenario: Edit Staff Member
      Given I add the staff "crewmember" "Billy"
       When I follow "Edit Billy"
        And I fill in "Name" with "Frank"
        And I press "Save Changes"
       Then should see "Staff member updated successfully"
        And I should see "Frank"
        And I should not see "Billy"
        
    Scenario: Remove Staff Member
      Given I add the staff "crewmember" "Billy"
       When I follow "Remove Billy"
        And I press "Remove"
       Then should see "Worker removed successfully"
        And I should not see "Billy"
