Feature: Admin Schedule Overrides
  As a Client
  In order to post special cases or optimize the system
  I need to be able to override scheduling decisions
  
  Background:
    Given today is "January 1, 2012"
     And I work standard hours
      
     And I operate in the following regions:
      | Name      | Boundaries               |
      | Vancouver | [[49.2587950,-123.0226219],[49.1991571,-123.0246819],[49.2063354,-123.0638207],[49.2000545,-123.1084526],[49.2000545,-123.1366051],[49.2126156,-123.1681908],[49.2103728,-123.2080162],[49.2229312,-123.2505882],[49.2852279,-123.2725609],[49.2959762,-123.2457817],[49.3026927,-123.1338585],[49.2937372,-123.0294884],[49.2820925,-123.0315483],[49.2556580,-123.0226219]] |
      
     And I offer the following services:
      | Name        | Time   | Fields           |
      | Estimating  | 60     |                  |
      | Intense     | 240    |                  |
      | Landscaping | 60     |                  |
      
     And I have the following crews:
      | Name        | Regions       | Services                  |
      | Landscapers | ["Vancouver"] | ["Estimating", "Intense"] |
      | Plumbers    | ["Vancouver"] | ["Estimating", "Intense"] |
      
    And using the following named locations:
      | Region       | Name               | Latitude           | Longitude            | Street             | City      | Code    | Province | Country | 
      | Vancouver    | Downtown Vancouver | 49.28493959708459  | -123.11656951904297  | 541 Howe St        | Vancouver | V6C 1K6 | BC       | Canada  | 

      
    Scenario: Discourage Booking outside of standard hours
      Given I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Estimating"
        And I am requesting Time "August 1 2:00"
       Then Crew "Plumbers" should be "discouraged"
      
    Scenario: Allow Booking within standard hours
      Given I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Estimating"
        And I am requesting Time "August 1 9:00 2012"
       Then Crew "Plumbers" should be "available"
      
    Scenario: Deny Booking when Crew is already booked
      Given Crew "Landscapers" is booked from "August 1 5:00" to "August 1 10:00" at "Downtown Vancouver"
        And I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Intense"
        And I am requesting Time "August 1 9:00"
       Then Crew "Landscapers" should be "unavailable"
       
    Scenario: Deny Booking which passes midnight
      Given I am booking from Location "Downtown Vancouver"
        And I am requesting Service "Intense"
        And I am requesting Time "August 1 23:00"
       Then Crew "Plumbers" should be "unavailable"
