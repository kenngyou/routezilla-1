<?php

namespace Orc\BookingBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class FlashInvalidDate
{
    protected $flashes;

    public function __construct(FlashBagInterface $flashes)
    {
        $this->flashes = $flashes;
    }

    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();
        $request = $event->getRequest();

        if (strpos($exception->getMessage(), 'DateTime::__construct') !== 0) {
            return;
        }

        $this->flashes->add('error', 'Could not understand date');

        if (!$referer = $request->server->get('HTTP_REFERER')) {
            if (strpos($request->getRequestUri(), '/dashboard') === 0) {
                $referer = '/dashboard';
            } else {
                $referer = '/';
            }
        }

        $event->setResponse(new RedirectResponse($referer));
    }
}
