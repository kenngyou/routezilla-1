<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Doctrine\ORM\EntityManager;
use Doctrine\Common\EventArgs;

class UpdateBookingStatus
{
    public function preUpdate(EventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if (!$entity instanceof Booking) {
            return;
        }

        if ($entity->getStatus() == BookingStatus::STATUS_CANCELLED or $entity->getStatus() == BookingStatus::STATUS_DRAFT) {
            $entity->setCrew(null);
            $this->recomputeChangeSet($em, $entity);
        }

        if ($entity->getStatus() == BookingStatus::STATUS_ASSIGNED and !$entity->getCrew()) {
            $entity->setStatus(BookingStatus::STATUS_UNASSIGNABLE);
            $this->recomputeChangeSet($em, $entity);
        }

        if ($entity->getStatus() == BookingStatus::STATUS_UNASSIGNABLE and $entity->getCrew()) {
            $entity->setStatus(BookingStatus::STATUS_ASSIGNED);
            $this->recomputeChangeSet($em, $entity);
        }
    }

    protected function recomputeChangeSet(EntityManager $em, Booking $entity)
    {
        $uow = $em->getUnitOfWork();
        $meta = $em->getClassMetadata(get_class($entity));
        $uow->recomputeSingleEntityChangeSet($meta, $entity);
    }
}
