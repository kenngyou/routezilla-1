<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\Worker;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

class AssignWorkerTokens implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate
        );
    }

    /**
     * New Bookings: set the default dateEnd from the service information
     * @param    LifecycleEventArgs
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Worker and !$entity->getToken()) {
            $entity->setToken($this->generateToken($entity));
       }
    }

    /**
     * Updated Bookings: set the default dateEnd ONLY IF the dateEnd has not been modified manually
     * @param    PreUpdateEventArgs
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Worker and !$entity->getToken()) {
            $entity->setToken($this->generateToken($entity));
            $uow = $em->getUnitOfWork();
            $meta = $em->getClassMetadata(get_class($entity));
            $uow->recomputeSingleEntityChangeSet($meta, $entity);
        }
    }

    /**
     * Generate a md5 hash for use as a token
     * @param    Worker
     * @return   string        Random token
     */
    protected function generateToken(Worker $worker)
    {
        return md5(uniqid() . $worker->getUser()->getPassword());
    }
}
