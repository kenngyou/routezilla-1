<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\HoursOfOperation;
use Orc\SaasBundle\Entity\Client;
use Doctrine\Common\EventArgs;

class CreateDefaultClientData
{
    protected $website;
    protected $phone;
    protected $arrivalWindow;
    protected $email;
    protected $brandingColor;
    protected $startOfDay;
    protected $endOfDay;
    protected $daysOff;
    protected $defaultRadius;

    public function __construct($website, $phone, $email, $arrivalWindow, $brandingColor, $startOfDay, $endOfDay, array $daysOff, $defaultRadius)
    {
        $this->website = $website;
        $this->phone = $phone;
        $this->email = $email;
        $this->arrivalWindow = $arrivalWindow;
        $this->brandingColor = $brandingColor;
        $this->startOfDay = $startOfDay;
        $this->endOfDay = $endOfDay;
        $this->daysOff = $daysOff;
        $this->defaultRadius = $defaultRadius;
    }

    public function postPersist(EventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Client) {

            for ($i = 0; $i < 7; $i++) {
                $day = new HoursOfOperation();
                $day->setDay($i);
                $day->setStartTime(new DateTime($this->startOfDay));
                $day->setEndTime(new DateTime($this->endOfDay));
                $day->setOff(in_array($i, $this->daysOff));

                $entity->addHoursOfOperation($day);
            }

            $entity->setWebsite($this->website);
            $entity->setPhone($this->phone);
            $entity->setArrivalWindow($this->arrivalWindow);
            $entity->setBrandingColor($this->brandingColor);
            $entity->setDefaultRadius($this->defaultRadius);

            $em->persist($entity);
            $em->flush();
        }
    }
}
