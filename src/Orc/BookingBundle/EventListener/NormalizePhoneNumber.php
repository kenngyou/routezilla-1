<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Booking\Customer;
use Orc\BookingBundle\Booking\CustomerBilling;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

class NormalizePhoneNumber implements EventSubscriber
{
    public function getSubscribedEvents()
    {
        return array(
            Events::preUpdate,
            Events::prePersist
        );
    }

    /**
     * Capture booking and previous/new crew  from UOW
     * @param    PreUpdateEventArgs
     */
    public function preUpdate(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if (!$entity instanceof Customer and !$entity instanceof CustomerBilling) {
            return;
        }

        $entity->setPhoneCanonical($this->strip($entity->getPhone()));

    }

    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if (!$entity instanceof Customer and !$entity instanceof CustomerBilling) {
            return;
        }

        $entity->setPhoneCanonical($this->strip($entity->getPhone()));
    }

    protected function strip($number)
    {
        return preg_replace('/(^\d)/', '', $number);
    }
}
