<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\Region;
use Doctrine\Common\EventArgs;

class AssignCrewsToNewRegions
{
    public function prePersist(EventArgs $event)
    {
        // disabled for now request by client
        return;

        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Region) {
            $crewRepository = $em->getRepository('OrcBookingBundle:Crew');
            foreach ($crewRepository->findAll() as $crew) {
                $crew->addRegion($entity);
                $em->persist($crew);
            }
        }
    }
}
