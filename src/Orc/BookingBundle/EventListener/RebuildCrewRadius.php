<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Booking\RadiusUpdater;
use Orc\BookingBundle\Entity\Booking;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;

class RebuildCrewRadius implements EventSubscriber
{
    protected $radiusUpdater;
    protected $step;
    protected $old;
    protected $new;
    protected $booking;

    public function __construct(RadiusUpdater $radiusUpdater)
    {
        $this->step = 0;
        $this->radiusUpdater = $radiusUpdater;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::preUpdate,
            Events::postUpdate
        );
    }

    /**
     * Capture booking and previous/new crew  from UOW
     * @param    PreUpdateEventArgs
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        if (!$event->getEntity() instanceof Booking or $this->step) {
            return;
        }

        $booking = $event->getEntity();

        $this->step = 1;

        $this->old = $event->hasChangedField('crew') ? $event->getOldValue('crew') : $booking->getCrew();
        $this->new = $booking->getCrew();
        $this->booking = $booking;
    }

    /**
     * Update Crew Radius if needed
     * @param    EventArgs
     */
    public function postUpdate(EventArgs $event)
    {
        if (!$this->booking) {
            return;
        }

        if ($this->step != 1) {
            return;
        }

        $this->step = 2;

        $newCrew = $this->new;
        $oldCrew = $this->old;
        $booking = $this->booking;

        $this->radiusUpdater->setEntityManager($event->getEntityManager());
        $this->radiusUpdater->updateRadius($booking, $oldCrew, $newCrew);

        $this->step = 0;
    }
}
