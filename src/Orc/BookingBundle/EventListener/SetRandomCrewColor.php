<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\Crew;
use Doctrine\Common\EventArgs;

class SetRandomCrewColor
{
    public function prePersist(EventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Crew) {
            $colorRepository = $em->getRepository('OrcBookingBundle:Color');

            if ($color = $colorRepository->findNextUnused()) {
                $entity->setColor($color);
            }
        }
    }
}
