<?php

namespace Orc\BookingBundle\EventListener;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\EventSubscriber;

class SetEndTimeFromService implements EventSubscriber
{
    protected $calculator;

    public function __construct(EndTimeCalculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate
        );
    }

    /**
     * New Bookings: set the default dateEnd from the service information
     * @param    LifecycleEventArgs
     */
    public function prePersist(LifecycleEventArgs $event)
    {
        $entity = $event->getEntity();
        $em = $event->getEntityManager();

        if ($entity instanceof Booking and $entity->getDateStart()) {
            $entity->setDateEnd($this->calculator->getEndTime($entity));
        }
    }

    /**
     * Updated Bookings: set the default dateEnd ONLY IF the dateEnd has not been modified manually
     * @param    PreUpdateEventArgs
     */
    public function preUpdate(PreUpdateEventArgs $event)
    {
        $booking = $event->getEntity();
        $em = $event->getEntityManager();

        if (!$booking instanceof Booking) {
            return;
        }

        $originals = $booking->getOriginalDates();

        if ($booking->getDateStart() != $originals['start'] and $booking->getDateEnd() == $originals['end']) {
            $booking->setDateEnd($this->calculator->getEndTime($booking));
            $uow = $em->getUnitOfWork();
            $meta = $em->getClassMetadata(get_class($booking));
            $uow->recomputeSingleEntityChangeSet($meta, $booking);
        }
    }
}
