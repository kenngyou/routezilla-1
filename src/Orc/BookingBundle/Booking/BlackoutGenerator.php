<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\BlackoutConfiguration;

class BlackoutGenerator
{
    protected $dailyCounter;
    protected $startingWeek;
    protected $lastWeek;
    protected $weekNumber;
    protected $lastMonth;
    protected $lastMonthCheck;
    protected $monthNumber;
    protected $weeks;
    protected $lastYear;
    protected $yearNumber;

    /**
     * Generates dates from a configuration
     *
     * @param BlackoutConfiguration $configuration
     * @return DateTime[]
     */
    public function getDates(BlackoutConfiguration $configuration)
    {
        $this->dailyCounter = 0;
        $this->lastYear = null;
        $this->startingWeek = null;
        $this->weekNumber = null;
        $this->lastMonth = null;
        $this->weekNumber = null;
        $this->yearNumber = null;
        $this->weeks = null;

        for (
            $dates = array(),
            $date = clone $configuration->getDateStart();
            $date <= $configuration->getDateEnd();
            $date->modify('+1 day')
        ) {
            if ($this->matchesFilters($date, $configuration)) {
                $dates[] = clone $date;
            }
        }

        return $dates;
    }

    /**
     * Filters out dates that don't match the configuration
     *
     * @param DateTime $date
     * @param BlackoutConfiguration $configuration
     * @return boolean true if matches
     */
    protected function matchesFilters(\DateTime $date, BlackoutConfiguration $configuration)
    {
        $multiplier = $configuration->getFrequencyMultiplier();

        if ($configuration->getFrequency() == 'daily') {
            return ++$this->dailyCounter % $multiplier == 0;
        }

        if ($configuration->getFrequency() == 'weekly') {
            if (!$this->startingWeek) {
                $this->startingWeek = $date->format('W');
                $this->lastWeek = $this->startingWeek;
            }
            if ($this->lastWeek != $date->format('W')) {
                $this->weekNumber++;
                $this->lastWeek = $date->format('W');
            }
            if ($this->weekNumber % $multiplier) {
                return false;
            }

            return in_array($date->format('w'), $configuration->getDaysOfWeek());
        }

        if ($configuration->getFrequency() == 'monthly' or $configuration->getFrequency() == 'yearly') {
            // yearly repeat check
            if ($configuration->getFrequency() == 'yearly') {
                if ($this->lastYear != $date->format('Y')) {
                    $this->lastYear = $date->format('Y');
                    $this->yearNumber++;
                }
                if ($multiplier != 1 and $this->yearNumber % $multiplier != 0) {
                    return false;
                }
                if (!in_array($date->format('n'), $configuration->getMonthsOfYear())) {
                    return false;
                }
            }

            // monthly repeat check
            if ($configuration->getFrequency() == 'monthly') {
                if ($this->lastMonth != $date->format('mY')) {
                    $this->lastMonth = $date->format('mY');
                    $this->monthNumber++;
                }
                if ($multiplier != 1 and $this->monthNumber % $multiplier != 0) {
                    return false;
                }
            }


            // days of month
            if ($configuration->getDaysOfMonth()) {
                return in_array($date->format('j'), $configuration->getDaysOfMonth());
            }

            // day on every week
            if (!$configuration->getNth()) {
                return in_array($date->format('w'), $configuration->getDaysOfWeek());
            }

            $nths = array(1 => 'first', 2 => 'second', 3 => 'third', 4 => 'fourth', 5 => 'fifth');
            $daysOfWeek = array('sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday');
            $months = array('', 'january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december');

            $expected = array();
            foreach ($configuration->getDaysOfWeek() as $weekDay) {
                $expected[] = new \DateTime(sprintf(
                    '%s %s of %s %d',
                    $nths[$configuration->getNth()],
                    $daysOfWeek[$weekDay],
                    $months[$date->format('n')],
                    $date->format('Y')
                ));
            }

            return in_array(
                $date->format('Y-m-d'),
                array_map(function(\DateTime $date) { return $date->format('Y-m-d'); }, $expected)
            );
        }

        throw new \Exception('wip');
    }

    /**
     * Converts each generated dateto a blackout
     *
     * @param BlackoutConfiguration $configuration
     * @param Blackout[]
     */
    public function getBlackouts(BlackoutConfiguration $configuration)
    {
        return array_map(
            function(\DateTime $date) use ($configuration) {
                return $configuration->toBlackout($date);
            },
            $this->getDates($configuration)
        );
    }
}
