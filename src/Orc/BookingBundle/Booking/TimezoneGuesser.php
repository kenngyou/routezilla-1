<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\Region;

class TimezoneGuesser
{
    public function getTimezone(Region $region)
    {
        try {
            $boundaries = $region->getBoundaries();
            return $this->queryGoogle(
                $boundaries[0]->getLocation()->getLatitude(),
                $boundaries[0]->getLocation()->getLongitude()
            );
        } catch (\Exception $e) { }

        return 'America/Vancouver';
    }

    protected function queryGoogle($lat, $lng)
    {
        $response = file_get_contents(sprintf(
            'https://maps.googleapis.com/maps/api/timezone/json?location=%s,%s&timestamp=%d&sensor=false',
            $lat,
            $lng,
            time()
        ));

        $info = json_decode($response, true);
        return $info['timeZoneId'];
    }
}
