<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Repository\BookingRepository;

class Reminders
{
    protected $repository;
    protected $reminderHour;

    public function __construct(BookingRepository $repository, $reminderHour)
    {
        $this->repository = $repository;
        $this->reminderHour = $reminderHour;
    }

    /**
     * Finds bookings scheduled for tomorrow, in client time
     * (only if it's time to send them out)
     *
     * @return Booking[]
     */
    public function findRemindable()
    {
        $remindable = array();
        foreach ($this->repository->findAllUpcoming(3) as $booking) {
            if (!$this->isCorrectTime($booking) or !$this->doesStartTomorrow($booking)) {
                continue;
            }

            $remindable[] = $booking;
        }

        return $remindable;
    }

    /**
     * Checks that the booking's local time matches
     * the time that we send out reminders
     *
     * @param Booking $booking
     * @return boolean true if match
     */
    protected function isCorrectTime(Booking $booking)
    {
        $now = new DateTime;
        $currentOffset = $now->format('Z');

        $clientTimezone = new \DateTimeZone($booking->getClient()->getTimezone());
        $clientTime = new DateTime('now', $clientTimezone);

        $timezoneOffset = $clientTime->format('Z') - $currentOffset;

        $clientTime = new DateTime('now');
        $clientTime->modify(sprintf('+%d seconds', $timezoneOffset));
        return $clientTime->format('G') == $this->reminderHour;
    }

    /**
     * Checks to see if the booking actually starts tomorrow
     * in client time
     *
     * @param Booking $booking
     * @return boolean true if it does
     */
    protected function doesStartTomorrow(Booking $booking)
    {
        $now = new DateTime;
        $currentOffset = $now->format('Z');

        $clientTimezone = new \DateTimeZone($booking->getClient()->getTimezone());
        $clientTime = new DateTime('now', $clientTimezone);

        $timezoneOffset = $clientTime->format('Z') - $currentOffset;

        $testBookingTime = clone $booking->getDateStart();
        $testBookingTime->modify(sprintf('+%d seconds', $timezoneOffset));

        $start = new DateTime('tomorrow', new \DateTimeZone($booking->getClient()->getTimezone()));
        $start->setTime(0, 0, 0);
        $end = new DateTime('tomorrow' , new \DateTimeZone($booking->getClient()->getTimezone()));
        $end->setTime(0, 0, 0);
        $end->modify('+1 days');

        return $testBookingTime >= $start && $testBookingTime < $end;
    }
}
