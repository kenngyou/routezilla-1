<?php

namespace Orc\BookingBundle\Booking;

/**
 * Solve the "Point in Polygon" problem using ray casting.
 * @see    http://en.wikipedia.org/wiki/Point_in_polygon
 */
class PointInPolygon
{
    /**
     * Check to see if $testPoint falls within $polygon
     * @param    array        lat,lng
     * @param    array        lat,lng coordinates
     * @return   boolean      true if point is within polygon
     */
    public function inPolygon(array $testPoint, array $polygon)
    {
        $inPolygon = false;

        foreach ($polygon as $i => $a) {
            if ($this->rayCrossesSegment($testPoint, $a, $this->getNextPoint($polygon, $i))) {
                $inPolygon = !$inPolygon;
            }
        }

        return $inPolygon;
    }

    /**
     * Gets the next point from the Polygon (connect end to beginning)
     * @param    array        Polygon (points)
     * @param    integer      Current index
     * @return   array        Next point
     */
    protected function getNextPoint($polygon, $index)
    {
        $j = $index + 1;
        if (!isset($polygon[$j])) {
            $j = 0;
        }

        return $polygon[$j];
    }

    /**
     * Check to see if a ray (a -> b) crosses a point
     * @param    array        Point
     * @param    array        Line Segment A
     * @param    array        Line Segment B
     * @return   boolean      true if point crosses line
     */
    protected function rayCrossesSegment($point, $a, $b)
    {
        list($py, $px) = $point;
        list($ay, $ax) = $a;
        list($by, $bx) = $b;

        // A must be below B
        if ($ay > $by) {
            list($ay, $ax, $by, $bx) = array($by, $bx, $ay, $ax);
        }

        if ($py == $ay or $py == $by) {
            $py += 0.00000001;
        }


        if (($py > $by or $py < $ay) or $px > max($ax, $bx)) {
            return false;
        }

        if ($px < min($ax, $bx)) {
            return true;
        }

        $red = ($ax != $bx) ? (($by - $ay) / ($bx - $ax)) : INF;
        $blue = ($ax != $px) ? (($py - $ay) / ($px - $ax)) : INF;

        return $blue >= $red;
    }
}
