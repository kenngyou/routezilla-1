<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\Location;

class DistanceCalculator
{
    const EARTH_RADIUS_KM = 6371.797;
    const EARTH_RADIUS_MILES = 3963.1676;

    /**
     * Calculates the distance between two points using the Haversine forumula
     * @param    Location A
     * @param    Location B
     * @param    float        Units (self::EARTH_RADIUS_*)
     * @return   float        Distance in selected units
     */
    public function getDistance(Location $from, Location $to, $r = self::EARTH_RADIUS_KM)
    {
        return $r * $this->c($from, $to, $this->a($from, $to));;
    }

    protected function a($from, $to)
    {
        $diffLatitude = $from->getLatitude() - $to->getLatitude();
        $diffLongitude = $from->getLongitude() - $to->getLongitude();

        return
           pow(sin(deg2rad($diffLatitude) / 2), 2) +
            pow(sin(deg2rad($diffLongitude) / 2), 2)
                * cos(deg2rad($to->getLatitude())) * cos(deg2rad($from->getLatitude()))
        ;
    }

    protected function c($from, $to, $a)
    {
        return 2 * atan2(sqrt($a), sqrt(1 - $a));
    }
}
