<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\ServiceField;

class EndTimeCalculator
{
    protected $interval;

    public function __construct($interval)
    {
        $this->interval = $interval;
    }

    /**
     * Calculates the end time for a booking
     *
     * @param Booking
     * @param DateTime optionally the original date start pre-modification
     */
    public function getEndTime(Booking $booking, \DateTime $originalStartDate = null)
    {
        if ($originalStartDate) {
            $duration = $booking->getDateEnd()->diff($originalStartDate, true);
            $end = clone $booking->getDateStart();
            $end->add($duration);

            return $end;
        }

        $minutes = $this->getTimeRequired($booking);

        $dateTime = clone $booking->getDateStart();
        $dateTime->modify(sprintf('+%d minutes', $minutes));

        return $dateTime;
    }

    /**
     * Calculates the time required to satisfy a booking
     */
    public function getTimeRequired(Booking $booking)
    {
        $minutes = $booking->getService()->getBaseTime();

        foreach ($booking->getService()->getFields() as $id => $field) {
            if ($field->getType() == ServiceField::TYPE_FREE) {
                continue;
            }

            if ($response = $booking->getFields()->get($id)) {
                $answer = max(0, $response->getAnswer());
                $minutes += $answer * $field->getAddMinutes();
            }

        }

        if ($extra = $minutes % $this->interval) {
            $minutes += $this->interval - $extra;
        }

        return $minutes;
    }
}
