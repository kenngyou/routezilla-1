<?php

namespace Orc\BookingBundle\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\CrewRadius;
use Doctrine\ORM\EntityManager;

class RadiusUpdater
{
    protected $em;
    protected $bookingRepository;
    protected $radiusRepository;

    /**
     * Bring the entity manager and required repositories into scope
     * @param    \Doctrine\ORM\EntityManager
     */
    public function setEntityManager(EntityManager $em)
    {
        $this->em = $em;
        $this->bookingRepository = $em->getRepository('OrcBookingBundle:Booking');
        $this->radiusRepository = $em->getRepository('OrcBookingBundle:CrewRadius');
    }

    /**
     * Update the Booking's radius
     * @param    Booking
     * @param    Crew|null
     * @param    Crew|null
     */
    public function updateRadius(Booking $booking, Crew $oldCrew = null, Crew $newCrew = null)
    {
        // Handle Old Radius
        if ($booking->getRadius()) {
            $oldDay = clone $booking->getRadius()->getDay();

            if ($newCrew) {
                $firstOnOld = $this->findFirstBooking($oldCrew, $oldDay);

                if ($booking != $firstOnOld or $oldDay->format('Y-m-d') != $booking->getDateStart()->format('Y-m-d')) {
                    $this->em->remove($booking->getRadius());
                    $booking->setRadius(null);
                    $this->em->persist($booking);

                    // Recreate with next best
                    if ($firstOnOld and $booking != $firstOnOld) {
                        $this->createRadius($oldCrew, clone $oldDay, $firstOnOld);
                    }
                }

            } else {
                $this->em->remove($booking->getRadius());
                $booking->setRadius(null);
                $this->em->persist($booking);
            }
        }

        $this->em->flush();

        // Handle New Crew Radius
        if ($newCrew) {
            $newDay = clone $booking->getDateStart();
            $newDay->setTime(0, 0);

            $crewRadius = $this->radiusRepository->findOneBy(array('crew' => $newCrew, 'day' => $newDay));
            $firstOnNew = $this->findFirstBooking($newCrew, $newDay);

            // New Crew Has Radius
            if ($crewRadius) {
                // Is Changing
                if ($crewRadius->getBooking() != $firstOnNew) {
                    $crewRadius->getBooking()->setRadius(null);
                    $this->em->remove($crewRadius);
                    $this->em->persist($crewRadius->getBooking());

                    if ($firstOnNew) {
                        $this->createRadius($newCrew, $booking->getDateStart(), $firstOnNew);
                    }
                }
            } elseif ($firstOnNew) {
                $this->createRadius($newCrew, $booking->getDateStart(), $firstOnNew);
            }
        }

        $this->em->flush();
    }

    private function createRadius(Crew $crew, \DateTime $day, Booking $booking)
    {
        $day = clone $day;
        $day->setTime(0, 0);

        $crewRadius = new CrewRadius();
        $crewRadius->setCrew($crew);
        $crewRadius->setDay($day);
        $crewRadius->setRadius($crew->getClient()->getDefaultRadius());
        $crewRadius->setLocation($this->cloneLocation($booking->getLocation()));
        $crewRadius->setBooking($booking);

        $this->em->persist($crewRadius);
    }

    private function cloneLocation(Location $location)
    {
        $newLocation = new Location();
        $newLocation->setStreet($location->getStreet());
        $newLocation->setCity($location->getCity());
        $newLocation->setProvince($location->getProvince());
        $newLocation->setCode($location->getCode());
        $newLocation->setCountry($location->getCountry());
        $newLocation->setLatitude($location->getLatitude());
        $newLocation->setLongitude($location->getLongitude());

        return $newLocation;
    }


    private function findFirstBooking(Crew $crew, \DateTime $day)
    {
        $day = clone $day;
        $day->setTime(0, 0);
        return $this->bookingRepository->findOriginatingByCrewOn($crew, $day);
    }
}
