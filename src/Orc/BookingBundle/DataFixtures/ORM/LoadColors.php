<?php

namespace Orc\BookingBundle\DataFixtures\ORM;

use Orc\BookingBundle\Entity\Color;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadColors implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $colorCodes = array(
            '#007FFF', // Aqua
            '#58E302', // Spring Green
            '#9C1010', // Dark Red
            '#E8A617', // Canteloupe
            '#0ACC92', // Cyan
            '#1F635F', // Teal
            '#B502AF', // Pink
            '#6C12B5', // Violet
            '#6666FF', // Orchid
            '#800040', // Maroon
            '#9C1010', // Strawberry
            '#008040', // Moss
            '#006699', // Sea Foam
            '#999933'  // Gold
        );

        foreach ($colorCodes as $colorCode) {
            $color = new Color();
            $color->setColor($colorCode);

            $manager->persist($color);
        }

        $manager->flush();
    }
}
