<?php

namespace Orc\BookingBundle;

final class BookingEvents
{
    const SCHEDULE_CHANGE = 'schedule.change';
}
