<?php

namespace Orc\BookingBundle\Mailer;

use Swift_Mailer;
use Swift_Message;
use Orc\BookingBundle\Entity\Booking;
use Symfony\Component\Templating\EngineInterface;

abstract class AbstractMailer
{
    protected $mailer;
    protected $templating;
    protected $from;
    protected $replyTo;

    const SENDTO_CLIENT = 'client';
    const SENDTO_CUSTOMER = 'customer';

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
    }

    /**
     * Sends mail about the Booking
     * Various mails are subclassed
     *
     * @param    Booking
     * @return   boolean        Mail status
     */
    public function send(Booking $booking)
    {
        $message = Swift_Message::newInstance();

        if ($this->getRecipient() == self::SENDTO_CLIENT) {
            $message
                ->setTo($booking->getClient()->getEmail())
                ->setFrom($this->from)
            ;

        } elseif ($this->getRecipient() == self::SENDTO_CUSTOMER) {
            $message
                ->setTo(array(
                    $booking->getCustomer()->getEmail() => $booking->getCustomer()->getName()
                ))
                ->setFrom(array(
                    key($this->from) => $booking->getClient()->getName()
                ))
                ->setReplyTo(array(
                    $booking->getClient()->getEmail()
                ))
            ;
        } else {
            throw new \BadMethodCallException("Mailer recipient must either be client or customer");
        }

        return $this->mailer->send($this->getContents($booking, $message));
    }

    /**
     * Get the recipient of the mail.
     * @return    string        SENDTO_CLIENT | SENDTO_CUSTOMER
     */
    abstract protected function getRecipient();

    /**
     * Format the mail based on the type of message
     * @param    Booking
     * @param    Swift_Message    Configured message
     * @return   Swift_Message    Configured and formatted message
     */
    abstract protected function getContents(Booking $booking, Swift_Message $message);
}
