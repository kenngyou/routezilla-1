<?php

namespace Orc\BookingBundle\Mailer;

use Swift_Message;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Mailer\AbstractMailer;

class ConfirmationMailer extends AbstractMailer
{
    protected function getRecipient()
    {
        return self::SENDTO_CUSTOMER;
    }

    protected function getContents(Booking $booking, Swift_Message $message)
    {
        return $message
            ->setSubject('Booking Confirmation from ' . $booking->getClient()->getName())
            ->setBody($this->templating->render('OrcBookingBundle:Email:confirmation.txt.twig', array(
                'booking'  => $booking,
                'client'   => $booking->getClient(),
                'customer' => $booking->getCustomer()
            )), 'text/html')
        ;
    }
}
