<?php

namespace Orc\BookingBundle\Mailer;

use Swift_Message;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Mailer\AbstractMailer;

class NotificationMailer extends AbstractMailer
{
    protected function getRecipient()
    {
        return self::SENDTO_CLIENT;
    }

    protected function getContents(Booking $booking, Swift_Message $message)
    {
        if ($booking->getStatus() == BookingStatus::STATUS_REQUESTED) {
            $subjectFormat = 'Callback notification from %s';
            $template = 'OrcBookingBundle:Email:notification-callback.txt.twig';
        } else {
            $subjectFormat = 'Booking notification from %s';
            $template = 'OrcBookingBundle:Email:notification.txt.twig';
        }

        return $message
            ->setSubject(sprintf($subjectFormat, $booking->getCustomer()->getName()))
            ->setBody($this->templating->render($template, array(
                'booking'  => $booking,
                'client'   => $booking->getClient(),
                'customer' => $booking->getCustomer()
            )))
        ;
    }
}
