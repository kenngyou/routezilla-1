<?php

namespace Orc\BookingBundle\Mailer;

use Swift_Message;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Mailer\AbstractMailer;

class CancellationMailer extends AbstractMailer
{
    protected function getRecipient()
    {
        return self::SENDTO_CUSTOMER;
    }

    protected function getContents(Booking $booking, Swift_Message $message)
    {
        return $message
            ->setSubject('Cancellation Notification from ' . $booking->getClient()->getName())
            ->setBody($this->templating->render('OrcBookingBundle:Email:cancellation.txt.twig', array(
                'booking'  => $booking,
                'client'   => $booking->getClient(),
                'customer' => $booking->getCustomer()
            )))
        ;
    }
}
