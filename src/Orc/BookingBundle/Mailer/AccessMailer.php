<?php

namespace Orc\BookingBundle\Mailer;

use Swift_Mailer;
use Swift_Message;
use Orc\BookingBundle\Entity\Worker;
use Symfony\Component\Templating\EngineInterface;

class AccessMailer
{
    protected $mailer;
    protected $templating;
    protected $from;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
    }

    /**
     * Email the Worker with the calendar invitation
     *
     * @param    Worker
     * @return   boolean        Mail status
     */
    public function send(Worker $worker)
    {
        $message = Swift_Message::newInstance()
            ->setTo(array(
                $worker->getUser()->getEmail() => sprintf('%s of %s', $worker->getName(), $worker->getClient()->getName())
            ))
            ->setFrom($this->from)
            ->setReplyTo(array(
                $worker->getClient()->getEmail() => $worker->getClient()->getName()
            ))
            ->setSubject('Job Schedule Mobile Access')
            ->setBody($this->templating->render('OrcBookingBundle:Email:access.txt.twig', array(
                'worker' => $worker,
                'client' => $worker->getClient()
            )))
        ;

        return $this->mailer->send($message);
    }
}
