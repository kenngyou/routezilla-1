<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\Handler\Handler;

class BookingCancelHandler extends Handler
{
    public function process($data)
    {
        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($data);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess(Booking $booking)
    {
        $booking->setStatus(BookingStatus::STATUS_CANCELLED);
        $booking->setCrew(null);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();
    }
}
