<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\ServiceField;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;

class DeleteServiceHandler extends Handler
{
    /**
     * Deletes the Service
     * @param    Service
     */
    protected function onSuccess(Service $service)
    {
        $bookingRepository = $this->container->get('orc_booking.repository.booking');
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($bookingRepository->findByService($service)) {
            $service->markAsDeleted();
            $em->persist($service);
            $em->flush();
            return;
        }

        $em->remove($service);
        $em->flush();
    }
}
