<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Worker;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\Form\FormInterface;

class WorkerHandler extends Handler
{
    /**
     * Saves the Worker
     * If it's a new user, set permissions
     *
     * @param    Worker
     */
    protected function onSuccess(Worker $worker)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $um = $this->container->get('fos_user.user_manager');

        $user = $worker->getUser();

        if ($user->getPlainPassword()) {
            $um->updatePassword($user);
        }

        if (!$user->getId()) {
            if ($worker->getType() == Worker::TYPE_CREW_MEMBER) {
                $user->addRole('ROLE_WORKER');
                $user->addRole('ROLE_STAFF');
            } elseif ($worker->getType() == Worker::TYPE_OFFICE_ADMIN) {
                $user->addRole('ROLE_STAFF');
            }

            $em->persist($user);
            $em->flush();
        } else {
            $em->persist($user);
        }

        $em->persist($worker);
        $em->flush();
    }
}
