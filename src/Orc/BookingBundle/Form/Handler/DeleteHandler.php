<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class DeleteHandler extends Handler
{
    public function process($data)
    {
        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($data);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess($entity)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->remove($entity);
        $em->flush();
    }
}
