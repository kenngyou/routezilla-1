<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Schedule\CrewEnquirer;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class BookingQuickHandler extends Handler
{
    public function process($data)
    {
        $this->form->setData($data);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                return $this->onSuccess($data);
            }
        }
    }

    protected function onSuccess(Booking $booking)
    {
        if ($this->isAvailable($booking) != CrewEnquirer::STATUS_UNAVAILABLE) {
            $em = $this->container->get('doctrine.orm.entity_manager');
            $em->persist($booking);
            $em->flush();

            return true;
        }

        return false;
    }

    public function query($data)
    {
        $this->form->setData($data);
        $this->form->bindRequest($this->request);

        if (!$this->form->isValid($data)) {
            return false;
        }

        return $this->isAvailable($data);
    }

    public function getError()
    {
        $errors = $this->form->getErrors();
        return $errors[0]->getMessage();
    }

    /**
     * Checks to see if the form-modified Booking is bookable
     * @param    Booking
     * @return   string        one of the STATUS_X constants
     */
    protected function isAvailable(Booking $booking)
    {
        if (!$booking->getCrew()) {
            return CrewEnquirer::STATUS_AVAILABLE;
        }

        return $this->container->get('orc_booking.schedule.scheduler.enquirer')->getCrewStatus(
            $booking->getCrew(),
            $booking->getDateStart(),
            $booking,
            $this->getClient()
        );
    }

    protected function getClient()
    {
        return $this->container->get('synd_multitenant.tenant');
    }
}
