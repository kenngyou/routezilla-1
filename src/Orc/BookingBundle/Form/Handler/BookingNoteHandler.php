<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingNote;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\Worker;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class BookingNoteHandler
{
    protected $form;
    protected $request;
    protected $container;

    public function __construct(FormInterface $form, Request $request, ContainerInterface $container)
    {
        $this->form = $form;
        $this->request = $request;
        $this->container = $container;
    }

    public function process($note, $booking, $worker)
    {
        $this->form->setData($note);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);
            return $this->onSuccess($note, $booking, $worker);
        }
    }


    protected function onSuccess(BookingNote $note, Booking $booking, Worker $worker)
    {
        if (trim($note->getText())) {
            $note->setWorker($worker);
            $booking->addWorkerNote($note);
        }

        if ($this->request->request->get('save') == 'done') {
            $booking->setStatus(BookingStatus::STATUS_DELIVERED);
        }

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        return true;
    }
}
