<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\Handler\Handler;

class BookingDateHandler extends Handler
{
    protected $date;

    public function process($data)
    {
        $this->date = clone $data->getDateStart();
        $data->setOriginalDates(clone $data->getDateStart(), $data->getDateEnd() ? clone $data->getDateEnd() : null);
        return parent::process($data);
    }

    protected function onSuccess(Booking $booking)
    {
        $booking->getDateStart()->setDate(
            $this->date->format('Y'),
            $this->date->format('m'),
            $this->date->format('d')
        );
        $booking->setDateStart(clone $booking->getDateStart());

        $originalStatus = $booking->getStatus();
        if ($originalStatus == BookingStatus::STATUS_DRAFT) {
            $booking->setStatus(BookingStatus::STATUS_ASSIGNED);
        }

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        if ($originalStatus == BookingStatus::STATUS_DRAFT or $booking->getCrew()) {
            $scheduler = $this->container->get('orc_booking.schedule.scheduler');
            $booking->setCrew($scheduler->getAvailableCrew($booking, $this->container->get('synd_multitenant.tenant')));

            $em->persist($booking);
            $em->flush();
        }
    }
}
