<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class CancelHandler extends Handler
{
    public function process($data)
    {
        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($data);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess($entity)
    {
        $booked = in_array($entity->getStatus(), array(
            BookingStatus::STATUS_ASSIGNED,
            BookingStatus::STATUS_UNASSIGNABLE
        ));

        $em = $this->container->get('doctrine.orm.entity_manager');
        $entity->setStatus(BookingStatus::STATUS_CANCELLED);
        $entity->setCrew(null);
        $em->flush();

        if ($booked) {
            $mailer = $this->container->get('orc_booking.mailer.cancellation');
            $mailer->send($entity);
        }
    }
}
