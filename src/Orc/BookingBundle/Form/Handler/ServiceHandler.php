<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\ServiceField;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\ORM\EntityManager;

class ServiceHandler extends Handler
{
    /**
     * Ensure there are enough default fields
     * @param    Service
     */
    public function process($data)
    {
        return parent::process($this->prepareService($data));
    }

    /**
     * Saves the Service
     * @param    Service
     */
    protected function onSuccess(Service $service)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($this->cleanupService($service, $em));
        $em->flush();
    }

    /**
     * Removes incomplete ServiceField rows from a service
     * @param    Service
     * @return   Service
     */
    protected function cleanupService(Service $service, EntityManager $entityManager = null)
    {
        foreach ($service->getFields() as $field) {
            if (!$field->getQuestion()) {
                $service->removeField($field);
                if ($field->getId() and $entityManager) {
                    $entityManager->remove($field);
                }
            }
        }

        return $service;
    }

    /**
     * Adds more ServiceField rows to a service until it's at the desired amount
     * @param    Service
     * @return   Service
     */
    protected function prepareService(Service $service)
    {
        $default = $this->container->getParameter('orc_booking.services.default');

        while (count($service->getFields()) < $default) {
            $service->addField(new ServiceField());
        }

        return $service;
    }
}
