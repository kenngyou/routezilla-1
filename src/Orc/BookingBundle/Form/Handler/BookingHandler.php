<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Junction;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Entity\Boundary;
use Orc\BookingBundle\Schedule\CrewEnquirer;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormError;

class BookingHandler extends Handler
{
    /**
     * Input branch options
     */
    const ACTION_UPDATE = 'update';
    const ACTION_CHECK_AVAILABILITY = 'check';
    const ACTION_CREATE_DRAFT = 'draft';
    const ACTION_CANCEL = 'cancel';
    const ACTION_SCHEDULE = 'schedule';
    const ACTION_SCHEDULE_UNASSIGNED = 'check-unassigned';
    const ACTION_PLACE = 'place';
    const ACTION_RESCHEDULE = 'reschedule';
    const ACTION_UNASSIGN = 'unassign';

    /**
     * Output branch options
     */
    const STATUS_DRAFT    = 'draft';
    const STATUS_BOOKED   = 'booked';
    const STATUS_SCHEDULE = 'schedule';
    const STATUS_JUNCTION = 'junction';
    const STATUS_ERROR    = 'error';
    const STATUS_UPDATED  = 'updated';
    const STATUS_RESCHEDULE = 'reschedule';

    /**
     * Input branch to method mapping
     * @var    array
     */
    protected $actionMap = array(
        // Create Actions
        self::ACTION_CHECK_AVAILABILITY => 'onCreateCheckAvailability',
        self::ACTION_CREATE_DRAFT       => 'onCreateDraft',
        self::ACTION_PLACE              => 'onPlace',

        // Edit Actions
        self::ACTION_UPDATE             => 'onUpdate',
        self::ACTION_RESCHEDULE         => 'onReschedule'
    );

    protected $originalCrew;
    protected $originalDate;

    /**
     * Main Decision Point
     * Actions are mapped via $actionMap based on different submit buttons
     */
    public function process($data)
    {
        $this->container->get('session')->set('junction', null);
        $this->container->get('session')->set('booking.unassignable', false);

        $this->form->setData($data);

        $this->originalCrew = $data->getCrew();
        $this->originalDate = $data->getDateStart() ? clone $data->getDateStart() : null;

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $action = $this->request->request->get('save');
                if (!isset($this->actionMap[$action])) {
                    throw new \Exception('ruh ruh');
                }

                return call_user_func(array($this, $this->actionMap[$action]), $data);
            }
        }
    }

    /**
     * Saves the new Booking as a draft
     */
    protected function onCreateDraft(Booking $booking)
    {
        $original = clone $booking;

        $booking->setStatus(BookingStatus::STATUS_DRAFT);

        $em = $this->container->get('doctrine.orm.entity_manager');
        if (!$this->persist($booking)) {
            return self::STATUS_ERROR;
        }
        $em->flush();

        if (!$this->regionUpdate($booking, $original)) {
            $junction = new Junction(Junction::REASON_REGION, array(
                self::ACTION_CANCEL,
                self::ACTION_SCHEDULE_UNASSIGNED
            ));

            $this->container->get('session')->set('junction', $junction);
            return self::STATUS_JUNCTION;
        }

        return self::STATUS_DRAFT;
    }

    /**
     * Creates the Booking and saves it as a Draft
     */
    protected function onCreateCheckAvailability(Booking $booking)
    {
        if (self::STATUS_DRAFT != $status = $this->onCreateDraft($booking)) {
            return $status;
        }

        return self::STATUS_SCHEDULE;
    }

    /**
     * Creates a placed booking and figures out how to handle it
     */
    protected function onPlace(Booking $booking)
    {
        if (!$this->originalDate) {
            throw new \LogicException('Cannot place booking without setting a date');
        }

        $originalBooking = clone $booking;

        $booking->setCrew($this->originalCrew);
        $booking->setDateStart($this->originalDate);
        $booking->setStatus(BookingStatus::STATUS_ASSIGNED);

        $em = $this->container->get('doctrine.orm.entity_manager');
        if (!$this->persist($booking)) {
            return self::STATUS_ERROR;
        }
        $em->flush();


        if (!$this->regionUpdate($booking, $originalBooking)) {
            $booking->setStatus(BookingStatus::STATUS_DRAFT);
            $booking->setCrew(null);
            $junction = new Junction(Junction::REASON_REGION, array(
                self::ACTION_CANCEL,
                self::ACTION_UNASSIGN
            ));

            $this->container->get('session')->set('junction', $junction);
            return self::STATUS_JUNCTION;
        }


        $availability = $this->enquire($booking);

        if ($availability == CrewEnquirer::STATUS_AVAILABLE) {
            $status = self::STATUS_BOOKED;

        } elseif ($availability == CrewEnquirer::STATUS_DISCOURAGED) {
            $status = self::STATUS_JUNCTION;

            $junction = new Junction(Junction::REASON_DISCOURAGED, array(
                self::ACTION_CANCEL,
                self::ACTION_UNASSIGN,
                self::ACTION_RESCHEDULE
            ));

            $this->container->get('session')->set('junction', $junction);

        } elseif ($availability == CrewEnquirer::STATUS_UNAVAILABLE) {
            $booking->setStatus(BookingStatus::STATUS_DRAFT);
            $booking->setCrew(null);

            $junction = new Junction(Junction::REASON_UNAVAILABLE, array(
                self::ACTION_CANCEL,
                self::ACTION_UNASSIGN,
                self::ACTION_RESCHEDULE
            ));

            $this->container->get('session')->set('junction', $junction);
            $status = self::STATUS_JUNCTION;
        }

        return $status;
    }

    /**
     * Updates an existing Booking
     * Saves Changes, Update Region/check, re-Check Availability
     */
    protected function onUpdate(Booking $booking)
    {
        $originalBooking = clone $booking;

        $em = $this->container->get('doctrine.orm.entity_manager');
        if (!$this->persist($booking)) {
            return self::STATUS_ERROR;
        }
        $em->flush();


        if (!$this->regionUpdate($booking, $originalBooking, true)) {
            $junction = new Junction(Junction::REASON_REGION, array(
                self::ACTION_CANCEL,
                self::ACTION_SCHEDULE_UNASSIGNED
            ));

            $this->container->get('session')->set('junction', $junction);
            return self::STATUS_JUNCTION;
        }


        $availability = $this->enquire($booking);

        if ($availability == CrewEnquirer::STATUS_AVAILABLE) {
            $status = self::STATUS_UPDATED;

        } elseif ($availability == CrewEnquirer::STATUS_DISCOURAGED) {
            $status = self::STATUS_JUNCTION;

            $junction = new Junction(Junction::REASON_DISCOURAGED, array(
                self::ACTION_CANCEL,
                self::ACTION_UNASSIGN,
                self::ACTION_RESCHEDULE
            ));

            $this->container->get('session')->set('junction', $junction);

        } elseif ($availability == CrewEnquirer::STATUS_UNAVAILABLE) {
            $booking->setStatus(BookingStatus::STATUS_DRAFT);
            $booking->setCrew(null);

            $junction = new Junction(Junction::REASON_UNAVAILABLE, array(
                self::ACTION_CANCEL,
                self::ACTION_UNASSIGN,
                self::ACTION_RESCHEDULE
            ));

            $this->container->get('session')->set('junction', $junction);
            $status = self::STATUS_JUNCTION;
        }

        return $status;
    }

    /**
     * When a user reschedules an existing Booking
     */
    protected function onReschedule(Booking $booking)
    {
        $originalBooking = clone $booking;

        $booking->setStatus(BookingStatus::STATUS_DRAFT);
        $em = $this->container->get('doctrine.orm.entity_manager');
        if (!$this->persist($booking)) {
            return self::STATUS_ERROR;
        }
        $em->flush();

        if (!$this->regionUpdate($booking, $originalBooking, true)) {
            $junction = new Junction(Junction::REASON_REGION, array(
                self::ACTION_CANCEL,
                self::ACTION_SCHEDULE_UNASSIGNED
            ));

            $this->container->get('session')->set('junction', $junction);
            return self::STATUS_JUNCTION;
        }

        return self::STATUS_RESCHEDULE;
    }

    /**
     * Update the Region on the given Booking
     *
     * @param    Booking
     * @param    Booking        clone of original pre-edit
     * @param    boolean        TRUE to force checking for new region
     * @return   boolean        TRUE if its all good
     */
    private function regionUpdate(Booking $booking, Booking $originalBooking, $force = false)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        if (!$originalBooking->getId() or !$originalBooking->getRegion() or $force) {
            if (!$region = $this->getRegion($booking->getLocation())) {
                return false;
            }

            $booking->setRegion($region);
            $em->persist($booking);
            $em->flush();
        }

        return true;
    }

    /**
     * Gets the Region containing Location, if any
     * @param    Location
     * @return   Region|null
     */
    private function getRegion(Location $location)
    {
        $pointInPolygon = $this->container->get('orc_booking.booking.inregion');
        $repository = $this->container->get('orc_booking.repository.region');

        $point = $this->locationToCoordinates($location);

        foreach ($repository->findAll() as $region) {
            $polygon = array_map(
                array($this, 'locationToCoordinates'),
                $region->getBoundaries()->toArray()
            );

            if ($pointInPolygon->inPolygon($point, $polygon)) {
                return $region;
            }
        }

        return false;
    }

    /**
     * Converts a Location entity into [lat,lng] array
     * @param    Location
     * @return   array        [lat,lng]
     */
    private function locationToCoordinates($location)
    {
        if ($location instanceof Boundary) {
            $location = $location->getLocation();
        }
        if (!$location instanceof Location) {
            throw new \InvalidArgumentException('$location must be a Location|Boundary');
        }

        return array((float)$location->getLatitude(), (float)$location->getLongitude());
    }

    /**
     * Force geo query for Location
     * @param    Location
     */
    private function forceGetCoordinates(Location $location)
    {
        $location->setAddress($location->getAddress());
    }

    /**
     * Persist changes to the Booking and related entities
     * @param    Booking
     */
    private function persist(Booking $booking)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $this->forceGetCoordinates($booking->getLocation());
        $this->forceGetCoordinates($booking->getCustomer()->getLocation());
        $this->forceGetCoordinates($booking->getCustomer()->getBilling()->getLocation());

        if (!$booking->getId()) {
            $booking->setUser(
                $this->container->get('security.context')->getToken()->getUser()
            );
        }

        $em->persist($booking);
        $em->persist($booking->getCustomer());
        $em->persist($booking->getCustomer()->getLocation());
        $em->persist($booking->getCustomer()->getBilling());
        $em->persist($booking->getCustomer()->getBilling()->getLocation());

        if (!$booking->getLocation()->getLatitude() or !$booking->getLocation()->getLongitude()) {
            $error = true;
            $this->form->addError(new FormError('We couldn\'t find that address.'));
        }

        foreach ($booking->getFields() as $field) {
            $field->setBooking($booking); // XXX ??

            if ($booking->getService()->getFields()->get($field->getSlug())) {
                $em->persist($field);
            } else {
                $booking->removeField($field);
                $em->remove($field);
            }
        }

        return empty($error);
    }

    /**
     * Shortcut for crew enquiry
     * @param    Booking
     * @return   string        CrewEnquirer::STATUS_X
     */
    private function enquire(Booking $booking)
    {
        return $this->container->get('orc_booking.schedule.scheduler.enquirer')->getCrewStatus(
            $booking->getCrew(),
            $booking->getDateStart(),
            $booking,
            $this->getClient()
        );
    }

    /**
     * Retrieve the current tenant
     * @return    Client
     */
    private function getClient()
    {
        return $this->container->get('synd_multitenant.tenant');
    }
}
