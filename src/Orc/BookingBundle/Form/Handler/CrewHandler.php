<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Form\Handler\Handler;

class CrewHandler extends Handler
{
    public function process($data)
    {
        $this->form->setData($data);

        if ($this->request->getMethod() == 'POST') {
            $this->removeCurrentWorkers($data);
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($data);
                return true;
            }
        }

        return false;
    }

    /**
     * Remove workers from the Crew
     * @param    Crew
     */
    protected function removeCurrentWorkers(Crew $crew)
    {
        foreach ($crew->getWorkers() as $worker) {
            $worker->setCrew(null);
        }
    }

    /**
     * Saves the Crew to the Client
     * Updates all selected staff members to be part of the Crew
     *
     * @param    Crew
     */
    protected function onSuccess(Crew $crew)
    {
        $crew->setClient($this->container->get('synd_multitenant.tenant'));

        $em = $this->container->get('doctrine.orm.entity_manager');

        foreach ($crew->getWorkers() as $worker) {
            $worker->setCrew($crew);
            $em->persist($worker);
        }

        $em->persist($crew);
        $em->flush();

        $this->rebuild();
    }

    protected function rebuild()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $crewRepository = $this->container->get('orc_booking.repository.crew');

        $tenant = $this->container->get('synd_multitenant.tenant');
        $tenant->setCrews(count($crewRepository->findActive()));

        $em->persist($tenant);
        $em->flush();
    }

}
