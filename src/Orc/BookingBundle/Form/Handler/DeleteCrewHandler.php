<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\DBAL\DBALException;

class DeleteCrewHandler extends Handler
{
    /**
     * Deletes the Crew
     * @param    Crew
     */
    protected function onSuccess(Crew $crew)
    {
        $bookingRepository = $this->container->get('orc_booking.repository.booking');
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($bookingRepository->findByCrew($crew)) {
            $crew->markAsDeleted();
            $em->persist($crew);
            $em->flush();
            $this->rebuild();
            return;
        }

        $em->remove($crew);
        $em->flush();

        $this->rebuild();
    }

    protected function isValid()
    {
        $errors = array();
        foreach ($this->form->getErrors() as $error) {
            if (strpos($error->getMessage(), "limit") === false) {
                $errors[] = $error;
            }
        }

        return !count($errors);
    }

    protected function rebuild()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $crewRepository = $this->container->get('orc_booking.repository.crew');

        $tenant = $this->container->get('synd_multitenant.tenant');
        $tenant->setCrews(count($crewRepository->findActive()));

        $em->persist($tenant);
        $em->flush();
    }
}
