<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServiceHandler extends Handler
{
    /**
     * Saves the booking + answers
     * Removes any fields which does not belong to the selected Service
     *
     * @param    Booking
     */
    protected function onSuccess(Booking $booking)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        foreach ($booking->getFields() as $field) {
            $field->setBooking($booking); // XXX ??

            if ($booking->getService()->getFields()->get($field->getSlug())) {
                if ($field->getAnswer() === null) {
                    $field->setAnswer('');
                }
                $em->persist($field);
            } else {
                $booking->removeField($field);
                $em->remove($field);
            }
        }

        $em->persist($booking);
        $em->flush();
    }
}
