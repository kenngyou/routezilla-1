<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\Handler\Handler;

class TimeHandler extends Handler
{
    protected $date;

    public function process($data)
    {
        $this->date = clone $data->getDateStart();
        $data->setOriginalDates(clone $data->getDateStart(), $data->getDateEnd() ? clone $data->getDateEnd() : null);
        return parent::process($data);
    }

    protected function onSuccess(Booking $booking)
    {
        $booking->getDateStart()->setDate(
            $this->date->format('Y'),
            $this->date->format('m'),
            $this->date->format('d')
        );

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        $start = clone $booking->getDateStart();
        $start->setTime(0, 0);
        $start->modify('-1 day');
        $end = clone $start;
        $end->modify('+2 days');

        $scheduler = $this->container->get('orc_booking.schedule.scheduler');
        $scheduler->setRange($start, $end);
        $booking->setCrew($scheduler->getAvailableCrew($booking, $this->container->get('synd_multitenant.tenant')));

        $em->persist($booking);
        $em->flush();
    }
}
