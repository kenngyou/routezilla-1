<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Form\Handler\Booking\LocationHandler;

class ConfirmLocationHandler extends LocationHandler
{
    public function process($data, $booking)
    {
        $this->form->setData($data);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $data->setKeepChanges(true);
                return $this->onSuccess($data, $booking);
            }
        }

        return false;
    }
}
