<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\Customer;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Location;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomerHandler
{
    protected $form;
    protected $request;
    protected $container;

    public function __construct(FormInterface $form, Request $request, ContainerInterface $container)
    {
        $this->form = $form;
        $this->request = $request;
        $this->container = $container;
    }

    public function process(Customer $customer, Booking $booking)
    {
        if (!$customer->getLocation()) {
            $customer->setLocation($this->cloneLocation($booking->getLocation()));
        }

        $this->form->setData($customer);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($customer, $booking);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess(Customer $customer, Booking $booking)
    {
        $booking->setCustomer($customer);
        $booking->setStatus(BookingStatus::STATUS_ASSIGNED);

        $customer->getLocation()->setKeepChanges(true);
        $customer->getBilling()->getLocation()->setKeepChanges(true);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($customer);
        $em->persist($booking);
        $em->flush();

        $customerMailer = $this->container->get('orc_booking.mailer.confirmation');
        $customerMailer->send($booking);

        $companyMailer = $this->container->get('orc_booking.mailer.notification');
        $companyMailer->send($booking);

    }

    protected function cloneLocation(Location $clone)
    {
        $location = new Location;
        $location->setStreet($clone->getStreet());
        $location->setLine2($clone->getLine2());
        $location->setCity($clone->getCity());
        $location->setProvince($clone->getProvince());
        $location->setCountry($clone->getCountry());
        $location->setCode($clone->getCode());
        $location->setLatitude($clone->getLatitude());
        $location->setLongitude($clone->getLongitude());

        return $location;
    }
}
