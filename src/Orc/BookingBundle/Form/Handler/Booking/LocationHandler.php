<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Boundary;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Booking\PointInPolygon;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LocationHandler
{
    protected $form;
    protected $request;
    protected $container;

    protected $outOfBounds = false;

    public function __construct(FormInterface $form, Request $request, ContainerInterface $container)
    {
        $this->form = $form;
        $this->request = $request;
        $this->container = $container;
    }

    public function process($location, $booking)
    {
        $this->form->setData($location);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);
            return $this->onSuccess($location, $booking);
        }
    }

    protected function onSuccess(Location $location, Booking $booking)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $booking->setLocation($location);

        // force persist to do something
        if ($location->getQueryAddress()) {
            $location->setLatitude(null);
            $location->setLongitude(null);
        }

        $em->persist($booking);
        $em->persist($location);
        $em->flush();

        if (!$location->getLatitude()) {
            return false;
        }

        if (!$region = $this->getRegion($location)) {
            $this->outOfBounds = true;
            return false;
        }

        $booking->setRegion($region);
        $em->persist($booking);
        $em->flush();

        return true;
    }

    /**
     * Gets the Region containing Location, if any
     * @param    Location
     * @return   Region|null
     */
    protected function getRegion(Location $location)
    {
        $pointInPolygon = $this->container->get('orc_booking.booking.inregion');
        $repository = $this->container->get('orc_booking.repository.region');

        $point = $this->locationToCoordinates($location);


        foreach ($repository->findAll() as $region) {
            $polygon = array_map(
                array($this, 'locationToCoordinates'),
                $region->getBoundaries()->toArray()
            );

            if ($pointInPolygon->inPolygon($point, $polygon)) {
                return $region;
            }
        }

        return false;
    }

    /**
     * Converts a Location entity into [lat,lng] array
     * @param    Location
     * @return   array        [lat,lng]
     */
    protected function locationToCoordinates($location)
    {
        if ($location instanceof Boundary) {
            $location = $location->getLocation();
        }
        if (!$location instanceof Location) {
            throw new \InvalidArgumentException('$location must be a Location|Boundary');
        }

        return array($location->getLatitude(), $location->getLongitude());
    }

    public function isOutOfBounds()
    {
        return $this->outOfBounds;
    }
}
