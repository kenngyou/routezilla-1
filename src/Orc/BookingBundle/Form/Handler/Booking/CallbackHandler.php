<?php

namespace Orc\BookingBundle\Form\Handler\Booking;

use Orc\BookingBundle\Entity\Boundary;

use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\Customer;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Location;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CallbackHandler
{
    protected $form;
    protected $request;
    protected $container;

    public function __construct(FormInterface $form, Request $request, ContainerInterface $container)
    {
        $this->form = $form;
        $this->request = $request;
        $this->container = $container;
    }

    public function process(Customer $customer, Booking $booking)
    {
        if (!$customer->getLocation() and $booking->getLocation()) {
            $customer->setLocation($this->cloneLocation($booking->getLocation()));
        }

        $this->form->setData($customer);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->form->isValid()) {
                $this->onSuccess($customer, $booking);
                return true;
            }
        }

        return false;
    }

    protected function onSuccess(Customer $customer, Booking $booking)
    {
        $booking->setCustomer($customer);
        $booking->setStatus(BookingStatus::STATUS_REQUESTED);

        $cLocation = $booking->getCustomer()->getLocation();
        $cLocation->setAddress(sprintf('%s, %s, %s', $cLocation->getCode(), $cLocation->getCity(), $cLocation->getProvince()));


        if (!$booking->getLocation()) {
            $booking->setLocation($this->cloneLocation($customer->getLocation()));
        }

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($customer);
        $em->persist($booking);
        $em->flush();

        if ($region = $this->getRegion($booking->getLocation())) {
            $booking->setRegion($region);
            $em->persist($booking);
            $em->flush();
        }

        $companyMailer = $this->container->get('orc_booking.mailer.notification');
        $companyMailer->send($booking);
    }

    /**
     * Converts a Location entity into [lat,lng] array
     * @param    Location
     * @return   array        [lat,lng]
     */
    protected function locationToCoordinates($location)
    {
        if ($location instanceof Boundary) {
            $location = $location->getLocation();
        }
        if (!$location instanceof Location) {
            throw new \InvalidArgumentException('$location must be a Location|Boundary');
        }

        return array($location->getLatitude(), $location->getLongitude());
    }

    /**
     * Gets the Region containing Location, if any
     * @param    Location
     * @return   Region|null
     */
    protected function getRegion(Location $location)
    {
        $pointInPolygon = $this->container->get('orc_booking.booking.inregion');
        $repository = $this->container->get('orc_booking.repository.region');

        $point = $this->locationToCoordinates($location);

        foreach ($repository->findAll() as $region) {
            $polygon = array_map(
                array($this, 'locationToCoordinates'),
                $region->getBoundaries()->toArray()
            );

            if ($pointInPolygon->inPolygon($point, $polygon)) {
                return $region;
            }
        }

        return false;
    }

    protected function cloneLocation(Location $clone)
    {
        $location = new Location;
        $location->setStreet($clone->getStreet());
        $location->setCity($clone->getCity());
        $location->setProvince($clone->getProvince());
        $location->setCountry($clone->getCountry());
        $location->setCode($clone->getCode());
        $location->setLatitude($clone->getLatitude());
        $location->setLongitude($clone->getLongitude());

        if ($clone->getQueryAddress()) {
            $location->setAddress($clone->getQueryAddress());
        }

        return $location;
    }
}
