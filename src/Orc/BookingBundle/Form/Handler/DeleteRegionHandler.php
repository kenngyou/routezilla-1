<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Region;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\ORM\EntityManager;

class DeleteRegionHandler extends Handler
{
    /**
     * Deletes the Region
     * @param    Region
     */
    protected function onSuccess(Region $region)
    {
        $bookingRepository = $this->container->get('orc_booking.repository.booking');
        $em = $this->container->get('doctrine.orm.entity_manager');

        if ($bookingRepository->findByRegion($region)) {
            $region->markAsDeleted();
            $em->persist($region);
            $em->flush();
            return;
        }

        $em->remove($region);
        $em->flush();
    }
}
