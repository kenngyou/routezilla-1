<?php

namespace Orc\BookingBundle\Form\Handler;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class Handler
{
    protected $form;
    protected $request;
    protected $container;

    public function __construct(FormInterface $form, Request $request, ContainerInterface $container)
    {
        $this->form = $form;
        $this->request = $request;
        $this->container = $container;
    }

    public function process($data)
    {
        $this->form->setData($data);

        if ($this->request->getMethod() == 'POST') {
            $this->form->bindRequest($this->request);

            if ($this->isValid()) {
                $this->onSuccess($data);
                return true;
            }
        }

        return false;
    }

    protected function isValid()
    {
        return $this->form->isValid();
    }
}
