<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Booking\BlackoutGenerator;
use Orc\BookingBundle\Entity\BlackoutConfiguration;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class BlackoutConfigurationHandler extends Handler
{
    protected function onSuccess(BlackoutConfiguration $configuration)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($configuration);
        $em->flush();

        $blackoutRepository = $this->container->get('orc_booking.repository.blackout');
        foreach ($blackoutRepository->findByConfiguration($configuration) as $blackout) {
            $em->remove($blackout);
        }
        $em->flush();

        $generator = new BlackoutGenerator();
        foreach ($generator->getBlackouts($configuration) as $blackout) {
            $em->persist($blackout);
        }

        $em->flush();
    }
}

