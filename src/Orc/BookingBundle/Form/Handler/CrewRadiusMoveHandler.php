<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\CrewRadius;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class CrewRadiusMoveHandler extends Handler
{
    protected function onSuccess(CrewRadius $crewRadius)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($crewRadius);
        $em->persist($crewRadius->getLocation());
        $em->flush();
    }
}
