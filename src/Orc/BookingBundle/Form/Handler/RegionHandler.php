<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Region;
use Orc\BookingBundle\Entity\Boundary;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\ORM\EntityManager;

class RegionHandler extends Handler
{
    /**
     * Ensure boundaries are cleaned up before passing to form
     * @param    Region
     */
    public function process($data)
    {
        return parent::process($this->prepareBoundaries($data));
    }

    /**
     * Associates the Region with the Client, and persists it
     * @param    Region
     */
    protected function onSuccess(Region $region)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($this->cleanupBoundaries($region, $em));
        $em->flush();
    }

    /**
     * Removes incomplete Boundary rows from a Region
     * @param Region
     * @return Region
     */
    protected function cleanupBoundaries(Region $region, EntityManager $entityManager = null)
    {
        $order = 0;
        foreach ($region->getBoundaries() as $boundary) {
            if (!$boundary->getLocation()->getLatitude() or !$boundary->getLocation()->getLongitude()) {
                $region->removeBoundaries($boundary);
                if ($boundary->getId() and $entityManager) {
                    $entityManager->remove($boundary->getLocation());
                    $entityManager->remove($boundary);
                }
            }

            $boundary->setOrder($order++);
        }

        return $region;
    }

    /**
     * Adds more Boundary rows to a Region until it's at the desired amount
     * @param Region
     * @return Region
     */
    protected function prepareBoundaries(Region $region)
    {
        while (count($region->getBoundaries()) < 3) {
            $region->addBoundaries(new Boundary());
        }

        return $region;
    }
}
