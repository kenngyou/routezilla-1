<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Worker;
use Orc\BookingBundle\Form\Handler\Handler;
use Doctrine\ORM\EntityManager;

class DeleteWorkerHandler extends Handler
{
    /**
     * Deletes the Worker
     * @param    Worker
     */
    protected function onSuccess(Worker $worker)
    {
        $worker->markAsDeleted();
        $worker->getUser()->setEnabled(false);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($worker);
        $em->flush();
    }
}
