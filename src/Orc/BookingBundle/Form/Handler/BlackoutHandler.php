<?php

namespace Orc\BookingBundle\Form\Handler;

use Orc\BookingBundle\Entity\Blackout;
use Orc\BookingBundle\Form\Handler\Handler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;

class BlackoutHandler extends Handler
{
    protected function onSuccess(Blackout $blackout)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($blackout);
        $em->flush();
    }
}
