<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class QuickEditSubscriber implements EventSubscriberInterface
{
    protected $calculator;
    protected $container;

    public function __construct(EndTimeCalculator $calculator, ContainerInterface $container)
    {
        $this->calculator = $calculator;
        $this->container = $container;
    }

    static public function getSubscribedEvents()
    {
        return array(FormEvents::BIND => 'onPostBind');
    }

    /**
     * Apply quick edit changes to the Booking
     * @param    Symfony\Component\Form\Event\DataEvent
     */
    public function onPostBind(DataEvent $event)
    {
        $form = $event->getForm();
        $booking = $event->getData();

        $request = $this->container->get('request');
        $data = $request->request->get('booking_quick') ?: $request->request->get('booking');

        if ($data['move']) {
            $originalDate = null;
            if ($booking->getDateStart()) {
                $originalDate = clone $booking->getDateStart();
            } else {
                $booking->setDateStart('today');
            }

            try {
                $date = clone $booking->getDateStart();
                $date->modify($data['move']);
                $booking->setDateStart($date);
                $booking->setDateEnd($this->calculator->getEndTime($booking, $originalDate));
            } catch (\ErrorException $e) { }

        }

        if ($data['extend']) {
            $date = clone $booking->getDateEnd();

            try {
                $date->modify($data['extend']);
                $booking->setDateEnd($date);
            } catch (\ErrorException $e) { }
        }
    }
}
