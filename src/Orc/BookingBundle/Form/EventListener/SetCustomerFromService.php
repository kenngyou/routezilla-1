<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\CustomerBilling;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Customer;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SetCustomerFromService implements EventSubscriberInterface
{
    protected $duplicateCustomer;
    protected $duplicateBilling;

    static public function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_BIND => 'onPreBind',
            FormEvents::BIND => 'onBind'
        );
    }

    /**
     * Before data is converted to entities, check for 'same'
     * @param    DataEvent
     */
    public function onPreBind(DataEvent $event)
    {
        $data = $event->getData();

        $this->duplicateCustomer = !empty($data['same']);
        $this->duplicateBilling = !empty($data['customer']['same']);
    }

    /**
     * If 'same' found above, duplicate customer info to billing info
     * @param    DataEvent
     */
    public function onBind(DataEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($this->duplicateCustomer) {
            $this->duplicateServiceToCustomer($data, $data->getCustomer());
        }
        if ($this->duplicateBilling) {
            $this->duplicateCustomerToBilling($data->getCustomer(), $data->getCustomer()->getBilling());
        }
    }

    /**
     * Duplicates customer info to billing info
     * @param    Customer
     * @param    CustomerBilling
     */
    protected function duplicateServiceToCustomer(Booking $booking, Customer $customer)
    {
        $location = clone $booking->getLocation();
        $location->setId(null);
        $customer->setLocation($location);
    }

    /**
     * Duplicates customer info to billing info
     * @param    Customer
     * @param    CustomerBilling
     */
    protected function duplicateCustomerToBilling(Customer $customer, CustomerBilling $billing)
    {
        $location = clone $customer->getLocation();
        $location->setId(null);

        $billing->setName($customer->getName());
        $billing->setPhone($customer->getPhone());
        $billing->setEmail($customer->getEmail());
        $billing->setLocation($location);
        // TODO email
    }
}
