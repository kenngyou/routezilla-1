<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\BookingServiceField;
use Orc\BookingBundle\Entity\Booking;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ServiceFieldsSubscriber implements EventSubscriberInterface
{
    static public function getSubscribedEvents()
    {
        return array(
            FormEvents::POST_SET_DATA => 'onPostSetData',
            FormEvents::BIND => 'onBind'
        );
    }

    /**
     * Expands the form dynamically using the service's defined fields
     * @param    FormEvent
     */
    public function onPostSetData(FormEvent $event)
    {
        if (!$booking = $event->getData()) {
            return;
        }

        if (!$service = $booking->getService()) {
            return;
        }

        foreach ($service->getFields() as $serviceField) {
            if (!$bookingField = $booking->getFields()->get($serviceField->getSlug())) {
                $bookingField = new BookingServiceField();
                $bookingField->setField($serviceField);

                $booking->addField($bookingField);
            }
        }

        foreach ($booking->getFields() as $field) {
            if (!$service->getFields()->get($field->getSlug())) {
                $booking->removeField($field);

            }
        }
    }

    /**
     * On bind, set all the new answer fields to their associated service field
     * @param    FormEvent
     */
    public function onBind(FormEvent $event)
    {
        $form = $event->getForm();
        $booking = $event->getData();

        if (!$booking->getService()) {
            return false;
        }

        foreach ($booking->getFields() as $key => $field) {
            $field->setField($booking->getService()->getFields()->get($key));
        }
    }
}
