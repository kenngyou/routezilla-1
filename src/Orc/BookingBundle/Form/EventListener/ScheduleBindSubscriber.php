<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\HoursChange;
use Orc\BookingBundle\Entity\HoursOfOperation;
use Orc\SaasBundle\Entity\Client;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ScheduleBindSubscriber implements EventSubscriberInterface
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        // XXX hacky; separation of concerns issue?
        $this->em = $em;
    }

    static public function getSubscribedEvents()
    {
        return array(FormEvents::BIND => 'onBind');
    }

    /**
     * Removes all HoursChange instances from a Client which match the
     * default HoursOfOperation for the given week day.
     *
     * @param    DataEvent
     */
    public function onBind(DataEvent $event)
    {
        $form = $event->getForm();
        $client = $event->getData();

        foreach ($client->getHoursChanges() as $date => $hourChange) {
            if (!$hourChange->isDefault()) {
                continue;
            }

            $client->removeHoursChange($hourChange);

            if ($hourChange->getId()) {
                $this->em->remove($hourChange);
            }
        }
    }
}
