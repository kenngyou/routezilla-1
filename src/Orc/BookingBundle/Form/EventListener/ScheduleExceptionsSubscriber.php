<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\HoursChange;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ScheduleExceptionsSubscriber implements EventSubscriberInterface
{
    protected $factory;
    protected $container;

    public function __construct(FormFactoryInterface $factory, ContainerInterface $container)
    {
        $this->factory = $factory;
        $this->container = $container;
    }

    static public function getSubscribedEvents()
    {
        return array(FormEvents::PRE_SET_DATA => 'preSetData');
    }

    public function preSetData(DataEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($data === null) {
            return;
        }

        $start = $this->getStart();
        $end = $this->getEnd();


        $changes = $this->getSchedule($data, $start, $end);
        $changes = array_merge($changes, $data->getHoursChanges()->toArray());

        usort($changes, function($a, $b) {
            if ($a->getDay() == $b->getDay()) {
                return 0;
            }

            return $a->getDay() < $b->getDay() ? -1 : 1;
        });

        foreach ($data->getHoursChanges() as $change) {
            $data->removeHoursChange($change);
        }

        foreach ($changes as $change) {
            $day = $change->getDay();
            if ($start <= $day and $day <= $end) {
                $data->addHoursChange($change);
            }
        }
    }

    /**
     * Returns a list of HoursChange entities to add to the Client
     * @param    Client
     * @param    DateTime start date
     * @param    DateTime end date
     * @return   array     HoursChange entities
     */
    protected function getSchedule(Client $client, \DateTime $start, \DateTime $end)
    {
        $defaults = $client->getHoursOfOperation();
        $currentHours = $client->getHoursChanges();

        $changes = array();

        for ($i = clone $start; $i <= $end; $i->modify('+1 day')) {
            $defaultDay = $defaults[$i->format('w')];

            $hourChange = new HoursChange();
            $hourChange->setDay(clone $i);
            $hourChange->setStartTime($defaultDay->getStartTime());
            $hourChange->setEndTime($defaultDay->getEndTime());
            $hourChange->setOff($defaultDay->getOff());

            $result = $currentHours->exists(function($key, $hoursChange) use ($i) {
                return $hoursChange->getDay()->format('m/d/Y') == $i->format('m/d/Y');
            });

            if (!$result) {
                $changes[] = $hourChange;
            }
        }

        return $changes;
    }

    /**
     * Determine the start date of items to show
     * @return  DateTime
     */
    protected function getStart()
    {
        try {
            if ($filter = $this->container->get('request')->get('schedulefilter')) {
                return new DateTime($filter['start']);
            }
        } catch (\Exception $e) {

        }

        return DateTime::getStartOfWeek();
    }

    /**
     * Determine the end date of items to show
     * @return  DateTime
     */
    protected function getEnd()
    {
        try {
            if ($filter = $this->container->get('request')->get('schedulefilter')) {
                return new DateTime($filter['end']);
            }
        } catch (\Exception $e) {

        }

        return DateTime::getEndOfWeek();
    }
}
