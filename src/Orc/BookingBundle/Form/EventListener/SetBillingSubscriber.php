<?php

namespace Orc\BookingBundle\Form\EventListener;

use Orc\BookingBundle\Entity\Customer;
use Orc\BookingBundle\Entity\CustomerBilling;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Event\DataEvent;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SetBillingSubscriber implements EventSubscriberInterface
{
    protected $duplicate;

    static public function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_BIND => 'onPreBind',
            FormEvents::BIND => 'onBind'
        );
    }

    /**
     * Before data is converted to entities, check for 'same'
     * @param    DataEvent
     */
    public function onPreBind(DataEvent $event)
    {
        $data = $event->getData();
        $this->duplicate = !empty($data['same']);
    }

    /**
     * If 'same' found above, duplicate customer info to billing info
     * @param    DataEvent
     */
    public function onBind(DataEvent $event)
    {
        $data = $event->getData();
        $form = $event->getForm();

        if ($this->duplicate) {
            $this->duplicateCustomerToBilling($data, $data->getBilling());
        }
    }

    /**
     * Duplicates customer info to billing info
     * @param    Customer
     * @param    CustomerBilling
     */
    protected function duplicateCustomerToBilling(Customer $customer, CustomerBilling $billing)
    {
        $location = clone $customer->getLocation();
        $location->setId(null);

        $billing->setName($customer->getName());
        $billing->setPhone($customer->getPhone());
        $billing->setEmail($customer->getEmail());
        $billing->setLocation($location);
        // TODO email
    }
}
