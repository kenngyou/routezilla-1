<?php

namespace Orc\BookingBundle\Form\DataTransformer;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Location;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TimeToDateTimeTransformer implements DataTransformerInterface
{
    /**
     * @param DateTime|null $dateTime
     * @return string|null
     */
    public function transform($dateTime)
    {
        if (null === $dateTime) {
            return null;
        }

        return $dateTime->format('G:i:s');
    }

    /**
     * @param  string $value
     * @return string|null
     */
    public function reverseTransform($value)
    {
        if ( (null === $value) || empty($value) ) {
            return null;
        }

        return new DateTime( $value );
    }
}
