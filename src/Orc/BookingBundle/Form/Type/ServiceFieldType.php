<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\ServiceField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceFieldType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('question', 'text', array(
                'attr' => array('class' => 'input-xlarge'),
                'required' => false
            ))
            ->add('type', 'choice', array(
                'choices' => ServiceField::getTypes()
            ))
            ->add('addMinutes', 'number', array(
                'label' => 'Additional Minutes',
                'attr' => array('class' => 'input-mini'),
                'help_inline' => '(per response unit)',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\ServiceField'
        ));
    }

    public function getName()
    {
        return 'servicefield';
    }
}
