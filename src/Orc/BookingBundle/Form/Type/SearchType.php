<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\BookingStatus;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('searchField', 'choice', array(
                'label' => 'Field',
                'data' => 'name',
                'choices' => array(
                    'name' => 'Name',
                    'email' => 'Email',
                    'phone' => 'Phone',
                    'street' => 'Street'
                ),
                'property_path' => 'field'
            ))
            ->add('start', 'date', array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('end', 'date', array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('status', 'choice', array(
                'empty_value' => 'Status',
                'choices' => BookingStatus::getStatuses(),
                'required' => false
            ))
            ->add('query', 'text', array(

            ))
            ->add('region', 'entity', array(
                'empty_value' => 'Region',
                'required' => false,
                'class' => 'Orc\BookingBundle\Entity\Region'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Request\SearchRequest'
        ));
    }

    public function getName()
    {
        return 'search_bookings';
    }
}
