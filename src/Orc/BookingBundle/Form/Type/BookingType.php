<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\EventListener\SetBillingSubscriber;
use Orc\BookingBundle\Form\EventListener\ServiceFieldsSubscriber;
use Orc\BookingBundle\Form\EventListener\SetCustomerFromService;
use Orc\BookingBundle\Form\EventListener\QuickEditSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingType extends AbstractType
{
    protected $fieldsSubscriber;
    protected $billingSubscriber;
    protected $customerSubscriber;
    protected $quickEditSubscriber;
    protected $intervals;

    public function __construct(ServiceFieldsSubscriber $fieldsSubscriber, SetBillingSubscriber $billingSubscriber, SetCustomerFromService $customerSubscriber, QuickEditSubscriber $quickEditSubscriber, $intervals)
    {
        $this->fieldsSubscriber = $fieldsSubscriber;
        $this->billingSubscriber = $billingSubscriber;
        $this->customerSubscriber = $customerSubscriber;
        $this->intervals = $intervals;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $customer = $builder->create('customer', 'customer');

        $location = $builder->create('location', 'full_location', array(
            'validation_groups' => null
        ));

        $builder
            ->add('service', null, array(
                'empty_value' => 'Please select a Service'
            ))
            ->add('fields', 'collection', array(
                'type' => 'booking_service_field',
                'allow_add' => true
            ))
            ->add('additionalInformation', 'textarea', array(
                'label' => 'Additional Information',
                'required' => false
            ))
            ->add('notes', 'textarea', array(
                'label' => 'Admin Notes',
                'required' => false
            ))
            ->add($customer)
            ->add('same', 'checkbox', array(
                'property_path' => false,
                'label' => 'Same as the customer information',
                'data' => true,
                'required' => false
            ))
            ->add($location)
            ->addEventSubscriber($this->fieldsSubscriber)
            ->addEventSubscriber($this->customerSubscriber)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Booking',
            'validation_groups' => array('create')
        ));
    }

    public function getName()
    {
        return 'booking';
    }
}
