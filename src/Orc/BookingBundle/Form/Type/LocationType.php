<?php

namespace Orc\BookingBundle\Form\Type;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', 'hidden', array(
                'required' => false
            ))
            ->add('latitude', 'text', array(
                'required' => false,
                'label' => 'Latitude'
            ))
            ->add('longitude', 'text', array(
                'required' => false,
                'label' => 'Longitude'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Location'
        ));
    }

    public function getName()
    {
        return 'location';
    }
}
