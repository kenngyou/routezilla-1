<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Form\Type\BoundaryType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class RegionType extends AbstractType
{
    protected $boundaryType;

    public function __construct(BoundaryType $boundaryType)
    {
        $this->boundaryType = $boundaryType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('boundaries', 'collection', array(
                 'type' => $this->boundaryType,
                 'allow_add' => true,
                 'by_reference' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Region',
            'csrf_protection' => false
        ));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->addVars(array('label' => 'Region Information'));
    }

    public function getName()
    {
        return 'region';
    }
}
