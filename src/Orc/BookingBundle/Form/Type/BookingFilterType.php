<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\BookingStatus;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;

class BookingFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', 'date', array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('end', 'date', array(
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('status', 'choice', array(
                'empty_value' => 'Status',
                'choices' => BookingStatus::getStatuses(),
                'required' => false
            ))
            ->add('sort', 'choice', array(
                'choices' => array(
                    'date' => 'by Date',
                    'posted' => 'by Date Posted'
                )
            ))
        ;
    }

    public function getName()
    {
        return 'bookingfilter';
    }
}
