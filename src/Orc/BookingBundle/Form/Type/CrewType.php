<?php

namespace Orc\BookingBundle\Form\Type;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CrewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('workers', null, array(
                'label' => 'Staff',
                'required' => false,
                'expanded' => true,
                'property' => 'nameAndCrew',
            ))
            ->add('services', null, array(
                'required' => false,
                'expanded' => true
            ))
            ->add('regions', null, array(
                'required' => false,
                'expanded' => true
            ))
            ->add('active', null, array(
                'label' => 'Accepting Bookings',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Crew'
        ));
    }

    public function getName()
    {
        return 'crew';
    }
}
