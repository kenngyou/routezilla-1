<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Orc\BookingBundle\Form\EventListener\ServiceFieldsSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceType extends AbstractType
{
    protected $subscriber;

    public function __construct(ServiceFieldsSubscriber $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('service', 'entity', array(
                'class' => 'Orc\BookingBundle\Entity\Service',
                'empty_value' => 'Please select a Service',
                'expanded' => true,
                'query_builder' => function($repository) {
                    return $repository->getBuilderForCustomerFacing();
                }
            ))
            ->add('additionalInformation', null, array(
                'label' => 'Additional Information'
            ))
            ->add('fields', 'collection', array(
                'type' => 'booking_service_field',
              #  'by_reference' => false,
                'allow_add' => true,
                'error_bubbling' => true,
               # 'meh'=>true
            ))
            ->addEventSubscriber($this->subscriber)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Booking',
            'validation_groups' => array('step2')
        ));
    }

    public function getName()
    {
        return 'booking_service';
    }
}
