<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType
{
    protected $subscriber;

    public function __construct(EventSubscriberInterface $subscriber)
    {
        $this->subscriber = $subscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('phone')
            ->add('email')
            ->add('location', 'full_location')
            ->add('same', 'checkbox', array(
                'property_path' => false,
                'label' => 'Same as my billing information',
                'data' => true,
                'required' => false
            ))
            ->add('billing', 'customer_billing', array(
                'required' => true
            ))
            ->addEventSubscriber($this->subscriber)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Customer',
        ));
    }

    public function getName()
    {
        return 'customer';
    }
}
