<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Orc\BookingBundle\Form\DataTransformer\AddressToLocationTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('address')
           #->prependNormTransformer(new AddressToLocationTransformer())
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Location',
            'validation_groups' => array(),
        ));
    }

    public function getName()
    {
        return 'query_location';
    }
}
