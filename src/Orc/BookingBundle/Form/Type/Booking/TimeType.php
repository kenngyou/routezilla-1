<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Orc\BookingBundle\Booking\EndTimeCalculator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TimeType extends AbstractType
{
    protected $transformer;
    protected $scheduler;
    protected $calculator;
    protected $container;
    protected $interval;
    protected $publicInterval;

    public function __construct(DataTransformerInterface $transformer, SchedulerInterface $scheduler, EndTimeCalculator $calculator, ContainerInterface $container, $interval, $publicInterval)
    {
        $this->transformer = $transformer;
        $this->scheduler = $scheduler;
        $this->calculator = $calculator;
        $this->container = $container;
        $this->interval = $interval;
        $this->publicInterval = $publicInterval;
    }

    public function setScheduler(SchedulerInterface $scheduler)
    {
        $this->scheduler = $scheduler;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            $builder
                ->create('dateStart', 'choice', array(
                    'choices' => $this->getChoices(clone $options['data']),
                    'label' => 'Time',
                    'expanded' => true
                ))
                ->prependNormTransformer($this->transformer)
        );
    }

    /**
     * Generates the choice of time options based on availability for the day
     * @param    Booking
     * @return   DateTime[]
     */
    protected function getChoices(Booking $booking)
    {
        $client = $this->container->get('synd_multitenant.tenant');

        $times = array();
        foreach ($this->getBookableHours($booking, $client) as $slot) {
            $next = clone $slot;
            $next->modify('+' . $client->getArrivalWindow() . ' hours');

            if ($client->getArrivalWindow()) {
                $message = sprintf('Between %1$s and %2$s', $slot->format('g:ia'), $next->format('g:ia'));
            } else {
                $message = $slot->format('g:ia');
            }

            $times[$slot->format('G:i:00')] = $message;
        }

        return $times;
    }

    /**
     * Query schedule for available times, and filter into bookable hours based on service length
     * @param    Booking
     * @param    Booking
     * @return   DateTime[]
     */
    protected function getBookableHours(Booking $booking, Client $client)
    {
        $date = $booking->getDateStart();
        $booking->setDateEnd($this->calculator->getEndTime($booking));

        $start = clone $date;
        $start->setTime(0, 0);
        $start->modify('-1 day');
        $end = clone $date;
        $end->setTime(0, 0);
        $end->modify('+1 day');

        $this->scheduler->setRange($start, $end);

        $availableSlots = $this->scheduler->getAvailableTimeSlots($client, $booking);
        $todaysSlots = array_filter($availableSlots, function($slot) use ($date) {
            return $slot->format('Y-m-d') == $date->format('Y-m-d');
        });


        $bookable = array();

        foreach ($todaysSlots as $slot) {
            if ($slot->format('i') % $this->publicInterval) {
                continue;
            }

            $bookable[] = $slot;
        }

        return $bookable;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Booking',
            'validation_groups' => array('step3')
        ));
    }

    public function getName()
    {
        return 'booking_time';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['label'] = 'What time would you like?';
    }
}
