<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LocationConfirmType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street')
            ->add('line2', 'text', array(
                'label' => 'Apt/ Unit #',
                'required' => false
            ))
            ->add('city')
            ->add('code', 'text', array(
                'label' => 'ZIP or Postal Code'
            ))
            ->add('province', null, array(
                'label' => 'Province or State'
            ))
            ->add('country')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Location'
        ));
    }

    public function getName()
    {
        return 'location';
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['label'] = 'Please confirm your location.';
    }
}
