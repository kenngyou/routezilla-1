<?php

namespace Orc\BookingBundle\Form\Type\Booking;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class FullLocationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street')
            ->add('line2', 'text', array(
                'label' => 'Apt/ Unit #',
                'required' => false
            ))
            ->add('city')
            ->add('code', 'text', array(
                'label' => 'ZIP or Postal Code'
            ))
            ->add('province', null, array(
                'label' => 'Province or State'
            ))
            ->add('country')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Location'
        ));
    }

    public function getName()
    {
        return 'full_location';
    }
}
