<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\ExportReport;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ExportReportType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Report Name',
                'required' => true,
            ))
            ->add('dateStart', 'date', array(
                'label' => 'Start Date',
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('dateEnd', 'date', array(
                'label' => 'End Date',
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('status', 'choice', array(
                'empty_value' => 'All',
                'choices' => BookingStatus::getStatuses(),
                'required' => false
            ))
            ->add('crew', 'entity', array(
                'empty_value' => 'All',
                'class' => 'Orc\BookingBundle\Entity\Crew',
                'required' => false
            ))
            ->add('region', 'entity', array(
                'empty_value' => 'All',
                'class' => 'Orc\BookingBundle\Entity\Region',
                'required' => false
            ))
            ->add('service', 'entity', array(
                'empty_value' => 'All',
                'class' => 'Orc\BookingBundle\Entity\Service',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\ExportReport'
        ));
    }

    public function getName()
    {
        return 'export_report';
    }
}
