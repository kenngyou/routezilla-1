<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Form\Type\HoursChangeType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;

class ScheduleExceptionsType extends AbstractType
{
    protected $hoursChangeType;
    protected $subscriber;
    protected $bindSubscriber;

    public function __construct(HoursChangeType $hoursChangeType, EventSubscriberInterface $subscriber, EventSubscriberInterface $bindSubscriber)
    {
        $this->hoursChangeType = $hoursChangeType;
        $this->subscriber = $subscriber; // XXX
        $this->bindSubscriber = $bindSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hoursChanges', 'collection', array(
                'type' => $this->hoursChangeType
            ))
            ->addEventSubscriber($this->subscriber)
            ->addEventSubscriber($this->bindSubscriber)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\SaasBundle\Entity\Client'
        ));
    }

    public function getName()
    {
        return 'scheduleexceptions';
    }
}
