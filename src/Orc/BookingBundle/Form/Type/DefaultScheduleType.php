<?php

namespace Orc\BookingBundle\Form\Type;

use DateTime;
use DateTimeZone;
use Orc\BookingBundle\Form\Type\HoursOfOperationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class DefaultScheduleType extends AbstractType
{
    protected $hoursOfOperationType;

    public function __construct(HoursOfOperationType $hoursOfOperationType)
    {
        $this->hoursOfOperationType = $hoursOfOperationType;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('timezone', 'choice', array(
                'choices' => $this->getTimezones()
            ))
            ->add('hoursOfOperation', 'collection', array(
                'type' => $this->hoursOfOperationType
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\SaasBundle\Entity\Client'
        ));
    }

    public function finishView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['label'] = 'Hours of Operation';
    }

    public function getName()
    {
        return 'client_hours';
    }

    protected function getTimezones()
    {
        $list = DateTimeZone::listAbbreviations();
        $idents = DateTimeZone::listIdentifiers();

        $data = $offset = $added = array();
        foreach ($list as $abbr => $info) {
            foreach ($info as $zone) {
                if (!empty($zone['timezone_id']) and !in_array($zone['timezone_id'], $added) and in_array($zone['timezone_id'], $idents)) {
                    $z = new DateTimeZone($zone['timezone_id']);
                    $c = new DateTime(null, $z);
                    $zone['time'] = $c->format('h:i a');
                    $data[] = $zone;
                    $offset[] = $z->getOffset($c);
                    $added[] = $zone['timezone_id'];
                }
            }
        }

        array_multisort($offset, SORT_ASC, $data);

        $options = array();
        foreach ($data as $key => $row) {
            $options[$row['timezone_id']] = $this->formatOption($row);
        }
        return $options;
    }

    protected function formatOption($row)
    {
        return sprintf(
            '%s - %s %s',
            $row['time'],
            $this->formatOffset($row['offset']),
            str_replace('_', ' ', $row['timezone_id'])
        );
    }

    protected function formatOffset($offset)
    {
        $hours = $offset / 3600;
        $remainder = $offset % 3600;
        $sign = $hours > 0 ? '+' : '-';
        $hour = (int) abs($hours);
        $minutes = (int) abs($remainder / 60);

        if ($hour == 0 and $minutes == 0) {
            $sign = ' ';
        }
        return 'GMT' . $sign . str_pad($hour, 2, '0', STR_PAD_LEFT) .':'. str_pad($minutes, 2, '0');
    }
}
