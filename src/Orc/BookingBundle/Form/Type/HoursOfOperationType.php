<?php

namespace Orc\BookingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoursOfOperationType extends AbstractType
{
    protected $intervals;

    public function __construct($intervals)
    {
        $this->intervals = $intervals;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startTime', 'time', array(
                'label' => 'Start Time',
                'minutes' => $this->getMinutes()
            ))
            ->add('endTime', 'time', array(
                'label' => 'End Time',
                'minutes' => $this->getMinutes()
            ))
            ->add('off', 'checkbox', array(
                'label' => '',
                'required' => false
            ))
        ;
    }

    protected function getMinutes()
    {
        if ($this->intervals == 60) {
            return array(0);
        }

        return range(0, 59, $this->intervals);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\HoursOfOperation'
        ));
    }

    public function getName()
    {
        return 'hoursofoperation';
    }
}

