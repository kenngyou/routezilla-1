<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\BlackoutConfiguration;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlackoutConfigurationType extends AbstractType
{
    protected $intervals;

    public function __construct($intervals)
    {
        $this->intervals = $intervals;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('dateStart', 'date', array(
                'label' => 'Start Time',
                'widget' => 'single_text'
            ))
            ->add('dateEnd', 'date', array(
                'label' => 'End Time',
                'widget' => 'single_text'
            ))
            ->add('timeStart', 'time', array(
                'label' => 'Start Time',
                'widget' => 'choice',
                'minutes' => $this->getMinutes()
            ))
            ->add('timeEnd', 'time', array(
                'label' => 'End Time',
                'widget' => 'choice',
                'minutes' => $this->getMinutes()
            ))
            ->add('frequency', 'choice', array(
                'choices' => array(
                    BlackoutConfiguration::FREQUENCY_DAILY => 'Days',
                    BlackoutConfiguration::FREQUENCY_WEEKLY => 'Weeks',
                    BlackoutConfiguration::FREQUENCY_MONTHLY => 'Months',
                    BlackoutConfiguration::FREQUENCY_YEARLY => 'Years'
                )
            ))
            ->add('frequencyMultiplier', 'choice', array(
                'label' => 'Every n Periods',
                'choices' => array_combine(range(1, 30), range(1, 30))
            ))
            ->add('daysOfWeek', 'choice', array(
                'multiple' => true,
                'label' => 'Days of Week',
                'choices' => array(
                    'Sunday',
                    'Monday',
                    'Tuesday',
                    'Wednesday',
                    'Thursday',
                    'Friday',
                    'Saturday'
                ),
                'expanded' => true
            ))
            ->add('nth', 'choice', array(
                'label' => 'On the',
                'choices' => array(
                    '' => 'Every',
                    1 => 'First',
                    2 => 'Second',
                    3 => 'Third',
                    4 => 'Fourth',
                    5 => 'Fifth'
                ),
                'required' => false
            ))
            ->add('daysOfMonth', 'choice', array(
                'multiple' => true,
                'label' => 'Days of Month',
                'choices' => array_combine($r = range(1, 31), $r),
                'expanded' => true
            ))
            ->add('monthsOfYear', 'choice', array(
                'multiple' => true,
                'label' => 'Months of Year',
                'choices' => $all = array(
                    1 => 'January',
                    2 => 'February',
                    3 => 'March',
                    4 => 'April',
                    5 => 'May',
                    6 => 'June',
                    7 => 'July',
                    8 => 'August',
                    9 => 'September',
                    10 => 'October',
                    11 => 'November',
                    12 => 'December'
                ),
                'expanded' => true
            ))
            ->add('service', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
            ->add('region', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
            ->add('crew', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\BlackoutConfiguration'
        ));
    }

    public function getName()
    {
        return 'blackoutconfiguration';
    }

    protected function getMinutes()
    {
        if ($this->intervals == 60) {
            return array(0);
        }

        return range(0, 59, $this->intervals);
    }
}
