<?php

namespace Orc\BookingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlackoutType extends AbstractType
{
    protected $intervals;

    public function __construct($intervals)
    {
        $this->intervals = $intervals;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('dateStart', 'date', array(
                'label' => 'Start Time',
                'widget' => 'single_text'
            ))
            ->add('dateEnd', 'date', array(
                'label' => 'End Time',
                'widget' => 'single_text'
            ))
            ->add('timeStart', 'time', array(
                'label' => 'Start Time',
                'minutes' => $this->getMinutes()
            ))
            ->add('timeEnd', 'time', array(
                'label' => 'End Time',
                'minutes' => $this->getMinutes()
            ))
            ->add('service', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
            ->add('region', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
            ->add('crew', null, array(
                'empty_value' => 'All',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Blackout'
        ));
    }

    public function getName()
    {
        return 'blackout';
    }

    protected function getMinutes()
    {
        if ($this->intervals == 60) {
            return array(0);
        }

        return range(0, 59, $this->intervals);
    }
}
