<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Entity\Worker;
use Orc\UserBundle\Form\Type\UserWorkerType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class WorkerType extends AbstractType
{
    protected $userTransformer;
    protected $registration;

    public function __construct(DataTransformerInterface $userTransformer, $registration = true)
    {
        $this->userTransformer = $userTransformer;
        $this->registration = $registration;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $userForm = $builder->create('user', 'form');
        $registrationType = new UserWorkerType('Orc\UserBundle\Entity\User', $this->registration);
        $registrationType->buildForm($userForm, array());
        $userForm->prependNormTransformer($this->userTransformer);

        $builder
            ->add('name', 'text', array(
                'label' => 'Name'
            ))
            ->add('type', 'choice', array(
                'choices' => Worker::getTypes()
            ))
            ->add('notes')
            ->add($userForm)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Worker'
        ));
    }

    public function getName()
    {
        return 'worker';
    }
}
