<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Form\Type\HoursOfOperationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HoursChangeType extends HoursOfOperationType
{
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\HoursChange'
        ));
    }

    public function getName()
    {
        return 'hourschange';
    }
}

