<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Form\Type\HoursChangeType;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;

class ScheduleFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', 'date', array(
                'widget' => 'single_text',
            ))
            ->add('end', 'date', array(
                'widget' => 'single_text',
            ))
        ;
    }

    public function getName()
    {
        return 'schedulefilter';
    }
}
