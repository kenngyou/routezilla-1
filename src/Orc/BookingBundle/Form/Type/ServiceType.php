<?php

namespace Orc\BookingBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ServiceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label' => 'Name of Service',
                'attr' => array('class' => 'input-xlarge')
            ))
            ->add('description' , 'textarea', array(
                'attr' => array('class' => 'input-xlarge'),
                'required' => false
            ))
            ->add('baseTime', 'integer', array(
                'label' => 'Time Required',
                'attr' => array('class' => 'input-mini'),
                'help_inline' => '(in minutes)'
            ))
            ->add('isActive', 'checkbox', array(
                'label' => 'Is Active?',
                'required' => false
            ))
            ->add('fields', 'collection', array(
                'type' => new ServiceFieldType(),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Service'
        ));
    }

    public function getName()
    {
        return 'service';
    }
}
