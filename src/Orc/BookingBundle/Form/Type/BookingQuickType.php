<?php

namespace Orc\BookingBundle\Form\Type;

use Orc\BookingBundle\Form\EventListener\QuickEditSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BookingQuickType extends AbstractType
{
    protected $intervals;
    protected $quickEditSubscriber;

    public function __construct($intervals, QuickEditSubscriber $quickEditSubscriber)
    {
        $this->intervals = $intervals;
        $this->quickEditSubscriber = $quickEditSubscriber;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('move', 'time', array(
                'label'   => 'Move to',
                'widget' => 'single_text',
                'property_path' => false,
                'required' => false,
                'error_bubbling' => true,
                'invalid_message' => 'Cannot understand when to move booking to'
            ))
            ->add('extend', 'text', array(
                'label' => 'OR extend Booking until',
                'property_path' => false,
                'required' => false,
                'error_bubbling' => true
            ))
            ->add('crew', null, array(
                'label' => 'Assigned Crew',
                'required' => false,
                'empty_value' => 'unassigned',
                'error_bubbling' => true,
                'invalid_message' => 'Cannot understand when to end booking'
            ))
            ->addEventSubscriber($this->quickEditSubscriber)
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\BookingBundle\Entity\Booking',
            'error_bubbling' => true,
            'validation_groups' => array('query')
        ));
    }

    public function getName()
    {
        return 'booking_quick';
    }
}
