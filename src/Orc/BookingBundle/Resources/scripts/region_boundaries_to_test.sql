SELECT region.name
     , CONCAT('[', 
              GROUP_CONCAT(CONCAT('[', 
                                  location.latitude, 
                                  ',', 
                                  location.longitude, 
                                  ']') 
                           ORDER BY boundary.ordered ), 
              ']') AS coordinates
  FROM region_boundary boundary
LEFT OUTER
  JOIN location
    ON location.id = boundary.location_id
LEFT OUTER
  JOIN region
    ON region.id = boundary.region_id
LEFT OUTER
  JOIN client
    ON client.id = region.client_id
 WHERE client.name = "Syndicate Theory"
GROUP
    BY region.name
ORDER
    BY region.name;