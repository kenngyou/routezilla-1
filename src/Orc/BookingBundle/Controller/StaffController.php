<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\Worker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StaffController extends Controller
{
    /**
     * List all staff members
     */
    public function indexAction()
    {
        $repository = $this->get('orc_booking.repository.worker');

        return $this->render('OrcBookingBundle:Staff:index.html.twig', array(
            'staff' => $repository->findAll(),
            'types' => Worker::getTypes(true)
        ));
    }

    public function viewAction(Worker $worker)
    {
        return $this->render('OrcBookingBundle:Staff:view.html.twig', array(
            'worker' => $worker
        ));
    }

    /**
     * Creates a new Worker (staff member)
     */
    public function createAction()
    {
        $worker = new Worker();

        $form = $this->get('orc_booking.form.worker');
        $handler = $this->get('orc_booking.form.handler.worker');

        if ($handler->process($worker)) {
            $this->get('session')->setFlash('success', 'Staff member added successfully.');
            $this->get('session.flashprompt')->add(
                'Would you like to send mobile access to ' . $worker->getName() . '?',
                $this->generateUrl('staff_admin_access', array('id' => $worker->getUser()->getId()))
            );

            return $this->redirect($this->generateUrl('staff_admin'));
        }

        return $this->render('OrcBookingBundle:Staff:create.html.twig', array(
            'form'   => $form->createView(),
            'worker' => $worker
        ));
    }

    /**
     * Edits an existing Worker (staff member)
     */
    public function editAction(Worker $worker, Request $request)
    {
        $form = $this->get('orc_booking.form.worker_edit');
        $handler = $this->get('orc_booking.form.handler.worker_edit');

        if ($worker->getType() == Worker::TYPE_ADMIN and $request->getMethod() == 'POST') {
            $this->get('session')->setFlash('error', 'The admin worker cannot be edited.');
            return $this->redirect($this->generateUrl('staff_admin'));
        }

        if ($handler->process($worker)) {
            $this->get('session')->setFlash('success', 'Staff member updated successfully.');
            return $this->redirect($this->generateUrl('staff_admin'));
        }

        return $this->render('OrcBookingBundle:Staff:edit.html.twig', array(
            'form'   => $form->createView(),
            'worker' => $worker
        ));
    }

    /**
     * Deletes an existing Worker (staff member)
     */
    public function deleteAction(Worker $worker)
    {
        if ($worker->getType() == Worker::TYPE_ADMIN) {
            $this->get('session')->setFlash('error', 'The admin worker cannot be removed.');
            return $this->redirect($this->generateUrl('staff_admin'));
        }

        $form = $this->get('orc_booking.form.delete');
        $handler = $this->get('orc_booking.form.handler.worker_delete');

        if ($handler->process($worker)) {
            $this->get('session')->setFlash('success', 'Worker removed successfully.');
            return $this->redirect($this->generateUrl('staff_admin'));
        }

        return $this->render('OrcBookingBundle:Staff:delete.html.twig', array(
            'form'   => $form->createView(),
            'worker' => $worker
        ));
    }

    /**
     * Sends the Worker a calendar invitation
     */
    public function inviteAction(Worker $worker)
    {
        $this->get('orc_calendar.mailer.invitation')->send($worker);

        $this->get('session')->setFlash('success', 'Worker emailed successfully.');
        return $this->redirect($this->generateUrl('staff_admin'));
    }

    /**
     * Sends the Worker mobile access information
     */
    public function accessAction(Worker $worker)
    {
        $this->get('orc_booking.mailer.access')->send($worker);

        $this->get('session')->setFlash('success', 'Worker emailed successfully.');
        return $this->redirect($this->generateUrl('staff_admin'));
    }
}
