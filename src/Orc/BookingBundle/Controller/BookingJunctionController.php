<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BookingJunctionController extends Controller
{
    /**
     * Prompt the user to see how they want to proceed
     */
    public function junctionAction(Booking $booking)
    {
        if (!$junction = $this->get('session')->get('junction')) {
            return $this->redirect($this->generateUrl('book_admin_view_day'));
        }

        return $this->render('OrcBookingBundle:BookingJunction:junction.html.twig', array(
            'booking'  => $booking,
            'junction' => $junction
        ));
    }

    public function draftAction(Booking $booking)
    {
        if (!$junction = $this->get('session')->get('junction')) {
            return $this->redirect($this->generateUrl('book_admin_view_day'));
        }

        $booking->setStatus(BookingStatus::STATUS_DRAFT);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        $this->get('session')->setFlash('success', 'Booking added successfully as a draft.');

        return $this->redirect($this->generateUrl('book_admin_view_list', array(
            'bookingfilter' => array(
                'status' => BookingStatus::STATUS_DRAFT,
                'sort' => 'posted'
            )
        )));
    }

    /**
     * Unassigns the Crew associated with the Booking
     */
    public function unassignAction(Booking $booking)
    {
        if (!$junction = $this->get('session')->get('junction')) {
            return $this->redirect($this->generateUrl('book_admin_view_day'));
        }

        $booking->setCrew(null);
        $booking->setStatus(BookingStatus::STATUS_UNASSIGNABLE);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->persist($booking);
        $em->flush();

        $this->get('session')->setFlash('success', 'Booking unassigned successfully.');

        return $this->redirect($this->generateUrl('book_admin_view_day', array(
            'date' => $booking->getDateStart()->format('Y-m-d')
        )));
    }
}
