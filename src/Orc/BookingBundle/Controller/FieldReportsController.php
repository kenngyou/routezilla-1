<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingNote;
use Orc\BookingBundle\Entity\Worker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class FieldReportsController extends Controller
{
    public function indexAction(Worker $worker, \DateTime $date = null)
    {
       if (!$date) {
           $date = new DateTime('today');
       }

       return $this->render('OrcBookingBundle:FieldReports:index.html.twig', array(
           'date' => $date,
           'worker' => $worker,
           'bookings' => $worker->getCrew()
               ? $this->get('orc_booking.repository.booking')->findByDateForCrew($worker->getCrew(), $date)
               : array()
       ));
    }

    public function createAction(Worker $worker,  $booking, Request $request)
    {
        if (!$booking = $this->get('orc_booking.repository.booking')->find($booking)) {
            return $this->createNotFoundException('Booking not found');
        }

        $note = new BookingNote();

        $form = $this->get('orc_booking.form.booking.note');
        $handler = $this->get('orc_booking.form.handler.booking.note');

        if ($handler->process($note, $booking, $worker)) {
            $this->get('session')->setFlash('success', 'Report saved successfully.');
            return $this->redirect($request->getRequestUri());
        }

        return $this->render('OrcBookingBundle:FieldReports:create.html.twig', array(
            'form' => $form->createView(),
            'worker' => $worker,
            'booking' => $booking,
            'notes' => $this->get('orc_booking.repository.bookingnote')->findByBooking($booking)
        ));
    }
}
