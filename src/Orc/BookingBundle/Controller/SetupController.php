<?php

namespace Orc\BookingBundle\Controller;

use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Event\ClientEvent;
use Orc\SaasBundle\SaasEvents;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\Worker;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SetupController extends Controller
{
    public function setupStep1Action(Request $request)
    {
        return $this->forward('OrcSaasBundle:Profile:profile', array('wizard' => true));
    }

    public function setupStep2Action(Request $request)
    {
        return $this->forward('OrcBookingBundle:Regions:index', array('wizard' => true));
    }

    public function setupStep3Action(Request $request)
    {
        return $this->forward('OrcBookingBundle:Schedule:default', array('wizard' => true));
    }

    public function setupStep4Action(Request $request)
    {
        return $this->forward('OrcBookingBundle:Services:create', array('wizard' => true));
    }

    public function setupStep5Action(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');

        if ($client->getStatus() != Client::STATUS_STEP_5) {
            $client->setStatus(Client::STATUS_STEP_5);

            $client->setCrews($client->getCrews() + 1);

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($client);

            $crew = new Crew();
            $crew->setName('Default Crew');
            $crew->setClient($client);
            foreach ($this->get('orc_booking.repository.region')->findAll() as $region) {
                $crew->addRegion($region);
            }
            foreach ($this->get('orc_booking.repository.service')->findAll() as $service) {
                $crew->addService($service);
            }

            $worker = new Worker();
            $worker->setCrew($crew);
            $worker->setUser($this->get('security.context')->getToken()->getUser());
            $worker->setName('Admin');
            $worker->setType(Worker::TYPE_ADMIN);

            $crew->addWorker($worker);

            $em->persist($crew);
            $em->persist($worker);
            $em->flush();

            $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_SETUP, new ClientEvent($client));
        }

        return $this->render('OrcBookingBundle:Wizard:5_setup_complete.html.twig', array(
            'client' => $this->get('synd_multitenant.tenant')
        ));
    }

    public function skipAction(Request $request, $toggle = 1)
    {
        $client = $this->get('synd_multitenant.tenant');
        $client->setSkipWizard($toggle);

        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($client);
        $em->flush();

        if ($toggle) {
            return $this->redirect($this->generateUrl('dashboard'));
        }

        return $this->redirect($this->generateUrl('setup_step' . ($client->getStatus() + 1)));
    }

    public function dismissAction()
    {
        $client = $this->get('synd_multitenant.tenant');
        $client->setSkipWizard(0);
        $client->setStatus(Client::STATUS_STEP_5);

        $em = $this->get('doctrine.orm.entity_manager');
        $em->persist($client);
        $em->flush();

        return $this->redirect($this->generateUrl('dashboard'));
    }
}
