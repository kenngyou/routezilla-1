<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Request\SearchRequest;

use Orc\BookingBundle\Form\Type\SearchType;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Form\Handler\BookingHandler;
use Orc\BookingBundle\Form\Handler\BookingQuickHandler;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Form\Type\BookingType;
use Orc\BookingBundle\Form\Type\BookingFilterType;
use Orc\BookingBundle\Form\Handler\BookingDateHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BookingController extends Controller
{
    /**
     * Lists all bookings
     * @deprecated
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('book_admin_view_day'));
    }

    /**
     * Create new bookings (taking them manually on the phone, etc.)
     */
    public function createAction(Request $request)
    {
        $booking = new Booking();
        if ($request->query->has('crew')) {
            $this->createWithCrewAndDate($booking, $request);
            if ($booking->getDateStart() < new DateTime()) {
                $this->get("session")->setFlash('error', 'You cannot place bookings in the past.');
                return $this->exitFlow($this->generateUrl('book_admin_view_day', array(
                    'date' => $booking->getDateStart()->format('Y-m-d')
                )));
            }
        }

        $form = $this->get('orc_booking.form.booking');
        $handler = $this->get('orc_booking.form.handler.booking');

        $response = $handler->process($booking);

        if ($response === BookingHandler::STATUS_BOOKED) {
            $this->get('session')->setFlash('success', 'Booking added successfully.');
            $this->get('session.flashprompt')->add(
                'Would you like to send an email confirmation to the customer?',
                $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'confirmation'))
            );

            return $this->exitFlow($this->generateUrl('book_admin_view_day', array(
                'date' => $booking->getDateStart()->format('Y-m-d')
            )));
        }

        if ($response === BookingHandler::STATUS_DRAFT) {
            $this->get('session')->setFlash('success', 'Booking added successfully as a draft.');
            return $this->exitFlow($this->generateUrl('book_admin_view_list', array(
                'bookingfilter' => array(
                    'status' => BookingStatus::STATUS_DRAFT,
                    'sort' => 'posted'
                )
            )));
        }

        if ($response == BookingHandler::STATUS_SCHEDULE) {
            $this->get('session')->setFlash('info', 'Booking saved as a draft temporarily.');
            return $this->redirect($this->generateUrl('book_admin_check', array(
                'id' => $booking->getId()
            )));
        }

        if ($response === BookingHandler::STATUS_JUNCTION) {
            if ($booking->getStatus() == BookingStatus::STATUS_DRAFT) {
                $this->get('session')->setFlash('error', 'Booking could not be completed.');
            } else {
                $this->get('session')->setFlash('success', 'Booked completed successfully.');
                $this->get('session.flashprompt')->add(
                    'Would you like to send an email confirmation to the customer?',
                    $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'confirmation'))
                );
            }

            return $this->redirect($this->generateUrl('book_admin_junction', array(
                'id' => $booking->getId()
            )));
        }

        return $this->render('OrcBookingBundle:Booking:create.html.twig', array(
            'form'    => $form->createView(),
            'booking' => $booking
        ));
    }

    protected function createWithCrewAndDate(Booking $booking, Request $request)
    {
        try {
            $date = new DateTime($request->query->get('date'));
        } catch (\Exception $e) {
            throw $this->createNotFoundException('The requested time could not be used.');
        }

        $crew = $this->get('orc_booking.repository.crew')->find($request->query->get('crew')) ?: null;

        $booking->setDateStart($date);
        $booking->setCrew($crew);
        $booking->setStatus(BookingStatus::STATUS_ASSIGNED);
    }

    /**
     * Edit an existing Booking
     */
    public function editAction(Booking $booking)
    {
        $form = $this->get('orc_booking.form.booking');
        $handler = $this->get('orc_booking.form.handler.booking');

        $response = $handler->process($booking);

        if ($response === BookingHandler::STATUS_UPDATED) {
            $this->get('session')->setFlash('success', 'Booking updated successfully.');
            return $this->exitFlow($this->generateUrl('book_admin_view_day', array(
                'date' => $booking->getDateStart()->format('Y-m-d')
            )));
        }

        if ($response === BookingHandler::STATUS_DRAFT) {
            $this->get('session')->setFlash('success', 'Booking drafted successfully.');
            return $this->exitFlow($this->generateUrl('book_admin_view_list', array(
                'bookingfilter' => array(
                    'status' => BookingStatus::STATUS_DRAFT,
                    'sort' => 'posted'
                )
            )));
        }

        if ($response == BookingHandler::STATUS_RESCHEDULE) {
            $this->get('session')->setFlash('info', 'Booking saved as a draft temporarily.');
            return $this->redirect($this->generateUrl('book_admin_check', array(
                'id' => $booking->getId()
            )));
        }

        if ($response === BookingHandler::STATUS_JUNCTION) {
            if ($booking->getStatus() == BookingStatus::STATUS_DRAFT) {
                $this->get('session')->setFlash('error', 'Booking could not be saved.');
            }

            return $this->redirect($this->generateUrl('book_admin_junction', array(
                'id' => $booking->getId()
            )));
        }

        return $this->render('OrcBookingBundle:Booking:edit.html.twig', array(
            'form'    => $form->createView(),
            'booking' => $booking
        ));
    }

    /**
     * Get Service Fields
     * AJAX Response to render custom service fields
     */
    public function fieldsAction(Service $service, $bookingId = null)
    {
        $booking = $this->getBooking($bookingId) ?: new Booking();
        $booking->setService($service);

        $form = $this->createForm('booking', $booking);

        return $this->render('OrcBookingBundle:Public:service-fields.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Edit booking (AJAX)
     */
    public function editQuickAction(Booking $booking, Request $request)
    {
        if ($booking->isReadOnly()) {
            return $this->render('OrcBookingBundle:Booking:edit-quick-readonly.html.twig', array(
                'booking' => $booking
            ));
        }

        $form = $this->createForm('booking_quick', $booking);
        $handler = new BookingQuickHandler($form, $request, $this->container);
        $originalDate = $booking->getDateStart();

        if ($status = $handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Booking updated successfully.');

            if ($originalDate != $booking->getDateStart()) {
                $this->get('session.flashprompt')->add(
                    'Would you like to notify the customer about this change?',
                    $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'change'))
                );
            }

            return $this->redirect($this->generateUrl('book_admin_view_day', array(
                'date' => $booking->getDateStart()->format('Y-m-d')
            )));
        }

        if ($status === false) {
            $this->get('session')->setFlash('error', 'Scheduling could not be completed.');
            return $this->redirect($this->generateUrl('book_admin_edit', array('id' => $booking->getId())));
        }

        return $this->render('OrcBookingBundle:Booking:edit-quick.html.twig', array(
            'form'    => $form->createView(),
            'booking' => $booking
        ));
    }

    /**
     * Check availability for a given Booking
     * Administrators can effectively book from this second on.
     */
    public function checkAction(Booking $booking, Request $request)
    {
        if ($booking->getDateStart() and !$request->query->get('unavailable')) {
            return $this->redirect($this->generateUrl('book_admin_check_day', array(
                'id' => $booking->getId(),
                'day' => $booking->getDateStart()->format('Y-m-d')
            )));
        }

        if ($request->query->get('unassignable') or $this->get('session')->get('booking.unassignable')) {
            $this->get('session')->set('booking.unassignable', $booking->getId());
        }

        return $this->render('OrcBookingBundle:Booking:check.html.twig', array(
            'booking' => $booking
        ));
    }

    /**
     * Check availability for a given Booking / Date
     * Administrators can effectively book from this second on.
     */
    public function checkDayAction(Booking $booking, \DateTime $day, Request $request)
    {
        $end = clone $day;
        $end->modify('+1 day');

        $alreadyBooked = (bool)$booking->getDateStart();

        if ($this->get('session')->get('booking.unassignable')) {
            $scheduler = $this->get('orc_booking.schedule.scheduler.unassignable');
            $scheduler->setRange($day, $end);
            $dayScheduler = $this->get('orc_booking.schedule.scheduler.unassignable.day');

            $start = $booking->getDateStart() ?: new DateTime();
            $start->setTime(0, 0);

            $dayScheduler->setRange($start, $end);
            $booking->setStatus(BookingStatus::STATUS_UNASSIGNABLE);

        } else {
            $scheduler = $this->get('orc_booking.schedule.scheduler');
            $dayScheduler = $this->get('orc_booking.schedule.scheduler.day');
            $dayScheduler->setRange($day, $end);
        }

        if (!$dayScheduler->isDayAvailable($this->getClient(), $day, $booking)) {
            $this->get('session')->setFlash('warning', 'Sorry, that day is unavailable.');
            return $this->redirect($this->generateUrl('book_admin_check', array('id' => $booking->getId(), 'unavailable' => true)));
        }

        if ($booking->getDateStart()) {
            $booking->setOriginalDates($booking->getDateStart(), $booking->getDateEnd());
            $day->setTime(
                $booking->getDateStart()->format('H'),
                $booking->getDateStart()->format('i')
            );
        }
        $booking->setDateStart($day);

        $type = $this->get('orc_booking.form.type.booking.time');
        $type->setScheduler($scheduler);
        $form = $this->createForm($type, $booking);
        $handler = new BookingDateHandler($form, $request, $this->container);

        if ($handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Booking updated successfully.');

            if ($alreadyBooked) {
                $this->get('session.flashprompt')->add(
                    'Would you like to notify the customer about this change?',
                    $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'change'))
                );
            } else {
                $this->get('session.flashprompt')->add(
                    'Would you like to send the customer an email confirmation?',
                    $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'confirmation'))
                );
            }

            return $this->exitFlow($this->generateUrl('book_admin_view_day', array('date' => $booking->getDateStart()->format('Y-m-d'))));
        }

        return $this->render('OrcBookingBundle:Booking:check-day.html.twig', array(
            'booking' => $booking,
            'day' => $day,
            'days' => $dayScheduler->getAvailableDays($this->getClient(), $booking),
            'form' => $form->createView()
        ));
    }

    /**
     * When performing a scheduling query, see what is available
     */
    public function calendarQueryAction(Booking $booking, Request $request)
    {
        $input = DateTime::createFromFormat('U', $request->query->get('start'));
        $start = DateTime::createFromFormat('Y-m-d h:ia', $input->format('Y-m-d') . '12:00am');
        $end   = clone $start;
        $end->modify($this->container->getParameter('orc_booking.schedule.calendar.lookahead'));

        if ($this->get('session')->get('booking.unassignable')) {
            $dayScheduler = $this->get('orc_booking.schedule.scheduler.unassignable.day');
            $dayScheduler->setRange($start, $end);
        } else {
            $dayScheduler = $this->get('orc_booking.schedule.scheduler.day');
            $dayScheduler->setRange($start, $end);($start);
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(
            $this->get('orc_booking.twig.extension.calendar')->getDatesAsJsonEvents(
                $dayScheduler->getAvailableDays($this->getClient(), $booking)
            )
        );

        return $response;
    }

    /**
     * Check to see whether or not a crew is available
     */
    public function checkCrewAvailabilityAction(Booking $booking, Request $request)
    {
        if ($request->request->get('booking')) {
            $request->request->set('booking_quick', $request->request->get('booking'));
        } else {
            $request->request->set('booking', $request->request->get('booking_quick'));
        }

        $form = $this->createForm('booking_quick', $booking);
        $handler = new BookingQuickHandler($form, $request, $this->container);

        if ($result = $handler->query($booking)) {
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode(array(
                'status' => $result,
                'message' => ''
            )));

            return $response;

        } elseif ($result === false) {
            $response = new Response();
            $response->headers->set('Content-Type', 'application/json');
            $response->setContent(json_encode(array(
                'status' => 'error',
                'message' => $handler->getError()
            )));

            return $response;
        }

        return $this->redirect($this->generateUrl('book_admin_edit', array('id' => $booking->getId())));
    }

    /**
     * Physically remove a Booking
     */
    public function deleteAction(Booking $booking)
    {
        $form = $this->get('orc_booking.form.delete');
        $handler = $this->get('orc_booking.form.hander.delete');

        if ($handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Booking removed successfully.');
            return $this->exitFlow($this->generateUrl('book_admin'));
        }

        return $this->render('OrcBookingBundle:Booking:delete.html.twig', array(
            'form' => $form->createView(),
            'booking' => $booking
        ));
    }

    /**
     * Cancel a Booking (remove from Schedule / archive)
     */
    public function cancelAction(Booking $booking)
    {
        $form = $this->get('orc_booking.form.booking.cancel');
        $handler = $this->get('orc_booking.form.handler.booking.cancel');

        if ($handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Booking cancelled successfully.');
            $this->get('session.flashprompt')->add(
                'Would you like to email the customer about this cancellation?',
                $this->generateUrl('book_admin_notify', array('id' => $booking->getId(), 'message' => 'cancellation'))
            );

            return $this->exitFlow($this->generateUrl('book_admin_view_day'));
        }

        return $this->render('OrcBookingBundle:Booking:cancel.html.twig', array(
            'form' => $form->createView(),
            'booking' => $booking
        ));
    }

    /**
     * Trigger an email notification
     */
    public function notifyAction(Booking $booking, $message = 'change', Request $request)
    {
        $mailer = $this->get("orc_booking.mailer.$message");
        $mailer->send($booking);

        $this->get('session')->setFlash('success', 'The customer has been emailed.');
        return $this->redirect(
            $request->headers->get('referer') ?:
            $this->getRedirectUrl($this->generateUrl('book_admin_view_day', array('date' => $booking->getDateStart()->format('Y-m-d'))))
        );
    }

    /**
     * View the day view / Gantt chart for a given $date
     */
    public function viewDayAction(\DateTime $date = null, Request $request)
    {
        $bookingRepository = $this->get('orc_booking.repository.booking');
        $blackoutRepository = $this->get('orc_booking.repository.blackout');
        $regionRepository  = $this->get('orc_booking.repository.region');

        $date = $date ?: new DateTime('today');

        $this->setRedirectUrl($request->getRequestUri());

        return $this->render('OrcBookingBundle:Booking:view-day.html.twig', array(
            'crews' => $crews = $bookingRepository->findDailyBookingsGroupedByCrew($date),
            'unassigned' => $unassigned = $bookingRepository->findDailyUnassignedBookings($date),
            'totalBookings' => $this->getTotalBookings($crews) + count($unassigned),
            'regions' => $regionRepository->findAll(),
            'blackouts' => $blackoutRepository->findAffectingDay($date),
            'date' => $date,
            'past' => $date < new DateTime('today'),
            'hourSpan' => 60 / $this->container->getParameter('orc_booking.schedule.intervals'),
            'apiKey' => $this->container->getParameter('orc_booking.maps.apikey')
        ));
    }

    /**
     * View bookings month by month
     */
    public function viewMonthAction()
    {
        return $this->render('OrcBookingBundle:Booking:view-month.html.twig');
    }

    /**
     * When performing a scheduling query, see what is available
     */
    public function monthQueryAction(Request $request)
    {
        $inStart = DateTime::createFromFormat('U', $request->query->get('start'));
        $start = DateTime::createFromFormat('Y-m-d h:ia', $inStart->format('Y-m-d') . '12:00am');

        $inEnd = DateTime::createFromFormat('U', $request->query->get('end'));
        $end = DateTime::createFromFormat('Y-m-d h:ia', $inEnd->format('Y-m-d') . '12:00am');

        $repository = $this->get('orc_booking.repository.booking');

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(
            $this->get('orc_booking.twig.extension.calendar')->getBookedDaysAsJsonEvents(
                $repository->findBookedDaysInMonth($start, $end)
            )
        );

        return $response;
    }

    public function viewListAction(Request $request)
    {
        $bookingRepository = $this->get('orc_booking.repository.booking');

        $form = $this->createForm(new BookingFilterType());
        $form->bindRequest($request);

        $this->setRedirectUrl($request->getRequestUri());

        return $this->render('OrcBookingBundle:Booking:view-list.html.twig', array(
            'bookings' => $bookingRepository->findMatching($request->query->get('bookingfilter') ?: array()),
            'form' => $form->createView()
        ));
    }

    /**
     * Search for Bookings
     */
    public function searchAction(Request $request)
    {
        $results = array();
        $form = $this->createForm(new SearchType(), $searchRequest = new SearchRequest());
        $this->setRedirectUrl($request->getRequestUri());

        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $bookingRepository = $this->get('orc_booking.repository.booking');
                $results = $bookingRepository->search($searchRequest);
            }
        }

        return $this->render('OrcBookingBundle:Booking:search.html.twig', array(
            'form' => $form->createView(),
            'results' => $results
        ));
    }

    protected function getTotalBookings($crews)
    {
        $total = 0;
        foreach ($crews as $crew) {
            $total += count($crew->getBookings());
        }

        return $total;
    }


    protected function getClient()
    {
        return $this->get('synd_multitenant.tenant');
    }

    protected function getBooking($id)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        return $em->getRepository('OrcBookingBundle:Booking')->find($id);
    }

    protected function exitFlow($url)
    {
        $response = $this->redirect($this->get('session')->get('booking_redirect') ?: $url);
        $this->setRedirectUrl(null);

        return $response;
    }

    protected function setRedirectUrl($url)
    {
        $this->get('session')->set('booking_redirect', $url);
    }

    protected function getRedirectUrl($defaultUrl)
    {
        $out = $this->get('session')->get('booking_redirect') ?: $defaultUrl;
        $this->setRedirectUrl(null);
    }
}
