<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\Blackout;
use Orc\BookingBundle\Entity\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlackoutsController extends Controller
{
    /**
     * Lists blackouts
     */
    public function indexAction()
    {
        $repository = $this->get('orc_booking.repository.blackout');

        return $this->render('OrcBookingBundle:Blackouts:index.html.twig', array(
            'current' => $repository->findCurrent(true),
            'upcoming' => $repository->findUpcoming(new DateTime('+3 months'), true)
        ));
    }

    /**
     * Creates a new Blackout
     */
    public function createAction()
    {
        $blackout = new Blackout();

        $form = $this->get('orc_booking.form.blackout');
        $handler = $this->get('orc_booking.form.handler.blackout');

        if ($handler->process($blackout)) {
            $this->get('session')->setFlash('success', 'Added a schedule blackout successfully.');
            return $this->redirect($this->generateUrl('blackouts_admin'));
        }

        return $this->render('OrcBookingBundle:Blackouts:create.html.twig', array(
            'blackout' => $blackout,
            'form' => $form->createView()
        ));
    }

    /**
     * Edit an existing Blackout
     */
    public function editAction(Blackout $blackout)
    {
        $form = $this->get('orc_booking.form.blackout');
        $handler = $this->get('orc_booking.form.handler.blackout');

        if ($handler->process($blackout)) {
            $this->get('session')->setFlash('success', 'Modified the schedule blackout successfully.');
            return $this->redirect($this->generateUrl('blackouts_admin'));
        }

        return $this->render('OrcBookingBundle:Blackouts:edit.html.twig', array(
            'blackout' => $blackout,
            'form' => $form->createView()
        ));
    }

    /**
     * Physically remove a Blackout
     */
    public function deleteAction(Blackout $blackout)
    {
        $form = $this->get('orc_booking.form.delete');
        $handler = $this->get('orc_booking.form.hander.delete');

        if ($handler->process($blackout)) {
            $this->get('session')->setFlash('success', 'Blackout removed successfully.');
            return $this->redirect($this->generateUrl('blackouts_admin'));
        }

        return $this->render('OrcBookingBundle:Blackouts:delete.html.twig', array(
            'blackout' => $blackout,
            'form' => $form->createView()
        ));
    }
}
