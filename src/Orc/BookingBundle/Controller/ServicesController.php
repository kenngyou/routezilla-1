<?php

namespace Orc\BookingBundle\Controller;

use Orc\SaasBundle\Entity\Client;

use Orc\BookingBundle\Entity\Service;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ServicesController extends Controller
{
    /**
     * Lists offered Services
     */
    public function indexAction()
    {
        $repository = $this->get('orc_booking.repository.service');

        return $this->render('OrcBookingBundle:Services:index.html.twig', array(
            'services'   => $repository->findAll()
        ));
    }

    /**
     * Creates a new Service
     */
    public function createAction($wizard = false)
    {
        $service = new Service();

        $form = $this->get('orc_booking.form.service');
        $handler = $this->get('orc_booking.form.handler.service');

        if ($handler->process($service)) {

            if ($wizard) {
                $client = $this->get('synd_multitenant.tenant');
                if ($client->getStatus() != Client::STATUS_STEP_5) {
                    $client->setStatus(Client::STATUS_STEP_4);
                    $em = $this->get('doctrine.orm.entity_manager');
                    $em->persist($client);
                    $em->flush();

                    return $this->redirect($this->generateUrl('setup_step5'));
                }
            }

            $this->get('session')->setFlash('success', 'Service added successfully.');
            return $this->redirect($this->generateUrl('services_admin'));
        }

        return $this->render(
            $wizard ? 'OrcBookingBundle:Wizard:4_service.html.twig' : 'OrcBookingBundle:Services:create.html.twig',
            array(
                'form'    => $form->createView(),
                'service' => $service
            )
        );
    }

    /**
     * Edits an existing Service
     */
    public function editAction(Service $service)
    {
        $form = $this->get('orc_booking.form.service');
        $handler = $this->get('orc_booking.form.handler.service');

        if ($handler->process($service)) {
            $this->get('session')->setFlash('success', 'Service updated successfully.');
            return $this->redirect($this->generateUrl('services_admin'));
        }

        return $this->render('OrcBookingBundle:Services:edit.html.twig', array(
            'form'    => $form->createView(),
            'service' => $service
        ));
    }

    /**
     * Deletes an existing Service
     */
    public function deleteAction(Service $service)
    {
        $form = $this->get('orc_booking.form.delete_service');
        $handler = $this->get('orc_booking.form.handler.delete_service');

        if ($handler->process($service)) {
            $this->get('session')->setFlash('success', 'Service removed successfully.');
            return $this->redirect($this->generateUrl('services_admin'));
        }

        return $this->render('OrcBookingBundle:Services:delete.html.twig', array(
            'form'    => $form->createView(),
            'service' => $service
        ));
    }
}
