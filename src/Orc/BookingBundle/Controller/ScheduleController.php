<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Form\Type\ScheduleFilterType;
use Orc\BookingBundle\Entity\HoursChange;
use Orc\BookingBundle\Entity\DateTime;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ScheduleController extends Controller
{
    public function defaultAction(Request $request, $wizard = false)
    {
        $client = $this->get('synd_multitenant.tenant');
        $form = $this->createForm($this->get('orc_booking.form.type.defaultschedule'), $client);

        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                if ($wizard) {
                    if ($client->getStatus() != Client::STATUS_STEP_5) {
                        $client->setStatus(Client::STATUS_STEP_3);
                    }
                }


                $em = $this->get('doctrine.orm.entity_manager');
                $em->persist($client);
                $em->flush();

                if ($wizard) {
                    return $this->redirect($this->generateUrl('setup_step4'));
                }

                $this->get('session')->setFlash('success', 'Default schedule updated successfully.');
                return $this->redirect($this->generateUrl('settings_hours'));
            }
        }

        return $this->render(
            $wizard ? 'OrcBookingBundle:Wizard:3_hours.html.twig' : 'OrcBookingBundle:Schedule:default.html.twig',
            array(
                'client' => $client,
                'form' => $form->createView(),
                'captions' => array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')
            )
        );
    }


    public function indexAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');
        $form = $this->createForm($this->get('orc_booking.form.type.scheduleexceptions'), $client);

        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $em = $this->get('doctrine.orm.entity_manager');
                $em->persist($client);
                $em->flush();

                $this->get('session')->setFlash('success', 'Schedule change added successfully.');
                return $this->redirect($request->getUri());
            }
        }

        $filter = $this->createForm(new ScheduleFilterType(), $this->getDefaultFilters());
        if ($request->get('schedulefilter')) {
            $filter->bindRequest($request);
        }

        return $this->render('OrcBookingBundle:Schedule:index.html.twig', array(
            'form'       => $form->createView(),
            'client'     => $client,
            'filterForm' => $filter->createView(),
            'defaults'   => json_encode($this->getDefaults($client)),
            'previous'   => http_build_query($this->getPreviousWeek($request->get('schedulefilter'))),
            'next'       => http_build_query($this->getNextWeek($request->get('schedulefilter'))),
            'changes'    => $this->get('orc_booking.repository.hourschange')->findUpcoming(90)
        ));
    }

    protected function getPreviousWeek($current)
    {
        try {
            if ($current) {
                $sunday = new DateTime($current['start']);
                $sunday->modify('-7 days');
            }
        } catch (\Exception $e) { }
        if (empty($sunday)) {
            $sunday = DateTime::getStartOfWeek();
            $sunday->modify('-7 days');
        }

        return $this->prepareStartEnd($sunday);
    }

    protected function getNextWeek($current)
    {
        try {
            if ($current) {
                $sunday = new DateTime($current['start']);
                $sunday->modify('+7 days');
            }
        } catch (\Exception $e) { }
        if (empty($sunday)) {
            $sunday = DateTime::getStartOfWeek();
            $sunday->modify('+7 days');
        }

        return $this->prepareStartEnd($sunday);
    }

    protected function prepareStartEnd(\DateTime $start)
    {
        $end = clone $start;
        $end->modify('+6 days');

        return array('schedulefilter' => array(
            'start' => $start->format('M d, Y'),
            'end' => $end->format('M d, Y')
        ));
    }

    protected function getDefaults(Client $client)
    {
        // XXX move to service or view layer?
        $defaults = array();
        foreach ($client->getHoursOfOperation() as $hoursOfOperation) {
            $defaults[] = sprintf(
                '%d:%d-%d:%d-%d',
                $hoursOfOperation->getStartTime()->format('G'),
                $hoursOfOperation->getStartTime()->format('i'),
                $hoursOfOperation->getEndTime()->format('G'),
                $hoursOfOperation->getEndTime()->format('i'),
                $hoursOfOperation->getOff()
            );
        }

        return $defaults;
    }

    protected function getDefaultFilters()
    {
        $end = new DateTime('today');
        $end->modify('this sunday');
        $start = clone $end;
        $start->modify('-7 days');
        $end->modify('-1 day');

        return array(
            'start' => $start,
            'end' => $end
        );
    }
}
