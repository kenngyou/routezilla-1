<?php

namespace Orc\BookingBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Form\Type\CrewType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CrewsController extends Controller
{
    public function indexAction()
    {
        $repository = $this->get('orc_booking.repository.crew');

        return $this->render('OrcBookingBundle:Crews:index.html.twig', array(
            'crews' => $repository->findAll()
        ));
    }

    /**
     * Creates a new Crew
     */
    public function createAction()
    {
        $crew = new Crew();

        $form = $this->get('orc_booking.form.crew');
        $handler = $this->get('orc_booking.form.handler.crew');

        if ($handler->process($crew)) {
            $this->get('session')->setFlash('success', 'Crew created successfully.');
            return $this->redirect($this->generateUrl('crew_admin'));
        }

        return $this->render('OrcBookingBundle:Crews:create.html.twig', array(
            'form' => $form->createView(),
            'crew' => $crew
        ));
    }

    /**
     * Edits an existing Crew
     */
    public function editAction(Crew $crew)
    {
        $form = $this->get('orc_booking.form.crew');
        $handler = $this->get('orc_booking.form.handler.crew');

        if ($handler->process($crew)) {
            $this->get('session')->setFlash('success', 'Crew updated successfully.');
            return $this->redirect($this->generateUrl('crew_admin'));
        }

        return $this->render('OrcBookingBundle:Crews:edit.html.twig', array(
            'form' => $form->createView(),
            'crew' => $crew
        ));
    }

    /**
     * Deletes an existing Crew
     */
    public function deleteAction(Crew $crew)
    {
        $form = $this->get('orc_booking.form.delete_crew');
        $handler = $this->get('orc_booking.form.handler.delete_crew');

        if ($handler->process($crew)) {
            $this->get('session')->setFlash('success', 'Crew removed successfully.');
            return $this->redirect($this->generateUrl('crew_admin'));
        }

        return $this->render('OrcBookingBundle:Crews:delete.html.twig', array(
            'form' => $form->createView(),
            'crew' => $crew
        ));
    }

    public function resizeRadiusAction(Crew $crew, \DateTime $day)
    {
        $repository = $this->get('orc_booking.repository.crewradius');
        if (!$radius = $repository->findOneBy(array('crew' => $crew, 'day' => $day))) {
            throw $this->createNotFoundException('There is no radius setting there yet.');
        }

        $form = $this->get('orc_booking.form.crewradius');
        $handler = $this->get('orc_booking.form.handler.crewradius');

        if ($handler->process($radius)) {
            return new Response();
        }

        return new Response('', 400);
    }

    public function moveRadiusAction(Crew $crew, \DateTime $day)
    {
        $repository = $this->get('orc_booking.repository.crewradius');
        if (!$radius = $repository->findOneBy(array('crew' => $crew, 'day' => $day))) {
            throw $this->createNotFoundException('There is no radius setting there yet.');
        }

        $form = $this->get('orc_booking.form.crewradius.move');
        $handler = $this->get('orc_booking.form.handler.crewradius.move');

        if ($handler->process($radius)) {
            return new Response();
        }

        return new Response('', 400);
    }
}
