<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingServiceField;
use Orc\BookingBundle\Entity\Customer;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Form\Handler\Booking\TimeHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PublicController extends Controller
{
    /**
     * Get Customer Location
     * Captures a Location entity based on the user's address, populated via Google Maps API
     */
    public function step1Action(Request $request)
    {
        if (strpos($request->server->get('HTTP_HOST'), $this->container->getParameter('orc_saas.login.domain')) === 0) {
            return $this->forward('orc_saas.controller.login:loginAction');
        }

        $booking = $this->getBooking() ?: new Booking();
        $location = $booking->getLocation() ?: new Location();

        $form = $this->get('orc_booking.form.booking.location');
        $handler = $this->get('orc_booking.form.handler.booking.location');

        if ($result = $handler->process($location, $booking)) {
            $this->get('session')->set('booking', $booking->getId());
            $this->get('session')->setFlash('success', 'Location fetched successfully.');

            return $this->redirect($this->generateUrl('book_location_confirm'));

        } else if ($handler->isOutOfBounds()) {
            $this->get('session')->set('booking', $booking->getId());

            return $this->redirect($this->generateUrl('book_location_outofbounds'));

        } else if ($result === false) {
            $this->get('session');
            return $this->redirect($this->generateUrl('book_location', array('unknown' => 1)));
        }

        return $this->render('OrcBookingBundle:Public:location.html.twig', array(
            'form' => $form->createView(),
            'location' => $location,
            'unknown' => $request->query->get('unknown', false)
        ));
    }

    /**
     * If the user's location was rejected, they'll end up here explaining why -> call us.
     */
    public function step1OutOfBoundsAction()
    {
        if (!$booking = $this->getBooking() or !$booking->getLocation()) {
            $this->get('session')->setFlash('warning', 'You must enter your location first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        return $this->render('OrcBookingBundle:Public:location-out-of-bounds.html.twig', array(
            'booking' => $booking,
            'location' => $booking->getLocation()
        ));
    }

    /**
     * Confirm Customer Location
     * After we have a potential Location, we confirm that our assumptions (GMaps data) is correct,
     * offering them to the ability to edit it.
     */
    public function step1ConfirmAction(Request $request)
    {
        if (!$booking = $this->getBooking() or !$booking->getLocation()) {
            $this->get('session')->setFlash('warning', 'You must enter your location first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        $form = $this->get('orc_booking.form.booking.locationconfirm');
        $handler = $this->get('orc_booking.form.handler.booking.locationconfirm');

        if ($handler->process($booking->getLocation(), $booking)) {
            $this->get('session')->setFlash('success', 'Location set successfully.');

            return $this->redirect($this->generateUrl('book_service'));
        }

        return $this->render('OrcBookingBundle:Public:location-confirm.html.twig', array(
            'form' => $form->createView(),
            'location' => $booking->getLocation()
        ));
    }

    /**
     * Get Requested Service
     * Captures an incomplete Booking entity based on the Service + custom fields
     */
    public function step2Action(Request $request)
    {
        if (!$booking = $this->getBooking() or !$booking->getLocation()) {
            $this->get('session')->setFlash('warning', 'You must enter your location first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        $form = $this->get('orc_booking.form.booking.service');
        $handler = $this->get('orc_booking.form.handler.booking.service');

        if ($handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Service set successfully');

            return $this->redirect($this->generateUrl('book_date'));
        }

        $services = array();
        foreach ($this->get('orc_booking.repository.service')->findCustomerFacing(true) as $service) {
            $services[$service->getId()] = $service;
        }

        return $this->render('OrcBookingBundle:Public:service.html.twig', array(
            'booking' => $booking,
            'services' => $services,
            'form' => $form->createView()
        ));
    }

    /**
     * Get Service Fields
     * AJAX Response to render custom service fields
     */
    public function step2FieldsAction(Service $service)
    {
        $booking = $this->getBooking() ?: new Booking();
        $booking->setService($service);

        $form = $this->createForm('booking_service', $booking);

        return $this->render('OrcBookingBundle:Public:service-fields.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Select a Date
     * Captures the day for the booking so we can pick a time and continue
     */
    public function step3Action()
    {
        if (!$booking = $this->getBooking()) {
            $this->get('session')->setFlash('warning', 'You must select a service first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        return $this->render('OrcBookingBundle:Public:date.html.twig', array(
           'days' => array()
        ));
    }

    /**
     * Select a Time (or choose another date)
     */
    public function step3timeAction(\DateTime $day, Request $request)
    {
        if (!$booking = $this->getBooking()) {
            $this->get('session')->setFlash('warning', 'You must select a service first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        $start = clone $day;

        if ($start <= new DateTime('today')) {
            $start = new DateTime('tomorrow');
        }

        $end = clone $day;
        $end->modify('+1 day');

        $dayScheduler = $this->get('orc_booking.schedule.scheduler.day');
        $dayScheduler->setRange($start, $end);

        if (!$dayScheduler->isDayAvailable($this->getClient(), $day, $booking)) {
            $this->get('session')->setFlash('warning', 'Sorry, that day is unavailable.');
            return $this->redirect($this->generateUrl('book_date'));
        }

        $date = $day;
        if ($booking->getDateStart()) {
            $date->setTime(
                $booking->getDateStart()->format('H'),
                $booking->getDateStart()->format('i')
            );
        }

        $booking->setDateStart($date);

        $type = $this->get('orc_booking.form.type.booking.time');
        $form = $this->createForm($type, $booking);
        $handler = new TimeHandler($form, $request, $this->container);

        if ($handler->process($booking)) {
            $this->get('session')->setFlash('success', 'Booking date has been set.');
            return $this->redirect($this->generateUrl('book_contact'));
        }

        return $this->render('OrcBookingBundle:Public:date-time.html.twig', array(
            'day'  => $date,
            'days' => array(),
            'form' => $form->createView()
        ));
    }

    /**
     * Calendar AJAX query for availability
     */
    public function step3QueryAction(Request $request)
    {
        if (!$booking = $this->getBooking()) {
            $this->get('session')->setFlash('warning', 'You must select a service first.');
            return $this->redirect($this->generateUrl('book_location'));
        }

        $input = DateTime::createFromFormat('U', $request->query->get('start'));
        $start = DateTime::createFromFormat('Y-m-d h:ia', $input->format('Y-m-d') . '12:00am');
        $end   = clone $start;
        $end->modify($this->container->getParameter('orc_booking.schedule.calendar.lookahead'));

        if ($start <= new DateTime('today')) {
            $start = new DateTime('tomorrow');
        }

        $dayScheduler = $this->get('orc_booking.schedule.scheduler.day');
        $dayScheduler->setRange($start, $end);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(
            $this->get('orc_booking.twig.extension.calendar')->getDatesAsJsonEvents(
                $dayScheduler->getAvailableDays($this->getClient(), $booking)
            )
        );

        return $response;
    }

    /**
     * Enter Customer Information
     */
    public function step4Action(Request $request)
    {
        if (!$booking = $this->getBooking()) {
            $this->get('session')->setFlash('warning', 'You must select a service first.');
            return $this->redirect($this->generateUrl('book_location'));
        }
        if (!$booking->getDateStart() or !$booking->getDateStart()->format('G')) {
            $this->get('session')->setFlash('warning', 'You must select a date first.');
            return $this->redirect($this->generateUrl('book_date'));
        }

        $customer = new Customer();

        $form = $this->get('orc_booking.form.booking.customer');
        $handler = $this->get('orc_booking.form.handler.booking.customer');

        if ($handler->process($customer, $booking)) {
            $this->get('session')->set('done.booking', $booking->getId());
            $this->get('session')->set('booking', null);
            return $this->redirect($this->generateUrl('book_confirm'));
        }

        return $this->render('OrcBookingBundle:Public:contact.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * Callback request by customer
     */
    public function callbackAction(Request $request)
    {
        if (!$booking = $this->getBooking()) {
            $booking = new Booking();
        }

        $customer = new Customer();

        $form = $this->get('orc_booking.form.booking.callback');
        $handler = $this->get('orc_booking.form.handler.booking.callback');

        if ($handler->process($customer, $booking)) {
            return $this->redirect($this->generateUrl('book_callback_thanks'));
        }

        return $this->render('OrcBookingBundle:Public:callback.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function callbackThanksAction()
    {
        return $this->render('OrcBookingBundle:Public:callback-thanks.html.twig');
    }

    /**
     * All Done!
     * Prints out relevant Booking / Customer information
     */
    public function confirmAction()
    {
        if (!$booking = $this->getBooking('done.booking')) {
            $this->get('session')->setFlash('warning', 'You must select a service first.');
            return $this->redirect($this->generateUrl('book_location'));
        }
        if (!$customer = $booking->getCustomer()) {
            $this->get('session')->setFlash('warning', 'We need to know who you are first.');
            return $this->redirect($this->generateUrl('book_customer'));
        }

        return $this->render('OrcBookingBundle:Public:confirm.html.twig', array(
            'booking' => $booking,
            'customer' => $customer
        ));
    }

    /**
     * Shortcut for getting current Client@
     * @return    Client
     */
    protected function getClient()
    {
        return $this->get('synd_multitenant.tenant');
    }

    /**
     * Fetch a managed Booking referenced from the session
     */
    protected function getBooking($key = 'booking')
    {
        if ($bookingId = $this->get('session')->get($key)) {
            return $this->get('orc_booking.repository.booking')->find($bookingId);
        }
    }

    /**
     * Override render() to prevent caching
     * {@inheritDoc}
     */
    public function render($template, array $parameters = array(), Response $response = null)
    {
        $response = parent::render($template, $parameters);
        $response->headers->set('Cache-Control', 'no-cache, no-store');

        return $response;
    }

    public function offlineAction()
    {
        return $this->render('OrcBookingBundle:Public:offline.html.twig');
    }
}
