<?php

namespace Orc\BookingBundle\Controller;

use Orc\SaasBundle\Entity\Client;

use Orc\BookingBundle\Entity\Region;
use Orc\BookingBundle\Form\Handler\DeleteHandler;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RegionsController extends Controller
{
    /**
     * List regions / map interface
     */
    public function indexAction($wizard = false)
    {
        $repository = $this->get('orc_booking.repository.region');

        return $this->render(
            $wizard ? 'OrcBookingBundle:Wizard:2_region.html.twig' : 'OrcBookingBundle:Regions:index.html.twig',
            array(
                'regions' => $repository->findAll(),
                'apiKey' => $this->container->getParameter('orc_booking.maps.apikey')
            )
        );
    }

    /**
     * Create new Region; Typically called from Map as AJAX request
     */
    public function createAction(Request $request)
    {
        $region = new Region();

        $form = $this->get('orc_booking.form.region');
        $handler = $this->get('orc_booking.form.handler.region');

        if ($handler->process($region)) {
            if ($request->isXmlHttpRequest()) {

                if ($request->request->get('wizard')) {
                    $client = $this->get('synd_multitenant.tenant');
                    if ($client->getStatus() != Client::STATUS_STEP_5) {

                        $client->setTimezone($this->get('orc_booking.timezone_guesser')->getTimezone($region));
                        $client->setStatus(Client::STATUS_STEP_2);
                        $em = $this->get('doctrine.orm.entity_manager');
                        $em->persist($client);
                        $em->flush();
                    }
                }

                return $this->successfulRegionResponse($region);
            }

            $this->get('session')->setFlash('success', 'Region added successfully.');
            return $this->redirect($this->generateUrl('regions_admin'));
        }

        return $this->render('OrcBookingBundle:Regions:create.html.twig', array(
            'form'   => $form->createView(),
            'region' => $region,
        ));
    }

    /**
     * Edit region; Typically called from Map as AJAX request
     */
    public function editAction(Region $region, Request $request)
    {
        $form = $this->get('orc_booking.form.region');
        $handler = $this->get('orc_booking.form.handler.region');

        if ($handler->process($region)) {
            if ($request->isXmlHttpRequest()) {
                return $this->successfulRegionResponse($region);
            }

            $this->get('session')->setFlash('success', 'Region updated successfully.');
            return $this->redirect($this->generateUrl('regions_admin'));
        }

        return $this->render('OrcBookingBundle:Regions:edit.html.twig', array(
            'form'   => $form->createView(),
            'region' => $region
        ));
    }

    /**
     * Delete a Region
     * CSRF protection is disabled since we're working from a map
     */
    public function deleteAction(Region $region, Request $request)
    {
        $form = $this->container->get('orc_booking.form.delete_region');
        $handler = $this->container->get('orc_booking.form.handler.delete_region');

        if ($handler->process($region)) {
            if ($request->isXmlHttpRequest()) {
                return new Response($this->renderRegionsTable());
            }

            $this->get('session')->setFlash('success', 'Region removed successfully.');
            return $this->redirect($this->generateUrl('regions_admin'));
        }

        return $this->render('OrcBookingBundle:Regions:delete.html.twig', array(
            'form'   => $form->createView(),
            'region' => $region
        ));
    }

    /**
     * Grab the user's coordinates based on their IP address
     */
    public function ipLookupAction(Request $request)
    {
        $response = new Response();
        $response->setContent(file_get_contents(sprintf(
            'http://freegeoip.net/%s/%s',
            'json',
            $request->getClientIp()
        )));

        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Return a successful Region response
     * @param    Region
     * @return   Response
     */
    protected function successfulRegionResponse(Region $region)
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent(json_encode(array(
            'id' => $region->getId(),
            'table' => $this->renderRegionsTable()
        )));

        return $response;
    }

    /**
     * Returns the regions table used in AJAX responses
     * @return    string        Rendered Region Table
     */
    protected function renderRegionsTable()
    {
        $repository = $this->get('orc_booking.repository.region');

        return $this->get('templating')->render('OrcBookingBundle:Regions:table.html.twig', array(
            'regions' => $repository->findAll()
        ));
    }
}
