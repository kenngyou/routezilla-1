<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\BlackoutConfiguration;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BlackoutConfigurationsController extends Controller
{
    public function indexAction()
    {
        $repository = $this->get('orc_booking.repository.blackoutconfiguration');

        return $this->render('OrcBookingBundle:BlackoutConfigurations:index.html.twig', array(
            'blackouts' => $repository->findAll()
        ));
    }

    public function createAction()
    {
        $configuration = new BlackoutConfiguration();

        $form = $this->get('orc_booking.form.blackoutconfiguration');
        $handler = $this->get('orc_booking.form.handler.blackoutconfiguration');

        if ($handler->process($configuration)) {
            $this->get('session')->setFlash('success', 'Added a recurring blackout successfully.');
            return $this->redirect($this->generateUrl('blackoutconfigurations_admin'));
        }

        return $this->render('OrcBookingBundle:BlackoutConfigurations:create.html.twig', array(
            'configuration' => $configuration,
            'form' => $form->createView()
        ));

    }

    public function editAction(BlackoutConfiguration $configuration)
    {
        $form = $this->get('orc_booking.form.blackoutconfiguration');
        $handler = $this->get('orc_booking.form.handler.blackoutconfiguration');

        if ($handler->process($configuration)) {
            $this->get('session')->setFlash('success', 'Modified the recurring schedule blackout successfully.');
            return $this->redirect($this->generateUrl('blackoutconfigurations_admin'));
        }

        return $this->render('OrcBookingBundle:BlackoutConfigurations:edit.html.twig', array(
            'configuration' => $configuration,
            'form' => $form->createView()
        ));

    }

    public function deleteAction(BlackoutConfiguration $configuration)
    {
        $form = $this->get('orc_booking.form.delete');
        $handler = $this->get('orc_booking.form.hander.delete');

        if ($handler->process($configuration)) {
            $this->get('session')->setFlash('success', 'Recurring blackout removed successfully.');
            return $this->redirect($this->generateUrl('blackoutconfigurations_admin'));
        }

        return $this->render('OrcBookingBundle:BlackoutConfigurations:delete.html.twig', array(
            'configuration' => $configuration,
            'form' => $form->createView()
        ));

    }
}
