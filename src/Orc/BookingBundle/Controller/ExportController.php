<?php

namespace Orc\BookingBundle\Controller;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Form\Type\ExportReportType;
use Orc\BookingBundle\Request\ExportRequest;
use Orc\BookingBundle\Entity\ExportReport;
use Orc\BookingBundle\Util\Csv;
use Orc\BookingBundle\Util\GetDotted;
use Orc\BookingBundle\Util\Slugify;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ExportController extends Controller
{
    public function indexAction()
    {
        $form = $this->createForm(new ExportReportType(), new ExportReport());
        $reportRepository = $this->get('orc_booking.repository.exportreport');
        return $this->render('OrcBookingBundle:Export:index.html.twig', array(
            'reports' => $reportRepository->findAll(),
            'open' => $this->getLastReportIdToDownload(),
            'form' => $form->createView()
        ));
    }

    public function csvAction(Request $request)
    {
        $export = new ExportReport();
        $form = $this->createForm(new ExportReportType(), $export);
        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($export);
            $em->flush();
            $this->get("session")->setFlash('success', 'Report saved');
            $this->setLastReportIdToDownload($export->getId());
            return $this->redirect($this->generateUrl('export'));
        }

        $this->get("session")->setFlash('error', 'Invalid report request');
        return $this->redirect($this->generateUrl('export'));
    }

    public function csvReportAction(ExportReport $export)
    {
        return $this->respondWithCsv($export);
    }

    public function removeReportAction(ExportReport $export)
    {
        $em = $this->get('doctrine.orm.entity_manager');
        $em->remove($export);
        $em->flush();
        $this->get("session")->setFlash('success', 'Report removed successfully');
        return $this->redirect($this->generateUrl('export'));
    }


    private function respondWithCsv(ExportReport $export)
    {
        $bookingRepository = $this->get('orc_booking.repository.booking');
        $bookings = $bookingRepository->findMatching($this->reportToCriteria($export));

        $cleanName = Slugify::generate($export->getName()) . '-' . date('Y-m-d');

        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', "attachment; filename=$cleanName.csv");
        $response->setContent($this->bookingsToCsv($bookings));
        return $response;
    }

    private function reportToCriteria(ExportReport $export)
    {
        $criteria = array();

        $start = $export->getDateStart() ? $export->getDateStart() : new DateTime('-20 years');
        $end = $export->getDateEnd() ? $export->getDateEnd() : new DateTime('+20 years');

        $criteria['start'] = $start->format(DateTime::ISO8601);
        $criteria['end'] = $end->format(DateTime::ISO8601);

        if ($status = $export->getStatus()) {
            $criteria['status'] = $status;
        }
        if ($crew = $export->getCrew()) {
            $criteria['crew'] = $crew->getId();
        }
        if ($region = $export->getCrew()) {
            $criteria['region'] = $region->getId();
        }
        if ($service = $export->getCrew()) {
            $criteria['service'] = $service->getId();
        }


        return $criteria;
    }

    /**
     * Converts the bookings array to CSV string
     * Needs to handle zero bookings case to provide valid headings
     */
    private function bookingsToCsv(array $bookings)
    {
        if (!count($bookings)) {
            return implode(", ", array_keys($this->bookingToCsvArray(new Booking()))) . "\r\n";
        }

        return Csv::arrayToCsv(
            array_filter(array_map(
                array($this, 'bookingToCsvArray'),
                $bookings
            ))
        );
    }

    /**
     * Convert a booking to the csv dictionary
     * Needs to be extremely error resistent (to handle null booking or broken nested fields)
     */
    private function bookingToCsvArray(Booking $booking)
    {
        try {
            return array(
                'job_date_start'         => $booking->getDateStart() ? $booking->getDateStart()->format(\DateTime::ISO8601) : '',
                'job_date_end'           => $booking->getDateEnd() ? $booking->getDateEnd()->format(\DateTime::ISO8601) : '',
                'job_status'             => BookingStatus::getStatusText($booking->getStatus()),
                'job_notes'              => $booking->getNotes(),
                'job_crew'               => GetDotted::get($booking, 'crew.name'),
                'job_region'             => GetDotted::get($booking, 'region.name'),
                'job_poster'             => GetDotted::get($booking, 'user.name', 'public'),
                'service_name'           => GetDotted::get($booking, 'service.name'),
                'service_additional'     => $booking->getAdditionalInformation(),
                'contact_name'           => GetDotted::get($booking, 'customer.name'),
                'contact_email'          => GetDotted::get($booking, 'customer.email'),
                'contact_phone'          => GetDotted::get($booking, 'customer.phone'),
                'contact_address'        => GetDotted::get($booking, 'customer.location.street'),
                'contact_address2'       => GetDotted::get($booking, 'customer.location.line2'),
                'contact_city'           => GetDotted::get($booking, 'customer.location.city'),
                'contact_code'           => GetDotted::get($booking, 'customer.location.code'),
                'contact_state_province' => GetDotted::get($booking, 'customer.location.province'),
                'contact_country'        => GetDotted::get($booking, 'customer.location.country'),
                'billing_name'           => GetDotted::get($booking, 'customer.billing.name'),
                'billing_email'          => GetDotted::get($booking, 'customer.billing.email'),
                'billing_phone'          => GetDotted::get($booking, 'customer.billing.phone'),
                'billing_address'        => GetDotted::get($booking, 'customer.billing.location.street'),
                'billing_address2'       => GetDotted::get($booking, 'customer.billing.location.line2'),
                'billing_city'           => GetDotted::get($booking, 'customer.billing.location.city'),
                'billing_code'           => GetDotted::get($booking, 'customer.billing.location.code'),
                'billing_state_province' => GetDotted::get($booking, 'customer.billing.location.province'),
                'billing_country'        => GetDotted::get($booking, 'customer.billing.location.country'),
                'billed'                 => $booking->getBilled() ? 'yes' : 'no'
            );
        } catch (\Exception $e) {
            $this->get('logger')->info($e);
            return null;
        }
    }

    private function setLastReportIdToDownload($id)
    {
        $this->get('session')->set('open_report', $id);
    }

    private function getLastReportIdToDownload()
    {
        $session = $this->get('session');
        if ($id = $session->get('open_report')) {
            $session->set('open_report', null);
        }
        return $id;
    }

}
