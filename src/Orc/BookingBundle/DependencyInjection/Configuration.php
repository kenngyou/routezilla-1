<?php

namespace Orc\BookingBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('orc_booking');

        $rootNode
            ->children()
                ->scalarNode('maps_api_key')
                    ->info('The Google Maps API key (browser key)')
                    ->example('t3b65dBqTy-BojfslKJLisjLKZJ3JD03j2ijNJs')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->validate()
                        ->ifTrue(function($v) { return $v == 'unauthorized' || $v == 'xxx'; })
                        ->then(function($v) {
                            throw new \RuntimeException("The google maps API key is invalid.");
                        })
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
