<?php

namespace Orc\BookingBundle\DependencyInjection;

use Orc\BookingBundle\DependencyInjection\Configuration;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;

class OrcBookingExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processCOnfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        foreach (array('schedule.yml', 'services.yml', 'repositories.yml', 'forms.yml', 'mail.yml') as $file) {
            $loader->load($file);
        }

        $container->setParameter('orc_booking.maps.apikey', $config['maps_api_key']);
    }
}
