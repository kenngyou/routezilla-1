<?php

namespace Orc\BookingBundle\Twig\Extension;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\GanttSlot;
use Orc\BookingBundle\Entity\DateTime;
use Doctrine\Common\Collection\ArrayCollection;

class GanttExtension extends \Twig_Extension
{
    protected $interval;

    public function __construct($interval)
    {
        $this->interval = $interval;
    }

    public function getFunctions()
    {
        return array(
            'gantt_times' => new \Twig_Function_Method($this, 'getTimes'),
            'gantt_slots' => new \Twig_Function_Method($this, 'getBookingSlots')
        );
    }

    /**
     * Generates the times to be used as headings in the Gantt chart
     * (start of day to end of day)
     *
     * @return    DateTime[]
     */
    public function getTimes(\DateTime $date = null)
    {
        $start = $date ?: new DateTime('today');
        $end = clone $start;
        $end->modify('+24 hours');

        $slots = array();

        for ($i = clone $start; $i < $end; $i->modify(sprintf('%d minutes', $this->interval))) {
            $slots[] = clone $i;
        }

        return $slots;
    }

    /**
     * Generates GanttSlot's to be used within the Gantt chart.
     * One slot per hour is returned, except if Bookings span multiple hours,
     * then the slots are combined.
     *
     * @param    ArrayCollection        Bookings to be represented
     * @return   GanttSlot[]            Slots potentially containing bookings
     */
    public function getBookingSlots($bookings = array(), \DateTime $date)
    {
        $out = array();
        $shown = array();

        foreach ($this->getTimes($date) as $time) {
            $slot = new GanttSlot($this->interval);
            $slot->setTime($time);

            foreach ($bookings as $booking) {
                $nextTime = clone $time;
                $nextTime->modify(sprintf('%d minutes', $this->interval));

                if ($this->isOverlapping($booking->getDateStart(), $booking->getDateEnd(), $time, $nextTime)) {
                    if (in_array($booking, $shown)) {
                        continue 2;
                    }

                    $shown[] = $booking;
                    $slot->setBooking($booking);
                }
            }

            $out[] = $slot;
        }

        return $out;
    }

    /**
     * Checks to see if two date ranges overlap
     * @param    DateTime (A start)
     * @param    DateTime (A end)
     * @param    DateTime (B start)
     * @param    DateTime (B end)
     * @return   booelan    true if they overlap
     */
    protected function isOverlapping(\DateTime $a1, \DateTime $a2, \DateTime $b1, \DateTime $b2)
    {
        return $a1 < $b2 && $a2 > $b1;
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'orc_booking_gantt';
    }
}
