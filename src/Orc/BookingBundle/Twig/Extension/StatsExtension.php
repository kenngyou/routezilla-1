<?php

namespace Orc\BookingBundle\Twig\Extension;

use Orc\BookingBundle\Entity\BookingStatus;
use Orc\SaasBundle\Form\Data\AnalyticsFilter;

class StatsExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'to_status' => new \Twig_Filter_Method($this, 'toStatus'),
        );
    }

    public function getFunctions()
    {
        return array(
            'conversion_totals' => new \Twig_Function_Method($this, 'getTotals'),
            'conversion_legends' => new \Twig_Function_Method($this, 'getLegends')
        );
    }

    public function getTotals($data, AnalyticsFilter $filter, $key)
    {
        if (!$data) {
            return '[]';
        }

        foreach ($data as $row) {
            $out['attempts'][] = array($row['m'], $row['attempts']);
            $out['converted'][] = array($row['m'], $row['converted']);
            $out['total'][] = array($row['m'], $row['total']);
        }

        return $out[$key];
    }

    public function getLegends($data, AnalyticsFilter $filter)
    {
        $months =  array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'May',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec'
        );

        $days = array(
            6 => 'Sun',
            0 => 'Mon',
            1 => 'Tue',
            2 => 'Wed',
            3 => 'Thu',
            4 => 'Fri',
            5 => 'Sat'
        );

        $out = array();
        foreach ($data as $row) {
            if ($filter->getGroupBy() == 'month') {
                $out[] = array($row['m'], $months[$row['m']]);
            } elseif ($filter->getGroupBy() == 'day') {
                $out[] = array($row['m'], $days[$row['m']]);
            } else {
                $out[] = array($row['m'], $row['m']);
            }
        }

        return $out;
    }

    public function toStatus($status)
    {
        return BookingStatus::getStatusText($status);
    }


    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        return 'orc_booking_stats';
    }
}
