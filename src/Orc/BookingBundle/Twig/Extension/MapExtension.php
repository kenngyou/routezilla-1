<?php

namespace Orc\BookingBundle\Twig\Extension;

use Orc\BookingBundle\Util\ColorGenerator;

class MapExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'regions_json' => new \Twig_Filter_Method($this, 'getRegionsAsJson', array('is_safe' => array('html'))),
            'crews_json' => new \Twig_Filter_Method($this, 'getBookingsAsJson', array('is_safe' => array('html'))),
            'unassigned_json' => new \Twig_Filter_Method($this, 'getUnassignedBookingsAsJson', array('is_safe' => array('html')))
        );
    }

    /**
     * Return regions in JSON format for map usage
     * @param    Orc\BookingBundle\Entity\Region[]
     * @return   string        JSON representation
     */
    public function getRegionsAsJson(array $regions)
    {
        $data = array();

        foreach ($regions as $region) {
            $locations = array();
            foreach ($region->getBoundaries() as $boundary) {
                if ($boundary->getLocation()) {
                    $locations[] = array(
                        'lat' => $boundary->getLocation()->getLatitude(),
                        'lng' => $boundary->getLocation()->getLongitude()
                    );
                }
            }

            $data[$region->getId()] = array('name' => $region->getName(), 'boundaries' => $locations);
        }

        return json_encode($data);
    }

    /**
     * Returnss bookings/crews in JSON format for map usage
     * @param    Orc\BookingBundle\Entity\Crew[]
     * @return   string        JSON representation
     */
    public function getBookingsAsJson(array $bookingsAsCrews)
    {
        $data = array();

        foreach ($bookingsAsCrews as $crew) {
            $crewData = array(
                'id'       => $crew->getId(),
                'name'     => $crew->getName(),
                'color'    => (string)$crew->getColor(),
                'bookings' => array()
            );

            $bookings = array();
            foreach ($crew->getBookings() as $booking) {
                if ($radius = $booking->getRadius()) {
                    $crewData['radius'] = array(
                        'id'       => $radius->getId(),
                        'distance' => $radius->getRadius(),
                        'location' => array(
                            'lat' => $radius->getLocation()->getLatitude(),
                            'lng' => $radius->getLocation()->getLongitude()
                        )
                    );
                }

                $crewData['bookings'][$booking->getId()] = array(
                    'id'       => $booking->getId(),
                    'name'     => $booking->getName(),
                    'start'    => $booking->getDateStart()->format('Y-m-d'),
                    'image'    => sprintf(
                        '//chart.googleapis.com/chart?chst=d_map_pin_letter&chld=%s|%s|%s',
                        $booking->getDateStart()->format('g'),
                        substr($crew->getColor(), 1),
                        ColorGenerator::getForeground($crew->getColor())
                    ),
                    'location' => array(
                        'lat'     => $booking->getLocation()->getLatitude(),
                        'lng'     => $booking->getLocation()->getLongitude(),
                        'address' => $booking->getLocation()->getAddress()
                    ),
                    'color' => (string)$crew->getColor()
                );
            }

            $data[$crew->getId()] = $crewData;
        }

        return json_encode($data);
    }

    public function getUnassignedBookingsAsJson(array $bookings)
    {
        $out = array();
        foreach ($bookings as $booking) {
            $out[$booking->getId()] = array(
                'id' => $booking->getId(),
                'name' => $booking->getName(),
                'start' => $booking->getDateStart()->format('Y-m-d'),
                'image'    => sprintf(
                    '//chart.googleapis.com/chart?chst=d_map_pin_letter&chld=%s|%s|%s',
                    $booking->getDateStart()->format('g'),
                    "000000",
                    "FFFFFF"
                ),
                'location' => array(
                    'lat'     => $booking->getLocation()->getLatitude(),
                    'lng'     => $booking->getLocation()->getLongitude(),
                    'address' => $booking->getLocation()->getAddress()
                )
            );

        }

        return json_encode($out);
    }

    public function getName()
    {
        return 'orc_booking_map';
    }
}
