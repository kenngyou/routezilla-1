<?php

namespace Orc\BookingBundle\Twig\Extension;

class CalendarExtension extends \Twig_Extension
{
    protected $calendarDateFormat;

    public function __construct($calendarDateFormat)
    {
        $this->calendarDateFormat = $calendarDateFormat;
    }

    public function getFilters()
    {
        return array(
            'timeShort' => new \Twig_Filter_Method($this, 'conditionalDate'),
            'date_today' => new \Twig_Filter_Method($this, 'todayDate'),
            'modify' => new \Twig_Filter_Method($this, 'modifyDate'),
            'to_date' => new \Twig_Filter_Method($this, 'toDate'),
            'json_calendar' => new \Twig_Filter_Method($this, 'getDatesAsJsonEvents', array('is_safe' => array('html'))),
            'json_booked_calendar' => new \Twig_Filter_Method($this, 'getBookedDaysAsJsonEvents', array('is_safe' => array('html')))
        );
    }

    public function todayDate(\DateTime $date, $short = 'g:ia', $long = 'Y-m-d g:ia')
    {
        $today = new \DateTime();
        $format = $today->format('Y-m-d') == $date->format('Y-m-d') ? $short : $long;
        return $date->format($format);
    }

    public function toDate($date, $format = null)
    {
        if ($format) {
            return \DateTime::createFromFormat($format, $date);
        }

        return new \DateTime($date);
    }

    public function conditionalDate(\DateTime $date)
    {
        if ($date->format('i') > 0) {
            return $date->format('g:ia');
        }

        return $date->format('ga');
    }

    public function modifyDate($in, $modification)
    {
        if (!$in instanceof \DateTime) {
            $in = new \DateTime($in);
        }

        $date = clone $in;
        $date->modify($modification);

        return $date;
    }

    public function getDatesAsJsonEvents(array $dates, $title = 'Available')
    {
        $out = array();
        foreach ($dates as $day) {
            $out[] = array(
                'start' => $day->format($this->calendarDateFormat),
                'title' => $title
            );
        }

        return json_encode($out);
    }

    public function getBookedDaysAsJsonEvents(array $days)
    {
        $out = array();
        foreach ($days as $day) {
            $out[] = array(
                'start' => $day->getDate()->format($this->calendarDateFormat),
                'title' => sprintf("%d Bookings", $day->getBookings())
            );
        }

        return json_encode($out);
    }

    public function getName()
    {
        return 'orc_booking_calendar';
    }
}
