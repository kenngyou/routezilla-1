<?php

namespace Orc\BookingBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Chronological extends Constraint
{
    public $chronological = 'The booking end time must be before the start time.';

    public $sameDay = 'The booking must end on the same day it starts.';

    public $interval = 'The time must fall on %interval% minute intervals.';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return 'validate_chronological';
    }
}
