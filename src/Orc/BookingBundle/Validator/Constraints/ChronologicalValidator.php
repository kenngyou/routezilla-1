<?php

namespace Orc\BookingBundle\Validator\Constraints;

use Orc\BookingBundle\Entity\Booking;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ChronologicalValidator extends ConstraintValidator
{
    protected $interval;

    public function __construct($interval)
    {
        $this->interval = $interval;
    }

    public function isValid($booking, Constraint $constraint)
    {
        if ($booking->getDateStart() >= $booking->getDateEnd()) {
            $this->setMessage($constraint->chronological);
            return false;
        }

        if ($booking->getDateStart()->format('Y-m-d') != $booking->getDateEnd()->format('Y-m-d')) {
            $this->setMessage($constraint->sameDay);
            return false;
        }

        if ($booking->getDateStart()->format('i') % $this->interval or $booking->getDateEnd()->format('i') % $this->interval) {
            $this->setMessage($constraint->interval, array('%interval%' => $this->interval));
            return false;
        }

        return true;
    }
}
