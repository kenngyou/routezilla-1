<?php

namespace Orc\BookingBundle\Tests\Schedule;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Schedule\CrewEnquirer;
use Orc\SaasBundle\Entity\Client;

class CrewEnquirerTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        DateTime::setDefault(date('Y-m-d'));
    }

    public function testReturnsPastWhenInPast()
    {
        $enquirer = new CrewEnquirer(
            $scheduler = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $overrider = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $calculator = $this->getMockBuilder('Orc\BookingBundle\Booking\EndTimeCalculator')->disableOriginalConstructor()->getMock()
        );

        $crew = new Crew();
        $slot = new DateTime();
        $booking = new Booking();
        $booking->setDateStart(new DateTime('2012-01-01'));
        $client = new Client();

        $this->assertEquals(CrewEnquirer::STATUS_PASTDATE, $enquirer->getCrewStatus($crew, $slot, $booking, $client));
    }

    public function testReturnsUnavailableWhenCrewIsUnavailable()
    {
        $enquirer = new CrewEnquirer(
            $scheduler = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $overrider = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $calculator = $this->getMockBuilder('Orc\BookingBundle\Booking\EndTimeCalculator')->disableOriginalConstructor()->getMock()
        );

        $crew = new Crew();
        $slot = new DateTime();
        $booking = new Booking();
        $booking->setDateStart(new DateTime('+1 day'));
        $client = new Client();

        $overrider
            ->expects($this->once())
            ->method('isCrewAvailableAt')
            ->with($crew, $slot, $booking, $client)
            ->will($this->returnValue(false));

        $this->assertEquals(CrewEnquirer::STATUS_UNAVAILABLE, $enquirer->getCrewStatus($crew, $slot, $booking, $client));
    }

    public function testReturnsDiscouragedWhenUnavailable()
    {
        $enquirer = new CrewEnquirer(
            $scheduler = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $overrider = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $calculator = $this->getMockBuilder('Orc\BookingBundle\Booking\EndTimeCalculator')->disableOriginalConstructor()->getMock()
        );

        $crew = new Crew();
        $slot = new DateTime();
        $booking = new Booking();
        $booking->setDateStart(new DateTime('+1 day'));
        $client = new Client();

        $overrider
            ->expects($this->once())
            ->method('isCrewAvailableAt')
            ->with($crew, $slot, $booking, $client)
            ->will($this->returnValue(true));

        $scheduler
            ->expects($this->once())
            ->method('isCrewAvailableAt')
            ->with($crew, $slot, $booking, $client)
            ->will($this->returnValue(false));


        $this->assertEquals(CrewEnquirer::STATUS_DISCOURAGED, $enquirer->getCrewStatus($crew, $slot, $booking, $client));
    }

    public function testReturnsAvailableWhenAvailable()
    {
        $enquirer = new CrewEnquirer(
            $scheduler = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $overrider = $this->getMock('Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface'),
            $calculator = $this->getMockBuilder('Orc\BookingBundle\Booking\EndTimeCalculator')->disableOriginalConstructor()->getMock()
        );

        $crew = new Crew();
        $slot = new DateTime();
        $booking = new Booking();
        $booking->setDateStart(new DateTime('+1 day'));
        $client = new Client();

        $overrider
            ->expects($this->once())
            ->method('isCrewAvailableAt')
            ->with($crew, $slot, $booking, $client)
            ->will($this->returnValue(true));

        $scheduler
            ->expects($this->once())
            ->method('isCrewAvailableAt')
            ->with($crew, $slot, $booking, $client)
            ->will($this->returnValue(true));

        $this->assertEquals(CrewEnquirer::STATUS_AVAILABLE, $enquirer->getCrewStatus($crew, $slot, $booking, $client));
    }
}
