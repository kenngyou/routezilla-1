<?php

namespace Orc\BookingBundle\Tests\Schedule;

use Orc\BookingBundle\Schedule\TimeSlotGenerator;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\HoursChange;
use Orc\BookingBundle\Entity\HoursOfOperation;
use Orc\SaasBundle\Entity\Client;

class TimeSlotGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function test9to5WithHourIncrementsGenerates8Slots()
    {
        $client = $this->getDefaultClient();
        $generator = new TimeSlotGenerator(60);

        $slots = $generator->getTimeSlots($client, new DateTime('june 1'), new DateTime('june 2'));

        $this->assertEquals(8, count($slots));
    }

    public function test9to5WithHalfHourIncrementsGenerates16Slots()
    {
        $client = $this->getDefaultClient();
        $generator = new TimeSlotGenerator(30);

        $slots = $generator->getTimeSlots($client, new DateTime('june 1'), new DateTime('june 2'));

        $this->assertEquals(16, count($slots));
    }

    public function testReturnedTimesAreCorrect()
    {
        $client = $this->getDefaultClient();
        $generator = new TimeSlotGenerator(60);

        $slots = $generator->getTimeSlots($client, new DateTime('june 1'), new DateTime('june 2'));

        for ($i = 0; $i < 8; $i++) {
            $hour = $i + 9;
            $this->assertEquals(new DateTime("june 1 $hour:00:00"), $slots[$i]);
        }
    }

    public function testReturnedDateTimesAreCorrect()
    {
        $client = $this->getDefaultClient();
        $generator = new TimeSlotGenerator(60);

        $slots = $generator->getTimeSlots($client, new DateTime('june 1'), new DateTime('june 10'));

        for ($j = 0, $day = 1; $day < 10; $day++) {
            for ($i = 0; $i < 8; $i++, $j++) {
                $hour = $i + 9;
                $this->assertEquals(new DateTime("june $day $hour:00:00"), $slots[$j]);
            }
        }
    }

    public function testDaysOffAreRemoved()
    {
        $client = $this->getDefaultClient();
        $days = $client->getHoursOfOperation();
        $days[0]->setOff(true); // sunday
        $days[6]->setOff(true); // saturday

        $generator = new TimeSlotGenerator(60);

        $slots = $generator->getTimeSlots($client, new DateTime('sunday'), new DateTime('sunday +7 days'));
        $days  = array_chunk($slots, 8);

        $expectedDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');

        $this->assertCount(5, $days);
        foreach ($days as $i => $hours) {
            $this->assertEquals($expectedDays[$i], $hours[0]->format('l'));
        }
    }

    public function testExceptionsAreRemoved()
    {
        // mark sunday as a 5-5 day
        $workSunday = new HoursChange();
        $workSunday->setOff(false);
        $workSunday->setDay(new DateTime('sunday'));
        $workSunday->setStartTime(new DateTime('5am'));
        $workSunday->setEndTime(new DateTime('5pm'));

        $client = $this->getDefaultClient();
        $client->addHoursChange($workSunday);
        $days = $client->getHoursOfOperation();
        $days[0]->setOff(true);
        $days[6]->setOff(true);


        $generator = new TimeSlotGenerator(60);
        $slots = $generator->getTimeSlots($client, new DateTime('sunday'), new DateTime('sunday +7 days'));

        foreach (array_slice($slots, 0, 12) as $i => $slot) {
            $this->assertEquals('Sunday', $slot->format('l'));
            $this->assertEquals(5 + $i, $slot->format('G'));
        }
        $slots = array_slice($slots, 12);
        $days = array_chunk($slots, 8);

        $expectedDays = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');

        $this->assertCount(5, $days);
        foreach ($days as $i => $hours) {
            $this->assertEquals($expectedDays[$i], $hours[0]->format('l'));
        }
    }

    protected function getDefaultClient()
    {
        $client = new Client();

        foreach ($this->getHoursOfOperation() as $hoursOfOperation) {
            $client->addHoursOfOperation($hoursOfOperation);
        }

        return $client;
    }


    protected function getHoursOfOperation($dayConfig = null)
    {
        if ($dayConfig === null) {
            $dayConfig = $this->getDefaultHourConfig();
        }

        $days = array();
        foreach ($dayConfig as $day) {
            $hours = new HoursOfOperation();
            $hours->setStartTime(new DateTime(sprintf('%s:00:00', $day['start'])));
            $hours->setEndTime(new DateTime(sprintf('%s:00:00', $day['end'])));
            $hours->setOff($day['off']);
            $days[] = $hours;
        }

        return $days;
    }

    protected function getDefaultHourConfig()
    {
        $defaults = array_fill(0, 7, array(
            'start' => 9,
            'end' => 17,
            'off' => false
        ));

        return $defaults;
    }
}
