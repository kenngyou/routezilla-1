<?php

namespace Orc\BookingBundle\Tests\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Orc\BookingBundle\EventListener\FlashInvalidDate;

class FlashInvalidDateTest extends \PHPUnit_Framework_TestCase
{
    public function testFlashAndRedirectIfExceptionIsDateTime()
    {
        $request = new Request();
        $request->server->set('HTTP_REFERER', $url = '/last-page');

        $event = new GetResponseForExceptionEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            $request,
            'type',
            new \Exception('DateTime::__construct() error')
        );

        $flashes = $this->getMock('Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface');
        $flashes
            ->expects($this->once())
            ->method('add')
            ->with('error', 'Could not understand date')
        ;

        $listener = new FlashInvalidDate($flashes);
        $listener->onKernelException($event);

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
        $this->assertEquals($url, $event->getResponse()->getTargetUrl());
    }

    public function testRedirectsToRootIfNoReferer()
    {
        $request = new Request();

        $event = new GetResponseForExceptionEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            $request,
            'type',
            new \Exception('DateTime::__construct() error')
        );

        $flashes = $this->getMock('Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface');
        $flashes
            ->expects($this->once())
            ->method('add')
            ->with('error', 'Could not understand date')
        ;

        $listener = new FlashInvalidDate($flashes);
        $listener->onKernelException($event);

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
        $this->assertEquals('/', $event->getResponse()->getTargetUrl());
    }

    public function testRedirectsToDashboardIfNoRefererAndInAdminArea()
    {
        $request = Request::create('/dashboard/some-sub-page');

        $event = new GetResponseForExceptionEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            $request,
            'type',
            new \Exception('DateTime::__construct() error')
        );

        $flashes = $this->getMock('Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface');
        $flashes
            ->expects($this->once())
            ->method('add')
            ->with('error', 'Could not understand date')
        ;

        $listener = new FlashInvalidDate($flashes);
        $listener->onKernelException($event);

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
        $this->assertEquals('/dashboard', $event->getResponse()->getTargetUrl());
    }

    public function testDoesNothingOtherwise()
    {
        $event = new GetResponseForExceptionEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            new Request(),
            'type',
            new \Exception('Some other exception')
        );

        $flashes = $this->getMock('Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface');

        $listener = new FlashInvalidDate($flashes);
        $listener->onKernelException($event);

        $this->assertNull($event->getResponse());
    }
}
