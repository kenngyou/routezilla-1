<?php

namespace Orc\BookingBundle\Tests\Booking;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingServiceField;
use Orc\BookingBundle\Entity\Service;
use Orc\BookingBundle\Entity\ServiceField;
use Orc\BookingBundle\Booking\EndTimeCalculator;

class EndTimeCalculatorTest extends \PHPUnit_Framework_TestCase
{
    public function testReuseCurrentDurationWhenEndTimeExists()
    {
        $originalDate = new \DateTime('2012-01-01 9:00');

        $booking = new Booking();
        $booking->setDateStart(new \DateTime('2012-01-01 10:00'));
        $booking->setDateEnd(new \DateTime('2012-01-01 10:00'));

        $calculator = new EndTimeCalculator(120);
        $endDate = $calculator->getEndTime($booking, new \DateTime('2012-01-01 9:00'));

        $this->assertEquals(new \DateTime('2012-01-01 11:00'), $endDate);
    }

    public function testUseServiceLength()
    {
        $booking = new Booking();
        $booking->setService($service = new Service());
        $booking->setDateStart(new \DateTime('2012-01-01 9:00'));
        $service->setBaseTime(60);

        $calculator = new EndTimeCalculator(60);
        $end = $calculator->getEndTime($booking);

        $this->assertEquals(new \DateTime('2012-01-01 10:00'), $end);
    }

    public function testUseServiceLengthPlusFields()
    {
        $service = new Service();
        $service->setBaseTime(60);

        $fields = array();
        $config = array('a' => 30, 'b' => 30);

        foreach ($config as $name => $minutes) {
            $field = new ServiceField();
            $field->setType(ServiceField::TYPE_NUMERIC);
            $field->setAddMinutes($minutes);
            $field->setSlug($name);

            $fields[$name] = $field;
            $service->addServiceField($field);
        }

        $booking = new Booking();
        $booking->setService($service);
        $booking->setDateStart(new \DateTime('2012-01-01 9:00'));

        foreach ($config as $name => $minutes) {
            $field = new BookingServiceField();
            $field->setField($fields[$name]);
            $field->setAnswer(2);

            $booking->addField($field);
        }

        $calculator = new EndTimeCalculator(60);
        $end = $calculator->getEndTime($booking);

        // 60 + (2 * 30) + (2 * 30) = 3 hours
        $this->assertEquals(new \DateTime('2012-01-01 12:00'), $end);
    }

    public function testIgnoreFreeServiceQuestions()
    {
        $service = new Service();
        $service->setBaseTime(60);

        $fields = array();

        for ($i = 0; $i < 10; $i++) {
            $field = new ServiceField();
            $field->setType(ServiceField::TYPE_FREE);
            $field->setSlug($slug = md5(uniqid()));

            $fields[$slug] = $field;
            $service->addServiceField($field);
        }

        $booking = new Booking();
        $booking->setService($service);
        $booking->setDateStart(new \DateTime('2012-01-01 9:00'));

        foreach ($fields as $slug => $serviceField) {
            $field = new BookingServiceField();
            $field->setField($fields[$slug]);
            $field->setAnswer('sdfasdfdf');

            $booking->addField($field);
        }

        $calculator = new EndTimeCalculator(60);
        $end = $calculator->getEndTime($booking);

        $this->assertEquals(new \DateTime('2012-01-01 10:00'), $end);

    }

    public function testPadMinutesToNextInterval()
    {
        $service = new Service();
        $service->setBaseTime(60);

        $field = new ServiceField();
        $field->setType(ServiceField::TYPE_NUMERIC);
        $field->setAddMinutes(15);
        $field->setSlug('sluggyslugslug');
        $service->addServiceField($field);

        $booking = new Booking();
        $booking->setService($service);
        $booking->setDateStart(new \DateTime('2012-01-01 9:00'));
        $booking->addField($answer = new BookingServiceField());

        $answer->setField($field);
        $answer->setAnswer(1);

        $calculator = new EndTimeCalculator(60);
        $end = $calculator->getEndTime($booking);

        // 60 + 15, pad to 120
        $this->assertEquals(new \DateTime('2012-01-01 10:00'), $end);

    }
}
