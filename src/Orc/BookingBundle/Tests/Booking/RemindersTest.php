<?php

namespace Orc\BookingBundle\Tests\Booking;

use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Booking\Reminders;

class RemindersTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        DateTime::setDefault('2013-01-01 02:15:00');
    }

    public function tearDown()
    {
        DateTime::setDefault('now');
    }

    public function testSendsFoundTomorrow()
    {
        DateTime::setDefault('2013-01-01 15:00:00');

        $bookings = array($booking = new Booking());
        $booking->setClient($client = new Client());
        $booking->setDateStart(new DateTime('+1 day'));
        $client->setTimezone('America/Vancouver');

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findAllUpcoming')
            ->with(3)
            ->will($this->returnValue($bookings))
        ;

        $reminders = new Reminders($repository, 15);
        $bookings = $reminders->findRemindable();

        $this->assertSame($booking, $bookings[0]);
    }

    public function testDoesntSendOnWrongDay()
    {
        DateTime::setDefault('2013-01-01 15:00:00');

        $bookings = array($booking = new Booking());
        $booking->setClient($client = new Client());
        $booking->setDateStart(new DateTime('+2 days'));
        $client->setTimezone('America/Vancouver');

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findAllUpcoming')
            ->with(3)
            ->will($this->returnValue($bookings))
        ;

        $reminders = new Reminders($repository, 15);
        $bookings = $reminders->findRemindable();

        $this->assertCount(0, $bookings);
    }

    public function testOnlySendsAtCorrectTime()
    {
        DateTime::setDefault('2013-01-01 02:00:00');

        $bookings = array($booking = new Booking());
        $booking->setClient($client = new Client());
        $booking->setDateStart(new DateTime('+1 day'));
        $client->setTimezone('America/Vancouver');

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findAllUpcoming')
            ->with(3)
            ->will($this->returnValue($bookings))
        ;

        $reminders = new Reminders($repository, 15);
        $bookings = $reminders->findRemindable();

        $this->assertCount(0, $bookings);
    }

    public function testHonorsTimezonesNotMatchingSystem()
    {
        // server thinks its 12pm
        DateTime::setDefault('2013-01-01 12:00:00');

        // it's 3pm in client's time
        $bookings = array($booking = new Booking());
        $booking->setClient($client = new Client());
        $booking->setDateStart(new DateTime('+1 day'));
        $client->setTimezone('America/New_York');

        $repository = $this->getMockBuilder('Orc\BookingBundle\Repository\BookingRepository')->disableOriginalConstructor()->getMock();
        $repository
            ->expects($this->once())
            ->method('findAllUpcoming')
            ->with(3)
            ->will($this->returnValue($bookings))
        ;

        $reminders = new Reminders($repository, 15);
        $bookings = $reminders->findRemindable();

        $this->assertSame($booking, $bookings[0]);
    }
}
