<?php

namespace Orc\BookingBundle\Tests\Booking;

use Orc\BookingBundle\Booking\BlackoutGenerator;
use Orc\BookingBundle\Entity\BlackoutConfiguration;

class BlackoutGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testDaily()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-01-31'));
        $configuration->setFrequency('daily');

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array_map(
                function($date) { return sprintf("2012-01-%02d", $date); },
                range(1, 31)
            ),
            array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            )
        );
    }

    public function testEverySecondDay()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-01-31'));
        $configuration->setFrequency('daily');
        $configuration->setFrequencyMultiplier(2);

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array_values(array_map(
                function($date) { return sprintf("2012-01-%02d", $date); },
                array_filter(range(1, 31), function($i ) { return $i %2 == 0; })
            )),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testWeekly()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-01-31'));
        $configuration->setFrequency('weekly');
        $configuration->setDaysOfWeek(array(3));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array_map(
                function($date) { return sprintf("2012-01-%02d", $date); },
                array(4, 11, 18, 25)
            ),
            array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            )
        );
    }

    public function testEveryThreeWednesdays()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-02-28'));
        $configuration->setFrequency('weekly');
        $configuration->setFrequencyMultiplier(3);
        $configuration->setDaysOfWeek(array(3));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array(
                "2012-01-18",
                "2012-02-08"
            ),
            array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            )
        );
    }

    public function testDayOfMonth()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-07-01'));
        $configuration->setFrequency('monthly');
        $configuration->setDaysOfMonth(array(15));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array_map(
                function($month) {
                    return "2012-0$month-15";
                },
                range(1, 6)
            ),
            array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            )
        );
    }

    public function testEverySecond15th()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-07-01'));
        $configuration->setFrequency('monthly');
        $configuration->setFrequencyMultiplier(2);
        $configuration->setDaysOfMonth(array(15));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array("2012-02-15", '2012-04-15', '2012-06-15'),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testWednesdayOfSecondMonth()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setName(true);
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-05-01'));
        $configuration->setFrequency('monthly');
        $configuration->setFrequencyMultiplier(2);
        $configuration->setDaysOfWeek(array(3));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array(
                '2012-02-01',
                '2012-02-08',
                '2012-02-15',
                '2012-02-22',
                '2012-02-29',
                '2012-04-04',
                '2012-04-11',
                '2012-04-18',
                '2012-04-25'
            ),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testSecondThursdayOfMonth()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2012-04-01'));
        $configuration->setFrequency('monthly');
        $configuration->setNth(2);
        $configuration->setDaysOfWeek(array(4));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array('2012-01-12', '2012-02-09', '2012-03-08'),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testTakeSummersOff()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2013-01-01'));
        $configuration->setFrequency('yearly');
        $configuration->setMonthsOfYear(array(7, 8));
        $configuration->setDaysOfMonth(range(1, 31));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            call_user_func(function() {
                $out = array();
                for (
                    $date = new \DateTime('2012-07-01');
                    $date < new \DateTime('2012-09-01');
                    $date->modify('+1 day')
                ) {
                    $out[] = $date->format('Y-m-d');
                }
                return $out;
            }),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testEverySecondYear()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-01'));
        $configuration->setDateEnd(new \DateTime('2016-01-01'));
        $configuration->setFrequency('yearly');
        $configuration->setFrequencyMultiplier(2);
        $configuration->setDaysOfMonth(array(15));
        $configuration->setMonthsOfYear(array(1));

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array('2013-01-15', '2015-01-15'),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }

    public function testSecondWeekdayStartingMidMonth()
    {
        $configuration = new BlackoutConfiguration();
        $configuration->setDateStart(new \DateTime('2012-01-18'));
        $configuration->setDateEnd(new \DateTime('2012-04-01'));
        $configuration->setFrequency('monthly');
        $configuration->setDaysOfWeek(array(4));
        $configuration->setNth(2);

        $generator = new BlackoutGenerator();
        $dates = $generator->getDates($configuration);

        $this->assertEquals(
            array('2012-02-09', '2012-03-08'),
            array_values(array_map(
                function($date) { return $date->format('Y-m-d');},
                $dates
            ))
        );
    }
}
