<?php

namespace Orc\BookingBundle\Tests\Booking;

use Orc\BookingBundle\Booking\PointInPolygon;

class PointInPolygonTest extends \PHPUnit_Framework_TestCase
{
    protected $regions = array(
        'kelowna' => array(
            array(49.9471964, -119.5234764),
            array(49.9224464, -119.6621788),
            array(49.8029373, -119.6319664),
            array(49.8118000, -119.4520653),
            array(49.8410352, -119.2611778),
            array(49.9392424, -119.2968834)
        ),
        'vernon' => array(
            array(50.1523949, -119.2570579),
            array(50.1572343, -119.5152366),
            array(50.3394535, -119.5893944),
            array(50.3850090, -119.2501915),
            array(50.2885901, -118.9315880),
            array(50.1523949, -119.0277183)
        ),
        'burnaby' =>  array(
            array(49.2585710, -123.0222786),
            array(49.2578988, -122.9817665),
            array(49.2446768, -122.9690636),
            array(49.2379523, -122.9347313),
            array(49.1996058, -122.9357612),
            array(49.1818801, -122.9790199),
            array(49.2014005, -123.0236519)
        )
    );
    /**
     * @dataProvider getTestPoints
     */
    public function testIt($testRegion, $point, $expected)
    {
        $test = new PointInPolygon();
        $this->assertEquals($expected, $test->inPolygon($point, $this->regions[$testRegion]));
    }

    public function getTestPoints()
    {
        return array(
            array('kelowna', array(0,            0),            false, 'Africa'),
            array('kelowna', array(49.8886627,  -119.4914126),  true,  'Downtown Kelowna'),
            array('kelowna', array(34.10300320, -118.41046840), false, 'California'),
            array('kelowna', array(50.04573610, -119.39586930), false, 'Lake Country'),
            array('kelowna', array(50.26701370, -119.27201070), false, 'Vernon'),
            array('kelowna', array(49.9514,     -119.3010),     false, 'Kelowna Airport (just out of bounds)'),
            array('kelowna', array(49.9400,     -119.3814),     true,  'Near University (just within bounds)'),
            array('kelowna', array(49.94214,    -119.35922),    false, 'Just past northern bounds'),
            array('kelowna', array(49.9505,     -119.3504),     false, 'A bit further past northern bounds'),


            array('vernon', array(50.2637089, -119.2735309),   true,  'Downtown Vernon'),
            array('vernon', array(49.9514,     -119.3010),     false, 'Kelowna Airport (just out of bounds)'),
            array('vernon', array(49.9400,     -119.3814),     false,  'Near University (just within bounds)'),
            array('vernon', array(49.94214,    -119.35922),    false, 'Just past northern bounds'),
            array('vernon', array(49.9505,     -119.3504),     false, 'A bit further past northern bounds'),

            array('burnaby', array(49.22592184601026, -123.00267219543457), true, 'Metrotown Burnaby')
        );
    }

    /**
     * Run this directly to see a demo of what our results look like
     * @param    string        kelowna|vernon
     * @return   string        Google maps static image URL
     */
    public function printToMap($region)
    {
        $options = array(
            'sensor' => 'true',
            'size' => '600x600',
            'zoom' => 10,
            'center' => $region == 'kelowna' ? '49.8886627,-119.4914126' : '50.2637089,-119.2735309'
        );

        $markers = '';
        foreach ($this->getTestPoints() as $test) {
            list($pRegion, $point, $result, $label) = $test;
            if ($region != $pRegion) {
                continue;
            }

            $color = $result ? 'green' : 'red';
            $label = $result ? 'Y' : 'N';
            $markers .= "&markers=color:$color|label:$label|$point[0],$point[1]";
        }

        foreach ($this->regions as $regionName => $polygon) {
            if ($region != $regionName) {
                continue;
            }
            foreach ($polygon as $num => $point) {
                $markers .= "&markers=color:black|label:$num|$point[0],$point[1]";
            }
        }

        if ($region == 'burnaby') {
            $options['center'] = '49.22592184601026,-123.00267219543457';
            $options['zoom'] = 11;
        }

        return 'http://maps.google.com/maps/api/staticmap?' . http_build_query($options) . $markers;
    }
}
