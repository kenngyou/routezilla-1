<?php

namespace Orc\BookingBundle\Tests\Entity;

use Orc\BookingBundle\Entity\Service;

class ServiceTest extends \PHPUnit_Framework_TestCase
{
    public function testStartsActive()
    {
        $service = new Service();
        $this->assertTrue($service->isActive());
    }

    public function testDeleting()
    {
        $service = new Service();
        $service->markAsDeleted();

        $this->assertTrue($service->isDeleted());
        $this->assertFalse($service->isActive());
    }
}
