<?php

namespace Orc\BookingBundle\Tests\Entity;

use Orc\BookingBundle\Entity\Crew;

class CrewTest extends \PHPUnit_Framework_TestCase
{
    public function testStartsActive()
    {
        $crew = new Crew();
        $this->assertTrue($crew->isActive());
    }

    public function testMarkDeleted()
    {
        $crew = new Crew();
        $crew->markAsDeleted();
        $this->assertTrue($crew->isDeleted());
    }
}
