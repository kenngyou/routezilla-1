<?php

namespace Orc\BookingBundle\Tests\Entity;

use Orc\BookingBundle\Entity\DateTime;

class DateTimeTest extends \PHPUnit_Framework_TestCase
{
    public function testNowIsDefault()
    {
        DateTime::setDefault('2012-01-01');
        $date = new DateTime();

        $this->assertEquals($date->format('Y-m-d'), DateTime::getDefault());
        $this->assertNotEquals(new DateTime(), new \DateTime());
    }

    public function todayIsOverriden()
    {
        DateTime::setDefault('2012-01-01');
        $date = new DateTime('today');
        $this->assertEquals(DateTime::getDefault(), $date->format('Y-m-d'));
    }

    public function testTodayWithTimeIsOverriden()
    {
        DateTime::setDefault('2012-01-01');
        $date = new DateTime('today 3pm');

        $this->assertEquals('2012-01-01 3pm', $date->format('Y-m-d ga'));
    }

    public function testTomorrowIsOverridden()
    {
        DateTime::setDefault('2012-01-01');
        $date = new DateTime('tomorrow 3pm');

        $this->assertEquals('2012-01-02 3pm', $date->format('Y-m-d ga'));
    }

    public function relativePlusMinusIsOverriden()
    {
        DateTime::setDefault('2012-01-01');

        $date = new DateTime('+1 day');
        $this->assertEquals('2012-01-02', $date->format('Y-m-d'));

        $date = new DateTime('-1 month');
        $this->assertEquals('2011-12-01', $date->format('Y-m-d'));
    }

    public function testNextPreviousAreOverriden()
    {
        DateTime::setDefault('2011-01-01');

        $date = new DateTime('next friday');
        $this->assertEquals('2011-01-07', $date->format('Y-m-d'));

        $date = new DateTime('previous thursday');
        $this->assertEquals('2010-12-30', $date->format('Y-m-d'));
    }
}
