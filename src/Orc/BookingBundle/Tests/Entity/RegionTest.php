<?php

namespace Orc\BookingBundle\Tests\Entity;

use Orc\BookingBundle\Entity\Region;

class RegionTest extends \PHPUnit_Framework_TestCase
{
    public function testStartsAsActive()
    {
        $region = new Region();
        $this->assertTrue($region->isActive());
    }

    public function testMarkDelete()
    {
        $region = new Region();
        $region->markAsDeleted();

        $this->assertTrue($region->isDeleted());
        $this->assertFalse($region->isActive());
    }
}
