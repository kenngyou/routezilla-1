<?php

namespace Orc\BookingBundle\Tests\Util;

use Orc\BookingBundle\Util\GetDotted;

class GetDottedTest extends \PHPUnit_Framework_TestCase
{
    public function testGetArrayKey()
    {
        $data = array('a' => 5);
        $this->assertEquals(5, GetDotted::get($data, 'a'));
    }

    public function testGetNestedArrayKey()
    {
        $data = array('a' => array('b' => 5));
        $this->assertEquals(5, GetDotted::get($data, 'a.b'));
    }

    public function testGetterAccess()
    {
        $data = new Post();
        $this->assertEquals('adrian', GetDotted::get($data, 'user.name'));
    }

    public function testEmptyFallback()
    {
        $data = new Post();
        $this->assertEquals('', GetDotted::get($data, 'post.location.billing.whatever'));
    }

    public function testCustomFallback()
    {
        $data = new Post();
        $this->assertEquals('meh', GetDotted::get($data, 'post.location.billing.whatever', 'meh'));

    }
}

class Post
{
    public function getUser()
    {
        return new User();
    }
}

class User
{
    public function getName()
    {
        return 'adrian';
    }
}
