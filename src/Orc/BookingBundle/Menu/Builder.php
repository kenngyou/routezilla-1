<?php

namespace Orc\BookingBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Matcher\Voter\UriVoter;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'breadcrumb');

        $menu->addChild('Steps');

        $counter = 1;
        $menu->addChild($counter++ . '. Location', array('route' => 'book_location'));
        $menu->addChild($counter++ . '. Service', array('route' => 'book_service'));
        $menu->addChild($counter++ . '. Date', array('route' => 'book_date'));
        $menu->addChild($counter++ . '. Contact', array('route' => 'book_contact'));

        return $menu;
    }
}
