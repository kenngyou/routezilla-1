<?php

namespace Orc\BookingBundle\Request;

use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Region;
use Orc\BookingBundle\Entity\Service;

class ExportRequest
{
    protected $start;
    protected $end;
    protected $status;
    protected $name;
    protected $crew;
    protected $region;
    protected $service;

    public function setStart(\DateTime $start = null)
    {
        $this->start = $start;
    }

    public function getStart()
    {
        return $this->start;
    }

    public function setEnd(\DateTime $end = null)
    {
        $this->end = $end;
    }

    public function getEnd()
    {
        return $this->end;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getCrew()
    {
        return $this->crew;
    }

    public function setCrew(Crew $crew)
    {
        $this->crew = $crew;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setRegion(Region $region)
    {
        $this->region = $region;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setService(Service $service)
    {
        $this->service = $service;
    }

}
