<?php

namespace Orc\BookingBundle\Request\ParamConverter;

use Orc\BookingBundle\Request\SearchRequest;
use Doctrine\Common\Persistence\ObjectRepository;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

class SearchRequestConverter implements ParamConverterInterface
{
    protected $regionRepository;

    public function __construct(ObjectRepository $repository)
    {
        $this->regionRepository = $repository;
    }

    public function apply(Request $request, ConfigurationInterface $configuration)
    {
        if (!$request->query->has('slots')) {
            return false;
        }

        $slots = $request->query->get('slots');
        $results = $this->repository->findBy(array('nameInternal' => $slots));

        $slotConfiguration = new SlotConfiguration($results);
        $request->attributes->set('slots', $slotConfiguration);

        return true;
    }

    public function supports(ConfigurationInterface $configuration)
    {
        return $configuration->getClass() == 'Orc\BookingBundle\Request\SlotConfiguration';
    }
}
