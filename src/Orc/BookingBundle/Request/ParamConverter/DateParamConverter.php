<?php

namespace Orc\BookingBundle\Request\ParamConverter;

use Orc\BookingBundle\Entity\DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationInterface;
use Symfony\Component\HttpFoundation\Request;

class DateParamConverter implements ParamConverterInterface
{
    public function apply(Request $request, ConfigurationInterface  $configuration)
    {
        $paramName = $configuration->getName();
        $dateString = $request->attributes->get($paramName);

        try {
            $date = new DateTime($dateString);
        } catch (\Exception $e) {
            $date = null;
        }

        $request->attributes->set($paramName, $date);
    }

    public function supports(ConfigurationInterface $configuration)
    {
        return strpos($configuration->getClass(), 'DateTime') !== false;
    }
}
