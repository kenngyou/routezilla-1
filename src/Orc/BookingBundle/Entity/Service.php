<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\ServiceRepository")
 * @ORM\Table(name="service")
 */
class Service implements MultiTenantInterface
{
    const STATUS_ACTIVE = 0;
    const STATUS_DELETED = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var    unique id for collection
     */
    protected $key;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="ServiceField", mappedBy="service", indexBy="slug", cascade={"persist", "remove"})
     */
    protected $fields;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    protected $baseTime;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isActive = true;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;


    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return Service
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Service
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add fields
     *
     * @param Orc\BookingBundle\Entity\ServiceField $fields
     * @return Service
     */
    public function addServiceField(\Orc\BookingBundle\Entity\ServiceField $field)
    {
        $field->setService($this);
        $this->fields[$field->getSlug()] = $field;

        return $this;
    }

    /**
     * Get fields
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set description
     *
     * @param text $description
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return text
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set baseTime
     *
     * @param integer $baseTime
     * @return Service
     */
    public function setBaseTime($baseTime)
    {
        $this->baseTime = $baseTime;
        return $this;
    }

    /**
     * Get baseTime
     *
     * @return integer
     */
    public function getBaseTime()
    {
        return $this->baseTime;
    }

    /**
     * Add fields
     *
     * @param Orc\BookingBundle\Entity\ServiceField $fields
     * @return Service
     */
    public function addField(\Orc\BookingBundle\Entity\ServiceField $fields)
    {
        $fields->setService($this);
        $this->fields[] = $fields;

        return $this;
    }

    /**
     * Remove fields
     *
     * @param <variableType$fields
     */
    public function removeField(\Orc\BookingBundle\Entity\ServiceField $fields)
    {
        $this->fields->removeElement($fields);
    }

    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function markAsDeleted()
    {
        $this->status = self::STATUS_DELETED;
    }

    public function __toString()
    {
        return $this->name;
    }
}
