<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\HoursChangeRepository")
 * @ORM\Table(name="client_hours_changes")
 */
class HoursChange implements MultiTenantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $day;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $startTime;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $endTime;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $off = false;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param integer $day
     * @return HoursOfOperation
     */
    public function setDay($day)
    {
        $this->day = $day;
        return $this;
    }

    /**
     * Get day
     *
     * @return integer
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set startTime
     *
     * @param datetime $startTime
     * @return HoursOfOperation
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
        return $this;
    }

    /**
     * Get startTime
     *
     * @return datetime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * Set endTime
     *
     * @param datetime $endTime
     * @return HoursOfOperation
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
        return $this;
    }

    /**
     * Get endTime
     *
     * @return datetime
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return HoursOfOperation
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set off
     *
     * @param boolean $off
     * @return HoursOfOperation
     */
    public function setOff($off)
    {
        $this->off = $off;
        return $this;
    }

    /**
     * Get off
     *
     * @return boolean
     */
    public function getOff()
    {
        return $this->off;
    }

    public function __toString()
    {
        return $this->getDay()->format('m-d-Y');
    }

    /**
     * Checks if this hour change is the same as the client's default for this day
     * @return    boolean        true if Default schedule
     */
    public function isDefault()
    {
        $defaults = $this->client->getHoursOfOperation();
        $defaultDay = $defaults[$this->getDay()->format('w')];

        return (
            $defaultDay->getStartTime()->format('H:i') == $this->startTime->format('H:i') and
            $defaultDay->getEndTime()->format('H:i') == $this->endTime->format('H:i') and
            $defaultDay->getOff() == $this->off
        );
    }

    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }
}
