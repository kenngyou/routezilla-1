<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\CrewRadiusRepository")
 * @ORM\Table(name="crew_radius")
 */
class CrewRadius
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Crew")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $crew;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $day;

    /**
     * @ORM\Column(type="decimal", scale=7)
     */
    protected $radius;

    /**
     * @ORM\ManyToOne(targetEntity="Location", cascade={"persist"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="Booking")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $booking;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set day
     *
     * @param datetime $day
     * @return CrewRadius
     */
    public function setDay($day)
    {
        $this->day = $day;
        return $this;
    }

    /**
     * Get day
     *
     * @return datetime
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set radius
     *
     * @param decimal $radius
     * @return CrewRadius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
        return $this;
    }

    /**
     * Get radius
     *
     * @return decimal
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * Set crew
     *
     * @param Orc\BookingBundle\Entity\Crew $crew
     * @return CrewRadius
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;
        return $this;
    }

    /**
     * Get crew
     *
     * @return Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }

    /**
     * Set booking
     *
     * @param Orc\BookingBundle\Entity\Booking $booking
     * @return CrewRadius
     */
    public function setBooking(\Orc\BookingBundle\Entity\Booking $booking = null)
    {
        $booking->setRadius($this);
        $this->booking = $booking;
        return $this;
    }

    /**
     * Get booking
     *
     * @return Orc\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set location
     *
     * @param Orc\BookingBundle\Entity\Location $location
     * @return CrewRadius
     */
    public function setLocation(\Orc\BookingBundle\Entity\Location $location = null)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return Orc\BookingBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }
}
