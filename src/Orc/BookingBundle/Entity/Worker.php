<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\WorkerRepository")
 * @ORM\Table(name="worker")
 * @UniqueEntity(fields={"user"}, message="That email is already in use")
 */
class Worker implements MultiTenantInterface
{
    const STATUS_ACTIVE = 0;
    const STATUS_DELETED = 1;
    const TYPE_ADMIN = 'admin';
    const TYPE_CREW_MEMBER = 'crewmember';
    const TYPE_OFFICE_ADMIN = 'officeadmin';

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;

    /**
     * @ORM\Id
     * @ORM\OneToOne(targetEntity="Orc\UserBundle\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\Valid
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\BookingBundle\Entity\Crew")
     * @ORM\JoinColumn(name="crew_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $crew;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    protected $token;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $status = self::STATUS_ACTIVE;

    /**
     * Set user
     *
     * @param Orc\UserBundle\Entity\User $user
     * @return Worker
     */
    public function setUser(\Orc\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Orc\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set crew
     *
     * @param Orc\BookingBundle\Entity\Crew $crew
     * @return Worker
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;
        return $this;
    }

    /**
     * Get crew
     *
     * @return Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Worker
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Worker
     */
    public function setType($type)
    {
        if (!in_array($type, array_keys(self::getTypes(true)))) {
            throw new \InvalidArgumentException(sprintf('"%s" is not a valid worker type', $type));
        }

        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getNameAndCrew()
    {
        if ($this->getCrew()) {
            return sprintf('%s (Assigned: %s)', $this->name, $this->getCrew()->getName());
        }

        return $this->name;
    }

    static public function getTypes($all = false)
    {
        $out = array(
            self::TYPE_CREW_MEMBER => 'Crew Member',
            self::TYPE_OFFICE_ADMIN => 'Office Admin'
        );

        if ($all) {
            $out[self::TYPE_ADMIN] = 'Admin';
        }

        return $out;
    }

    /**
     * Set notes
     *
     * @param text $notes
     * @return Worker
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Get notes
     *
     * @return text
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Worker
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }



    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    /**
     * Set token
     *
     * @param string $token
     * @return Worker
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get token
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    public function markAsActive()
    {
        $this->status = self::STATUS_ACTIVE;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function markAsDeleted()
    {
        $this->status = self::STATUS_DELETED;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }
}
