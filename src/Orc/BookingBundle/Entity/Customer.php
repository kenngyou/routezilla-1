<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer")
 */
class Customer
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Email address (later used for logging in?)
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "step4", "create"})
     * @Assert\Email(groups={"Default", "step4", "create"})
     */
    protected $email;

    /**
     * Contact name for Customer
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "step4", "create"})
     */
    protected $name;

    /**
     * Contact number for Customer
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "step4", "create"})
     */
    protected $phone;

    /**
     * Canonical (searchable) Contact number for Customer
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phoneCanonical;

    /**
     * Customer's contact location
     *
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    protected $location;

    /**
     * Customer's billing information (often a duplicate of this)
     *
     * @ORM\OneToOne(targetEntity="CustomerBilling", cascade={"persist", "remove"})
     * @ORM\JoinColumn(name="billing_id", referencedColumnName="id", onDelete="CASCADE")
     * @Assert\Valid
     */
    protected $billing;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customer
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Customer
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set location
     *
     * @param Orc\BookingBundle\Entity\Location $location
     * @return Customer
     */
    public function setLocation(\Orc\BookingBundle\Entity\Location $location = null)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return Orc\BookingBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set billing
     *
     * @param Orc\BookingBundle\Entity\CustomerBilling $billing
     * @return Customer
     */
    public function setBilling(\Orc\BookingBundle\Entity\CustomerBilling $billing = null)
    {
        $billing->setCustomer($this);
        $this->billing = $billing;
        return $this;
    }

    /**
     * Get billing
     *
     * @return Orc\BookingBundle\Entity\CustomerBilling
     */
    public function getBilling()
    {
        return $this->billing;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Customer
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phoneCanonical
     *
     * @param string $phoneCanonical
     * @return Customer
     */
    public function setPhoneCanonical($phoneCanonical)
    {
        $this->phoneCanonical = $phoneCanonical;

        return $this;
    }

    /**
     * Get phoneCanonical
     *
     * @return string
     */
    public function getPhoneCanonical()
    {
        return $this->phoneCanonical;
    }
}
