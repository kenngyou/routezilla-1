<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\BlackoutRepository")
 * @ORM\Table(name="blackout")
 */
class Blackout implements MultiTenantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\BookingBundle\Entity\BlackoutConfiguration")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $configuration;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    protected $dateStart;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    protected $dateEnd;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $timeStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $timeEnd;

    /**
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $service;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="Crew")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $crew;

    public function __construct()
    {
        if (!$this->timeStart) {
            $this->timeStart = new \DateTime('today 00:00:00');
        }

        if (!$this->timeEnd) {
            $this->timeEnd= new \DateTime('today 23:30:00');
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Blackout
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dateStart
     *
     * @param datetime $dateStart
     * @return Blackout
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return datetime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param datetime $dateEnd
     * @return Blackout
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return datetime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Blackout
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set service
     *
     * @param Orc\BookingBundle\Entity\Service $service
     * @return Blackout
     */
    public function setService(\Orc\BookingBundle\Entity\Service $service = null)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * Get service
     *
     * @return Orc\BookingBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set region
     *
     * @param Orc\BookingBundle\Entity\Region $region
     * @return Blackout
     */
    public function setRegion(\Orc\BookingBundle\Entity\Region $region = null)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * Get region
     *
     * @return Orc\BookingBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set crew
     *
     * @param Orc\BookingBundle\Entity\Crew $crew
     * @return Blackout
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;
        return $this;
    }

    /**
     * Get crew
     *
     * @return Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }


    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     * @return Blackout
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param \DateTime $timeEnd
     * @return Blackout
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set configuration
     *
     * @param \Orc\BookingBundle\Entity\BlackoutConfiguration $configuration
     * @return Blackout
     */
    public function setConfiguration(\Orc\BookingBundle\Entity\BlackoutConfiguration $configuration = null)
    {
        $this->configuration = $configuration;

        return $this;
    }

    /**
     * Get configuration
     *
     * @return \Orc\BookingBundle\Entity\BlackoutConfiguration
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    public function getEffectiveStart()
    {
        $start = clone $this->dateStart;
        if ($this->timeStart) {
            $start->setTime($this->timeStart->format('G'), $this->timeStart->format('i'), $this->timeStart->format('s'));
        }  else {
            $start->setTime(0, 0, 0);
        }
        return $start;
    }

    public function getEffectiveEnd()
    {
        $end = clone $this->dateEnd;
        if ($this->timeEnd) {
            $end->setTime($this->time->format('G'), $this->timeStart->format('i'), $this->time->format('s'));
        }  else {
            $end->setTime(0, 0, 0);
        }
        return $end;
    }

    public function getEffectiveRanges()
    {
        $ranges = array();

        $day = clone $this->dateStart;

        if (!$this->timeStart) {
            $this->timeStart = new \DateTime('today 00:00:00');
        }
        if (!$this->timeEnd) {
            $this->timeEnd= new \DateTime('today 23:30:00');
        }

        while ($day <= $this->dateEnd) {
            $timeStart = clone $this->timeStart;
            $timeStart->setDate($day->format('Y'), $day->format('m'), $day->format('d'));
            $timeEnd = clone $this->timeEnd;
            $timeEnd->setDate($day->format('Y'), $day->format('m'), $day->format('d'));

            $ranges[] = array($timeStart, $timeEnd);
            $day->modify('+1 day');
        }

        return $ranges;
    }
}
