<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Orc\BillingBundle\Validator\Constraints\MaxCrews;
use Orc\BillingBundle\Validator\Constraints\NotScheduled;
use Orc\BookingBundle\Util\ColorGenerator;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\CrewRepository")
 * @ORM\Table(name="crew")
 * @MaxCrews
 * @NotScheduled
 */
class Crew implements MultiTenantInterface
{
    const STATUS_ACTIVE = 0;
    const STATUS_DELETED = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;


    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Worker", mappedBy="crew", cascade={"persist"})
     */
    protected $workers;

    /**
     * @ORM\ManyToMany(targetEntity="Service")
     */
    protected $services;

    /**
     * @ORM\OneToMany(targetEntity="Booking", mappedBy="crew")
     */
    protected $bookings;

    /**
     * @ORM\ManyToMany(targetEntity="Region", inversedBy="crews")
     */
    protected $regions;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active = true;

    /**
     * @ORM\ManyToOne(targetEntity="Color")
     * @ORM\JoinColumn(name="color_id", referencedColumnName="id")
     */
    protected $color;

    public function __construct()
    {
        $this->workers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->regions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = self::STATUS_ACTIVE;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Crew
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add workers
     *
     * @param Orc\BookingBundle\Entity\Worker $workers
     * @return Crew
     */
    public function addWorker(\Orc\BookingBundle\Entity\Worker $workers)
    {
        $workers->setCrew($this);
        $this->workers[] = $workers;
        return $this;
    }

    /**
     * Remove workers
     *
     * @param <variableType$workers
     */
    public function removeWorker(\Orc\BookingBundle\Entity\Worker $workers)
    {
        $workers->setCrew(null);
        $this->workers->removeElement($workers);
    }

    /**
     * Get workers
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getWorkers()
    {
        return $this->workers;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Add services
     *
     * @param Orc\BookingBundle\Entity\Service $services
     * @return Crew
     */
    public function addService(\Orc\BookingBundle\Entity\Service $service)
    {
        $this->services[] = $service;
        return $this;
    }

    /**
     * Remove services
     *
     * @param <variableType$services
     */
    public function removeService(\Orc\BookingBundle\Entity\Service $service)
    {
        $this->services->removeElement($service);
    }

    /**
     * Get services
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getServices()
    {
        return $this->services;
    }

    public function setServices($services)
    {
        foreach ($services as $service) {
            $this->addService($service);
        }
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Crew
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Crew
     */
    public function setActive($active)
    {
        $this->active = $active;
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Add bookings
     *
     * @param Orc\BookingBundle\Entity\Booking $bookings
     * @return Crew
     */
    public function addBooking(\Orc\BookingBundle\Entity\Booking $bookings)
    {
        $this->bookings[] = $bookings;
        return $this;
    }

    /**
     * Remove bookings
     *
     * @param <variableType$bookings
     */
    public function removeBooking(\Orc\BookingBundle\Entity\Booking $bookings)
    {
        $this->bookings->removeElement($bookings);
    }

    /**
     * Get bookings
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getBookings()
    {
        return $this->bookings;
    }

    /**
     * Set color
     *
     * @param Orc\BookingBundle\Entity\Color $color
     * @return Crew
     */
    public function setColor(\Orc\BookingBundle\Entity\Color $color = null)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * Get color
     *
     * @return Orc\BookingBundle\Entity\Color
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add region
     *
     * @param Orc\BookingBundle\Entity\Region $region
     * @return Crew
     */
    public function addRegion(\Orc\BookingBundle\Entity\Region $region)
    {
        $region->addCrew($this);
        $this->regions[] = $region;
        return $this;
    }

    /**
     * Remove region
     *
     * @param Region $region
     */
    public function removeRegion(\Orc\BookingBundle\Entity\Region $region)
    {
        $region->removeCrew($this);
        $this->regions->removeElement($region);
    }

    /**
     * Get regions
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * Marks the crew as deleted
     *
     * @return Region
     */
    public function markAsDeleted()
    {
        $this->status = self::STATUS_DELETED;
        return $this;
    }

    /**
     * Is the crew deleted?
     *
     * @return boolean
     */
    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }


    /**
     * Is the crew active?
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }


    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }
}
