<?php

namespace Orc\BookingBundle\Entity;

class DateTime extends \DateTime
{
    static protected $default = 'now';

    /**
     * In order to test date functionality, we need to override the current time
     * If nothing (PHP default: "now") is provided, we'll inject a static time instead.
     *
     * @param    string        Initial time
     */
    public function __construct($string = "now", \DateTimeZone $tz = null)
    {
        if (self::$default == 'now') {
            return parent::__construct($string);
        }

        if ($string == 'now') {
            $string = self::$default;

        } elseif (strpos($string, 'today') === 0) {
            $string = str_replace('today', self::$default, $string);

        } elseif (strpos($string, 'tomorrow') === 0) {
            $string = str_replace('tomorrow', self::$default. ' +1 day ', $string);

        } elseif (in_array($string[0], array('+', '-'))) {
            $string = self::$default . " $string";

        } elseif (in_array(substr($string, 0, 4), array('next', 'prev'))) {
            $string = self::$default . " $string";
        }

        return parent::__construct($string, $tz);
    }

    static public function getStartOfWeek($reference = 'now')
    {
        $date = new self($reference);
        $date->modify('tomorrow');
        $date->modify('previous sunday');

        return $date;
    }

    static public function getEndOfWeek($reference = 'now')
    {
        $date = new self($reference);
        $date->modify('tomorrow');
        $date->modify('previous sunday');
        $date->modify('+6 days');

        return $date;
    }

    static public function create($string)
    {
        $date = new self();
        $date->modify($string);

        return $date;
    }

    /**
     * Override the default time to be used
     * @param    string        Any date/time string
     */
    static public function setDefault($time)
    {
        self::$default = $time;
    }

    /**
     * Get the custom default time
     * @return    string        Default date/time string
     */
    static public function getDefault()
    {
        return self::$default;
    }
}
