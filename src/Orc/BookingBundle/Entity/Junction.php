<?php

namespace Orc\BookingBundle\Entity;

class Junction
{
    const REASON_REGION = 'region';
    const REASON_UNAVAILABLE = 'unavailable';
    const REASON_DISCOURAGED = 'discouraged';

    public function __construct($reason, array $options)
    {
        $this->reason = $reason;
        $this->options = $options;
    }

    public function getReason()
    {
        return $this->reason;
    }

    public function getOptions()
    {
        return $this->options;
    }
}
