<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="customer_billing")
 */
class CustomerBilling
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Ties to Customer
     *
     * @ORM\OneToOne(targetEntity="Customer")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $customer;

    /**
     * Contact name for Billing
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "create"})
     */
    protected $name;

    /**
     * Contact email for Billing
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "create"})
     * @Assert\Email(groups={"Default", "create"})
     */
    protected $email;

    /**
     * Contact number for Billing
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank(groups={"Default", "create"})
     */
    protected $phone;

    /**
     * Canonical (searchable) Contact number for Customer
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phoneCanonical;

    /**
     * Billing location
     *
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    protected $location;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return CustomerBilling
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return CustomerBilling
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set customer
     *
     * @param Orc\BookingBundle\Entity\Customer $customer
     * @return CustomerBilling
     */
    public function setCustomer(\Orc\BookingBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * Get customer
     *
     * @return Orc\BookingBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set location
     *
     * @param Orc\BookingBundle\Entity\Location $location
     * @return CustomerBilling
     */
    public function setLocation(\Orc\BookingBundle\Entity\Location $location = null)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return Orc\BookingBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return CustomerBilling
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phoneCanonical
     *
     * @param string $phoneCanonical
     * @return CustomerBilling
     */
    public function setPhoneCanonical($phoneCanonical)
    {
        $this->phoneCanonical = $phoneCanonical;

        return $this;
    }

    /**
     * Get phoneCanonical
     *
     * @return string
     */
    public function getPhoneCanonical()
    {
        return $this->phoneCanonical;
    }
}
