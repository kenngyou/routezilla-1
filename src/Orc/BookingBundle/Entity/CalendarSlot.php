<?php

namespace Orc\BookingBundle\Entity;

class CalendarSlot
{
    protected $date;
    protected $bookings = 0;

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setBookings($bookings)
    {
        $this->bookings = $bookings;
    }

    public function addBooking()
    {
        $this->bookings++;
    }

    public function getBookings()
    {
        return $this->bookings;
    }
}
