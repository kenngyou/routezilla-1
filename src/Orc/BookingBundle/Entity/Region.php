<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\RegionRepository")
 * @ORM\Table(name="region")
 */
class Region implements MultiTenantInterface
{
    const STATUS_ACTIVE = 0;
    const STATUS_DELETED = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;

    /**
     * @ORM\OneToMany(targetEntity="Orc\BookingBundle\Entity\Boundary", mappedBy="region", cascade={"persist", "remove"})
     */
    protected $boundaries;

    /**
     * @ORM\ManyToMany(targetEntity="Crew", mappedBy="regions")
     */
    protected $crews;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status;


    public function __construct()
    {
        $this->boundaries = new ArrayCollection();
        $this->status = self::STATUS_ACTIVE;
    }


    /**
     * Get boundaries
     *
     * @return array
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

    /**
     * Set boundaries
     * @param    ArrayCollection
     * @return   Region
     */
    public function setBoundaries($boundaries)
    {
        foreach ($boundaries as $boundary) {
            $this->addBoundaries($boundary);
        }
        return $this;
    }

    /**
     * Add Boundary
     *
     * @param Orc\BookingBundle\Entity\Boundary $boundary
     * @return Client
     */
    public function addBoundaries(\Orc\BookingBundle\Entity\Boundary $boundary)
    {
        $boundary->setRegion($this);
        if (!$this->boundaries->contains($boundary)) {
            $this->boundaries[] = $boundary;
        }
        return $this;
    }

    /**
     * Remove Boundary
     *
     * @param Boundary
     */
    public function removeBoundaries(\Orc\BookingBundle\Entity\Boundary $boundary)
    {
        $this->boundaries->removeElement($boundary);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set crew
     *
     * @param Orc\BookingBundle\Entity\Crew $crew
     * @return Region
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;
        return $this;
    }

    /**
     * Get crew
     *
     * @return Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Region
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Add boundaries
     *
     * @param Orc\BookingBundle\Entity\Boundary $boundaries
     * @return Region
     */
    public function addBoundarie(\Orc\BookingBundle\Entity\Boundary $boundaries)
    {
        $this->boundaries[] = $boundaries;
        return $this;
    }

    /**
     * Remove boundaries
     *
     * @param <variableType$boundaries
     */
    public function removeBoundarie(\Orc\BookingBundle\Entity\Boundary $boundaries)
    {
        $this->boundaries->removeElement($boundaries);
    }

    /**
     * Add crews
     *
     * @param Orc\BookingBundle\Entity\Crew $crews
     * @return Region
     */
    public function addCrew(\Orc\BookingBundle\Entity\Crew $crews)
    {
        $this->crews[] = $crews;
        return $this;
    }

    /**
     * Remove crews
     *
     * @param <variableType$crews
     */
    public function removeCrew(\Orc\BookingBundle\Entity\Crew $crews)
    {
        $this->crews->removeElement($crews);
    }

    /**
     * Get crews
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getCrews()
    {
        return $this->crews;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isDeleted()
    {
        return $this->status == self::STATUS_DELETED;
    }

    public function markAsDeleted()
    {
        $this->status = self::STATUS_DELETED;
    }


    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    public function __toString()
    {
        return $this->name;
    }
}
