<?php

namespace Orc\BookingBundle\Entity;

use Orc\BookingBundle\Util\Slugify;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="service_field")
 */
class ServiceField
{
    const TYPE_NUMERIC = 0;
    const TYPE_FREE = 1;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    protected $service;

    /**
     * @ORM\Column(type="string")
     */
    protected $slug;

    /**
     * @ORM\Column(type="string")
     */
    protected $question;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $addMinutes;

    /**
     * @ORM\Column(type="integer")
     */
    protected $type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return ServiceField
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        if ($question) {
            $this->slug = Slugify::generate($question);
        }

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set addMinutes
     *
     * @param integer $addMinutes
     * @return ServiceField
     */
    public function setAddMinutes($addMinutes)
    {
        $this->addMinutes = $addMinutes;
        return $this;
    }

    /**
     * Get addMinutes
     *
     * @return integer
     */
    public function getAddMinutes()
    {
        return $this->addMinutes;
    }

    /**
     * Set service
     *
     * @param Orc\BookingBundle\Entity\Service $service
     * @return ServiceField
     */
    public function setService(\Orc\BookingBundle\Entity\Service $service = null)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * Get service
     *
     * @return Orc\BookingBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    static public function getTypes()
    {
        return array(
            self::TYPE_NUMERIC  => 'Numeric Response',
            self::TYPE_FREE => 'Free Response'
        );
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return ServiceField
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return ServiceField
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }
}
