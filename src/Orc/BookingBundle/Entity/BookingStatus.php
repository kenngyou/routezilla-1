<?php

namespace Orc\BookingBundle\Entity;

class BookingStatus
{
    /**
     * Bookings start as drafts when they are initially created.
     * Drafts move on when all the required information is gathered, and they usable.
     */
    const STATUS_ATTEMPT = 1;

    /**
     * These bookings are assigned to a Crew, and the work is queued to be performed.
     */
    const STATUS_ASSIGNED = 2;

    /**
     * These bookings have been completed (the job has been done).
     */
    const STATUS_DELIVERED = 3;

    /**
     * These bookings have been completed, and the customer has been invoiced
     */
    const STATUS_BILLED = 4;

    /**
     * Drafts are admin-created booking which do not have all the required information
     */
    const STATUS_DRAFT = -1;

    /**
     * Bookings which have all the required fields filled out, but won't be put into the schedule.
     */
    const STATUS_UNASSIGNABLE = -2;

    /**
     * Bookings which have been cancelled by the customer
     */
    const STATUS_CANCELLED = -3;

    /**
     * Booking attempts which were not completed within a reasonable time.
     */
    const STATUS_ABANDONED = -4;

    /**
     * Booking attempts which ended in the Customer requesting a callback
     */
    const STATUS_REQUESTED = -5;

    static public function getStatuses()
    {
        return array(
            self::STATUS_ATTEMPT      => 'Attempt',
            self::STATUS_ASSIGNED     => 'Assigned',
            self::STATUS_DELIVERED    => 'Delivered',
            self::STATUS_BILLED       => 'Billed',

            self::STATUS_DRAFT        => 'Draft',
            self::STATUS_UNASSIGNABLE => 'Unassignable',
            self::STATUS_CANCELLED    => 'Cancelled',
            self::STATUS_ABANDONED    => 'Abandoned',
            self::STATUS_REQUESTED    => 'Requested'
        );
    }

    static public function getStatusText($status)
    {
        $statuses = self::getStatuses();
        return $statuses[$status];
    }
}
