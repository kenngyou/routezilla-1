<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="booking_service_answer")
 */
class BookingServiceField
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Booking")
     * @ORM\JoinColumn(name="booking_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $booking;

    /**
     * @ORM\ManyToOne(targetEntity="ServiceField")
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $field;

    /**
     * @ORM\Column(type="string")
     */
    protected $slug;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $answer;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return BookingServiceField
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set booking
     *
     * @param Orc\BookingBundle\Entity\Booking $booking
     * @return BookingServiceField
     */
    public function setBooking(\Orc\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;
        return $this;
    }

    /**
     * Get booking
     *
     * @return Orc\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set field
     *
     * @param Orc\BookingBundle\Entity\ServiceField $field
     * @return BookingServiceField
     */
    public function setField(\Orc\BookingBundle\Entity\ServiceField $field = null)
    {
        $this->field = $field;
        if ($field) {
            $this->slug = $field->getSlug();
        }
        return $this;
    }

    /**
     * Get field
     *
     * @return Orc\BookingBundle\Entity\ServiceField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return BookingServiceField
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }
}
