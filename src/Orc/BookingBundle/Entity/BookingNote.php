<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\BookingNoteRepository")
 * @ORM\Table(name="booking_note")
 * @ORM\HasLifecycleCallbacks
 */
class BookingNote
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Booking")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $booking;

    /**
     * @ORM\ManyToOne(targetEntity="Worker")
     * @ORM\JoinColumn(name="worker_id", referencedColumnName="user_id", onDelete="SET NULL")
     */
    protected $worker;

    /**
     * @ORM\Column(type="text")
     */
    protected $text;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\GeneratedValue
     */
    protected $datePosted;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return BookingNote
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set datePosted
     *
     * @param \DateTime $datePosted
     * @return BookingNote
     */
    public function setDatePosted($datePosted)
    {
        $this->datePosted = $datePosted;

        return $this;
    }

    /**
     * Get datePosted
     *
     * @return \DateTime
     */
    public function getDatePosted()
    {
        return $this->datePosted;
    }

    /**
     * Set booking
     *
     * @param Orc\BookingBundle\Entity\Booking $booking
     * @return BookingNote
     */
    public function setBooking(\Orc\BookingBundle\Entity\Booking $booking = null)
    {
        $this->booking = $booking;

        return $this;
    }

    /**
     * Get booking
     *
     * @return Orc\BookingBundle\Entity\Booking
     */
    public function getBooking()
    {
        return $this->booking;
    }

    /**
     * Set worker
     *
     * @param Orc\BookingBundle\Entity\Worker $worker
     * @return BookingNote
     */
    public function setWorker(\Orc\BookingBundle\Entity\Worker $worker = null)
    {
        $this->worker = $worker;

        return $this;
    }

    /**
     * Get worker
     *
     * @return Orc\BookingBundle\Entity\Worker
     */
    public function getWorker()
    {
        return $this->worker;
    }

    /**
     * @ORM\PrePersist
     */
    public function updateTimestamps()
    {
        $this->datePosted = new DateTime();
    }
}
