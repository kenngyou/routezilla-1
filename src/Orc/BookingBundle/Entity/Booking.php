<?php

namespace Orc\BookingBundle\Entity;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\Crew;
use Orc\BookingBundle\Entity\Location;
use Orc\BookingBundle\Util\Slugify;
use Orc\BookingBundle\Validator\Constraints\Chronological;
use FOS\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;

/**
 * @Chronological(groups={"query"})
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\BookingRepository")
 * @ORM\Table(name="booking")
 * @ORM\HasLifecycleCallbacks
 */
class Booking implements MultiTenantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    protected $client;

    /**
     * @Assert\Valid
     * @ORM\ManyToOne(targetEntity="Customer")
     */
    protected $customer;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $user;

    /**
     * @Assert\NotBlank(groups={"step2", "create"}, message="Please choose a service")
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id", nullable=true)
     */
    protected $service;

    /**
     * @Assert\NotBlank(groups={"step3"}, message="Please choose a Time above")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateStart;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateEnd;

    /**
     * @ORM\OneToOne(targetEntity="Location", cascade={"persist", "remove"})
     * @Assert\Valid
     */
    protected $location;

    /**
     * @ORM\ManyToOne(targetEntity="Crew")
     * @ORM\JoinColumn(name="crew_id", referencedColumnName="id", nullable=true)
     */
    protected $crew;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $region;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status = BookingStatus::STATUS_ATTEMPT;

    /**
     * Service-Specific Answers
     * @ORM\OneToMany(targetEntity="BookingServiceField", mappedBy="booking", indexBy="slug", cascade={"persist", "remove"})
     * @Assert\Collection(
     *     fields = {
     *         "answer": {
     *             @Assert\NotBlank
     *         }
     *     },
     *     allowMissingFields = true,
     *     allowExtraFields = true,
     *     missingFieldsMessage = "Please answer all of the questions.",
     *     groups = {"step2"}
     * )
     */
    protected $fields;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $additionalInformation;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $notes;

    /**
     * @ORM\OneToMany(targetEntity="BookingNote", mappedBy="booking", cascade={"persist", "remove"})
     */
    protected $workerNotes;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $billed = false;

    /**
     * @ORM\OneToOne(targetEntity="CrewRadius", mappedBy="booking")
     * @ORM\JoinColumn(name="radius_id", onDelete="SET NULL")
     */
    protected $radius;

    /**
     * @ORM\Column(type="datetime")
     * @ORM\GeneratedValue
     */
    protected $datePosted;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateUpdated;

    protected $originalDates;

    protected $required;

    public function __construct()
    {
        $this->fields = new \Doctrine\Common\Collections\ArrayCollection();
        $this->workerNotes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param datetime $dateStart
     * @return Booking
     */
    public function setDateStart(\DateTime $dateStart = null)
    {
        $this->dateStart = $dateStart;
        return $this;
    }

    /**
     * Get dateStart
     *
     * @return datetime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param datetime $dateEnd
     * @return Booking
     */
    public function setDateEnd(\DateTime $dateEnd = null)
    {
        $this->dateEnd = $dateEnd;
        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return datetime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Booking
     */
    public function setStatus($status)
    {
        $validStatuses = BookingStatus::getStatuses();

        if (!isset($validStatuses[$status])) {
            throw new \InvalidArgumentException(sprintf('$status should be one of STATUS_* constants, "%s" given', $status));
        }

        $this->status = $status;
        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get status text
     * @return   string
     */
    public function getStatusText()
    {
        $statuses = BookingStatus::getStatuses();
        return $statuses[$this->status];
    }

    /**
     * Set additionalInformation
     *
     * @param text $additionalInformation
     * @return Booking
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
        return $this;
    }

    /**
     * Get additionalInformation
     *
     * @return text
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * Set billed
     *
     * @param boolean $billed
     * @return Booking
     */
    public function setBilled($billed)
    {
        $this->billed = $billed;
        return $this;
    }

    /**
     * Get billed
     *
     * @return boolean
     */
    public function getBilled()
    {
        return $this->billed;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Booking
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set location
     *
     * @param Orc\BookingBundle\Entity\Location $location
     * @return Booking
     */
    public function setLocation(\Orc\BookingBundle\Entity\Location $location = null)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return Orc\BookingBundle\Entity\Location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set crew
     *
     * @param Orc\BookingBundle\Entity\Crew $crew
     * @return Booking
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;
        return $this;
    }

    /**
     * Get crew
     *
     * @return Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updateTimestamps()
    {
        if (!$this->datePosted) {
            $this->datePosted = new DateTime();
        }

        $this->dateUpdated = new DateTime();
    }

    /**
     * Set datePosted
     *
     * @param datetime $datePosted
     * @return Booking
     */
    public function setDatePosted(\DateTime $datePosted)
    {
        $this->datePosted = $datePosted;
        return $this;
    }

    /**
     * Get datePosted
     *
     * @return datetime
     */
    public function getDatePosted()
    {
        return $this->datePosted;
    }

    /**
     * Set dateUpdated
     *
     * @param datetime $dateUpdated
     * @return Booking
     */
    public function setDateUpdated(\DateTime $dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
        return $this;
    }

    /**
     * Get dateUpdated
     *
     * @return datetime
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * Set service
     *
     * @param Orc\BookingBundle\Entity\Service $service
     * @return Booking
     */
    public function setService(\Orc\BookingBundle\Entity\Service $service = null)
    {
        $this->service = $service;
        return $this;
    }

    /**
     * Get service
     *
     * @return Orc\BookingBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Get fields
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Set fields
     * @param    array      $fields
     */
    public function setFields($fields)
    {
        foreach ($fields as $key => $field) {
            $this->addField($field);
        }
    }

    /**
     * Add fields
     *
     * @param Orc\BookingBundle\Entity\BookingServiceField $fields
     * @return Booking
     */
    public function addField(\Orc\BookingBundle\Entity\BookingServiceField $field)
    {
        $field->setBooking($this);
        $this->fields->set($field->getSlug(), $field);

        return $this;
    }

    /**
     * Remove fields
     *
     * @param <variableType$fields
     */
    public function removeField(\Orc\BookingBundle\Entity\BookingServiceField $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Set customer
     *
     * @param Orc\BookingBundle\Entity\Customer $customer
     * @return Booking
     */
    public function setCustomer(\Orc\BookingBundle\Entity\Customer $customer = null)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * Get customer
     *
     * @return Orc\BookingBundle\Entity\Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set notes
     *
     * @param text $notes
     * @return Booking
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Get notes
     *
     * @return text
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Generates the booking time required in minutes
     * @return    integer        Minutes required for booking
     */
    public function getTimeRequired()
    {
        if (!$this->dateEnd) {
            throw new \BadMethodCallException('Cant calculate required time without dateEnd');
        }

        $required = $this->dateEnd->diff($this->dateStart);
        return ($required->format('%h') * 60) + $required->format('%i');
    }

    /**
     * Generates the name of the booking from other information
     * @return    string        Booking.name
     */
    public function getName()
    {
        $street = $this->getLocation()->getStreet();
        if (preg_match('/^([0-9]+) /', $street)) {
            $street = preg_replace('/^([0-9]+) /', '', $street);
        }

        if (!trim($street)) {
            $street = 'no location';
        }

        $serviceName = $this->getService() ? $this->getService()->getName() : 'no service';
        return sprintf('%s/%s', $street, $serviceName);
    }

    /**
     * Sets original dates for comparison in doctrine hooks
     * @param    DateTime   startDate
     * @param    DateTime   endDate
     */
    public function setOriginalDates(\DateTime $start, \DateTime $end = null)
    {
        if ($this->originalDates) {
            return;
        }

        $this->originalDates = array('start' => clone $start, 'end' => $end ? clone $end : null);
    }

    public function getOriginalDates()
    {
        return $this->originalDates;
    }

    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    /**
     * Set radius
     *
     * @param Orc\BookingBundle\Entity\CrewRadius $radius
     * @return Booking
     */
    public function setRadius(\Orc\BookingBundle\Entity\CrewRadius $radius = null)
    {
        $this->radius = $radius;
        return $this;
    }

    /**
     * Get radius
     *
     * @return Orc\BookingBundle\Entity\CrewRadius
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * Set region
     *
     * @param Orc\BookingBundle\Entity\Region $region
     * @return Booking
     */
    public function setRegion(\Orc\BookingBundle\Entity\Region $region = null)
    {
        $this->region = $region;
    }

    /**
     * Get region
     *
     * @return Orc\BookingBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Is the booking read only? (has the date passed)
     */
    public function isReadOnly()
    {
        return $this->getDateEnd() && new DateTime() > $this->getDateEnd();
    }

    public function isRequest()
    {
        return $this->status == BookingStatus::STATUS_REQUESTED;
    }

    /**
     * Set user
     *
     * @param Orc\UserBundle\Entity\User $user
     * @return Booking
     */
    public function setUser(\Orc\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Orc\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add workerNotes
     *
     * @param Orc\BookingBundle\Entity\BookingNote $workerNotes
     * @return Booking
     */
    public function addWorkerNote(\Orc\BookingBundle\Entity\BookingNote $workerNotes)
    {
        $workerNotes->setBooking($this);
        $this->workerNotes[] = $workerNotes;

        return $this;
    }

    /**
     * Remove workerNotes
     *
     * @param Orc\BookingBundle\Entity\BookingNote $workerNotes
     */
    public function removeWorkerNote(\Orc\BookingBundle\Entity\BookingNote $workerNotes)
    {
        $this->workerNotes->removeElement($workerNotes);
    }

    /**
     * Get workerNotes
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getWorkerNotes()
    {
        return $this->workerNotes;
    }

    /**
     * Gets the age of the booking in days
     *
     * @return integer
     */
    public function getAgeCreatedAt()
    {
        return $this->datePosted->diff($this->client->getDateCreated())->days;
    }

    public function isComplete()
    {
        return in_array($this->status, array(
            BookingStatus::STATUS_DELIVERED,
            BookingStatus::STATUS_BILLED
        ));
    }

    public function getDateCompleted()
    {
        foreach ($this->getWorkerNotes() as $note) {
            $date = $note->getDatePosted();
        }

        return !empty($date) ? $date : null;
    }
}
