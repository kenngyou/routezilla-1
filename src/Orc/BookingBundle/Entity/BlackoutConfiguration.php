<?php

namespace Orc\BookingBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Orc\BookingBundle\Repository\BlackoutConfigurationRepository")
 * @ORM\Table(name="blackout_configuration")
 */
class BlackoutConfiguration implements MultiTenantInterface
{
    const FREQUENCY_DAILY = 'daily';
    const FREQUENCY_WEEKLY = 'weekly';
    const FREQUENCY_MONTHLY = 'monthly';
    const FREQUENCY_YEARLY = 'yearly';

    static public function getFrequencies()
    {
        return array(
            self::FREQUENCY_DAILY,
            self::FREQUENCY_WEEKLY,
            self::FREQUENCY_MONTHLY,
            self::FREQUENCY_YEARLY
        );
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;

    /**
     * @ORM\Column(type="string")
     */
    public $name;


    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    protected $dateStart;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\NotBlank
     */
    protected $dateEnd;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $frequency;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    protected $frequencyMultiplier = 1;

    /**
     * @ORM\Column(type="array")
     */
    protected $daysOfWeek;

    /**
     * @ORM\Column(type="array")
     */
    protected $daysOfMonth;

    /**
     * @ORM\Column(type="array")
     */
    protected $monthsOfYear;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $nth;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $timeStart;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    protected $timeEnd;

    /**
     * @ORM\ManyToOne(targetEntity="Service")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $service;

    /**
     * @ORM\ManyToOne(targetEntity="Region")
     * @ORM\JoinColumn(nullable=true)
     */
    protected $region;

    /**
     * @ORM\ManyToOne(targetEntity="Crew")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $crew;

    public function __construct()
    {
        $this->daysOfWeek = array();
        $this->daysOfMonth = array();
        $this->monthsOfYear = array();

        if (!$this->timeStart) {
            $this->timeStart = new \DateTime('today 00:00:00');
        }
        if (!$this->timeEnd) {
            $this->timeEnd= new \DateTime('today 23:30:00');
        }
    }

    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     * @return BlackoutConfiguration
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     * @return BlackoutConfiguration
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set frequency
     *
     * @param string $frequency
     * @return BlackoutConfiguration
     */
    public function setFrequency($frequency)
    {
        $this->frequency = $frequency;

        return $this;
    }

    /**
     * Get frequency
     *
     * @return string
     */
    public function getFrequency()
    {
        return $this->frequency;
    }

    /**
     * Set frequencyMultiplier
     *
     * @param integer $frequencyMultiplier
     * @return BlackoutConfiguration
     */
    public function setFrequencyMultiplier($frequencyMultiplier)
    {
        $this->frequencyMultiplier = $frequencyMultiplier;

        return $this;
    }

    /**
     * Get frequencyMultiplier
     *
     * @return integer
     */
    public function getFrequencyMultiplier()
    {
        return $this->frequencyMultiplier;
    }

    /**
     * Set daysOfWeek
     *
     * @param array $daysOfWeek
     * @return BlackoutConfiguration
     */
    public function setDaysOfWeek($daysOfWeek)
    {
        $this->daysOfWeek = $daysOfWeek;

        return $this;
    }

    /**
     * Get daysOfWeek
     *
     * @return array
     */
    public function getDaysOfWeek()
    {
        return $this->daysOfWeek;
    }

    /**
     * Set timeStart
     *
     * @param \DateTime $timeStart
     * @return BlackoutConfiguration
     */
    public function setTimeStart($timeStart)
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    /**
     * Get timeStart
     *
     * @return \DateTime
     */
    public function getTimeStart()
    {
        return $this->timeStart;
    }

    /**
     * Set timeEnd
     *
     * @param \DateTime $timeEnd
     * @return BlackoutConfiguration
     */
    public function setTimeEnd($timeEnd)
    {
        $this->timeEnd = $timeEnd;

        return $this;
    }

    /**
     * Get timeEnd
     *
     * @return \DateTime
     */
    public function getTimeEnd()
    {
        return $this->timeEnd;
    }

    /**
     * Set client
     *
     * @param \Orc\SaasBundle\Entity\Client $client
     * @return BlackoutConfiguration
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set service
     *
     * @param \Orc\BookingBundle\Entity\Service $service
     * @return BlackoutConfiguration
     */
    public function setService(\Orc\BookingBundle\Entity\Service $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return \Orc\BookingBundle\Entity\Service
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set region
     *
     * @param \Orc\BookingBundle\Entity\Region $region
     * @return BlackoutConfiguration
     */
    public function setRegion(\Orc\BookingBundle\Entity\Region $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \Orc\BookingBundle\Entity\Region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set crew
     *
     * @param \Orc\BookingBundle\Entity\Crew $crew
     * @return BlackoutConfiguration
     */
    public function setCrew(\Orc\BookingBundle\Entity\Crew $crew = null)
    {
        $this->crew = $crew;

        return $this;
    }

    /**
     * Get crew
     *
     * @return \Orc\BookingBundle\Entity\Crew
     */
    public function getCrew()
    {
        return $this->crew;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BlackoutConfiguration
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set daysOfMonth
     *
     * @param array $daysOfMonth
     * @return BlackoutConfiguration
     */
    public function setDaysOfMonth($daysOfMonth)
    {
        $this->daysOfMonth = $daysOfMonth;

        return $this;
    }

    /**
     * Get daysOfMonth
     *
     * @return array
     */
    public function getDaysOfMonth()
    {
        return $this->daysOfMonth;
    }

    /**
     * Set monthsOfYear
     *
     * @param array $monthsOfYear
     * @return BlackoutConfiguration
     */
    public function setMonthsOfYear($monthsOfYear)
    {
        $this->monthsOfYear = $monthsOfYear;

        return $this;
    }

    /**
     * Get monthsOfYear
     *
     * @return array
     */
    public function getMonthsOfYear()
    {
        return $this->monthsOfYear;
    }

    public function getStepSize()
    {
        if ($this->frequency == 'daily'){
            return sprintf('%d days', $this->frequencyMultiplier);
        }

        if ($this->frequency == 'weekly') {
            return sprintf('%d weeks', $this->frequencyMultiplier);
        }

        if ($this->frequency == 'monthly'){
            return sprintf('%d months', $this->frequencyMultiplier);
        }

        if ($this->frequency == 'yearly'){
            return sprintf('%d years', $this->frequencyMultiplier);
        }
    }

    public function toBlackout(\DateTime $date)
    {
        $end = clone $date;

        $blackout = new Blackout();
        $blackout->setName($this->name);
        $blackout->setDateStart($date);
        $blackout->setDateEnd($end);
        $blackout->setTimeStart($this->timeStart);
        $blackout->setTimeEnd($this->timeEnd);
        $blackout->setCrew($this->crew);
        $blackout->setService($this->service);
        $blackout->setRegion($this->region);
        $blackout->setConfiguration($this);

        return $blackout;
    }

    /**
     * Set nth
     *
     * @param integer $nth
     * @return BlackoutConfiguration
     */
    public function setNth($nth)
    {
        $this->nth = $nth;

        return $this;
    }

    /**
     * Get nth
     *
     * @return integer
     */
    public function getNth()
    {
        return $this->nth;
    }
}
