<?php

namespace Orc\BookingBundle\Entity;

class GanttSlot
{
    protected $interval;

    protected $time;
    protected $booking;

    public function __construct($interval)
    {
        $this->interval = $interval;
    }

    public function setTime(\DateTime $time)
    {
        $this->time = $time;
    }

    public function getTime()
    {
        return $this->time;
    }

    public function setBooking(Booking $booking)
    {
        $this->booking = $booking;
    }

    public function getBooking()
    {
        return $this->booking;
    }

    public function getSlots()
    {
        if (!$this->booking) {
            return 1;
        }

        return $this->booking->getTimeRequired() / $this->interval;
    }
}
