<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Worker;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class WorkerRepository extends MultiTenantRepository
{
    public function findAll()
    {
        return parent::findBy(array('status' => Worker::STATUS_ACTIVE));
    }
}
