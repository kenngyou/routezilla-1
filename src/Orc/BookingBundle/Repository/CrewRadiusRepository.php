<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Crew;
use Doctrine\ORM\EntityRepository;

class CrewRadiusRepository extends EntityRepository
{
    public function findNextBooking(Crew $crew, \DateTime $day)
    {
        $day->setTime(0, 0);
        $end = clone $day;
        $end->setTime(0, 0);

        $query = '
            SELECT radius
                 , booking
                 , location
              FROM OrcBookingBundle:CrewRadius radius
              JOIN radius.booking booking
              JOIN booking.location location
             WHERE radius.crew = :crew
               AND radius.day >= :start
               AND radius.day <= :end
        ';

        $result = $this->getEntityManager()->createQuery($query)
        ->setParameter('start', $start)
        ->setParameter('end', $end)
        ->setParameter('crew', $crew->getId())
        ->getResult()
        ;
    }

    /**
     * Find all CrewRadius' for a Crew within a given time range
     * Results are indexed by the date
     *
     * @param    Crew
     * @param    DateTime    start time
     * @param    DateTiem    end time
     * @return   CrewRadius[]
     */
    public function findByCrewWithinRange(Crew $crew, \DateTime $start, \DateTime $end)
    {
        $start->setTime(0, 0);
        $end->setTime(0, 0);

        $query = '
            SELECT radius
                 , location
              FROM OrcBookingBundle:CrewRadius radius
              JOIN radius.location location
             WHERE radius.crew = :crew
               AND radius.day >= :start
               AND radius.day <= :end
        ';

        $result = $this->getEntityManager()->createQuery($query)
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('crew', $crew->getId())
            ->getResult()
        ;

        $out = array();
        foreach ($result as $radius) {
            $out[$radius->getDay()->format('Y-m-d')] = $radius;
        }

        return $out;
    }
}
