<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\Crew;
use \Orc\BookingBundle\Entity\DateTime;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class BlackoutRepository extends MultiTenantRepository
{
    /**
     * Finds all active/current Blackouts
     *
     * @param boolean $hide
     * @return   Blackout[]
     */
    public function findCurrent($hide = false)
    {
        $query = '
            SELECT blackout
                 , region
                 , crew
                 , service
              FROM OrcBookingBundle:Blackout blackout
            LEFT
              JOIN blackout.region region
            LEFT
              JOIN blackout.crew crew
            LEFT
              JOIN blackout.service service
             WHERE blackout.client = :client
               AND blackout.dateStart <= :now
               AND blackout.dateEnd >= :now
        ' . ($hide ? ' AND blackout.configuration IS NULL ' : ''). '
            ORDER
                BY blackout.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->_em->getTenant()->getId())
            ->setParameter('now', new DateTime('today 00:00'))
            ->getResult()
        ;
    }

    /**
     * Finds all active/current Blackouts
     *
     * @param DateTime $end
     * @param boolean $hide
     * @return   Blackout[]
     */
    public function findUpcoming(\DateTime $end, $hide = false)
    {
        $start = new DateTime();

        $query = '
            SELECT blackout
                 , region
                 , crew
                 , service
              FROM OrcBookingBundle:Blackout blackout
            LEFT
              JOIN blackout.region region
            LEFT
              JOIN blackout.crew crew
            LEFT
              JOIN blackout.service service
             WHERE blackout.client = :client
               AND blackout.dateStart > :now
               AND blackout.dateStart < :end
        ' . ($hide ? ' AND blackout.configuration IS NULL ' : ''). '
            ORDER
                BY blackout.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->_em->getTenant()->getId())
            ->setParameter('now', new DateTime)
            ->setParameter('end', $end)
            ->getResult()
        ;
    }

    /**
     * Finds all affecting a date period
     * @param    Crew
     * @param    DateTime        start of period
     * @param    DateTime        end of period
     * @param    Booking|null
     * @return   Blackout[]
     */
    public function findAffectingCrew($crew, \DateTime $start, \DateTime $end, Booking $booking = null)
    {
        $query = $this->createQueryBuilder('blackout')
            #->andWhere('blackout.dateStart < :end')
            #->andWhere('blackout.dateEnd > :start')
            ->setParameter('crew', $crew->getId())
            ->andWhere('blackout.crew = :crew')
            ->orderBy('blackout.dateStart')
        ;

        $query
            #->setParameter('end', $end)
            #->setParameter('start', $start)
        ;

        if ($booking) {
            if ($booking->getRegion()) {
                $query->andWhere('(blackout.region IS NULL OR blackout.region = :region)');
                $query->setParameter('region', $booking->getRegion()->getId());
            }
            if ($booking->getService()) {
                $query->andWhere('(blackout.service IS NULL OR blackout.service = :service)');
                $query->setParameter('service', $booking->getService()->getId());
            }
        }

        return $query->getQuery()->getResult();
    }

    /**
     * Finds all affecting a Booking (date + region|service)
     * Note, Crew is ignored here since Bookings can be shifted to different Crews
     *
     * @param    Booking
     * @param    \DateTime
     * @param    \DateTime
     * @return   Blackout[]
     */
    public function findAffectingBooking(Booking $booking, \DateTime $start, \DateTime $end)
    {
        $query = '
            SELECT blackout
              FROM OrcBookingBundle:Blackout blackout
             WHERE blackout.client = :client
               AND blackout.crew IS NULL
               AND blackout.dateStart <= :end
               AND blackout.dateEnd >= :start
               AND ( blackout.region IS NULL OR blackout.region = :region )
               AND ( blackout.service IS NULL OR blackout.service = :service )
            ORDER
                BY blackout.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->_em->getTenant()->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('region', $booking->getRegion() ? $booking->getRegion()->getId() : 0)
            ->setParameter('service', $booking->getService()->getId())
            ->getResult()
        ;
    }

    /**
     * Finds all bookings that land on a particular day
     *
     * @param \DateTime $day
     * @return Blackout[]
     */
    public function findAffectingDay(\DateTime $day)
    {
        $end = clone $day;
        $end->modify('+1 day');
        $end->modify('-1 minute');

        $query = '
            SELECT blackout
              FROM OrcBookingBundle:Blackout blackout
             WHERE blackout.client = :client
               AND blackout.dateStart <= :end
               AND blackout.dateEnd >= :start
            ORDER
                BY blackout.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->_em->getTenant()->getId())
            ->setParameter('start', $day)
            ->setParameter('end', $end)
            ->getResult()
        ;
    }
}
