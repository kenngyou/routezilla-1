<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Request\SearchRequest;

use Orc\BookingBundle\Entity\Booking;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\BookingBundle\Entity\CalendarSlot;
use Orc\BookingBundle\Entity\DateTime;
use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Entity\Crew;
use Doctrine\ORM\EntityRepository;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

/**
 * This is a MULTI-TENANT repository.
 * All queries will filter by the current Client unless explicity noted otherwise
 */
class BookingRepository extends MultiTenantRepository
{
    protected $lookAhead;

    public function setLookAhead($lookAhead)
    {
        $this->lookAhead = $lookAhead;
    }

    /**
     * Find all bookings in the database across all Clients
     * @return    Booking[]
     */
    public function findAllForRemoval()
    {
        $query = '
            SELECT booking
                 , location
                 , customer
                 , customer_location
                 , customer_billing

            FROM OrcBookingBundle:Booking booking
            LEFT JOIN booking.service service
            LEFT JOIN booking.location location
            LEFT JOIN booking.customer customer
            LEFT JOIN customer.location customer_location
            LEFT JOIN customer.billing customer_billing
        ';

        return $this->getEntityManager()->createQuery($query)->getResult();
    }

    public function findLast($number = 1)
    {
        $query = '
            SELECT booking
                 , crew
                 , location
                 , service
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.crew crew
              JOIN booking.location location
              JOIN booking.service service
             WHERE booking.client = :client
            ORDER
                BY booking.datePosted DESC
                 , booking.id         DESC
        ';

        $results = $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->getResult()
        ;

        $index = $number - 1;

        return count($results) ? $results[$index] : null;
    }

    /**
     * Find upcoming Bookings
     * We start query from now, and end at our lookahead period
     * @param    integer        Number of days look behind
     * @param    integer        Look as far as (# of days)
     * @return   Booking[]
     */
    public function findRecentlyBooked($lookBehind, $limit = \PHP_INT_MAX)
    {
        $query = '
            SELECT booking
                 , crew
                 , location
                 , service
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.crew crew
              JOIN booking.location location
              JOIN booking.service service
             WHERE booking.client = :client
               AND booking.datePosted > :cutoff
               AND booking.status = :status
             ORDER
                BY booking.datePosted DESC
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('cutoff', new DateTime(sprintf('-%d days', $lookBehind)))
            ->setParameter('status', BookingStatus::STATUS_ASSIGNED)
            ->setMaxResults($limit)
            ->getResult()
        ;
    }

    /**
     * Find upcoming Bookings
     * We start query from now, and end at our lookahead period
     * @return   Booking[]
     */
    public function findMatching(array $filters, $limit = \PHP_INT_MAX)
    {
        $parameters = array();

        $query = $this->createQueryBuilder('booking')
            ->select('booking', 'crew', 'location', 'service')
            ->leftJoin('booking.crew', 'crew')
            ->leftJoin('booking.location', 'location')
            ->leftJoin('booking.service', 'service')
        ;

        if (isset($filters['status']) and $filters['status'] !== '') {
            $query->andWhere('booking.status = :status');
            $query->setParameter('status', $filters['status']);
        } else {
            $query->andWhere('booking.status IN (:statuses)');
            $query->setParameter('statuses', array(
                BookingStatus::STATUS_ASSIGNED,
                BookingStatus::STATUS_DELIVERED,
                BookingStatus::STATUS_BILLED,
                BookingStatus::STATUS_DRAFT,
                BookingStatus::STATUS_UNASSIGNABLE
            ));
        }

        if (!empty($filters['start']) and !empty($filters['end'])) {
            $query->andWhere('booking.dateStart > :start');
            $query->andWhere('booking.dateEnd < :end');
            $query->setParameter('start', new DateTime($filters['start']));
            $query->setParameter('end', new DateTime($filters['end']));
        }

        $sortBy = empty($filters['sort']) || $filters['sort'] == 'date'
            ? 'booking.dateStart'
            : 'booking.datePosted'
        ;
        $query->orderBy($sortBy, "DESC");

        return $query->getQuery()->getResult();
    }

    /**
     * Find upcoming days, with bookings attached
     * @return   CalendarSlot[]
     */
    public function findUpcomingDays()
    {
        $query = '
            SELECT booking.dateStart
              FROM OrcBookingBundle:Booking booking
             WHERE booking.client = :client
               AND booking.dateEnd > :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
        ';

        $results = $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('start', new DateTime('last month'))
            ->setParameter('end', new DateTime($this->lookAhead))
            ->setParameter('statuses', $this->getValidStatuses(true))
            ->getResult()
        ;

        $days = array();
        foreach ($results as $booking) {
            $dateKey = $booking['dateStart']->format('m/d/Y');

            if (empty($days[$dateKey])) {
                $slot = new CalendarSlot();
                $slot->setDate(clone $booking['dateStart']);
                $slot->getDate()->setTime(0, 0);
                $days[$dateKey] = $slot;
            } else {
                $slot = $days[$dateKey];
            }

            $slot->addBooking();
        }

        return $days;
    }

    /**
     * Find days that contain bookings for a given month
     *
     * @param    DateTime
     * @return   CalendarSlot[]
     */
    public function findBookedDaysInMonth(\DateTime $start, \DateTime $end)
    {
        $query = '
            SELECT booking.dateStart
              FROM OrcBookingBundle:Booking booking
             WHERE booking.client = :client
               AND booking.dateEnd > :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
        ';

        $results = $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('statuses', $this->getValidStatuses(true))
            ->getResult()
        ;

        $days = array();
        foreach ($results as $booking) {
            $dateKey = $booking['dateStart']->format('m/d/Y');

            if (empty($days[$dateKey])) {
                $slot = new CalendarSlot();
                $slot->setDate(clone $booking['dateStart']);
                $slot->getDate()->setTime(0, 0);
                $days[$dateKey] = $slot;
            } else {
                $slot = $days[$dateKey];
            }

            $slot->addBooking();
        }

        return $days;
    }

    /**
     * Find ALL bookings (across all tenants) booked for a given day
     * @param    DateTime     Day to show bookings for
     * @return   Booking[]
     */
    public function findBookedByDay(\DateTime $day)
    {
        $day->setTime(0, 0);
        $end = clone $day;
        $end->modify('+1 day');

        $query = '
            SELECT booking
                 , location
                 , service
                 , customer
                 , client
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.client client
            LEFT
              JOIN booking.customer customer
            LEFT
              JOIN booking.location location
            LEFT
              JOIN booking.service service
             WHERE booking.dateStart >= :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('start', $day)
            ->setParameter('end', $end)
            ->setParameter('statuses', array(BookingStatus::STATUS_ASSIGNED, BookingStatus::STATUS_UNASSIGNABLE))
            ->getResult()
        ;
    }

    /**
     * Find all Crews and their assigned Bookings for a given Day
     * @param    DateTime        Day to show bookings on
     * @return   Crew[]          Crews and their bookings
     */
    public function findDailyBookingsGroupedByCrew(\DateTime $day)
    {
        $day->setTime(0, 0);
        $end = clone $day;
        $end->modify('+1 day');

        $query = '
            SELECT booking
                 , crew
                 , color
                 , location
                 , service
                 , radius
                 , radius_location
              FROM OrcBookingBundle:Crew crew
            LEFT
              JOIN crew.color color
            LEFT
              JOIN crew.bookings booking
              WITH booking.dateStart >= :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
            LEFT
              JOIN booking.location location
            LEFT
              JOIN booking.service service
            LEFT
              JOIN booking.radius radius
            LEFT
              JOIN radius.location radius_location
             WHERE crew.client = :client
               AND ( crew.status <> :deleted OR crew.bookings IS NOT EMPTY )
            ORDER
                BY crew.name
                 , booking.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('start', $day)
            ->setParameter('end', $end)
            ->setParameter('statuses', $this->getValidStatuses(false))
            ->setParameter('deleted', Crew::STATUS_DELETED)
            ->getResult()
        ;
    }

    public function findDailyUnassignedBookings(\DateTime $day)
    {
        $day->setTime(0, 0);
        $end = clone $day;
        $end->modify('+1 day');

        $query = '
            SELECT booking
                 , location
                 , service
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.location location
            LEFT
              JOIN booking.service service
             WHERE booking.client = :client
               AND booking.dateStart >= :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
               AND booking.crew IS NULL
            ORDER
                BY booking.dateStart
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('start', $day)
            ->setParameter('end', $end)
            ->setParameter('statuses', array(
                BookingStatus::STATUS_UNASSIGNABLE,
                BookingStatus::STATUS_DELIVERED,
                BookingStatus::STATUS_BILLED
            ))
            ->getResult()
        ;
    }

    /**
     * Find assigned Bookings
     * @return   Booking[]
     */
    public function findUnassigned($limit)
    {
        $query = '
            SELECT booking
                 , crew
                 , location
                 , service
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.crew crew
              JOIN booking.location location
              JOIN booking.service service
             WHERE booking.client = :client
               AND booking.dateEnd > :start
               AND booking.dateEnd < :end
               AND booking.status = :status
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('start', new DateTime())
            ->setParameter('end', new DateTime($this->lookAhead))
            ->setParameter('status', BookingStatus::STATUS_UNASSIGNABLE)
            ->setMaxResults($limit)
            ->getResult()
        ;
    }

    /**
     * Find upcoming Bookings for a Crew
     * We start query from tomorrow, midnight, and end at our lookahead period (def 1yr)
     *
     * @param    Crew
     * @param    \DateTime  Day to show
     * @return   Booking[]
     */
    public function findUpcomingByCrew(Crew $crew, \DateTime $end = null, \DateTime $start = null)
    {
        $query = '
            SELECT b
              FROM OrcBookingBundle:Booking b
             WHERE b.crew = :crew
               AND b.dateStart > :start
               AND b.dateEnd < :end
               AND b.status IN (:statuses)
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('crew', $crew->getId())
            ->setParameter('start', $start ?: new DateTime('tomorrow'))
            ->setParameter('end', $end ?: new DateTime($this->lookAhead))
            ->setParameter('statuses', $this->getValidStatuses(false))
            ->getResult()
        ;
    }

    public function findByDateForCrew(Crew $crew, \DateTime $date)
    {
        $date = clone $date;
        $date->setTime(0, 0);
        $start = $date;
        $end   = clone $start;
        $end->modify('+24 hours');

        $query = '
            SELECT b
              FROM OrcBookingBundle:Booking b
             WHERE b.crew = :crew
               AND b.dateStart > :start
               AND b.dateEnd < :end
               AND b.status IN (:statuses)
             ORDER
                BY b.dateStart ASC
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('crew', $crew->getId())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->setParameter('statuses', $this->getValidStatuses(false))
            ->getResult()
        ;
    }

    /**
     * Fetch the first-booked Booking for a Crew on a given Day
     * @param    Crew
     * @param    DateTime
     * @return   Booking|null
     */
    public function findOriginatingByCrewOn(Crew $crew, \DateTime $day)
    {
        $nextDay = clone $day;
        $nextDay->modify('+1 day');

        $query = '
            SELECT b
                 , l
              FROM OrcBookingBundle:Booking b
              JOIN b.location l
             WHERE b.crew = :crew
               AND b.status = :status
               AND b.dateStart >= :start
               AND b.dateEnd < :end
            ORDER
                BY b.datePosted
        ';

        $results = $this->getEntityManager()->createQuery($query)
            ->setParameter('crew', $crew->getId())
            ->setParameter('status', BookingStatus::STATUS_ASSIGNED)
            ->setParameter('start', $day)
            ->setParameter('end', $nextDay)
            ->getResult()
        ;

        return $results ? $results[0] : null;
    }

    public function findConflictingWith(Crew $crew, Booking $booking)
    {
        $query = '
            SELECT booking
              FROM OrcBookingBundle:Booking booking
             WHERE booking.client = :client
               AND booking.crew = :crew
               AND booking.dateStart < :end
               AND booking.dateEnd > :start
               AND booking.status IN (:statuses)
               AND booking.id <> :booking
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->getClient())
            ->setParameter('crew', $crew->getId())
            ->setParameter('start', $booking->getDateStart())
            ->setParameter('end', $booking->getDateEnd())
            ->setParameter('statuses', $this->getValidStatuses(false))
            ->setParameter('booking', $booking->getId() ?: -5)
            ->getResult()
        ;
    }

    public function findBillableByMonth(\DateTime $month, Client $client)
    {
        $end = clone $month;
        $end->modify("+1 month");

        $query = '
            SELECT booking
              FROM OrcBookingBundle:Booking booking
             WHERE booking.client = :client
               AND booking.datePosted >= :start
               AND booking.datePosted < :end
               AND booking.status IN (:statuses)
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $client)
            ->setParameter('start', $month)
            ->setParameter('end', $end)
            ->setParameter('statuses', $this->getValidStatuses())
            ->getResult()
        ;
    }

    public function search(SearchRequest $search)
    {
        $query = $this->createQueryBuilder('booking')
            ->select('booking', 'crew', 'location', 'service', 'customer')
            ->leftJoin('booking.crew', 'crew')
            ->leftJoin('booking.location', 'location')
            ->leftJoin('booking.service', 'service')
            ->leftJoin('booking.customer', 'customer')
        ;

        $fields = array(
            'name' => 'customer.name',
            'email' => 'customer.email',
            'phone' => 'customer.phone',
            'street' => 'location.street'

        );

        if ($search->getStart()) {
            $query
                ->andWhere('booking.dateStart >= :start')
                ->setParameter('start', $search->getStart())
            ;
        }
        if ($search->getEnd()) {
            $query
                ->andWhere('booking.dateStart < :end')
                ->setParameter('end', $search->getEnd())
            ;
        }

        if ($search->getStatus()) {
            $query
                ->andWhere('booking.status = :status')
                ->setParameter('status', $search->getStatus())
            ;
        }

        if ($search->getRegion()) {
            $query
                ->andWhere('booking.region = :region')
                ->setParameter('region', $search->getRegion()->getId())
            ;
        }

        if ($field = $fields[$search->getField()] and $terms = $search->getQuery()) {
            if ($search->getField() == 'phone') {
                $query
                    ->andWhere("$field LIKE :q")
                    ->setParameter('q', sprintf('%%%s%%', preg_replace('/([^\d])/', '', $terms)))
                ;

            } else {
                foreach (preg_split('/\s+/', $terms) as $i => $word) {
                    $query
                        ->andWhere("$field LIKE :q$i")
                        ->setParameter("q$i", sprintf('%%%s%%', $word))
                    ;
                }
            }


        }

        return $query->getQuery()->getResult();
    }

    public function findAllUpcoming($days)
    {
        $day = new DateTime('now');
        $day->setTime(0, 0);
        $end = clone $day;
        $end->modify(sprintf('+%d days', $days));

        $query = '
            SELECT booking
                 , location
                 , service
                 , customer
                 , client
              FROM OrcBookingBundle:Booking booking
            LEFT
              JOIN booking.client client
            LEFT
              JOIN booking.customer customer
            LEFT
              JOIN booking.location location
            LEFT
              JOIN booking.service service
             WHERE booking.dateStart >= :start
               AND booking.dateEnd < :end
               AND booking.status IN (:statuses)
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('start', $day)
            ->setParameter('end', $end)
            ->setParameter('statuses', array(BookingStatus::STATUS_ASSIGNED, BookingStatus::STATUS_UNASSIGNABLE))
            ->getResult()
        ;
    }

    protected function getValidStatuses($unassignable = false)
    {
        $statuses = array(
            BookingStatus::STATUS_ASSIGNED,
            BookingStatus::STATUS_DELIVERED,
            BookingStatus::STATUS_BILLED
        );

        if ($unassignable) {
            $statuses[] = BookingStatus::STATUS_UNASSIGNABLE;
        }

        return $statuses;
    }

    protected function getClient()
    {
        return $this->_em->getTenant();
    }
}
