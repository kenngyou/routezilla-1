<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Service;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class ServiceRepository extends MultiTenantRepository
{
    /**
     * Find all non-deleted services
     * @return Service[]
     */
    public function findAll()
    {
        return $this->findBy(
            array('status' => Service::STATUS_ACTIVE)
        );
    }

    /**
     * Find all customer-facing (active + non-deleted) services
     * @return Service[]
     */
    public function findCustomerFacing()
    {
        return $this->findBy(
            array('status' => Service::STATUS_ACTIVE, 'isActive' => true)
        );
    }

    /**
     * Return a builder for all customer-facing services
     */
    public function getBuilderForCustomerFacing()
    {
        return $this->createQueryBuilder('service')
            ->select('service')
            ->andWhere('service.isActive = 1')
            ->andWhere('service.status = ' . Service::STATUS_ACTIVE)
            ->orderBy('service.name')
        ;
    }
}
