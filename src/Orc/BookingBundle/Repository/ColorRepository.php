<?php

namespace Orc\BookingBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ColorRepository extends EntityRepository
{
    /**
     * Find a randomly unused Color
     *
     * @return   Orc\BookingBundle\Entity\CrewColor
     */
    public function findNextUnused()
    {
        $db = $this->_em->getConnection();
        $colors = $db->fetchAll('
            SELECT color_id
              FROM crew
             WHERE client_id = :client
               AND color_id IS NOT NULL
        ', array('client' => $this->_em->getTenant()->getId()));

        $ids = array();
        foreach ($colors as $color) {
            if (empty($ids[$color['color_id']])) {
                $ids[$color['color_id']] = 0;
            }
            $ids[intval($color['color_id'])]++;
        }

        $query = '
            SELECT color
              FROM OrcBookingBundle:Color color
        ';

        $results = $this->getEntityManager()
            ->createQuery($query)
            ->getResult()
        ;

        foreach ($results as $color) {
            if (!empty($ids[$color->getId()])) {
                $color->counter = $ids[$color->getId()];
            } else {
                $color->counter = 0;
            }
        }

        usort($results, array($this, 'sortResults'));
        return $results[0];
    }

    public function sortResults($a, $b)
    {
        if ($a->counter == $b->counter) {
            return rand(0, 1) ? -1 : 1;
        }

        return $a->counter < $b->counter ? -1 : 1;
    }
}
