<?php

namespace Orc\BookingBundle\Repository;

use Orc\SaasBundle\Form\Data\AnalyticsFilter;
use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\BookingStatus;
use Orc\SaasBundle\Entity\Client;
use Orc\BookingBundle\Schedule\Scheduler\SchedulerInterface;
use Doctrine\DBAL\Connection;

class StatsRepository
{
    protected $db;
    protected $scheduler;
    protected $generator;
    protected $interval;
    protected $filter;

    public function __construct(Connection $db, SchedulerInterface $scheduler, $interval)
    {
        $this->db = $db;
        $this->scheduler = $scheduler;
        $this->interval = $interval;
    }

    public function setFilter(AnalyticsFilter $filter)
    {
        $this->filter = $filter;
    }

    public function countByMethod(Client $client)
    {
        return $this->db->fetchAssoc("
            SELECT COALESCE(SUM(IF(booking.user_id IS NULL, 1, 0)), 0) AS online
                 , COALESCE(SUM(IF(booking.user_id IS NOT NULL, 1, 0)), 0) AS phone
                 , COUNT(booking.id) AS total
              FROM booking
             WHERE booking.client_id = :client
               AND booking.datePosted >= :start
               AND booking.datePosted < :end
        " . ( $this->filter->getRegion() ? " AND booking.region_id = :region" : "" ) . "
        " . ( $this->filter->getService() ? " AND booking.service_id = :service" : "") . "
        ", $this->getParameters($client));
    }

    public function countByReturning(Client $client)
    {
        return $this->db->fetchAssoc("
            SELECT COALESCE(SUM(IF(first_customer.id IS NOT NULL, 0, 1)), 0) AS first
                 , COALESCE(SUM(IF(first_customer.id IS NOT NULL, 1, 0)), 0) AS returning
                 , COUNT(booking.id) AS total
              FROM booking
            LEFT OUTER
              JOIN ( SELECT MIN(b.id) AS id
                          , c.email
                       FROM customer c
                       JOIN booking b
                         ON b.customer_id = c.id
                      WHERE b.client_id = :client
                     GROUP
                         BY c.email
                     HAVING COUNT(b.id) > 1 ) first_customer
                ON booking.id = first_customer.id
             WHERE booking.client_id = :client
               AND booking.datePosted >= :start
               AND booking.datePosted < :end
        " . ( $this->filter->getRegion() ? " AND booking.region_id = :region" : "" ) . "
        " . ( $this->filter->getService() ? " AND booking.service_id = :service" : "") . "
        ", $this->getParameters($client));
    }

    public function totalsByConversion(Client $client)
    {
        $statuses = array(
            BookingStatus::STATUS_DELIVERED,
            BookingStatus::STATUS_BILLED,
            BookingStatus::STATUS_ATTEMPT
        );

        $groupMethods = array(
            'month' => 'MONTH',
            'year' => 'YEAR',
            'day' => 'WEEKDAY'
        );

        $method = $groupMethods[$this->filter->getGroupBy()];

        return $this->db->fetchAll("
            SELECT $method(booking.datePosted) as m
                 , COALESCE(SUM(IF(booking.status = :attemptStatus, 1, 0)), 0) AS attempts
                 , COALESCE(SUM(IF(booking.status = :attemptStatus, 0, 1)), 0) AS converted
                 , COUNT(booking.id) AS total
              FROM booking
             WHERE booking.client_id = :client
               AND booking.status IN (" . implode(',', $statuses) . ")
               AND booking.datePosted >= :start
               AND booking.datePosted < :end
               AND booking.user_id IS NULL
        " . ( $this->filter->getRegion() ? " AND booking.region_id = :region" : "" ) . "
        " . ( $this->filter->getService() ? " AND booking.service_id = :service" : "") . "
            GROUP
                BY MONTH(booking.datePosted)
        ", $this->getParameters($client, array('attemptStatus' => 1)));
    }

    public function countAvailabilityByPeriod(Client $client)
    {
        $crewHours = array();

        $ranges = array(
            'today' => array(new DateTime('today'), new DateTime('tomorrow')),
            'week'  => array(new DateTime('today'), new DateTime('+1 week')),
            'month' => array(new DateTime('today'), new DateTime('+1 month'))
        );

        foreach ($ranges as $name => $range) {
            list($start, $end) = $range;
            $this->scheduler->setRange($start, $end, false);

            $crewHours[$name]['available'] = $this->scheduler->countAvailableSlots($client) /  60 * $this->interval;
            $crewHours[$name]['booked'] = $this->countBookingsByPeriod($client, $start, $end);
        }

        return $crewHours;
    }

    protected function countBookingsByPeriod(Client $client, \DateTime $start, \DateTime $end)
    {
        $statuses = array(
            BookingStatus::STATUS_ASSIGNED,
            BookingStatus::STATUS_DELIVERED,
            BookingStatus::STATUS_BILLED
        );

        $out = $this->db->fetchAssoc("
            SELECT COALESCE(SUM(TIME_TO_SEC(TIMEDIFF(booking.dateEnd, booking.dateStart))), 0) AS elapsed
              FROM booking
             WHERE booking.client_id = :client
               AND booking.status IN (" . implode(',', $statuses) . ")
               AND booking.dateStart >= :start
               AND booking.dateStart < :end
        ", array(
            'client' => $client->getId(),
            'start' => $start->format('Y-m-d'),
            'end' => $end->format('Y-m-d')
        ));

        return $out['elapsed'] ? $out['elapsed'] / 3600 : 0;
    }

    protected function getParameters(Client $client, array $additional = array())
    {
        $params = array_merge($additional, array(
            'client' => $client->getId(),
            'start' => $this->filter->getDateStart()->format('Y-m-d'),
            'end' => $this->filter->getDateEnd()->format('Y-m-d')
        ));

        if ($region = $this->filter->getRegion()) {
            $params['region'] = $region->getId();
        }

        if ($service = $this->filter->getService()) {
            $params['service'] = $service->getId();
        }

        return $params;
    }
}
