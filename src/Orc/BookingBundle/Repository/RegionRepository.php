<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Region;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class RegionRepository extends MultiTenantRepository
{
    public function findAll()
    {
        return $this->findBy(array('status' => Region::STATUS_ACTIVE));
    }
}
