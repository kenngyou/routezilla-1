<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Booking;
use Doctrine\ORM\EntityRepository;

class BookingNoteRepository extends EntityRepository
{
    public function findByBooking(Booking $booking)
    {
        $query = '
            SELECT note
              FROM OrcBookingBundle:BookingNote note
            LEFT
              JOIN note.worker worker
            LEFT
              JOIN worker.user user
             WHERE note.booking = :booking
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('booking', $booking->getId())
            ->getResult()
        ;
    }
}
