<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\Crew;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class CrewRepository extends MultiTenantRepository
{
    public function findAll()
    {
        return $this->findBy(array('status' => Crew::STATUS_ACTIVE));
    }

    public function findActive()
    {
        return $this->findBy(array('status' => Crew::STATUS_ACTIVE, 'active' => 1));
    }

    public function getTenant()
    {
        return $this->_em->getTenant();
    }
}
