<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\ExportReport;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class ExportReportRepository extends MultiTenantRepository
{
    public function getTenant()
    {
        return $this->_em->getTenant();
    }
}
