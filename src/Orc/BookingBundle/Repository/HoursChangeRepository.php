<?php

namespace Orc\BookingBundle\Repository;

use Orc\BookingBundle\Entity\DateTime;
use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class HoursChangeRepository extends MultiTenantRepository
{
    public function findUpcoming($days = 7)
    {
        $query = '
            SELECT hourschange
              FROM OrcBookingBundle:HoursChange hourschange
             WHERE hourschange.client = :client
               AND hourschange.day >= :start
               AND hourschange.day <= :end
        ';

        $start = new DateTime('today 00:00');
        $end = clone $start;
        $end->modify(sprintf('+%d days', $days));

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('client', $this->_em->getTenant())
            ->setParameter('start', $start)
            ->setParameter('end', $end)
            ->getResult()
        ;
    }
}
