<?php

namespace Orc\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

class MailTestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:mail:test')
            ->setDescription('Test the mail for a given booking / event.')
            ->setDefinition(array(
                new InputArgument('type', InputArgument::REQUIRED, 'The mail type to test.'),
                new InputArgument('booking', InputArgument::REQUIRED, 'The Booking ID to use')
            ))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $service = sprintf('orc_booking.mailer.%s', $input->getArgument('type'));
        if (!$this->getContainer()->has($service)) {
            return $output->writeln("<error>Invalid mail type requested.</error> Valid options: confirmation, reminder, cancellation, change, notification");
        }

        $bookingRepository = $this->getContainer()->get('orc_booking.repository.booking');

        if (!$booking = $bookingRepository->findOneById($input->getArgument('booking'))) {
            return $output->writeln("<error>Booking not found</error>");
        }

        $mailer = $this->getContainer()->get($service);
        $mailer->send($booking);

        $output->writeln(sprintf(
            "<info>%s</info> mail has been sent to <info>%s</info> for <info>%s</info> successfully",
            ucfirst($input->getArgument('type')),
            $booking->getCustomer()->getName(),
            $booking->getName()
        ));

        $this->flush($this->getContainer()->get('mailer'));
    }

    protected function flush($mailer)
    {
        $transport = $mailer->getTransport();

        if ($transport instanceof \Swift_Transport_SpoolTransport) {
            $spool = $transport->getSpool();
            $sent = $spool->flushQueue($this->getContainer()->get('swiftmailer.transport.real'));
        }
    }
}
