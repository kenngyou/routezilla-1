<?php

namespace Orc\BookingBundle\Command;

use Orc\BookingBundle\Entity\DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendInactivityRemindersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:mail:send-inactivity-reminders')
            ->setDescription('Sends out emails to clients who haven\'t logged in for a while.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $mailer = $this->getContainer()->get('orc_saas.mailer.inactive');
        $repository = $this->getContainer()->get('orc_saas.repository.client');

        foreach ($repository->findInactive($this->getContainer()->getParameter('orc_saas.mailer.inactive_days')) as $client) {
            $mailer->send($client);
            $output->writeln(sprintf(
                'Inactivity notice mail has been spooled to <info>%s</info>',
                $client->getName()
            ));

            $client->setLastNotification(new DateTime());
            $em->persist($client);
        }

        $em->flush();
    }
}
