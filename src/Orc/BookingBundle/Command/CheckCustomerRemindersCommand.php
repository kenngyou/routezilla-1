<?php

namespace Orc\BookingBundle\Command;

use Orc\BookingBundle\Entity\DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CheckCustomerRemindersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:mail:check-customer-reminders')
            ->setDescription('Check sending out emails to customers the day before')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $reminding = $this->getContainer()->get('orc_booking.reminder');

        $counter = 0;
        foreach ($reminding->findRemindable() as $booking) {
            $counter++;
            $output->writeln(sprintf(
                'Confirmation mail would have been spooled to <info>%s</info> for <info>%s</info>',
                $booking->getCustomer()->getName(),
                $booking->getName()
            ));
        }

        $output->writeln("Would have sent to $counter users");
    }
}
