<?php

namespace Orc\BookingBundle\Command;

use Orc\BookingBundle\Entity\DateTime;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SendCustomerRemindersCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:mail:send-customer-reminders')
            ->setDescription('Sends out emails to customers the day before')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('orc_booking.mailer.reminder');
        $reminding = $this->getContainer()->get('orc_booking.reminder');

        foreach ($reminding->findRemindable() as $booking) {
            $mailer->send($booking);
            $output->writeln(sprintf(
                'Confirmation mail has been spooled to <info>%s</info> for <info>%s</info>',
                $booking->getCustomer()->getName(),
                $booking->getName()
            ));
        }
    }
}
