<?php

namespace Orc\BookingBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClearCustomerDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:clear:customer-data')
            ->setDescription('Removes all customer data (bookings, locations, customer, etc.)')
            ->setDefinition(array(
                new InputOption('force', null, InputOption::VALUE_NONE, 'Confirms action on production environment.')
            ))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('force')) {
            $output->write('ATTENTION: This operation should not be executed in a production environment.' . PHP_EOL . PHP_EOL);
            $output->writeln("Use <info>--force</info> to confirm");
            return;
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $bookingRepository = $this->getContainer()->get('orc_booking.repository.booking');
        $customerRepository = $this->getContainer()->get('orc_booking.repository.customer');

        foreach ($bookingRepository->findAllForRemoval() as $booking) {
            if ($location = $booking->getLocation()) {
                $em->remove($location);
            }

            if ($radius = $booking->getRadius()) {
                $em->remove($radius);
            }

            $em->remove($booking);
        }

        foreach ($customerRepository->findAll() as $customer) {
            if ($customer = $booking->getCustomer()) {
                if ($location = $customer->getLocation()) {
                    $em->remove($location);
                }

                if ($billing = $customer->getBilling()) {
                    if ($billingLocation = $billing->getLocation()) {
                        $em->remove($billingLocation);
                    }
                    $em->remove($billing);
                }
                $em->remove($customer);
            }
        }

        $em->flush();
        $output->writeln("Success - all customer data has been removed");
    }
}
