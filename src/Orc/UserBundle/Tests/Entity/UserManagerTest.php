<?php


namespace Orc\UserBundle\Tests\Entity;

use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Orc\UserBundle\Entity\UserManager;

class UserManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testCreateUserWithoutTenantHasNoTenant()
    {
        return;
        $manager = $this->getManager();
        $user = $manager->createUser();

        $this->assertNull($user->getTenant());
    }

    public function testCreateUserWithTenantSetsTenant()
    {
        return;
        $tenant = $this->getMock('Synd\MultiTenantBundle\Entity\TenantInterface');
        $manager = $this->getManager();
        $manager->setTenant($tenant);

        $user = $manager->createUser();

        $this->assertSame($tenant, $user->getTenant());
    }

    protected function getManager()
    {
        return new UserManager(
            $this->getMock('Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface'),
            $this->getMock('FOS\UserBundle\Util\CanonicalizerInterface'),
            $this->getMock('FOS\UserBundle\Util\CanonicalizerInterface'),
            $this->getEntityManager(),
            'Orc\UserBundle\Tests\Entity\MultiTenantUser'
        );
    }

    protected function getEntityManager()
    {
        $mock = $this ->getMockBuilder('Doctrine\ORM\EntityManager')
            ->disableOriginalConstructor()
            ->getMock();

        $mock
            ->expects($this->any())
            ->method('getRepository')
            ->will($this->returnValue(null));

        $mock
            ->expects($this->any())
            ->method('getClassMetadata')
            ->will($this->returnValue((object)array('name' => 'Orc\UserBundle\Tests\Entity\MultiTenantUser')))
        ;

        return $mock;
    }
}

class MultiTenantUser implements MultiTenantInterface
{
    protected $tenant;

    public function getTenant()
    {
        return $this->tenant;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->tenant = $tenant;
    }
}
