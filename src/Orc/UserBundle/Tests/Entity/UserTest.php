<?php

namespace Orc\UserBundle\Tests\Entity;

use Orc\UserBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    public function testGetTenant()
    {
        $user = new User();
        $user->setTenant($tenant = $this->getMock('Synd\MultiTenantBundle\Entity\TenantInterface'));

        $this->assertSame($tenant, $user->getTenant());
    }
}
