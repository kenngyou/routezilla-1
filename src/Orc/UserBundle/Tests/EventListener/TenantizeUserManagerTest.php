<?php

namespace Orc\UserBundle\Tests\EventListener;

use Orc\UserBundle\EventListener\TenantizeUserManager;
use Synd\MultiTenantBundle\Event\TenantEvent;

class TenantizeUserManagerTest extends \PHPUnit_Framework_TestCase
{
    public function testUserManagerGetsTenant()
    {
        $tenant = $this->getMock('Synd\MultiTenantBundle\Entity\TenantInterface');
        $userManager = $this->getMock('Synd\MultiTenantBundle\Entity\MultiTenantInterface');

        $listener = new TenantizeUserManager($userManager);

        $userManager
            ->expects($this->any())
            ->method('setTenant')
            ->with($tenant)
        ;

        $event = new TenantEvent($tenant);
        $listener->onTenantFound($event);

    }
}
