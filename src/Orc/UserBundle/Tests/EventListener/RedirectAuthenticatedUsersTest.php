<?php

namespace Orc\UserBundle\Tests\EventListener;

use Orc\UserBundle\EventListener\RedirectAuthenticatedUsers;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class RedirectAuthenticatedUsersTest extends \PHPUnit_Framework_TestCase
{
    protected $router;
    protected $listener;

    protected function setUp()
    {
        $this->router = $this->getMock('Symfony\Component\Routing\RouterInterface');
        $this->router
            ->expects($this->any())
            ->method('generate')
            ->will($this->returnValue('yoyoma'));

        $this->listener = new RedirectAuthenticatedUsers();
        $this->listener->setRouting($this->router, 'some_route');
        $this->listener->setPatterns(array('^/login$'));
    }

    protected function tearDown()
    {
        $this->router = null;
        $this->listener = null;
    }

    public function testUnauthenticatedUsersDoesNothing()
    {
        $this->listener->setSecurity($this->getContext('IS_AUTHENTICATED_ANONYMOUSLY'), 'ROLE_USER');
        $this->listener->onKernelRequest($event = $this->getEvent('/'));
        $this->assertInternalType('null', $event->getResponse());
    }

    public function testAuthenticateUserIsRedirectedWhenRouteMatches()
    {
        $this->listener->setSecurity($this->getContext('ROLE_USER'), 'ROLE_USER');
        $this->listener->onKernelRequest($event = $this->getEvent('/login'));
        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
    }

    public function testAuthenticateUserIsNotRedirectedWhenNoRouteMatches()
    {
        $this->listener->setSecurity($this->getContext('ROLE_USER'), 'ROLE_USER');
        $this->listener->onKernelRequest($event = $this->getEvent('/some_safe_path'));
        $this->assertInternalType('null', $event->getResponse());
    }

    /**
     * Returns a security.context where the user has $role
     * @param    string        Role name
     * @return   Symfony\Component\Security\Core\SecurityContextInterface
     */
    protected function getContext($role)
    {
        $mock = $this->getMock('Symfony\Component\Security\Core\SecurityContextInterface', array('getToken', 'setToken', 'isGranted'));
        $mock
            ->expects($this->any())
            ->method('isGranted')
            ->will($this->returnCallback(function($testRole) use ($role) {
                return $testRole === $role;
            }))
        ;

        $mock
            ->expects($this->any())
            ->method('getToken')
            ->will($this->returnValue($this->getToken()))
        ;


        return $mock;
    }

    protected function getToken()
    {
        return $this->getMock('Symfony\Component\Security\Core\Token\TokenInterface');
    }

    /**
     * Returns an event to trigger the listener with
     * @param    string        URI to request
     * @return   Symfony\Component\HttpKernel\Event\GetResponseEvent
     */
    protected function getEvent($uri)
    {
        $kernel = $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface');
        $event = new GetResponseEvent(
            $kernel,
            Request::create($uri),
            HttpKernelInterface::MASTER_REQUEST
        );

        return $event;
    }
}
