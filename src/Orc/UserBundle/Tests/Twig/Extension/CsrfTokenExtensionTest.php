<?php

namespace Orc\UserBundle\Tests\Twig\Extension;

use Orc\UserBundle\Twig\Extension\CsrfTokenExtension;

class CsrfTokenExtensionTest extends \PHPUnit_Framework_TestCase
{
    public function testGuestAddsCsrfGlobal()
    {
        $extension = $this->getInstance('IS_AUTHENTICATED_ANONYMOUSLY', $token = 'auth token');
        $globals = $extension->getGlobals();

        $this->assertEquals($globals['csrfToken'], $token);
    }

    public function testAuthenticatedUserDoesNotAddGlobal()
    {
        $extension = $this->getInstance('ROLE_USER', $token = 'logged in token');
        $this->assertCount(0, $extension->getGlobals());
    }

    protected function getInstance($role, $token)
    {
        return new CsrfTokenExtension(
            $this->getContext($role),
            $this->getProvider($token)
        );
    }

    protected function getContext($role)
    {
        $mock = $this->getMock('Symfony\Component\Security\Core\SecurityContextInterface', array('getToken', 'setToken', 'isGranted'));
        $mock->expects($this->any())
             ->method('isGranted')
             ->will($this->returnCallback(function($testRole) use ($role) {
                return $testRole === $role;
            }));

        return $mock;
    }

    protected function getProvider($token)
    {
        $mock = $this->getMock('Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface');
        $mock->expects($this->any())
             ->method('generateCsrfToken')
             ->will($this->returnValue($token));

        return $mock;
    }
}
