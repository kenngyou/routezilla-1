<?php

namespace Orc\UserBundle;

use Orc\UserBundle\DependencyInjection\Compiler\ChangeUserManagerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrcUserBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ChangeUserManagerPass());
    }

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
