<?php

namespace Orc\UserBundle\Repository;

use Doctrine\ORM\EntityRepository;

use Synd\MultiTenantBundle\ORM\Repository\MultiTenantRepository;

class UserRepository extends MultiTenantRepository
{
    protected $tenantField = 'client';

    public function loadUserByUsername($username)
    {
        return EntityRepository::findOneBy(array('username' => $username));
    }

    public function findDuplicates($criteria)
    {
        return EntityRepository::findBy($criteria);
    }
}
