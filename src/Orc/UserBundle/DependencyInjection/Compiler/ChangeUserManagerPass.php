<?php

namespace Orc\UserBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class ChangeUserManagerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('fos_user.user_manager.default')) {
            return;
        }

        $definition = $container->getDefinition('fos_user.user_manager.default');
        $definition->setClass('Orc\UserBundle\Entity\UserManager');

        $definition = $container->getDefinition('fos_user.registration.form.type');
        $definition->setClass('Orc\UserBundle\Form\Type\RegistrationFormType');

        $definition = $container->getDefinition('fos_user.profile.form.type');
        $definition->setClass('Orc\UserBundle\Form\Type\ProfileFormType');
    }
}
