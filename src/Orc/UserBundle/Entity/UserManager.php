<?php

namespace Orc\UserBundle\Entity;

use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class UserManager extends FOSUserManager implements MultiTenantInterface
{
    protected $tenant;

    public function getTenant()
    {
        return $this->tenant;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->tenant = $tenant;
    }

    /**
     * Set the tenant for new users
     * {@inheritDoc}
     */
    public function createUser()
    {
        $user = parent::createUser();
        if ($this->tenant) {
            $user->setTenant($this->tenant);
        }

        return $user;
    }

    /**
     * Find a user by username
     * If it belongs to a tenant, ensure it matches the current tenant.
     *
     * @param    string        Username to search for
     * @return   User
     * @throws   UsernameNotFoundException
     */
    public function loadUserByUsername($username)
    {
        if (!$user = $this->repository->loadUserByUsername($username)) {
            throw new UsernameNotFoundException(sprintf('No user with name "%s" was found.', $username));
        }

        if ($this->tenant and $user->getTenant() and $user->getTenant()->getId() != $this->tenant->getId()) {
            throw new UsernameNotFoundException(sprintf('No user with name "%s" was found.', $username));
        }

        return $user;
    }
}
