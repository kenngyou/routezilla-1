<?php

namespace Orc\UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\ExecutionContext;

/**
 * @ORM\Entity(repositoryClass="Orc\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @UniqueEntity(fields={"emailCanonical"}, message="That email is already in use", repositoryMethod="findDuplicates")
 * @UniqueEntity(fields={"usernameCanonical"}, message="That email is already in use", repositoryMethod="findDuplicates")
 * @Assert\Callback(methods={"hasEmailAndPassword"})
 */
class User extends BaseUser implements MultiTenantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     */
    protected $client;


    /**
     * Also sets the username as the email
     *
     * {@inheritDoc}
     */
    public function setEmail($email)
    {
        $this->username = $email;
        return parent::setEmail($email);
    }

    /**
     * Also sets the username as the email
     *
     * {@inheritDoc}
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->usernameCanonical = $emailCanonical;
        return parent::setEmailCanonical($emailCanonical);
    }

    /**
     * Tosses away username changes in favor for Email field
     *
     * {@inheritDoc}
     */
    public function setUsername($username)
    {
        return $this;
    }

    /**
     * Tosses away username changes in favor for Email field
     *
     * {@inheritDoc}
     */
    public function setUsernameCanonical($usernameCanonical)
    {
        return $this;
    }

    public function getTenant()
    {
        return $this->client;
    }

    public function setTenant(TenantInterface $tenant)
    {
        $this->client = $tenant;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return User
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function hasEmailAndPassword(ExecutionContext $context)
    {
        if (!$this->password and !$this->plainPassword) {
            $context->addViolationAtSubPath('plainPassword', 'Please enter a password', array(), null);
        }
        if (!$this->email and !$this->username) {
            $context->addViolationAtSubPath('email', 'Please enter an email', array(), null);
        }
    }
}
