<?php

namespace Orc\UserBundle\Command;

use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use FOS\UserBundle\Command\CreateUserCommand as BaseCommand;

class CreateUserCommand extends BaseCommand
{
    protected function configure()
    {
        parent::configure();
        $this->addOption('site', null, InputOption::VALUE_OPTIONAL, 'Enter the domain to associate the user with', null);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($domain = $input->getOption('site')) {
            $repository = $this->getContainer()->get('orc_saas.repository.site');

            if (!$site = $repository->findOneByDomain($domain)) {
                $output->writeln(sprintf('<error>Site "%s" not found</error>', $domain));
                return;
            }

            $userManager = $this->getContainer()->get('fos_user.user_manager');
            $userManager->setTenant($site);
        }

        parent::execute($input, $output);
    }
}
