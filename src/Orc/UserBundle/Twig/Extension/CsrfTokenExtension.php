<?php

namespace Orc\UserBundle\Twig\Extension;

use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Form\Extension\Csrf\CsrfProvider\CsrfProviderInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Twig_Extension;
use Twig_Filter_Method;
use Twig_Function_Method;

class CsrfTokenExtension extends Twig_Extension
{
    protected $context;
    protected $csrfProvider;

    public function __construct(SecurityContextInterface $context, CsrfProviderInterface $csrfProvider)
    {
        $this->context = $context;
        $this->csrfProvider = $csrfProvider;
    }

    public function getGlobals()
    {
        try {
            if (!$this->context->isGranted('ROLE_USER')) {
                return array('csrfToken' => $this->csrfProvider->generateCsrfToken('authenticate'));
            }
        } catch (AuthenticationCredentialsNotFoundException $e) {

        }

        return array();
    }

    public function getName()
    {
        return 'orc_user_csrf';
    }

}
