<?php

namespace Orc\UserBundle\Form\DataTransformer;

use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

class UserFieldsToUserTransformer implements DataTransformerInterface
{
    protected $userManager;

    public function __construct(UserManagerInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function transform($value)
    {
        if (!$value) {
            return array();
        }

        return array(
            'email' => $value->getEmail()
        );
    }

    public function reverseTransform($value)
    {
        if (!$user = $this->userManager->findUserByEmail($value['email'])) {
            $user = $this->userManager->createUser();
            $user->setEnabled(true);
        }

        $user->setEmail($value['email']);

        if (!empty($value['plainPassword'])) {
            $user->setPlainPassword($value['plainPassword']);
        }

        return $user;
    }
}
