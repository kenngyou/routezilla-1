<?php

namespace Orc\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserWorkerType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class, $registration)
    {
        $this->class = $class;
        $this->registration = $registration;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'form.email', 'translation_domain' => 'FOSUserBundle'))
            ->add('plainPassword', 'repeated', array(
                'type' => 'password',
                'required' => $this->registration,
                'options' => array('translation_domain' => 'FOSUserBundle'),
                'first_options' => array('label' => $this->registration ? 'form.password' : 'Change Password'),
                'second_options' => array('label' => 'Re-type'),
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention'  => 'registration',
        ));
    }

    public function getName()
    {
        return 'fos_user_registration';
    }
}
