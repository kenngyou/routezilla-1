<?php

namespace Orc\UserBundle\EventListener;

use Synd\MultiTenantBundle\Event\TenantEvent;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CheckSitePermissions
{
    protected $context;
    protected $role;
    protected $tenant;

    public function __construct(SecurityContextInterface $context, $role)
    {
        $this->context = $context;
        $this->role = $role;
    }

    public function onTenantFound(TenantEvent $event)
    {
        $this->tenant = $event->getTenant();
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->tenant or !$this->context->getToken() or !$this->context->isGranted('IS_AUTHENTICATED_FULLY')) {
            return;
        }

        $user = $this->context->getToken()->getUser();

        if ($user->getTenant() and $user->getTenant()->getId() == $this->tenant->getId()) {
            return;
        }

        if ($this->context->isGranted($this->role)) {
            return;
        }

        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST) {
            throw new AccessDeniedHttpException();
        }
    }
}
