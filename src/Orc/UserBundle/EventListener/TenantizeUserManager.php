<?php

namespace Orc\UserBundle\EventListener;

use Synd\MultiTenantBundle\Event\TenantEvent;
use Synd\MultiTenantBundle\Entity\MultiTenantInterface;

class TenantizeUserManager
{
    protected $userManager;

    public function __construct(MultiTenantInterface $userManager)
    {
        $this->userManager = $userManager;
    }

    public function onTenantFound(TenantEvent $event)
    {
        $this->userManager->setTenant($event->getTenant());
    }
}
