<?php

namespace Orc\UserBundle\EventListener;

use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class RedirectAuthenticatedUsers
{
    protected $context;

    protected $role;

    protected $router;

    protected $targetRoute;

    protected $patterns;

    public function setSecurity(SecurityContextInterface $context, $role)
    {
        $this->context = $context;
        $this->role = $role;
    }

    public function setRouting(RouterInterface $router, $targetRoute)
    {
        $this->router = $router;
        $this->targetRoute = $targetRoute;
    }

    public function setPatterns(array $patterns)
    {
        $this->patterns = $patterns;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->context->getToken() or !$this->context->isGranted($this->role)) {
            return;
        }

        $request = $event->getRequest();
        $url = $request->getPathInfo();

        foreach ($this->patterns as $pattern) {
            if (preg_match("#$pattern#", $url)) {
                $event->setResponse(new RedirectResponse($this->router->generate($this->targetRoute)));
                return;
            }
        }
    }
}
