<?php

namespace Orc\UserBundle\Features\Context;

use Orc\SaasBundle\Features\Context\FeatureContext as SaasContext;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

//
// Require 3rd-party libraries here:
//
//   require_once 'PHPUnit/Autoload.php';
//   require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class UserContext extends BehatContext
                  implements KernelAwareInterface
{
    private $kernel;
    private $parameters;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @Given /I have an admin user "([^"]*)" with password "([^"]*)"$/
     */
    public function iHaveAnAdminUser($username, $password)
    {
        $userManager = $this->kernel->getContainer()->get('fos_user.user_manager');

        $user = $userManager->createUser();
        $user->setEmail('u' . md5($username) . '@site.com');
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole(\FOS\UserBundle\Model\User::ROLE_SUPER_ADMIN);

        $userManager->updateUser($user);
    }

    /**
     * @Given /I have a user "([^"]*)" with password "([^"]*)" on site "([^"]*)"$/
     */
    public function iHaveAUser($email, $password, $domain)
    {
        $userManager = $this->kernel->getContainer()->get('fos_user.user_manager');
        $userManager->setTenant($this->getMainContext()->getSubcontext('saas')->getSubcontext('saas')->findSite($domain));

        $user = $userManager->createUser();
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled(true);

        $userManager->updateUser($user);
    }

    /**
     * @When /I log in as user "([^"]*)" with password "([^"]*)"$/
     */
    public function iLogIn($email, $password)
    {
        $this->getMainContext()->getSubcontext('mink')->visit('/login');
        $this->getMainContext()->getSubcontext('mink')->fillField('username', $email);
        $this->getMainContext()->getSubcontext('mink')->fillField('password', $password);
        $this->getMainContext()->getSubcontext('mink')->pressButton('_submit');
    }

    /**
     * @AfterScenario
     * @BeforeScenario
     */
    public function cleanup()
    {
        $db = $this->kernel->getContainer()->get('database_connection');
        $db->executeQuery('SET foreign_key_checks = 0');

        $classes = array(
            'Orc\SaasBundle\Entity\Site',
            'Orc\SaasBundle\Entity\Client',
            'Orc\UserBundle\Entity\User'
        );

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($classes as $entityClass) {
            $repo = $em->getRepository($entityClass);
            if ($entityClass == 'Orc\UserBundle\Entity\User') {
                $repo->setTenantFiltering(false);
            }
            foreach ($repo->findAll() as $entity) {
                $em->remove($entity);
            }
        }

        $em->flush();
        $db->executeQuery('SET foreign_key_checks = 1');
    }
}
