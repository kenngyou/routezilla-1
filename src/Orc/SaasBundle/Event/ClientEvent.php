<?php

namespace Orc\SaasBundle\Event;

use Orc\SaasBundle\Entity\Client;
use Symfony\Component\EventDispatcher\Event;

class ClientEvent extends Event
{
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getClient()
    {
        return $this->client;
    }
}
