<?php

namespace Orc\SaasBundle\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;

class OrcSaasExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        foreach (array('forms.yml', 'listeners.yml', 'mail.yml', 'repositories.yml', 'services.yml') as $file) {
            $loader->load($file);
        }

        $container->setParameter('orc_saas.code',         $config['code']);
        $container->setParameter('orc_saas.parentdomain', $config['domain']);
        $container->setParameter('orc_saas.admin.domain', $config['admin_subdomain']);
        $container->setParameter('orc_saas.login.domain', $config['login_subdomain']);
    }
}
