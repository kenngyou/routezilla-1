<?php

namespace Orc\SaasBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('orc_saas');

        $rootNode
            ->children()
                ->scalarNode('domain')->defaultValue('orcamax.dev')->end()
                ->scalarNode('admin_subdomain')->defaultValue('admin')->end()
                ->scalarNode('login_subdomain')->defaultValue('login')->end()
                ->scalarNode('code')->defaultValue(null)->end()
                ->scalarNode('intercom_app_id')->defaultValue(null)->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
