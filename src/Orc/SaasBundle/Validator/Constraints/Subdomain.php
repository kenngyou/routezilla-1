<?php

namespace Orc\SaasBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Subdomain extends Constraint
{
    public $message = '"%domain" is not a valid domain name.';
    public $mustStartWithAlphaNumeric = '"%domain%" must not start with a symbol.';
    public $mustEndWithAlphaNumeric = '"%domain%" must not end with a symbol.';
    public $mustOnlyContain = '"%domain%" must only contain letters, numbers and hyphens';
    public $mustBeSubdomainOf = '"%domain%" must be a subdomain of "%parent%"';
    public $alreadyExists = 'That domain is already taken.  Please choose another.';
    public $tooLong = 'The domain name must not exceed %maxLength% characters (currently at %chars%).';
    public $disallowed = 'That domain is not allowed.';

    public function validatedBy()
    {
        return 'subdomain_validator';
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}
