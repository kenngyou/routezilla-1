<?php

namespace Orc\SaasBundle\Validator\Constraints;

use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Entity\Site;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class SubdomainValidator extends ConstraintValidator
{
    protected $parentDomain;
    protected $disallowed;
    protected $siteRepository;
    protected $maxLength;

    public function __construct($parentDomain, array $disallowed, EntityRepository $siteRepository, $maxLength = 30)
    {
        $this->parentDomain = $parentDomain;
        $this->disallowed = $disallowed;
        $this->siteRepository = $siteRepository;
        $this->maxLength = $maxLength;
    }

    public function isValid($value, Constraint $constraint)
    {
        $entity = $value;

        if ($value instanceof Client) {
            $value = $value->getDomain();
        } else if ($value instanceof Site) {
            $value = $value->getDomain();
        }

        if (strpos($value, $this->parentDomain) === false) {
            $this->setMessage($constraint->mustBeSubdomainOf, array('%domain%' => $value, '%parent%' => $this->parentDomain));
            return false;
        }

        if (strlen($value) > $this->maxLength) {
            $this->setMessage($constraint->tooLong, array('%chars%' => strlen($value), '%maxLength%' => $this->maxLength));
            return false;
        }

        $domain = strtolower(str_replace('.' . $this->parentDomain, '', $value));

        if (in_array($domain, $this->disallowed)) {
            $this->setMessage($constraint->disallowed);
            return false;
        }

        if (!$this->startsWithALetter($domain)) {
            $this->setMessage($constraint->mustStartWithAlphaNumeric, array('%domain%' => $domain));
            return false;
        }

        if (!$this->endsWithASymbol($domain)) {
            $this->setMessage($constraint->mustEndWithAlphaNumeric, array('%domain%' => $domain));
            return false;
        }

        if ($this->containsBadCharacters($domain)) {
            $this->setMessage($constraint->mustOnlyContain, array('%domain%' => $domain));
            return false;
        }

        if ($this->siteAlreadyExists($value, $entity)) {
            $this->setMessage($constraint->alreadyExists);
        }

        return true;
    }

    protected function startsWithALetter($domain)
    {
        return preg_match('/^([a-z0-9])/', $domain);
    }

    protected function endsWithASymbol($domain)
    {
        return preg_match('/([a-z0-9])$/', $domain);
    }

    protected function containsBadCharacters($domain)
    {
        return preg_match('/([^-a-z0-9])/', $domain);
    }

    protected function siteAlreadyExists($domain, $current = null)
    {
        if ($current->getId()) {
            return false;
        }

        return (bool)$this->siteRepository->findOneByDomain($domain);
    }
}
