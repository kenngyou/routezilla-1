<?php

namespace Orc\SaasBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class ExportEmailsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:export-clients')
            ->setDescription('Exports all clients as CSV')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $clients = array();
        foreach ($this->getClients() as $client) {
            $clients[] = array(
                'email' => $client->getEmail(),
                'name' => $client->getName(),
                'site' => $client->getSite()->getDomain()
            );
        }

        $output->writeln($this->toCsv($clients));
    }

    protected function getClients()
    {
        $clientRepository = $this->getContainer()->get('orc_saas.repository.client');
        return $clientRepository->findAll();
    }

    protected function toCsv(array $data)
    {
        $rows = array();
        $rows[] = $this->csvRow(array_keys($data[0]));
        foreach ($data as $row) {
            $rows[] = $this->csvRow(array_values($row));
        }

        return implode("\n", $rows);
    }

    protected function csvRow(array $row)
    {
        return implode(',', array_map(array($this, 'quote'), $row));
    }

    protected function quote($text)
    {
        return '"' . addslashes($text) . '"';
    }
}
