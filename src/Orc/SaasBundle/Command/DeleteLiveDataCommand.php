<?php

namespace Orc\SaasBundle\Command;

use Stripe;
use Stripe_Customer;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

class DeleteLiveDataCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:billing:clean')
            ->setDescription('Deletes all clients')
            ->setDefinition(array(
                new InputOption('confirm', 'c', InputOption::VALUE_REQUIRED, 'Please confirm'),
            ))
        ;
    }

    /**
     * Import clients into Stripe
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('confirm') == 'adrian') {
            return $output->writeln("<error>No way dude.</error>");
        }

        Stripe::setApiKey('sk_UW8rEQWzVuD6X77NOa1hjb3JOGVOt');

        foreach (Stripe_Customer::all(array('count' => 100))->data as $customer) {
            $output->writeln(sprintf('Deleting <info>%s</info>', $customer['id']));
            $customer->delete();
        }
    }
}
