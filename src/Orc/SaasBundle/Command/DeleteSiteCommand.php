<?php

namespace Orc\SaasBundle\Command;

use Orc\SaasBundle\Entity\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DeleteSiteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:site:delete')
            ->setDescription('Delete an entire site')
            ->setDefinition(array(
                new InputArgument('domain', InputArgument::REQUIRED, 'The domain of the site')
            ))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $repository = $this->getContainer()->get('orc_saas.repository.client');
        if (!$client = $repository->findOneByDomain($domain = $input->getArgument('domain'))) {
            return $output->writeln(sprintf('<error>Site/client "%s" could not be found.', $domain));
        }

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $db = $this->getContainer()->get('database_connection');

        $db->executeQuery('SET foreign_key_checks = 0');
        $db->executeQuery('DELETE FROM user WHERE client_id = :client', array('client' => $client->getId()));
        $db->executeQuery('SET foreign_key_checks = 1');


        $this->removeBookings($client);

        $remove = array(
            'OrcBookingBundle:Blackout',
            'OrcBookingBundle:Crew',
            'OrcBookingBundle:Region',
            'OrcBookingBundle:Service',
            'OrcBookingBundle:Worker'
        );

        foreach ($remove as $entityName) {
            $repository = $em->getRepository($entityName);
            foreach ($repository->findByClient($client) as $entity) {
                $em->remove($entity);
            }
        }

        $em->remove($client);
        $em->flush();
    }

    protected function removeBookings(Client $client)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $bookingRepository = $this->getContainer()->get('orc_booking.repository.booking');
        foreach ($bookingRepository->findByClient($client) as $booking) {
            if ($location = $booking->getLocation()) {
                $em->remove($location);
            }
            if ($radius = $booking->getRadius()) {
                $em->remove($radius);
                if ($location = $radius->getLocation()) {
                    $em->remove($location);
                }
            }

            if ($customer = $booking->getCustomer()) {
                if ($location = $customer->getLocation()) {
                    $em->remove($location);
                }

                if ($billing = $customer->getBilling()) {
                    if ($billingLocation = $billing->getLocation()) {
                        $em->remove($billingLocation);
                    }
                    $em->remove($billing);
                }
                $em->remove($customer);
            }

            $em->remove($booking);
        }
    }
}
