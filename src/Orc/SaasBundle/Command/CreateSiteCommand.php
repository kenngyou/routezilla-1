<?php

namespace Orc\SaasBundle\Command;

use Orc\SaasBundle\Entity\Site;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateSiteCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('orc:site:create')
            ->setDescription('Create a new site')
            ->setDefinition(array(
                new InputArgument('domain', InputArgument::REQUIRED, 'The domain of the site')
            ))
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $site = new Site();
        $site->setDomain($input->getArgument('domain'));

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $em->persist($site);
        $em->flush();

        $output->writeln(sprintf('<info>"%s" has been created</info>', $site->getDomain()));
    }
}
