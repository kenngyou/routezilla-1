<?php

namespace Orc\SaasBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Redirects requests for admin-domain routes for tenant areas.  Hacky workaround until
 * Symfony supports domain-based routing (2.2)
 *
 * @author  Syndicate Theory
 */
class RedirectAdminRequests
{
    protected $adminDomain;

    protected $redirections;

    public function __construct($adminDomain, array $redirections)
    {
        $this->adminDomain = $adminDomain;
        $this->redirections = $redirections;
    }

    /**
     * Redirect admin paths as per $redirections keys
     * ex: / goes to /signup -> for admin domain only
     *
     * Throws a 404 for routes that are only supposed to exist in SaaS areas
     *
     * @param    Symfony\Component\HttpKernel\Event\GetResponseEvent
     * @throws   Symfony\Component\HttpKernel\Exception\NotFoundHttpException when route is admin only
     */
    public function onEarlyKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        if ($this->isOnAdminSite($request) and $this->isRedirectableRoute($request)) {
            $event->setResponse(new RedirectResponse(
                $this->redirections[$request->getPathInfo()]
            ));
        }

        if (!$this->isOnAdminSite($request) and $this->isAdminRoute($request)) {
            throw new NotFoundHttpException('Admin domain only');
        }
    }

    protected function isRedirectableRoute(Request $request)
    {
        return isset($this->redirections[$request->getPathInfo()]);
    }

    protected function isOnAdminSite(Request $request)
    {
        return strpos($request->server->get('HTTP_HOST'), $this->adminDomain) === 0;
    }

    protected function isAdminRoute(Request $request)
    {
        return strpos($request->attributes->get('_route'), 'saas') === 0;
    }
}
