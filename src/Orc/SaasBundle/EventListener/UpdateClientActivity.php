<?php

namespace Orc\SaasBundle\EventListener;

use FOS\UserBundle\Model\UserInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UpdateClientActivity
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface) {
            if ($client = $user->getClient()) {
                $client->setLastLogin(new \DateTime());
                $this->em->persist($client);
                $this->em->flush();
            }
        }
    }
}
