<?php

namespace Orc\SaasBundle\EventListener;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Listens for login and activates SnapEngage popup
 *
 * @author  Syndicate Theory
 */
class SnapEngageOnLogin
{
    public function __construct(SessionInterface $session)
    {
        $this->session = $session;
    }

    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $this->session->getFlashBag()->add('snapengage', true);
    }
}
