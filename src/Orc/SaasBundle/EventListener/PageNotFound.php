<?php

namespace Orc\SaasBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;

/**
 * Redirects 404 responses where URL has trailing slash and removes it.
 *
 * @author  Syndicate Theory
 */
class PageNotFound
{
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        if ($this->isTrailingSlash404($event)) {
            $event->setResponse(new RedirectResponse(substr($event->getRequest()->getPathInfo(), 0, -1)));
        }
    }

    protected function isTrailingSlash404($event)
    {
        if (!$event->getException() instanceof NotFoundHttpException) {
            return false;
        }

        if (substr($event->getRequest()->getPathInfo(), -1) != '/') {
            return false;
        }

        if ($event->getRequest()->getPathInfo() == '/') {
            return false;
        }

        return true;
    }
}
