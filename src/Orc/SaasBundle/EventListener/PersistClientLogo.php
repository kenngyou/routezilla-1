<?php

namespace Orc\SaasBundle\EventListener;

use Orc\SaasBundle\Entity\Client;
use Imagine\Image\ImagineInterface;
use Imagine\Image\Box;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;

/**
 * Handles persisting of file uploads when the Client entity is saved
 *
 * @author  Syndicate Theory
 */
class PersistClientLogo implements EventSubscriber
{
    protected $oldImage;

    public function __construct(ImagineInterface $imagine, $logoWidth, $logoHeight)
    {
        $this->imagine = $imagine;
        $this->logoWidth = $logoWidth;
        $this->logoHeight = $logoHeight;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::prePersist,
            Events::preUpdate,
            Events::postPersist,
            Events::postUpdate,
            Events::postRemove
        );
    }

    public function prePersist(EventArgs $event)
    {
        if ($this->isSupported($event->getEntity())) {
            $this->preSave($event->getEntity(), $event->getEntityManager());
        }
    }

    public function preUpdate(EventArgs $event)
    {
        if ($this->isSupported($event->getEntity())) {
            $this->preSave($event->getEntity(), $event->getEntityManager());
        }
    }

    public function postPersist(EventArgs $event)
    {
        if ($this->isSupported($event->getEntity())) {
            $this->postSave($event->getEntity());
        }
    }

    public function postUpdate(EventArgs $event)
    {
        if ($this->isSupported($event->getEntity())) {
            $this->postSave($event->getEntity());
        }
    }

    protected function preSave(Client $client, EntityManager $em)
    {
        if ($client->file !== null) {

            if ($client->oldBrandingLogo and $exists = $client->getAbsolutePath($client->oldBrandingLogo)) {
                $this->oldImage = $exists;
            }

            $client->setBrandingLogo(sprintf('%s.%s', uniqid(), $client->file->guessExtension()));

            $uow = $em->getUnitOfWork();
            $meta = $em->getClassMetadata(get_class($client));
            $uow->recomputeSingleEntityChangeSet($meta, $client);
        }
    }

    protected function postSave(Client $client)
    {
        if ($client->file !== null) {
            $client->file->move($client->getUploadRootDir(), $client->getBrandingLogo());
            unset($client->file);

            if ($this->oldImage) {
                unlink($this->oldImage);
            }

            $this->resize($client->getAbsolutePath(), $this->logoWidth, $this->logoHeight);
        }
    }

    public function postRemove(LifecycleEventArgs $event)
    {
        if (!$this->isSupported($event->getEntity())) {
            return;
        }

        $client = $event->getEntity();
        if ($file = $client->getAbsolutePath()) {
            unlink($file);
        }
    }

    protected function isSupported($entity)
    {
        return $entity instanceof Client;
    }

    protected function resize($imagePath, $width, $height)
    {
        $this->imagine
            ->open($imagePath)
            ->thumbnail(new Box($width, $height))
            ->save($imagePath)
        ;
    }
}
