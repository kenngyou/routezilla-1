<?php

namespace Orc\SaasBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RedirectToDomain
{
    protected $loginDomain;

    public function __construct($loginDomain, ContainerInterface $container)
    {
        $this->loginDomain = $loginDomain;
        $this->container = $container;
    }

    public function onRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $url = $request->getRequestUri();

        if (strpos($request->server->get('HTTP_HOST'), $this->loginDomain. '.') !== 0) {
            return;
        }
        if ($url == '/' or strpos($url, '/resetting') === 0) {
            return;
        }

        if ($this->container->get('security.context')->isGranted('ROLE_ADMIN')) {
            $event->setResponse(New Response('Admins cannot use global login.'));
            return;
        }

        if (!$this->container->get('security.context')->isGranted('ROLE_USER')) {
            throw new HttpNotFoundException();
        }

        $user = $this->container->get('security.context')->getToken()->getUser();
        $event->setResponse(new RedirectResponse(sprintf(
            '%s://%s%s',
            $request->getScheme(),
            $user->getClient()->getSite()->getDomain(),
            $url
        )));
    }
}
