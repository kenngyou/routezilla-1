<?php

namespace Orc\SaasBundle\EventListener;

use Orc\SaasBundle\Event\ClientEvent;
use Orc\SaasBundle\Mailer\SetupMailer;

class EmailNotificationOnSetup
{
    protected $mailer;

    public function __construct(SetupMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function onSetup(ClientEvent $event)
    {
        $this->mailer->send($event->getClient());
    }
}
