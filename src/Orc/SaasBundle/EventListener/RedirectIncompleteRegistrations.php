<?php

namespace Orc\SaasBundle\EventListener;

use Orc\SaasBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Synd\MultiTenantBundle\Event\TenantEvent;

class RedirectIncompleteRegistrations
{
    protected $session;
    protected $router;
    protected $adminDomain;
    protected $rootDomain;
    protected $client;

    public function __construct(SessionInterface $session, UrlGeneratorInterface $router, $adminDomain, $rootDomain)
    {
        $this->session = $session;
        $this->router = $router;
        $this->adminDomain = $adminDomain;
        $this->rootDomain = $rootDomain;
    }

    /**
     * Bring the Client into scope
     * @param Client $client
     */
    public function onTenantFound(TenantEvent $event)
    {
        $this->client = $event->getTenant();
    }

    /**
     * Redirects clients back to signup if they deviate
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$this->client or $this->client->getUser()) {
            return;
        }

        $this->session->set('client', $this->client->getID());
        $event->setResponse(new RedirectResponse($this->getUrl($event->getRequest())));
    }

    /**
     * Generates the URL to redirect to
     * @param Request $request
     * @return string $url
     */
    protected function getUrl(Request $request)
    {
        $https = $request->server->get('HTTPS');
        return sprintf(
            '%s://%s.%s%s',
            $https && $https !== 'off' ? 'http': 'http',
            $this->adminDomain,
            $this->rootDomain,
            $this->router->generate('saas_signup_account')
        );
    }
}
