<?php

namespace Orc\SaasBundle\EventListener;

use Synd\MultiTenantBundle\Event\TenantEvent;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Listens for the the tenant.not_found event, and throws a 404 Site Not found if domain
 * is not the administrative domain.
 *
 * @author  Syndicate Theory
 */
class TenantNotFound
{
    protected $request;
    protected $safeDomains;

    public function __construct(Request $request, array $domains)
    {
        $this->request = $request;
        $this->safeDomains = $domains;
    }

    public function onTenantNotFound(TenantEvent $event)
    {
        foreach ($this->safeDomains as $domain) {
            if (strpos($this->request->server->get('HTTP_HOST'), $domain) === 0) {
                return;
            }
        }

        throw new NotFoundHttpException('Site not found');
    }
}
