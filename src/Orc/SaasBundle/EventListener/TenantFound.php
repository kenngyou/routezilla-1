<?php

namespace Orc\SaasBundle\EventListener;

use Synd\MultiTenantBundle\Event\TenantEvent;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Listens for the the tenant.found event, passing Site to Twig as a global, and
 * Bounces client administration requests back to settings page if settings are not completed.
 *
 * @author  Syndicate Theory
 */
class TenantFound
{
    protected $twig;

    protected $route;

    public function __construct(\Twig_Environment $twig, RouterInterface $router, SessionInterface $session, $route)
    {
        $this->twig = $twig;
        $this->router = $router;
        $this->session = $session;
        $this->route = $route;
    }

    public function onTenantFound(TenantEvent $event)
    {
        $this->twig->addGlobal('site', $event->getTenant()->getSite());

        $request = $event->getRequest();
        $tenant = $event->getTenant();

        $request->attributes->set('client', $tenant);

        date_default_timezone_set($tenant->getTimezone());
    }
}
