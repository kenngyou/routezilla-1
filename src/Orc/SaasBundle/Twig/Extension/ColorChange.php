<?php

namespace Orc\SaasBundle\Twig\Extension;

class ColorChange extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            'lighten' => new \Twig_Filter_Method($this, 'lighten'),
            'darken'  => new \Twig_Filter_Method($this, 'darken')
        );
    }

    public function modify($color, $amount)
    {
        $hasHash = false;
        if (strpos($color, '#') === 0) {
            $hasHash = true;
            $color = substr($color, 1);
        }
        $number = hexdec($color);

        $red   = $this->normalize($amount + ($number >> 16));
        $blue  = $this->normalize($amount + (($number >> 8) & 0x00FF));
        $green = $this->normalize($amount + ($number & 0x0000FF));

        return sprintf(
            '%s%s%s%s',
            $hasHash ? '#' : '',
            $this->numberToHex($red),
            $this->numberToHex($blue),
            $this->numberToHex($green)
        );
    }

    public function lighten($color, $amount)
    {
        return $this->modify($color, $amount);
    }

    public function darken($color, $amount)
    {
        return $this->modify($color, -$amount);
    }

    protected function normalize($number)
    {
        return max(0, min($number, 255));
    }

    protected function numberToHex($number)
    {
        $number = dechex($number);
        if (strlen($number) == 1) {
            $number = '0' . $number;
        }
        return $number;
    }

    public function getName()
    {
        return 'colorchange';
    }
}
