Feature: Signing up as a new client
  In order to use the Orcamax system
  As a new client
  I should be able to sign up and create an account
  
  Scenario: Full Signup Process
    Given I am on "http://admin.orcamax.dev/"
     Then I should be on "/signup"
      And I should see "Sign Up"
     When I fill in "Name" with "Client A"
      And I fill in "Domain" with "clienta"
      And I press "submit"
      Then the response should contain "Created"
     Then I should see "Profile Created"
      And I should be on "/signup/account"
      And I fill in "Email" with "frank@site.com"
      And I fill in "Password" with "topsecret"
      And I fill in "Re-enter password" with "topsecret"
      And I press "submit"
    Then I should be on "/dashboard/setup/1"
    Then I should see "Setup: Company Name"
      
  Scenario: Sign up on existing site
    Given I am on "http://admin.orcamax.dev/"
      And I have a site "client.orcamax.dev"
      And I go to "/"
     When I fill in "Name" with "Client"
      And I fill in "Domain" with "client"
      And I press "submit"
     Then I should see "already taken"
      
  Scenario: Skipping to Account Page
    Given I am on "http://admin.orcamax.dev/"
     When I go to "/signup/account"
     Then I should be on "/signup"
      And I should see "Sorry"
      
  Scenario: Going to Signup After Signup
    Given I am on "http://admin.orcamax.dev/"
     When I go to "/signup"
     Then I should see "Sign Up"
     When I fill in "Name" with "Client A"
      And I fill in "Domain" with "clienta.orcamax.dev"
      And I press "submit"
      And I should be on "/signup/account"
      But I go to "/signup"
     Then I should see "already been created"
      And I should be on "/signup/account"
      
  Scenario: Skipping to Account Complete
    Given I am on "http://admin.orcamax.dev/"
     When I go to "/signup/complete"
     Then I should see "Sorry"
      And I should be on "/signup"
