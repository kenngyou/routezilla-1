Feature: Domain-Based Routing
  When I view the application from various domains
  As a user
  I should see the correct instance
  
  Scenario: View a site that doesn't exist
     Then I should get a 404 when I try to visit "http://madeup.orcamax.dev/"
     
  Scenario: View a site that exists
    Given I have a site "exists.orcamax.dev"
      And I am on "http://exists.orcamax.dev/login"
     Then I should see "login"