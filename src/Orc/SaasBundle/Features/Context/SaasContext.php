<?php

namespace Orc\SaasBundle\Features\Context;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Orc\SaasBundle\Entity\Site;
use Orc\SaasBundle\Entity\Client;
use Orc\BillingBundle\Entity\StripeCustomer;
use Symfony\Component\HttpKernel\KernelInterface;
use Behat\Symfony2Extension\Context\KernelAwareInterface;
use Behat\MinkExtension\Context\MinkContext;

use Behat\Behat\Context\BehatContext;
use Behat\Behat\Exception\PendingException;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Context\Step;

//
// Require 3rd-party libraries here:
//
require_once 'PHPUnit/Autoload.php';
require_once 'PHPUnit/Framework/Assert/Functions.php';
//

/**
 * Feature context.
 */
class SaasContext extends BehatContext //MinkContext if you want to test web
                  implements KernelAwareInterface
{
    private $kernel;
    private $parameters;

    protected $client;

    /**
     * Initializes context with parameters from behat.yml.
     *
     * @param array $parameters
     */
    public function __construct(array $parameters)
    {
        $this->parameters = $parameters;
    }

    /**
     * Sets HttpKernel instance.
     * This method will be automatically called by Symfony2Extension ContextInitializer.
     *
     * @param KernelInterface $kernel
     */
    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }


    /**
     * @Given /I should get a 404 when I try to visit "([^"]+)"$/
     */
    public function iTryToVisit($path)
    {
        try {
            $this->getMainContext()->getSubcontext('mink')->visit($path);
            assertFalse(true);
        } catch (NotFoundHttpException $e) {
            return;
        }
    }

    /**
     * @Given /I'm on "([^"]*)"$/
     */
    public function iAmOn($domain)
    {
        $this
            ->getMainContext()
            ->getSubcontext('mink')
            ->getSession()
            ->getDriver()
            ->setRequestHeader('HTTP_HOST', $domain)
        ;
    }

    /**
     * @Then /I should be on the "([^"]*)" domain$/
     */
    public function assertDomain($domain)
    {
        $serverName = $this
            ->getMainContext()
            ->getSubcontext('mink')
            ->getSession()
            ->getDriver()
            ->getClient()
            ->getRequest()
            ->server
            ->get('HTTP_HOST')
        ;

        assertEquals($domain, $serverName);
    }

    /**
     * @Given /I have a site "([^"]*)"$/
     */
    public function iHaveASite($domain)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        $site = new Site();
        $site->setDomain($domain);
        $client = new Client();
        $client->setName($domain);
        $client->setSite($site);
        $client->setStatus(Client::STATUS_ACTIVE);
        $site->setClient($client);

        $client->setCustomer($customer = new StripeCustomer());
        $customer->setLastUpdated(new \DateTime());

        $id = md5(uniqid());

        $plan = $em->getRepository('Orc\BillingBundle\Entity\Plan')->findDefault();

        $client->setStripeId($id);
        $customer->setId($id);
        $client->setPlan($plan);

        $em->persist($site);
        $em->persist($client);
        $em->persist($customer);
        $em->flush();

        $this->client = $client;
    }

    /**
     * @Given /I create a company "([^"]*)" at "([^"]*)"$/
     */
    public function iCreateACompany($name, $domain)
    {
        return array(
            new Step\Given('I am on "http://admin.orcamax.dev/signup"'),
            new Step\When(sprintf('I fill in "Name" with "%s"', $name)),
            new Step\When(sprintf('I fill in "Domain" with "%s"', $domain)),
            new Step\Then('I press "submit"')
        );
    }

    /**
     * @Given /I'm logged in as a new client$/
     */
    public function iAmLoggedInAsANewClient()
    {
        return array(
            new Step\Given('I am on "http://admin.orcamax.dev/signup"'),
            new Step\When(sprintf('I fill in "Name" with "%s"', 'Syndicate Theory')),
            new Step\When(sprintf('I fill in "Domain" with "%s"', 'syndicatetheory')),
            new Step\Then('I press "submit"'),
            new Step\Then('I fill in "Email" with "frank@site.com"'),
            new Step\Then('I fill in "Password" with "letmein"'),
            new Step\Then('I fill in "Re-enter password" with "letmein"'),
            new Step\Then('I press "submit"'),
            new Step\Then('I follow "Skip Setup for now"'),
            new Step\Then('I follow "Company Profile"'),
            new Step\Then('I press "Update Profile"')
        );
    }

    /**
     * @AfterScenario
     * @BeforeScenario
     */
    public function cleanup()
    {
        $db = $this->kernel->getContainer()->get('database_connection');
        $db->executeQuery('SET foreign_key_checks = 0');

        $types = array(
            'OrcSaasBundle:Client',
            'OrcSaasBundle:Site',
            'OrcUserBundle:User',
            'OrcBillingBundle:StripeCustomer'
        );

        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');

        foreach ($types as $type) {
            $em->createQuery('DELETE ' . $type)->execute();
        }

        $em->flush();

        $db->executeQuery('SET foreign_key_checks = 1');
    }

    public function findSite($domain)
    {
        $em = $this->kernel->getContainer()->get('doctrine.orm.entity_manager');
        $repo = $em->getRepository('Orc\SaasBundle\Entity\Client');

        return $repo->findOneByDomain($domain);
    }




    public function getClient()
    {
        return $this->client;
    }
}
