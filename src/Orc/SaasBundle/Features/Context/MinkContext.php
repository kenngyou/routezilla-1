<?php

namespace Orc\SaasBundle\Features\Context;

use Behat\Mink\Exception\ElementNotFoundException;
use Behat\MinkExtension\Context\MinkContext as BaseContext;
use Behat\Behat\Context\Step;

class MinkContext extends BaseContext
{
    protected $baseUrl;

    /**
     * Overrides routing to use last domain found
     *
     * {@inheritDoc}
     */
    public function locatePath($path)
    {
        if (strpos($path, 'http') === 0) {
            $this->baseUrl = 'http://' . parse_url($path, \PHP_URL_HOST);
            return $path;
        }

        return $this->baseUrl . $path;
    }

    /**
     * @Given /^I press the "([^"]*)" option$/
     */
    public function iTouch($label)
    {
        $mink = $this->getMainContext()->getSubcontext('mink');

        $input = $mink->getSession()->getPage()->findField($label);
        if (null === $input) {

            throw new ElementNotFoundException(
            $mink->getSession(), 'form field', 'id|name|label|value', $label
            );
        }

        $value = $input->getAttribute('value');
        $mink->fillField($label, $value);
    }

    /**
     * @Given /^I press the "([^"]*)" button$/
     */
    public function iPressHack($label)
    {
        $mink = $this->getMainContext()->getSubcontext('mink');

        $input = $mink->getSession()->getPage()->findField($label);
        if (null === $input) {

            throw new ElementNotFoundException(
            $mink->getSession(), 'form field', 'id|name|label|value', $label
            );
        }

        $value = $input->getAttribute('value');
        $mink->fillField($label, $value);

        return new Step\Then('I press "_submit"');
    }

    /**
     * @Given /^I submit$/
     */
    public function iSubmit()
    {
        return new Step\Then('I press "_submit"');
    }

    /**
     * @Then /^I should see a "([^"]*)" message containing "([^"]*)"$/
     */
    public function assertFlash($type, $message)
    {
        return new Step\Then(sprintf(
            'I should see "%s" in the "%s" element',
            $message,
            'div.alert.alert-' . $type
        ));
    }
}
