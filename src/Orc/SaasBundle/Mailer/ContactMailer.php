<?php

namespace Orc\SaasBundle\Mailer;

use Symfony\Component\HttpFoundation\Request;

use Swift_Mailer;
use Swift_Message;
use Orc\UserBundle\Entity\User;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Templating\EngineInterface;

class ContactMailer
{
    protected $mailer;
    protected $templating;
    protected $from;
    protected $to;
    protected $bcc;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress, $toAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
        $this->to = array($toAddress);
    }

    public function setBcc(array $addresses)
    {
        $this->bcc = $addresses;
    }

    /**
     * Email the product owner with new signup information
     *
     * @param    Client
     * @param    User
     * @param    array          Data
     * @return   boolean        Mail status
     */
    public function send(Client $client, User $user, Request $request, array $data)
    {
        $message = Swift_Message::newInstance()
            ->setTo($this->to)
            ->setFrom($this->from)
            ->setSubject('Message From: ' . $client->getName())
            ->setReplyTo(array($user->getEmail() => $client->getName()))
            ->setBody($this->templating->render('OrcSaasBundle:Email:contact.txt.twig', array(
                'client' => $client,
                'request' => $request,
                'user' => $user,
                'data' => $data
            )))
        ;

        if ($this->bcc) {
            $message->setBcc($this->bcc);
        }

        return $this->mailer->send($message);
    }
}
