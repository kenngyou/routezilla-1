<?php

namespace Orc\SaasBundle\Mailer;

use Swift_Mailer;
use Swift_Message;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Templating\EngineInterface;

class InactiveMailer
{
    protected $mailer;
    protected $templating;
    protected $from;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
    }

    /**
     * Email a client letting them know they haven't logged in for a while.
     *
     * @param    Client
     * @return   boolean        Mail status
     */
    public function send(Client $client)
    {
        $message = Swift_Message::newInstance()
            ->setTo($this->from)
            ->setFrom($this->from)
            ->setSubject('Routezilla Account Inactive')
            ->setBody($this->templating->render('OrcSaasBundle:Email:inactive.txt.twig', array(
                'client' => $client
            )))
        ;

        return $this->mailer->send($message);
    }
}
