<?php

namespace Orc\SaasBundle\Mailer;

use Swift_Mailer;
use Swift_Message;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Templating\EngineInterface;

class WelcomeMailer
{
    protected $mailer;
    protected $templating;
    protected $from;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
    }

    /**
     * Email the new customer a welcome email
     *
     * @param    Client
     * @return   boolean        Mail status
     */
    public function send(Client $client)
    {
        $message = Swift_Message::newInstance()
            ->setTo($client->getEmail())
            ->setFrom($this->from)
            ->setSubject('Welcome to Routezilla!')
            ->setBody($this->templating->render('OrcSaasBundle:Email:welcome.txt.twig', array(
                'client' => $client
            )))
        ;

        return $this->mailer->send($message);
    }
}
