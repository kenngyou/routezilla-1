<?php

namespace Orc\SaasBundle\Mailer;

use Swift_Mailer;
use Swift_Message;
use Orc\SaasBundle\Entity\Client;
use Symfony\Component\Templating\EngineInterface;

class SignupMailer
{
    protected $mailer;
    protected $templating;
    protected $from;
    protected $to;
    protected $bcc;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $fromName, $fromAddress, $to)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from = array($fromAddress => $fromName);
        $this->to = array($to);
    }

    public function setBcc(array $addresses)
    {
        $this->bcc = $addresses;
    }

    /**
     * Email the product owner with new signup information
     *
     * @param    Worker
     * @return   boolean        Mail status
     */
    public function send(Client $client)
    {
        $message = Swift_Message::newInstance()
            ->setTo($this->to)
            ->setFrom($this->from)
            ->setSubject('New Company Signup: ' . $client->getName())
            ->setBody($this->templating->render('OrcSaasBundle:Email:signup.txt.twig', array(
                'client' => $client
            )))
        ;

        if ($this->bcc) {
            $message->setBcc($this->bcc);
        }

        return $this->mailer->send($message);
    }
}
