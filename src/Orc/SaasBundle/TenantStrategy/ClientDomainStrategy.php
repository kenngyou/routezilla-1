<?php

namespace Orc\SaasBundle\TenantStrategy;

use Synd\MultiTenantBundle\TenantStrategy\TenantStrategyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

class ClientDomainStrategy implements TenantStrategyInterface
{
    /**
     * @var    EntityManager
     */
    protected $em;

    /**
     * @var    string        Requested host name
     */
    protected $host;

    /**
     * Brings hostname into scope from Request
     *
     * @param    EntityManager
     * @param    ContainerInterface
     */
    public function __construct(EntityManager $em, ContainerInterface $container)
    {
        $this->em = $em;
        $this->host = $container->get('request')->server->get('HTTP_HOST');
    }

    /**
     * Fetches the tenant from the database based on the Host header
     * @return    TenantInterface|null    on failure
     */
    public function getTenant()
    {
        return $this
            ->em
            ->getRepository('OrcSaasBundle:Client')
            ->findOneByDomain($this->host)
        ;
    }
}
