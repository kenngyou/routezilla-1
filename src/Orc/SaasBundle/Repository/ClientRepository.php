<?php

namespace Orc\SaasBundle\Repository;

use Orc\BookingBundle\Entity\DateTime;
use Doctrine\ORM\EntityRepository;

class ClientRepository extends EntityRepository
{
    public function findOneByStripeId($id)
    {
        return $this->findOneBy(array('stripeId' => $id));
    }

    public function findOneByDomain($domain)
    {
        $query = '
            SELECT client
                 , site
              FROM OrcSaasBundle:Client client
              JOIN client.site site
             WHERE site.domain = :domain
        ';

        $row = $this
            ->getEntityManager()
            ->createQuery($query)
            ->setParameter('domain', $domain)
            ->getResult()
        ;

        if (!empty($row[0])) {
            return $row[0];
        }
    }

    public function findOneByEmail($email)
    {
        $query = '
            SELECT client
                 , site
              FROM OrcSaasBundle:Client client
              JOIN client.site site
             WHERE client.email = :email
        ';

        $row = $this
            ->getEntityManager()
            ->createQuery($query)
            ->setParameter('email', $email)
            ->getResult()
        ;

        if (!empty($row[0])) {
            return $row[0];
        }
    }

    public function findInactive($days)
    {
        $query = '
            SELECT client
              FROM OrcSaasBundle:Client client
             WHERE client.lastLogin IS NOT NULL
               AND client.lastLogin < :cutoff
               AND client.lastNotification IS NULL
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('cutoff', new DateTime(sprintf('-%d days', $days)))
            ->getResult()
        ;
    }

    public function findDeliquent($days)
    {
        $start = new DateTime(sprintf('-%d days', $days));
        $next = clone $start;
        $next->modify('+1 day');

        $query = '
            SELECT client
              FROM OrcSaasBundle:Client client
             WHERE client.deliquentSince IS NOT NULL
               AND client.deliquentSince >= :start
               AND client.deliquentSince < :end
        ';

        return $this->getEntityManager()->createQuery($query)
            ->setParameter('start', $start)
            ->setParameter('end', $next)
            ->getResult()
        ;
    }
}
