<?php

namespace Orc\SaasBundle\Entity;

use Orc\BookingBundle\Entity\DateTime;
use Orc\BookingBundle\Entity\HoursOfOperation;
use Orc\SaasBundle\Validator\Constraints as OrcAssert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Synd\MultiTenantBundle\Entity\TenantInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="Orc\SaasBundle\Repository\ClientRepository")
 * @ORM\Table(name="client")
 * @ORM\HasLifecycleCallbacks
 * @OrcAssert\Subdomain
 */
class Client implements TenantInterface
{
    const STATUS_NEW = 0;
    const STATUS_STEP_1 = 1;
    const STATUS_STEP_2 = 2;
    const STATUS_STEP_3 = 3;
    const STATUS_STEP_4 = 4;
    const STATUS_STEP_5 = 5;
    const STATUS_ACTIVE = 10;
    const STATUS_EXPIRED = 15;
    const STATUS_CANCELLED = 16;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToOne(targetEntity="Site", cascade={"persist"})
     * @ORM\JoinColumn(name="site_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $site;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\UserBundle\Entity\User", cascade={"remove"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\OneToOne(targetEntity="Orc\BillingBundle\Entity\StripeCustomer", cascade={"remove"})
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;


    /**
     * @ORM\OneToMany(targetEntity="Orc\UserBundle\Entity\User", mappedBy="client", cascade={"remove"})
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    protected $users;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $dateCreated;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastNotification;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $website = 'http://www.yoursite.com/';

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank
     */
    protected $phone = '1-800-YOUR-NUMBER';

    /**
     * @ORM\Column(type="string")
     */
    protected $email = 'you@site.com';

    /**
     * @ORM\Column(type="integer")
     */
    protected $arrivalWindow = 2;

    /**
     * @ORM\Column(type="integer")
     */
    protected $status = self::STATUS_NEW;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $skipWizard = false;

    /**
     * @ORM\OneToMany(targetEntity="Orc\BookingBundle\Entity\HoursOfOperation", mappedBy="client", cascade={"persist", "remove"})
     */
    protected $hoursOfOperation;

    /**
     * @ORM\OneToMany(targetEntity="Orc\BookingBundle\Entity\HoursChange", mappedBy="client", cascade={"persist", "remove"})
     */
    protected $hoursChanges;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $brandingColor;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $brandingLogo;

    public $oldBrandingLogo;

    public $file;

    protected $parentDomain;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $stripeId;

    /**
     * @ORM\ManyToOne(targetEntity="Orc\BillingBundle\Entity\Plan")
     */
    protected $plan;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deliquentSince;


    /**
     * @ORM\Column(type="integer")
     */
    protected $crews = 0;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $hasTrialed = false;

    /**
     * @ORM\Column(type="decimal", scale=7)
     */
    protected $defaultRadius = 5;

    /**
     * @ORM\Column(type="string")
     */
    protected $timezone = 'America/Vancouver';

    /**
     * @ORM\Column(type="string")
     */
    protected $units = 'm';

    /**
     * Creates the Client with default Hours of Operation
     */
    public function __construct()
    {
        $this->hoursOfOperation = new ArrayCollection();
        $this->hoursChanges = new ArrayCollection();
        $this->dateCreated = new DateTime();
        $this->defaultRadius = 5;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set site
     *
     * @param Orc\SaasBundle\Entity\Site $site
     * @return Client
     */
    public function setSite(\Orc\SaasBundle\Entity\Site $site = null)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * Get site
     *
     * @return Orc\SaasBundle\Entity\Site
     */
    public function getSite()
    {
        return $this->site;
    }

    public function getDomain()
    {
        if ($this->site) {
            return $this->site->getDomain();
        }
    }

    public function setDomain($domain)
    {
        $site = new Site();
        $site->setClient($this);
        $site->setDomain($this->parentDomain ? sprintf('%s.%s', $domain, $this->parentDomain) : $domain);
        $this->setSite($site);
    }

    public function setParentDomain($domain)
    {
        $this->parentDomain = $domain;
    }

    /**
     * Set user
     *
     * @param Orc\UserBundle\Entity\User $user
     * @return Client
     */
    public function setUser(\Orc\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Orc\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getAbsolutePath($logo = null)
    {
        if (!$logo) {
            $logo = $this->brandingLogo;
        }

        return null === $logo ? null : $this->getUploadRootDir().'/'.$logo;
    }

    public function getWebPath()
    {
        return null === $this->brandingLogo ? null : '/' . $this->getUploadDir() . '/' . $this->brandingLogo;
    }

    public function getUploadRootDir()
    {
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    public function getUploadDir()
    {
        return 'uploads/branding';
    }

    /**
     * Set brandingColor
     *
     * @param string $brandingColor
     * @return Client
     */
    public function setBrandingColor($brandingColor)
    {
        $this->brandingColor = $brandingColor;
        return $this;
    }

    /**
     * Get brandingColor
     *
     * @return string
     */
    public function getBrandingColor()
    {
        return $this->brandingColor;
    }

    /**
     * Set brandingLogo
     *
     * @param string $brandingLogo
     * @return Client
     */
    public function setBrandingLogo($brandingLogo)
    {
        $this->brandingLogo = $brandingLogo;
        return $this;
    }

    /**
     * Sets a temporary branding logo, while keeping the old one.
     * Doctrine won't pick up changes to the upload field, so we have to fake one
     *
     * @param    string        Branding logo
     * @return   Client
     */
    public function setTemporaryBrandingLogo($brandingLogo)
    {
        $this->oldBrandingLogo = $this->brandingLogo;
        $this->brandingLogo = $brandingLogo;
        return $this;
    }

    /**
     * Get brandingLogo
     *
     * @return string
     */
    public function getBrandingLogo()
    {
        return $this->brandingLogo;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Client
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function hasTrialed()
    {
        return $this->hasTrialed;
    }

    public function setHasTrialed()
    {
        $this->hasTrialed = true;
    }

    public function isCancelled()
    {
        return $this->status == self::STATUS_CANCELLED;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get hoursOfOperation
     *
     * @return array
     */
    public function getHoursOfOperation()
    {
        return $this->hoursOfOperation;
    }

    /**
     * Add hoursOfOperation
     *
     * @param Orc\BookingBundle\ENtity\HoursOfOperation $hoursOfOperation
     * @return Client
     */
    public function addHoursOfOperation(\Orc\BookingBundle\Entity\HoursOfOperation $hoursOfOperation)
    {
        $hoursOfOperation->setClient($this);
        $this->hoursOfOperation[] = $hoursOfOperation;
        return $this;
    }

    /**
     * Remove hoursOfOperation
     *
     * @param <variableType$hoursOfOperation
     */
    public function removeHoursOfOperation(\Orc\BookingBundle\ENtity\HoursOfOperation $hoursOfOperation)
    {
        $this->hoursOfOperation->removeElement($hoursOfOperation);
    }

    /**
     * Add hoursChanges
     *
     * @param Orc\BookingBundle\Entity\HoursChange $hoursChanges
     * @return Client
     */
    public function addHoursChange(\Orc\BookingBundle\Entity\HoursChange $hoursChanges)
    {
        $hoursChanges->setClient($this);
        $this->hoursChanges[(string)$hoursChanges] = $hoursChanges;
        return $this;
    }

    /**
     * Remove hoursChanges
     *
     * @param <variableType$hoursChanges
     */
    public function removeHoursChange(\Orc\BookingBundle\Entity\HoursChange $hoursChanges)
    {
        $this->hoursChanges->removeElement($hoursChanges);
    }

    /**
     * Get hoursChanges
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getHoursChanges()
    {
        return $this->hoursChanges;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Client
     */
    public function setWebsite($website)
    {
        if (strpos($website, 'http') !== 0) {
            $website = 'http://' . $website;
        }

        $this->website = $website;
        return $this;
    }

    /**
     * Get website
     *
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    public function hasWebsite()
    {
        return $this->website && strlen($this->website) > 7;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Client
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set arrivalWindow
     *
     * @param integer $arrivalWindow
     * @return Client
     */
    public function setArrivalWindow($arrivalWindow)
    {
        $this->arrivalWindow = $arrivalWindow;
        return $this;
    }

    /**
     * Get arrivalWindow
     *
     * @return integer
     */
    public function getArrivalWindow()
    {
        return $this->arrivalWindow;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set skipWizard
     *
     * @param boolean $skipWizard
     * @return Client
     */
    public function setSkipWizard($skipWizard)
    {
        $this->skipWizard = (bool)$skipWizard;

        return $this;
    }

    /**
     * Get skipWizard
     *
     * @return boolean
     */
    public function getSkipWizard()
    {
        return $this->skipWizard;
    }

    /**
     * Add users
     *
     * @param Orc\UserBundle\Entity\User $users
     * @return Client
     */
    public function addUser(\Orc\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param Orc\UserBundle\Entity\User $users
     */
    public function removeUser(\Orc\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set lastLogin
     *
     * @param \DateTime $lastLogin
     * @return Client
     */
    public function setLastLogin(\DateTime $lastLogin = null)
    {
        $this->lastLogin = $lastLogin;

        return $this;
    }

    /**
     * Get lastLogin
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    /**
     * Set lastNotification
     *
     * @param \DateTime $lastNotification
     * @return Client
     */
    public function setLastNotification($lastNotification)
    {
        $this->lastNotification = $lastNotification;

        return $this;
    }

    /**
     * Get lastNotification
     *
     * @return \DateTime
     */
    public function getLastNotification()
    {
        return $this->lastNotification;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     * @return Client
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    public function isActive()
    {
        return $this->status == self::STATUS_ACTIVE;
    }

    public function isExpired()
    {
        return $this->status == self::STATUS_EXPIRED;
    }

    public function isTrialing()
    {
        return (
            $this->status == self::STATUS_STEP_5 and
            $this->plan and
            $this->plan->getTrialLength()
        );
    }

    /**
     * Set stripeId
     *
     * @param string $stripeId
     * @return Client
     */
    public function setStripeId($stripeId)
    {
        $this->stripeId = $stripeId;

        return $this;
    }

    /**
     * Get stripeId
     *
     * @return string
     */
    public function getStripeId()
    {
        return $this->stripeId;
    }

    /**
     * Set plan
     *
     * @param Orc\BillingBundle\Entity\Plan $plan
     * @return Client
     */
    public function setPlan(\Orc\BillingBundle\Entity\Plan $plan = null)
    {
        $this->plan = $plan;
        return $this;
    }

    /**
     * Get plan
     *
     * @return Orc\BillingBundle\Entity\Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    public function trialEndsWithin($days)
    {
        if ($this->plan and $this->customer->getTrialEndDate()) {
            $difference = $this->customer->getTrialEndDate()->diff(new DateTime());
            return $difference->days <= $days;
        }

        return false;
    }

    public function getTrialEnd()
    {
        if ($this->plan and $this->customer->getTrialEndDate()) {
            $difference = $this->customer->getTrialEndDate()->diff(new DateTime());
            return $difference->days;
        }

        return 0;
    }

    /**
     * Set crews
     *
     * @param integer $crews
     * @return Client
     */
    public function setCrews($crews)
    {
        $this->crews = $crews;

        return $this;
    }

    /**
     * Get crews
     *
     * @return integer
     */
    public function getCrews()
    {
        return $this->crews;
    }

    /**
     * Set deliquentSince
     *
     * @param \DateTime $deliquentSince
     * @return Client
     */
    public function setDeliquentSince($deliquentSince)
    {
        $this->deliquentSince = $deliquentSince;

        return $this;
    }

    /**
     * Get deliquentSince
     *
     * @return \DateTime
     */
    public function getDeliquentSince()
    {
        return $this->deliquentSince;
    }

    /**
     * Set defaultRadius
     *
     * @param float $defaultRadius
     * @return Client
     */
    public function setDefaultRadius($defaultRadius)
    {
        $this->defaultRadius = $defaultRadius;

        return $this;
    }

    /**
     * Get defaultRadius
     *
     * @return float
     */
    public function getDefaultRadius()
    {
        return $this->defaultRadius;
    }

    /**
     * Get hasTrialed
     *
     * @return boolean
     */
    public function getHasTrialed()
    {
        return $this->hasTrialed;
    }

    /**
     * Set customer
     *
     * @param Orc\BillingBundle\Entity\StripeCustomer $customer
     * @return Client
     */
    public function setCustomer(\Orc\BillingBundle\Entity\StripeCustomer $customer = null)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return Orc\BillingBundle\Entity\StripeCustomer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set timezone
     *
     * @param string $timezone
     * @return Client
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;

        return $this;
    }

    /**
     * Get timezone
     *
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * Set units
     *
     * @param string $units
     * @return Client
     */
    public function setUnits($units)
    {
        $this->units = $units;

        return $this;
    }

    /**
     * Get units
     *
     * @return string
     */
    public function getUnits()
    {
        return $this->units;
    }
}
