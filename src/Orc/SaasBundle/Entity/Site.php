<?php

namespace Orc\SaasBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Orc\SaasBundle\Validator\Constraints as OrcAssert;
use Synd\MultiTenantBundle\Entity\TenantInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="site")
 * @OrcAssert\Subdomain
 */
class Site implements TenantInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", unique=true)
     * @Assert\MaxLength(255)
     */
    protected $domain;

    /**
     * @ORM\OneToOne(targetEntity="Orc\SaasBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $client;


    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain
     *
     * @param string $domain
     * @return Site
     */
    public function setDomain($domain)
    {
        $this->domain = strtolower($domain);
        return $this;
    }

    /**
     * Get domain
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    public function getUrl($path = '/')
    {
        return sprintf('http://%s%s', $this->domain, $path);
    }

    /**
     * Set client
     *
     * @param Orc\SaasBundle\Entity\Client $client
     * @return Site
     */
    public function setClient(\Orc\SaasBundle\Entity\Client $client = null)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Get client
     *
     * @return Orc\SaasBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }
}
