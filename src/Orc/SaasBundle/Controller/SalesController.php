<?php

namespace Orc\SaasBundle\Controller;

use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Form\Type\FindCustomerType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Orc\SaasBundle\SaasEvents;
use Orc\SaasBundle\Event\ClientEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SalesController extends Controller
{
    public function queryAction(Request $request)
    {
        $form = $this->createForm(new FindCustomerType());
        $this->get('session')->set('client', null);

        if ($request->getMethod() === 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                $data = $form->getData();
                $repository = $this->get('orc_saas.repository.client');
                $client = $repository->findOneByEmail($data['email']);

                if ($client) {
                    $this->get('session')->set('client', $client->getId());
                    return $this->redirect($this->generateUrl('sales_review'));
                }

                $form->addError(new FormError('Could not find a customer with that email address'));
            }
        }

        return $this->render('OrcSaasBundle:Sales:query.html.twig', array(
            'form' => $form->createView()
        ));
    }

    public function reviewAction(Request $request)
    {
        $client = $this->getClient();
        $form = $this->createForm('upgrade');
        $customerService = $this->get('orc_billing.stripe.customer');
        $customer = $client->getStripeId() ? $customerService->view($client->getStripeId()) : null;

        if ($client->getStatus() == Client::STATUS_ACTIVE) {
            $existing = $customerService->fetchCardDetails($client->getStripeId());
        } else {
            $existing = null;
        }

        return $this->render('OrcSaasBundle:Sales:review.html.twig', array(
            'client' => $this->getClient(),
            'existing' => $existing,
            'customer' => $customer
        ));
    }

    public function cardAction(Request $request)
    {
        $client = $this->getClient();
        $form = $this->createForm('upgrade');
        $customerService = $this->get('orc_billing.stripe.customer');
        $customer = $client->getStripeId() ? $customerService->view($client->getStripeId()) : null;

        if ($client->getStatus() == Client::STATUS_ACTIVE) {
            $existing = $customerService->fetchCardDetails($client->getStripeId());
        }

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {
                $data = $form->getData();

                if (!$customerService->validateToken($data['card']['token'])) {
                    $this->get('session')->getFlashBag()->add('error', 'Invalid card specified');
                    return $this->redirect($this->generateUrl('sales_card'));
                }

                try {
                    $billingService = $this->get('orc_billing.service.customer');
                    $billingService->subscribe($client, $data['plan'], $data['card']['token'], $data['coupon']);
                } catch (\Stripe_CardError $e) {
                    $this->get('session')->getFlashBag()->add('error', $e->getMessage());
                    return $this->redirect($this->generateUrl('sales_card'));
                }

                $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_CONVERT, new ClientEvent($client));
                $this->get('session')->getFlashBag()->add('success', 'Subscribed successfully');
                return $this->redirect($this->generateUrl('sales_query'));
            } else {
                $messages = array();
                foreach ($form->getErrors() as $error) {
                    $messages[] = $error->getMessage();
                }
                $this->get('session')->getFlashBag()->add('error', implode(',', $messages));
            }
        }

        return $this->render('OrcSaasBundle:Sales:card.html.twig', array(
            'client' => $client,
            'existing' => !empty($existing) ? $existing : null,
            'api_public_key' => $this->container->getParameter('orc_billing.stripe.api_public_key'),
            'form' => $form->createView()
        ));
    }

    protected function getClient()
    {
        $clientId = $this->get('session')->get('client');
        if (!$clientId) {
            $this->get('session')->setFlash('error', 'Your session has expired.');
            return $this->redirect($this->generateUrl('sales_query'));
        }

        $repository = $this->get('orc_saas.repository.client');
        $client = $repository->findOneById($clientId);
        if (!$client) {
            $this->get('session')->setFlash('error', 'Your session has expired.');
            return $this->redirect($this->generateUrl('sales_query'));
        }

        return $client;
    }
}
