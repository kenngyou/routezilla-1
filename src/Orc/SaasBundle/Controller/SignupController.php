<?php

namespace Orc\SaasBundle\Controller;

use Orc\BookingBundle\Entity\DateTime;
use Orc\SaasBundle\Event\ClientEvent;
use Orc\SaasBundle\SaasEvents;
use Symfony\Component\Form\FormError;
use Orc\UserBundle\Entity\User;
use Orc\SaasBundle\Entity\Client;
use Orc\SaasBundle\Form\Type\ClientType;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Security\Core\Exception\AccountStatusException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SignupController extends Controller
{
    public function signupAction(Request $request)
    {
        if ($this->getUser()) {
            if (!$this->getUser()->getClient()) {
                return $this->redirect('/admin');
            }
            return $this->redirect($this->generateUrl('saas_signup_complete'));

        } else {
            if ($client = $this->getClientById($this->get('session')->get('client'))) {
                $this->get('session')->setFlash('success', 'Your account has already been created.');
                return $this->redirect($this->generateUrl('saas_signup_account'));
            }
        }

        $client = new Client();
        $client->setDateCreated(new DateTime());
        $client->setParentDomain($this->container->getParameter('orc_saas.parentdomain'));
        $client->setBrandingColor($this->container->getParameter('orc_saas.branding.defaultColor'));

        $form = $this->createForm(new ClientType(), $client);

        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);
            $clientInfo = $request->request->get('client');

            if ($code = $this->container->getParameter('orc_saas.code')) {
                if ($code != $clientInfo['code']) {
                    $form->addError(new FormError(
                        'Invalid access code.  Please contact %number% to contact sales.',
                        array('%number%' => $this->container->getParameter('orc_saas.phone'))
                    ));
                }
            }

            if ($form->isValid()) {
                $em = $this->get('doctrine.orm.entity_manager');
                $em->persist($client);
                $em->flush();

                $this->get('session')->set('client', $client->getId());
                $this->get('session')->setFlash('success', 'New Profile Created');


                return $this->redirect($this->generateUrl('saas_signup_account'));
            }
        }

        return $this->render('OrcSaasBundle:Signup:signup.html.twig', array(
            'form' => $form->createView(),
            'code' => $this->container->getParameter('orc_saas.code')
        ));
    }

    public function accountAction(Request $request)
    {
        if (!$client = $this->getClientById($this->get('session')->get('client'))) {
            $this->get('session')->setFlash('warning', 'Sorry, we could not find your account.');
            return $this->redirect($this->generateUrl('saas_signup'));
        }

        if ($this->getUser()) {
            $this->get('session')->setFlash('success', 'Your account is ready!');
            return $this->redirect($this->generateUrl('saas_signup_complete'));
        }

        $form = $this->get('fos_user.registration.form');
        $formHandler = $this->get('fos_user.registration.form.handler');
        $userManager = $this->get('fos_user.user_manager');
        $userManager->setTenant($client);

        $process = $formHandler->process(false);
        if ($process) {
            $user = $form->getData();
            $user->addRole('ROLE_CLIENT');
            $user->addRole('ROLE_STAFF');
            $client->setEmail($user->getEmail());

            $this->get('orc_saas.mailer.welcome')->send($client);
            $this->get('orc_saas.mailer.signup')->send($client);

            $this->authenticateUser($user);

            $client->setUser($user);

            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($client);
            $em->persist($user);

            $em->flush();

            $this->get('event_dispatcher')->dispatch(SaasEvents::CLIENT_CREATE, new ClientEvent($client));

            $this->get('session')->setFlash('success', 'New Account Created');
            return $this->redirect($this->generateUrl('saas_signup_complete'));
        }

        return $this->render('OrcSaasBundle:Signup:account.html.twig', array(
            'client' => $client,
            'form'   => $form->createView()
        ));
    }

    public function completeAction(Request $request)
    {
        if (!$this->get('security.context')->isGranted('ROLE_USER')) {
            $this->get('session')->setFlash('warning', 'Sorry, you must first create an account.');
            return $this->redirect($this->generateUrl('saas_signup_account'));
        }

        if (!$client = $this->getClientByUser($this->getUser())) {
            $this->get('session')->setFlash('warning', 'Sorry, you must first create a new account.');
            return $this->redirect($this->generateUrl('saas_signup'));
        }

        return $this->redirect(
            $client->getSite()->getUrl($this->generateUrl('dashboard'))
        );
    }

    public function campaignAction()
    {
        return $this->render('OrcSaasBundle:Signup:campaign.html.twig');
    }

    /**
     * Authenticate a user with Symfony Security
     *
     * @param \FOS\UserBundle\Model\UserInterface        $user
     */
    protected function authenticateUser(UserInterface $user)
    {
        try {
            $this->get('fos_user.security.login_manager')->loginUser(
                $this->container->getParameter('fos_user.firewall_name'),
                $user
            );
        } catch (AccountStatusException $ex) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }

    protected function getClientById($id)
    {
        if ($id) {
            return $this->get('orc_saas.repository.client')->find($id);
        }
    }

    protected function getClientByUser(User $user)
    {
        $repository = $this->get('orc_saas.repository.client');
        return $repository->findOneByUser($user);
    }
}
