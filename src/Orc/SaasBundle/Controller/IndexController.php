<?php

namespace Orc\SaasBundle\Controller;

use Symfony\Component\HttpFoundation\Response;

use Orc\SaasBundle\Form\Type\ContactType;
use Orc\SaasBundle\Form\Data\AnalyticsFilter;
use Orc\BookingBundle\Entity\DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{
    public function dashboardAction(Request $request)
    {
        $client = $this->get('synd_multitenant.tenant');

        if (!$client->getStatus() and !$client->getSkipWizard()) {
            return $this->redirect($this->generateUrl('setup_step1'));
        }

        $bookingRepository = $this->get('orc_booking.repository.booking');
        $blackoutsRepository = $this->get('orc_booking.repository.blackout');
        $hoursChangeRepository = $this->get('orc_booking.repository.hourschange');

        $scheduler = $this->get('orc_booking.schedule.scheduler');
        $scheduler->setRange(new DateTime('-7 days'), new DateTime('+7 days'));

        $filter = new AnalyticsFilter();
        $form = $this->createForm($this->get('orc_saas.form.type.filteranalytics'), $filter);

        if ($request->getMethod() == 'POST') {
            $form->bind($request);
        }

        $statsRepository = $this->get('orc_booking.repository.stats');
        $statsRepository->setFilter($filter);

        return $this->render('OrcSaasBundle:Index:dashboard.html.twig', array(
            'form' => $form->createView(),
            'filter' => $filter,
            'availableSlots' => $scheduler->countAvailableSlots($client, 5),
            'recentBookings' => $bookingRepository->findRecentlyBooked(
                $this->container->getParameter('orc_booking.dashboard.recent_bookings_days'),
                $this->container->getParameter('orc_booking.dashboard.recent_bookings')
            ),
            'upcomingChanges' => $hoursChangeRepository->findUpcoming(),
            'currentBlackouts' => $blackoutsRepository->findCurrent(),
            'upcomingBlackouts' => $blackoutsRepository->findUpcoming(new DateTime('+3 months')),
            'apiKey' => $this->container->getParameter('orc_booking.maps.apikey'),
            'stats' => array(
                'byMethod' => $statsRepository->countByMethod($client),
                'byReturning' => $statsRepository->countByReturning($client),
                'totalsConverting' => $statsRepository->totalsByConversion($client)
            ),
            'availability' => $statsRepository->countAvailabilityByPeriod($client)
        ));
    }

    public function contactAction(Request $request)
    {
        $form = $this->createForm(new ContactType());

        if ($request->getMethod() == 'POST') {
            $form->bind($request);

            if ($form->isValid()) {

                $this->get('orc_saas.mailer.contact')->send(
                    $this->get('synd_multitenant.tenant'),
                    $this->get('security.context')->getToken()->getUser(),
                    $request,
                    $form->getData()
                );

                $this->get('session')->getFlashBag()->add('success', 'Message sent!');
                return $this->redirect($request->headers->get('referer') ?: $this->generateUrl('dashboard'));
            }
        }

        return $this->render(
            $request->isXmlHttpRequest() ? 'OrcSaasBundle:Index:contact_content.html.twig' : 'OrcSaasBundle:Index:contact.html.twig',
            array('form' => $form->createView())
        );
    }
}
