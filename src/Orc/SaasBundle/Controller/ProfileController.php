<?php

namespace Orc\SaasBundle\Controller;

use Orc\SaasBundle\Entity\Client;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ProfileController extends Controller
{
    public function profileAction(Request $request, $wizard = false)
    {
        $client = $this->get('synd_multitenant.tenant');
        $form = $this->createForm($this->get('orc_saas.form.type.settings'), $client);

        if ($request->getMethod() == 'POST') {
            $form->bindRequest($request);

            if ($form->isValid()) {
                // XXX
                if ($client->file) {
                    $client->setTemporaryBrandingLogo(uniqid());
                }

                if ($client->getStatus() == Client::STATUS_NEW) {
                    if ($wizard) {
                        $client->setStatus(Client::STATUS_STEP_1);
                    } else {
                        $client->setStatus(Client::STATUS_STEP_5);
                    }
                }


                $em = $this->get('doctrine.orm.entity_manager');
                $em->persist($client);
                $em->flush();

                if ($wizard) {
                    return $this->redirect($this->generateUrl('setup_step2'));
                }

                $this->get('session')->setFlash('success', 'Profile updated successfully.');
                return $this->redirect($this->generateUrl('settings_profile'));
            }
        }

        return $this->render(
            $wizard ? 'OrcBookingBundle:Wizard:1_company_name.html.twig' : 'OrcSaasBundle:Settings:profile.html.twig',
            array(
                'client' => $client,
                'form' => $form->createView(),
            )
        );
    }

    public function indexAction()
    {
        return $this->redirect($this->generateUrl('settings_profile'));
    }

}
