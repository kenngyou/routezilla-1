<?php

namespace Orc\SaasBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class LoginController
{
    protected $container;

    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Handles global logins
     *
     * @param Request $request
     * @return Response
     */
    public function loginAction(Request $request)
    {
        $errors = array();
        $security = $this->container->get('security.context');

        if ($security->isGranted('ROLE_ADMIN')) {
            return new Response('Admins cannot use global login.');
        }
        if ($security->isGranted('ROLE_USER')) {
            $user = $security->getToken()->getUser();
            return $this->redirectToAdmin($user, $request->getScheme());
        }

        if ($request->getMethod() == 'POST') {
            $email = $request->request->get('email');
            $password = $request->request->get('password');
            $provider = $this->container->get('fos_user.user_manager');

            try {
                $user = $provider->loadUserByUsername($request->request->get('email'));

                if ($this->authenticate($user, $password)) {
                    $this->login($user, $password);
                    if ($user->hasRole('ROLE_ADMIN')) {
                        return new Response('Admins cannot use global login.');
                    }
                    return $this->redirectToAdmin($user, $request->getScheme());
                }

                $errors[] = 'Invalid credentials.';

            } catch (UsernameNotFoundException $e) {
                $errors[] = 'Invalid email address.';
            }
        }

        return new Response(
            $this->container->get('templating')->render('OrcSaasBundle:Login:login.html.twig', array(
                'errors' => $errors
            ))
        );
    }

    protected function authenticate($user, $password)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        return $encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt());
    }

    protected function login($user, $password)
    {
        $providerKey = $this->container->getParameter('fos_user.firewall_name');
        $token = new UsernamePasswordToken($user, $password, $providerKey, $user->getRoles());
        $this->container->get("security.context")->setToken($token);
    }

    protected function redirectToAdmin($user, $protocol)
    {
        return new RedirectResponse(sprintf(
            '%s://%s%s',
            $protocol,
            $user->getClient()->getSite()->getDomain(),
            $this->container->get('router')->generate('dashboard')
        ));
    }
}
