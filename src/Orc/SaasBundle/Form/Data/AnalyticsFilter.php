<?php

namespace Orc\SaasBundle\Form\Data;

use Orc\BookingBundle\Entity\Service;

use Orc\BookingBundle\Entity\Region;

use Symfony\Component\Validator\Constraints as Assert;

class AnalyticsFilter
{
    /**
     * @Assert\Date
     */
    protected $dateStart;

    /**
     * @Assert\Date
     */
    protected $dateEnd;

    /**
     * @Assert\Choice(
     *     choices={"day", "month", "year"},
     *     message="Please choose a valid grouping."
     * )
     */
    protected $groupBy;

    protected $region;

    protected $service;

    public function __construct()
    {
        $this->dateStart = new \DateTime('january 1');
        $this->dateEnd = clone $this->dateStart;
        $this->dateEnd->modify('+1 year');

        $this->groupBy = 'month';
    }


    public function setDateStart(\DateTime $date)
    {
        $this->dateStart = $date;
    }

    public function getDateStart()
    {
        return $this->dateStart;
    }

    public function setDateEnd(\DateTime $date)
    {
        return $this->dateEnd = $date;
    }

    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    public function setRegion(Region $region = null)
    {
        $this->region = $region;
    }

    public function getRegion()
    {
        return $this->region;
    }

    public function setService(Service $service = null)
    {
        $this->service = $service;
    }

    public function getService()
    {
        return $this->service;
    }

    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }
}
