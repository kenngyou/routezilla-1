<?php

namespace Orc\SaasBundle\Form\Type;

use Orc\BookingBundle\Form\Type\BoundaryType;
use Orc\BookingBundle\Form\Type\HoursOfOperationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;

class FilterAnalyticsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateStart', 'date', array(
                'widget' => 'single_text',
                'error_bubbling' => true,
                'invalid_message' => 'Please choose a valid start date.'
            ))
            ->add('dateEnd', 'date', array(
                'widget' => 'single_text',
                'error_bubbling' => true,
                'invalid_message' => 'Please choose a valid end date.'
            ))
            ->add('service', 'entity', array(
                'class' => 'Orc\BookingBundle\Entity\Service',
                'empty_value' => 'All Services',
                'required' => false
            ))
            ->add('region', 'entity', array(
                'class' => 'Orc\BookingBundle\Entity\Region',
                'empty_value' => 'All Regions',
                'required' => false
            ))
            ->add('groupBy', 'hidden', array(
/*                 'choices' => array(
                    'day' => 'Weekday',
                    'month' => 'Month',
                    'year' => 'Year'
                ), */
                'data' => 'month',
                'error_bubbling' => true,
                'invalid_message' => 'Please choose a grouping method.'
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\SaasBundle\Form\Data\AnalyticsFilter'
        ));
    }

    public function getName()
    {
        return 'filter_analytics';
    }
}
