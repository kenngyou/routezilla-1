<?php

namespace Orc\SaasBundle\Form\Type;

use Orc\BookingBundle\Form\Type\BoundaryType;
use Orc\BookingBundle\Form\Type\HoursOfOperationType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\FormViewInterface;
use Symfony\Component\Form\FormInterface;

class SettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', 'text', array(
                'label'    => 'Company Name'
            ))
            ->add('email', null, array(
                'label' => 'Contact Email'
            ))
            ->add('website', null, array(
                'help_inline' => '(optional)'
            ))
            ->add('phone')
            ->add('arrivalWindow', null, array(
                'label' => 'Arrival Window',
                'help_inline' => '(in hours)',
                'attr' => array('class' => 'input-mini')
            ))
            ->add('units', 'choice', array(
                'choices' => array('km' => 'Kilometers', 'm' => 'Miles')
            ))
            ->add('defaultRadius', null, array(
                'label' => 'Radius Size',
                'help_inline' => '(in above units)',
                'attr' => array('class' => 'input-mini')
            ))
            ->add('brandingColor', 'text', array(
                'label'    => 'Branding Color',
                'help_inline' => '(optional)',
                'attr' => array('class' => 'color {hash:true,pickerFaceColor:"transparent",pickerFace:3,pickerBorder:0,pickerInsetColor:"black"}'),
                'required' => false
            ))
            ->add('file', 'file', array(
                'label'    => 'Your Logo',
                'help_inline' => '(optional)',
                'required' => false
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Orc\SaasBundle\Entity\Client'
        ));
    }

    public function getName()
    {
        return 'client_settings';
    }
}
