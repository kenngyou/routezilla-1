<?php

namespace Orc\SaasBundle;

final class SaasEvents
{
    const CLIENT_CREATE = 'client.create';
    const CLIENT_SETUP = 'client.setup';
    const CLIENT_CONVERT = 'client.convert';
    const CLIENT_CHANGE_PLAN = 'client.plan';
    const CLIENT_CHANGE_CARD = 'client.card';
}
