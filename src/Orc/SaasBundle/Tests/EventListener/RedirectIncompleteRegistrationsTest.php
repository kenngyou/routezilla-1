<?php

namespace Orc\SaasBundle\Tests\EventListener;

use Orc\SaasBundle\Entity\Client;
use Orc\UserBundle\Entity\User;
use Orc\SaasBundle\EventListener\RedirectIncompleteRegistrations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Synd\MultiTenantBundle\Event\TenantEvent;

class RedirectIncompleteRegistrationsTest extends \PHPUnit_Framework_TestCase
{
    public function testSkipsWithoutTenantSet()
    {
        $listener = new RedirectIncompleteRegistrations(
            $session = $this->getMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
            $router = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface'),
            'admin',
            'routezilla.com'
        );

        $listener->onKernelRequest($this->getEvent());
    }

    public function testSkipsIfClientHasAccount()
    {
        $client = new Client();
        $client->setUser($user = new User());

        $listener = new RedirectIncompleteRegistrations(
            $session = $this->getMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
            $router = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface'),
            'admin',
            'routezilla.com'
        );

        $listener->onTenantFound($this->getTenantEvent($client));
        $listener->onKernelRequest($event = $this->getEvent());
        $this->assertNull($event->getResponse());
    }

    public function testRedirectsIncompleteAccounts()
    {
        $client = new Client();
        $client->setId(5);

        $listener = new RedirectIncompleteRegistrations(
            $session = $this->getMock('Symfony\Component\HttpFoundation\Session\SessionInterface'),
            $router = $this->getMock('Symfony\Component\Routing\Generator\UrlGeneratorInterface'),
            'admin',
            'routezilla.com'
        );

        $session
            ->expects($this->once())
            ->method('set')
            ->with('client')
            ->will($this->returnValue(5))
        ;

        $router
            ->expects($this->once())
            ->method('generate')
            ->with('saas_signup_account')
            ->will($this->returnValue('/goto'))
        ;

        $listener->onTenantFound($this->getTenantEvent($client));
        $listener->onKernelRequest($event = $this->getEvent());

        $response = $event->getResponse();
        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $response);
        $this->assertEquals('http://admin.routezilla.com/goto', $response->getTargetUrl());
    }

    protected function getTenantEvent($client)
    {
        return new TenantEvent($client);
    }

    protected function getEvent($request = null)
    {
        return new GetResponseEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            $request ?: new Request(),
            'meh'
        );
    }
}
