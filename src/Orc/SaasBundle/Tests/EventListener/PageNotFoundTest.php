<?php

namespace Orc\SaasBundle\Tests\EventListener;

use Orc\SaasBundle\EventListener\PageNotFound;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageNotFoundTest extends \PHPUnit_Framework_TestCase
{
    public function testDoesntCatchRegularExceptions()
    {
        $event = $this->getEvent(Request::create('/'), new \Exception());

        $listener = new PageNotFound();
        $listener->onKernelException($event);

        $this->assertNull($event->getResponse());
    }

    public function testCatches404Exceptions()
    {
        $event = $this->getEvent(Request::create('/blah/'), new NotFoundHttpException());

        $listener = new PageNotFound();
        $listener->onKernelException($event);

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
    }

    public function testDoesntCatchRequestsWithoutTrailingSlash()
    {
        $event = $this->getEvent(Request::create('/blah'), new NotFoundHttpException());

        $listener = new PageNotFound();
        $listener->onKernelException($event);

        $this->assertNull($event->getResponse());
    }

    public function testCatchesRequestsWithTrailingSlash()
    {
        $event = $this->getEvent(Request::create('/blah/'), new NotFoundHttpException());

        $listener = new PageNotFound();
        $listener->onKernelException($event);

        $this->assertInstanceOf('Symfony\Component\HttpFoundation\RedirectResponse', $event->getResponse());
        $this->assertEquals('/blah', $event->getResponse()->getTargetUrl());
    }

    public function test404AtRootIsIgnored()
    {
        $event = $this->getEvent(Request::create('/'), new NotFoundHttpException());

        $listener = new PageNotFound();
        $listener->onKernelException($event);

        $this->assertNull($event->getResponse());
    }

    protected function getEvent($request, $exception)
    {
        return new GetResponseForExceptionEvent(
            $this->getMock('Symfony\Component\HttpKernel\HttpKernelInterface'),
            $request,
            'whatsthis',
            $exception
        );
    }
}
