<?php

namespace Orc\SaasBundle\Tests\EventListener;

use Orc\SaasBundle\EventListener\TenantNotFound;
use Symfony\Component\HttpFoundation\Request;
use Synd\MultiTenantBundle\Event\TenantEvent;

class TenantNotFoundTest extends \PHPUnit_Framework_TestCase
{
    public function testNotFoundThrows404()
    {
        $request = Request::create('/', 'GET', array(), array(), array(), array('HTTP_HOST' => 'something.site.com'));
        $listener = new TenantNotFound($request, array('admin'));

        $this->setExpectedException('Symfony\Component\HttpKernel\Exception\NotFoundHttpException');
        $listener->onTenantNotFound(new TenantEvent());
    }

    public function testSafeDomainNotFoundDoesNothing()
    {
        $request = Request::create('/', 'GET', array(), array(), array(), array('HTTP_HOST' => 'admin.site.com'));
        $listener = new TenantNotFound($request, array('admin'));

        $listener->onTenantNotFound(new TenantEvent());
    }
}
