<?php

namespace Orc\SaasBundle\Tests\Entity;

use Orc\SaasBundle\Entity\Site;

class SiteTest extends \PHPUnit_Framework_TestCase
{
    public function testDomainIsLowerCased()
    {
        $site = new Site();
        $site->setDomain('WWW.something.com');

        $this->assertEquals('www.something.com', $site->getDomain());
    }
}
