<?php

namespace Orc\SaasBundle\Tests\Entity;

use Orc\SaasBundle\Entity\Site;
use Orc\SaasBundle\Entity\Client;

class ClientTest extends \PHPUnit_Framework_TestCase
{
    public function testSetDomainCreatesNewSite()
    {
        $clientA = new Client();
        $clientB = new Client();

        $clientA->setDomain('something.com');

        $this->assertEquals('something.com', $clientA->getSite()->getDomain());
        $this->assertNull($clientB->getSite());
    }

    public function testGetDomainGetsFromSite()
    {
        $client = new Client();
        $client->setDomain('something.com');

        $this->assertEquals('something.com', $client->getDomain());
        $this->assertEquals('something.com', $client->getSite()->getDomain());
    }

    public function testGetDomainOnNoSiteReturnsNull()
    {
        $client = new Client();
        $this->assertNull($client->getDomain());
    }

    public function testParentDomainAppendDomain()
    {
        $client = new Client();
        $client->setParentDomain('orcamax.com');
        $client->setDomain('something');

        $this->assertEquals('something.orcamax.com', $client->getDomain());

    }
}
