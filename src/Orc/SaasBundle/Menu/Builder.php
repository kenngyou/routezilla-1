<?php

namespace Orc\SaasBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\Matcher\Matcher;
use Knp\Menu\Matcher\Voter\UriVoter;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav');

        $menu->addChild('Dashboard', array('route' => 'dashboard'));
        $menu->addChild('Bookings', array('route' => 'book_admin_view_day'));

        $menu
            ->addChild('Availability', array('route' => 'availability'))
            ->setAttribute('dropdown', true)
        ;
        $menu['Availability']->addChild('Hours of operations change', array('route' => 'schedule_admin'));
        $menu['Availability']->addChild('Blackouts', array('route' => 'blackouts_admin'));
        $menu['Availability']->addChild('Recurring Blackouts', array('route' => 'blackoutconfigurations_admin'));


        return $menu;
    }

    public function settingsMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav');

        $menu
            ->addChild('Settings', array('route' => 'settings'))
            ->setAttribute('dropdown', true)
        ;

        $menu['Settings']->addChild('Company Profile', array('route' => 'settings_profile'));
        $menu['Settings']->addChild('Hours', array('route' => 'settings_hours'));
        $menu['Settings']->addChild('Services', array('route' => 'services_admin'));
        $menu['Settings']->addChild('Service Area', array('route' => 'regions_admin'));
        $menu['Settings']->addChild('Staff', array('route' => 'settings_staff'));
        $menu['Settings']->addChild('Crews', array('route' => 'settings_crews'));
        $menu['Settings']->addChild('Reports', array('route' => 'export'));
        $menu['Settings']->addChild('Billing', array('route' => 'billing'));

        return $menu;
    }

    public function accountMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav');

        $menu
            ->addChild('Account', array('route' => 'dashboard'))
            ->setAttribute('dropdown', true)
        ;

        $menu['Account']->addChild('Edit Profile', array('route' => 'fos_user_profile_edit'));
        $menu['Account']->addChild('Edit Password', array('route' => 'fos_user_change_password'));
        $menu['Account']->addChild('Logout', array('route' => 'fos_user_security_logout'));

        return $menu;
    }
}
