<?php

namespace Orc\SaasBundle\Menu\Voter;

use Knp\Menu\ItemInterface;
use Knp\Menu\Matcher\Voter\VoterInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RequestVoter implements VoterInterface
{
    protected $request;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function matchItem(ItemInterface $item)
    {
        $requestUri = $this->container->get('request')->getRequestUri();

        if ($item->getUri() == '/dashboard' or $item->getUri() == '/') {
            return $requestUri == $item->getUri();
        }

        return strpos($requestUri, $item->getUri()) === 0;
    }
}
