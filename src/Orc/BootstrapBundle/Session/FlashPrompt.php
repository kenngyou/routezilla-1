<?php

namespace Orc\BootstrapBundle\Session;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Templating\EngineInterface;

class FlashPrompt
{
    protected $session;
    protected $templating;
    protected $template;

    public function __construct(SessionInterface $session, EngineInterface $templating, $template)
    {
        $this->session  = $session;
        $this->templating = $templating;
        $this->template = $template;
    }

    /**
     * Adds a flash to the next page containing a rendered template
     * @param    string        Message to pass to template
     * @param    string        URL to pass to template
     */
    public function add($message, $url)
    {
        $this->session->setFlash(
            'prompt',
            $this->templating->render($this->template, array(
                'message' => $message,
                'url' => $url
            ))
        );
    }
}
