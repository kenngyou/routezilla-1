<?php

namespace Orc\BootstrapBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OrcBootstrapBundle extends Bundle
{
    public function getParent()
    {
        return 'MopaBootstrapBundle';
    }
}
