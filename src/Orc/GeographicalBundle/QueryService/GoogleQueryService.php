<?php

namespace Orc\GeographicalBundle\QueryService;

use Orc\GeographicalBundle\QueryService\QueryResult;
use Vich\GeographicalBundle\QueryService\QueryServiceInterface;

class GoogleQueryService implements QueryServiceInterface
{
    /**
     * {@inheritDoc}
     */
    public function queryCoordinates($query)
    {
        $result = new QueryResult();
        $rawResult = $this->query($query);

        if ($rawResult->status !== 'OK') {
            return $result;
        }

        $components = $rawResult->results[0]->address_components;

        $result->setLatitude($rawResult->results[0]->geometry->location->lat);
        $result->setLongitude($rawResult->results[0]->geometry->location->lng);

        $result->setStreetNumber($this->findComponent('street_number', $components));
        $result->setRoute($this->findComponent('route', $components));
        $result->setCity($this->findComponent('locality', $components));

        if (!$result->getCity()) {
            $result->setCity($this->findComponent('sublocality', $components));
        }

        $result->setProvince($this->findComponent('administrative_area_level_1', $components));
        $result->setCountry($this->findComponent('country', $components));
        $result->setCode($this->findComponent('postal_code', $components));
        if (!$result->getCode()) {
            $result->setCode($this->findComponent('zip_code', $components));
        }

        return $result;
    }

    /**
     * Query Google Maps for location data
     * @param    string        Search query (address string)
     * @return   object        Result object
     */
    protected function query($query)
    {
        return json_decode(file_get_contents(sprintf(
            'http://maps.google.com/maps/api/geocode/json?address=%s&sensor=false',
            urlencode(str_replace(' ', '+', $query))
        )));
    }

    protected function findComponent($component, $componentData)
    {
        foreach ($componentData as $i => $result) {
            if (in_array($component, $result->types)) {
                return $result->long_name;
            }
        }
    }
}
