<?php

namespace Orc\GeographicalBundle\QueryService;

use Orc\GeographicalBundle\QueryService\QueryResult;
use Vich\GeographicalBundle\QueryService\QueryServiceInterface;

class DummyQueryService implements QueryServiceInterface
{
    protected $cacheFile;

    public function setCacheFile($cacheFile)
    {
        $this->cacheFile = $cacheFile;
    }

    /**
     * {@inheritDoc}
     */
    public function queryCoordinates($query)
    {
        if (!file_exists($this->cacheFile)) {
            throw new \Exception('Cannot query dummy data without cache');
        }

        $locations = include $this->cacheFile;

        $result = new QueryResult();

        if (empty($locations[$query])) {
            return $result;
        }

        $found = $locations[$query];

        $streetParts = explode(' ', $found['Street'], 2);

        $result->setLatitude((float)$found['Latitude']);
        $result->setLongitude((float)$found['Longitude']);
        $result->setStreetNumber(reset($streetParts));
        $result->setRoute(end($streetParts));
        $result->setCity($found['City']);
        $result->setProvince($found['Province']);
        $result->setCode($found['Code']);
        $result->setCountry($found['Country']);

        return $result;
    }
}
