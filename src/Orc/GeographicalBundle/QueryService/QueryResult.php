<?php

namespace Orc\GeographicalBundle\QueryService;

use Vich\GeographicalBundle\QueryService\QueryResult as BaseQueryResult;

/**
 * QueryResult.
 *
 * @author Dustin Dobervich
 */
class QueryResult extends BaseQueryResult
{
    protected $streetNumber;

    protected $route;

    protected $city;

    protected $province;

    protected $country;

    protected $code;

    public function setStreetNumber($streetNumber)
    {
        $this->streetNumber = $streetNumber;
    }

    public function getStreetNumber()
    {
        return $this->streetNumber;
    }

    public function setRoute($route)
    {
        $this->route = $route;
    }

    public function getRoute()
    {
        return $this->route;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setProvince($province)
    {
        $this->province = $province;
    }

    public function getProvince()
    {
        return $this->province;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }
    public function getCountry()
    {
        return $this->country;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }
}
