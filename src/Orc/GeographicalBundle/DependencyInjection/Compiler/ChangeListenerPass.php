<?php

namespace Orc\GeographicalBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class ChangeListenerPass implements CompilerPassInterface
{
    /**
     * Change the default listener to use ours instead.
     * In TEST mode, use a dummy query service.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('vich_geographical.listener.geographical')) {
            return;
        }

        $listenerDefinition = $container->getDefinition('vich_geographical.listener.geographical');
        $listenerDefinition->setClass('Orc\GeographicalBundle\EventListener\GeographicalListener');

        if ($container->getParameter('kernel.environment') == 'test') {
            $serviceDefinition = $container->getDefinition('orc_geographical.query_service.orc');
            $serviceDefinition->setClass('Orc\GeographicalBundle\QueryService\DummyQueryService');
            $serviceDefinition->addMethodCall('setCacheFile', array(sprintf('%s/locations.php', $container->getParameter('kernel.cache_dir'))));
        }
    }
}
