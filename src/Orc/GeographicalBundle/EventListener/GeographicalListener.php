<?php

namespace Orc\GeographicalBundle\EventListener;

use Vich\GeographicalBundle\EventListener\GeographicalListener as BaseListener;
use Doctrine\Common\EventArgs;
use Doctrine\Common\EventSubscriber;

class GeographicalListener extends BaseListener implements EventSubscriber
{
    /**
     * {@inheritDoc}
     */
    protected function updateObject($obj, $geo, $geoQuery, $isUpdate = false, EventArgs $eventArgs = null)
    {
        if (null === $geo) {
            return;
        }

        if (null === $geoQuery) {
            throw new \InvalidArgumentException('Unable to find GeograhicalQuery annotation.');
        }


        $queryMethod = $geoQuery->getMethod();
        $query = $obj->$queryMethod();


        // only update when obj has a queryable address

        if (!$query = $this->requiresQuery($obj, $queryMethod)) {
            return;
        }

        $result = $this->queryService->queryCoordinates($query);

        $latSetter = sprintf('set%s', $geo->getLat());
        $lngSetter = sprintf('set%s', $geo->getLng());

        $obj->$latSetter($result->getLatitude());
        $obj->$lngSetter($result->getLongitude());

        if ($obj->getQueryAddress() and !$obj->getKeepChanges()) {
            if (!$obj->getStreet()) {
                $obj->setStreet(sprintf('%s %s', $result->getStreetNumber(), $result->getRoute()));
            }
            $obj->setCity($result->getCity());
            $obj->setProvince($result->getProvince());
            $obj->setCountry($result->getCountry());
            $obj->setCode($result->getCode());
        }

        if ($isUpdate) {
            $this->adapter->recomputeChangeSet($eventArgs);
        }
    }

    protected function requiresQuery($obj, $queryMethod)
    {
        if (!$obj->getLongitude() and !$obj->getLatitude() and $obj->getAddress()) {
            return $obj->getAddress();
        }

        return $obj->$queryMethod();
    }
}
