<?php

namespace Orc\GeographicalBundle;

use Orc\GeographicalBundle\DependencyInjection\Compiler\ChangeListenerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OrcGeographicalBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new ChangeListenerPass());
    }

    public function getParent()
    {
        return 'VichGeographicalBundle';
    }
}
