# RouteZilla High-Level Overview

## Project Goals

RouteZilla is a booking/appointment system to allow business owners to accept bookings online.  It is not a business management tool meant for internal tracking of staff and scheduling.  The scheduling component exists so the business can scale their incoming bookings process while being confident the jobs are being assigned to the most efficient crews / dates.

## Primary Entities

**Client** - The company offering `Bookings`.

**Service** - A type of `Booking`, defined and offered by the `Client`.

**Region** - A geographical region, defined by a series of `Locations`.  Created and serviced by the `Client`.

**Booking** - An appointment requested by a `Customer`.

**Crew** - An assignable unit which performs `Bookings`. 

**Worker** - A person who may be part of a `Crew`.

**Location** - A geographical point, defined with latitude/latitude and/or address information.

**Customer** - The end-user who requests `Bookings` from the `Client`.


## Schedule Calculations

The scheduling component consists of a series of timeslots.  Calculating availability depends on how many `Crews` are available, and the context of the scheduling query. 

### Initial Timeslots

Scheduling is defined with a blanket "Hours of Operation" configuration.  This creates all of the initial timeslots for the application to use.  The `Client` can create exceptions ("schedule adjustments") to change their hours or days off for specific days.  With these per-day changes factored in, we have the "potential availability" calculated.

### Blackouts

The `Client` can also "black out" (take off) entire days, optionally for specific `Crews`, `Regions` or `Services`.  These days are taken out of the schedule openings depending on what is being requested.  Since these `Blackouts` depend on the `Location` and `Service` requested, they are only calculated in the context of a `Booking`.

### Crew Conflicts

When a `Crew` is already booked, those specific time slots are also eliminated.  When a `Crew` is already booked that day, its serviceable area is constrained to within a 5km (by default) radius of that booking.  This is to prevent wasteful driving times.  `Crews` are also fully eliminated when they are unable to satisfy a `Booking`: either they don't service the `Region`, or they don't do the `Service` requested.

## Scheduling Contexts

There are three scheduling contexts, and each treats the scheduling algorithm a little differently.

### 1. Public Bookings

`Bookings` requested by the public use the strictest filtering and calculations.  They may only be assigned within the defined hours of operation, assuming all of the above criteria are met (ie, conflicting timeslots removed).  They may only be assigned to a valid and open `Crew`.  They may not be booked from out of the predefined `Regions`. 

Newly created public `Bookings` are set to the `ATTEMPT` status.