# Routezilla overview

Routezilla automates the booking process for small to medium-sized contractor service businesses. Routezilla accepts booking requests online and assigns them to work crews automatically, taking into account geographic location. This schedule is used as a guideline for business owners to direct their work crews.

The Routezilla application is defined by two areas:

1. The `Public` booking area which gathers `Customer` information and service requests.
2. The `Admin` area where the `Client` can define the services offered by the company, manage `Bookings` and `Booking Availability`.


## Admin

### Setup

The business owner (or `Client`) must initially set up **6** things before accepting bookings:

- **Company Profile** - specifically `Company Name`, `Website` and `Phone`
- **Hours** - this is used to calculate `Booking Availability` (see below)
- **Staff** - people who may be part of a `Crew`
- **Crew** - an assignable unit which performs `Bookings`
- **Services** - a type of `Booking`, defined and offered by the `Client`
- **Service Area** - a geographical region where `Bookings` can be accepted

### Main activities

- reviewing, adjusting, and creating `Bookings`





## Key concepts

### Booking Availability

Booking Availability is the potential number of time slots open for booking, which is effectively the `Hours of operation` multiplied by the number of `Crews`. This base Availability is then adjusted based on an `Hours of operation change` or a `Blackout`.

When a `Booking` is requested, the application displays all the available time slots accounting for any `Hours of operation change` that extend or limit the default `Hours`, and removing any time slots for applicable `Blackouts`.

Administrators can _override_ the automated scheduling by editing a Booking from the Admin interface and clicking `Save Changes` without `Checking Availability`. Although the application will display the `Booking`, it will not automatically assign it to a `Crew` nor will it subtract the manual `Booking` from the overall Booking Availability.


### Blackouts

The `Client` can limit the availability of specific `Crews`, `Regions` or `Services` by means of a `Blackout`. Since these `Blackouts` depend on the `Location` and `Service` requested, they are only calculated in the context of a `Booking`.


### Radius



