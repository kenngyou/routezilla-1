# Billing Implementation

Routezilla charges customers on a per-booking basis using Stripe.  Customers subscribe to a free monthly plan, and are billed 1.99 per valid booking that month at the end of the month.

## Billing lifecycle

**New Customer Signup** - When a customer signs up, a new Stripe customer is also created and associated with the account.  Their first bill is also created at this time for the current month.  Since Stripe does not support first-of-month billing, the customer's trial end date is set to the end of the month, which allows of to offset their billing cycle to the start of the next month.  The newly created bill is empty for now.

**1st of Next Month** - On the first of every month, all of the applicable bookings are tallied up, and the total is added as a line-item to their next Stripe invoice.  These invoices are charged on the second of every month, which gives us the first of each month to prepare them.  Additionally, any bookings created during the trial period (first 30 days of account activity) are tallied with no cost.

**2nd of month** - On the second day of the month, Stripe charges all of the pending invoices to the customers' cards on file.

**Nightly Cleanup** - Every night, a task is run to ensure each client has a bill prepared for the upcoming month.