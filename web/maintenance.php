<?php

header('HTTP/1.1 503 Service Unavailable');
header('Content-Type: text/html');

?>
<html>
    <head>
        <meta http-equiv="refresh" content="5">
        <title>RouteZilla / Maintenance Mode</title>

        <style>
        body {
            padding-top: 60px; 
            padding-bottom: 40px;
        }
        </style>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-responsive.css">
        <link rel="stylesheet" href="/css/global.css">
        <link rel="stylesheet" href="/css/booking.css">
    </head>
    <body>
        <div class="booking well error">
            <h1>Routezilla is currently down for maintenance</h1>
            <p class="lead">We should be back online momentarily.</p>
        </div>
    </body>
</html>