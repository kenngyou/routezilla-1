var dayView = function(day, routes) {
    var map;
    var crews;
    var bookings;
    var bounds;
    var routes;
    
    var mode;
    
    this.modes = {
        View: "view",
        EditRadius: "edit_radius"
    };
    
    var self = this;

    /**
     * Desaturates map when bookings in the past
     */
    var blackAndWhiteStyle = [{
        featureType: "all", 
        elementType: "all", 
        stylers: [
            { saturation: -80 },
            { gamma: 0.7 }
        ]
    }];

    var units;
    
    /**
     * Starts the interactive map
     * @param   DOMElement      Map element
     * @param   obj             Bookings grouped into crews
     */
    this.init = function(el, rawBookings, unassignedBookings, regions, blackAndWhite, _units) {
        units = _units;
        drawMap(el, blackAndWhite);
        
        bounds = new google.maps.LatLngBounds();
        
        setupBookings(rawBookings);
        setupUnassignedBookings(unassignedBookings);
        addRegions(regions);
        finalizeBounds();
        
        bookings = rawBookings;
        
        this.setMode(this.modes.View);
    };
    
    /**
     * Adds the bookings to the map
     */
    var setupBookings = function(rawBookings) {
        for (var j in rawBookings) {
            if (!rawBookings.hasOwnProperty(j)) {
                continue;
            }
                
            var crew = rawBookings[j];
            
            for (var i in crew.bookings) {
                if (!crew.bookings.hasOwnProperty(i)) {
                    continue;
                }
               
                var booking = crew.bookings[i];
                booking.crew = crew;
                booking = addBookingMarker(booking);
                
                bounds.extend(booking.position);
            }
            
            addRouting(crew);
            if (typeof crew.radius != 'undefined') {
                addRadius(crew);
            }
        }
    };
    
    /**
     * Draws a Radius to indicate the Crew's serviceable area for the day
     * @param   obj     Crew (with radius)
     */
    var addRadius = function(crew) {
        var modifier = units == 'km' ? 1000 : 1609.344;
        var circle = new google.maps.Circle({
            map: map,
            center: new google.maps.LatLng(crew.radius.location.lat, crew.radius.location.lng),
            radius: modifier * crew.radius.distance,
            fillColor: crew.color,
            strokeColor: crew.color,
            fillOpacity: 0.15,
            strokeOpacity: 0.6,
            strokeWeight: 1,
            editable: false
        });
        
        crew.circle = circle;
        circle.bindTo('position', map);
        
        // Click to Edit
        google.maps.event.addListener(circle, 'click', function() {
            self.setMode(self.modes.EditRadius, crew.id);
        });
        
        // On Resize: Save
        google.maps.event.addListener(circle, 'radius_changed', function() {
            var data = {
                crew_radius: { radius: circle.getRadius() / modifier } 
            };
            
            $.post(routes.update_radius + '/' + crew.id + '/' + day,  data, function(response) {
                self.setMode(self.modes.View);
            });
            
            return false;
        });
        
        // On Move: Save
        google.maps.event.addListener(circle, 'center_changed', function() {
            var data = {
                crew_radius_move: {
                    location: {
                        latitude: circle.getCenter().lat(),
                        longitude: circle.getCenter().lng()
                    }
                }
            };
            $.post(routes.move_radius + '/' + crew.id + '/' + day,  data, function(response) {
                self.setMode(self.modes.View);
            });
        });
        
        bounds.extend(circle.getCenter());
    };
    
    /**
     * Adds unassigned bookings to the map as markers
     */
    var setupUnassignedBookings = function(rawBookings) {
        for (var i in rawBookings) {
            if (!rawBookings.hasOwnProperty(i)) {
                continue;
            }
            
            var booking = addBookingMarker(rawBookings[i]);
            bounds.extend(booking.position);
        }
    };
    
    var addRegions = function(regions) {
        for (var i in regions) {
            if (!regions.hasOwnProperty(i)) {
                continue;
            }

            addRegionPolygon(regions[i].boundaries);
        }
    };
    
    /**
     * Adds a set of boundaries to the map as a polygon
     * @param   array       Boundaries (lat,lng combinations)
     * @return  google.maps.Polygon
     */
    var addRegionPolygon = function(boundaries) {
        var paths = [];
        for (var i in boundaries) {
            paths.push(new google.maps.LatLng(boundaries[i].lat, boundaries[i].lng));
        }
        
        return new google.maps.Polygon({
            map: map,
            paths: paths,
            fillColor: "#000",
            fillOpacity: 0.05,
            strokeColor: '#000',
            strokeOpacity: 0.2,
            strokeWeight: 1
        });
    };
    
    /**
     * Zooms to the bounds of the markers
     * Zooms out if too close
     */
    var finalizeBounds = function() {
        var runOnceListener = google.maps.event.addListener(map, 'bounds_changed', function(event) {
            if (this.getZoom() > 13) {
                this.setZoom(13);
            }
            google.maps.event.removeListener(runOnceListener);
        });
            
        
        map.fitBounds(bounds);  
    };
    
    /**
     * Add routing for the crew
     */
    var addRouting = function(crew, bounds) {
        var directions = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(map);
        directionsDisplay.setOptions({ 
            suppressMarkers: true,
            preserveViewport: true,
            polylineOptions: {
                strokeColor: crew.color
            }
        });
        
        var directionsRequest = splitBookingsIntoRequest(crew.bookings);
        
        if (directionsRequest.origin && directionsRequest.destination) {
            directions.route(directionsRequest, function(response, status) {
               if (status == google.maps.DirectionsStatus.OK) { 
                   directionsDisplay.setDirections(response);
               }
            });
        }
    };
    
    /**
     * Splits the crew's bookings into a google.maps.DirectionsRequest object
     * @param   obj     Bookings (id as keys)
     * @return  obj     origin, destination, waypoints, etc.
     */
    var splitBookingsIntoRequest = function(bookings) {
        var request = {
            origin: null,
            destination: null,
            waypoints: null,
            travelMode: google.maps.TravelMode.DRIVING
        };
        
        var bookingsArray = [];
        for (var i in bookings) {
            if (bookings.hasOwnProperty(i)) {
                bookingsArray.push(bookings[i].position);
            }
        }
        
        request.origin = bookingsArray.shift();
        request.destination = bookingsArray.pop();
        request.waypoints = [];
        
        for (var i in bookingsArray) {
            request.waypoints.push({ location: bookingsArray[i] });
        }
        
        return request;
    };
    
    /**
     * Adds a marker to the map from a booking
     * @param   obj     Booking info
     * @return  obj     Booking info, with marker / position added
     */
    var addBookingMarker = function(rawBooking) {
        rawBooking.position = new google.maps.LatLng(rawBooking.location.lat, rawBooking.location.lng);
        
        rawBooking.marker = new google.maps.Marker({
           map: map,
           position: rawBooking.position,
           title: rawBooking.name,
           icon: new google.maps.MarkerImage(
               rawBooking.image, 
               new google.maps.Size(20, 34),
               new google.maps.Point(0, 0),
               new google.maps.Point(10, 34)
           ),
           draggable: false
        });

        google.maps.event.addListener(rawBooking.marker, 'click', function() {
            $('#b' + rawBooking.id + ' a').click();
            return false;
        });

        return rawBooking;
    };
    
    /**
     * Draws the map in el
     * @param   DOMElement     Element to place map in
     */
    var drawMap = function(el, blackAndWhite) {
        map = new google.maps.Map(el, {
           zoom: 10,
           center: findCenter(),
           mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        if (blackAndWhite) {
            map.setOptions({ styles: blackAndWhiteStyle });
        }
    };
    
    /**
     * Returns the calculated center of the map
     * @return  google.maps.LatLng
     */
    var findCenter = function() {
        return new google.maps.LatLng( 49.8879519, -119.4960106);
    };
    
    /**
     * Switches modes
     * @param	string		Mode (dayView.modes.X)
     * @param	integer		Crew ID, if applicable
     */
    this.setMode = function(newMode, id) {
        var crew;
        var editCrew;
    	mode = newMode;
    	
    	for (var i in bookings) {
    	    if (!bookings.hasOwnProperty(i)) {
    	        continue;
    	    }
    	    
    	    crew = bookings[i];
    	    if (typeof crew.radius != 'undefined') {
    	        crew.circle.setEditable(false);
    	    }
    	    if (crew.id == id) {
    	        editCrew = crew;
    	    }
    	}
    	
    	if (mode == self.modes.EditRadius) {
    	    editCrew.circle.setEditable(true);
    	} 
    };
    
    this.getMode = function() {
    	return mode;
    };
};
