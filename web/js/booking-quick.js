var quickEditor = function($container) {
	
	var url;
	var lastError;

	if ($container.is('form')) {
	    $form = $container;
	} else {
	    $form = $container.parents('form');
	}

	var $feedback = $('#feedback');
	var $feedbackText = $('#feedback p.help-inline');
	
	
	/**
	 * Gets things rolling
	 */
	this.init = function(targetUrl) {
		url = targetUrl;
        $container.find('input,select').blur(onBlur);
        $form.submit(onSubmit);
        
	};
	
	/**
	 * Triggers validation of the input and returns the result
	 * @return		string		Status code (unavailable, success, baddate)
	 */
	var validate = function() {
		var returnValue = false;
		
		$.ajax({
			type: "POST",
			url: url,
			async: false,
			data: $form.serialize(),
			success: function(result) {
				lastError = null;
				if (result.status == 'error') {
					lastError = result.message;
				}
				returnValue = result.status;
			},
			error: function(error) {
				alert('An error has occured');
			}
		});
		
		return returnValue;
	};

	/**
	 * Triggered when the user moves away from a form element
	 * Updates the on-screen feedback based on their input
	 */
	var onBlur = function(e) {
		var status = validate();
		if (!$(this).val()) {
			clearStatusResult();
			return;
		}
		displayStatusResult(status);
	};
	
	/**
	 * Clears the onscreen feedback
	 */
	var clearStatusResult = function() {
		setFeedback('', '');
	};
	
	/**
	 * Sets the onscreen feedback based on the result
	 * @param	string		Result string (unavailable | success | baddate)
	 */
	var displayStatusResult = function(result) {
        console.log(result);
		if (result == 'error') {
			setFeedback('error', lastError);
		} else if (result == 'available') {
			setFeedback('success', 'That time is available!');
		} else if (result == 'discouraged') {
		    setFeedback('warning', 'That time is not recommended.');
		} else if (result == 'unavailable') {
			setFeedback('error', 'That time is NOT available!');
		} else if (result == 'baddate') {
			setFeedback('error', 'Cannot unstand requested time.');
		} else if (result == 'pastdate') {
            setFeedback('error', 'That date has already passed.');
        }
	};
	
	/**
	 * Prevents submitting of bad data
	 * Updates the onscreen feedback
	 */
	var onSubmit = function(e) {
		var status = validate();
		displayStatusResult(status);
		
		if (status != 'available' && status != 'discouraged') {
			alert('You must assign this booking to a valid crew / date before continuing.');
			return false;
		}
		
		return true;
	};
	
	/**
	 * Update on-screen feedback with a style and text
	 * @param	string		Class name to set feedback with
	 * @param	string		Feedback text to show within inline help
	 */
	var setFeedback = function(className, text) {
        $feedback.removeClass('success warning error').addClass(className);
        $feedbackText.text(text);
	};
};
