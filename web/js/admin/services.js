$(function() {
	$('.collection-item').each(function(index, value) {
		var $col = $(this);
		$(this).find('select').change(function() {
			// Free Response
			if ($(this).val() == 1) {
				$col.find('input:last').parents('.control-group:first').hide();
			// Numeric Response
			} else {
				$col.find('input:last').parents('.control-group:first').show();
			}
		});
		$(this).find('select').change();
	});
});