$('input#client_name').keypress(function (event) {
    setTimeout(function() {
        var str = $('input#client_name').val().replace(/[^a-zA-Z\-]+/g, '');
        $('input#client_domain').val(str.toLowerCase());
    }, 1);
});


// This bit makes sure that if the domain is edited manually
// all UPPERCASE letters are autoconverted to lowerCASE

$("#client_domain").bind('keyup', function (e) {
    if (e.which >= 97 && e.which <= 122) {
        var newKey = e.which - 32;
        e.keyCode = newKey;
        e.charCode = newKey;
    }

    $("#client_domain").val(($("#client_domain").val()).toLowerCase());
});