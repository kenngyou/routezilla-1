$(function() {
	$('.day-off input').change(function() {
        var $row = $(this).parents('tr');

        if ($(this).attr('checked')) {
            $row.addClass('day-off');
        } else {
            $row.removeClass('day-off');
        }
    });
	
	/**
	 * Takes a $(tr) and serializes it into a comparable string
	 * @param  jQuery Collection
	 * @return string
	 */
	var serializeRow = function($row) {
	    var str = '';
	    
	    str += $row.find('td.start select:first').val();
	    str += ':';
	    str += $row.find('td.start select:last').val();
	    
	    str += '-';
	    
        str += $row.find('td.end select:first').val();
        str += ':';
        str += $row.find('td.end select:last').val();
        
        str += '-';
        str += $row.find('input').attr('checked') ? 1 : 0;
        
        return str;
	};
	
	$('tr.day').each(function() {
		var $row = $(this);
		var week = $(this).data('weekday');
		
		$(this).data('default', scheduleDefaults[week]);
		$(this).find('input,select').change(function() {
			if ($row.data('default') !== serializeRow($row)) {
				$row.addClass('modified');
			} else {
				$row.removeClass('modified');
			}
		});
	});
});