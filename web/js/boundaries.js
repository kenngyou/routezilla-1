var Boundaries = function() {
    var map;
    var draw;
    var mode;
    var tmpRegion;
    var regions;
    var routes;
    var self = this;

    this.modes = {
        Browse: "browse",
        AddRegion: "add_region",
        EditRegion: "edit_region"
    };

    var helpContent = {
        browse: {
            heading: "Region Editor",
            body: "Use the buttons below to start defining your service area.  Each region represents an area of which you will accept bookings from."
        },
        add_region: {
            heading: "Create New Region",
            body: "Click on the map to plot points around the area you wish to service.  Connect the points to finish your new region."
        },
        edit_region: {
            heading: "Edit Region",
            body: "Drag the points to edit your region area.  Click a point directly to remove it.  All changes are saved immmediately. "
        }
    };

    var newName;

    /**
     * Prepares the map for usage
     */
    this.init = function(el, existingRegions, systemRoutes) {
        routes = systemRoutes;
        drawMap(el);
        regions = setupRegions(existingRegions);

        this.setMode(this.modes.Browse);
    };

    /**
     * Prepares all existing regions for map display
     * @param   array       Regions (name, boundaries)
     * @return  array       Regions registered to map (name, polygon)
     */
    var setupRegions = function(existingRegions) {
        var out = [];
        var path;
        var bounds = new google.maps.LatLngBounds();

        for (var i in existingRegions) {
            if (!existingRegions.hasOwnProperty(i)) {
                continue;
            }

            var polygon = addRegionPolygon(existingRegions[i].boundaries);

            out[i] = {
                id: i,
                name: existingRegions[i].name,
                polygon: polygon
            };

            polygon.setRegion(out[i]);

            google.maps.event.addListener(polygon, 'mouseover', onPolygonMouseOver);
            google.maps.event.addListener(polygon, 'mouseout', onPolygonMouseOut);
            google.maps.event.addListener(polygon, 'click', onPolygonClick);
            google.maps.event.addListener(polygon, 'click', onVertexClick);

            path = polygon.getPath().getArray();
            for (var i in path) {
                bounds.extend(path[i]);
            }

            map.fitBounds(bounds);
        }

        return out;
    };

    /**
     * Adds a set of boundaries to the map as a polygon
     * @param   array       Boundaries (lat,lng combinations)
     * @return  google.maps.Polygon
     */
    var addRegionPolygon = function(boundaries) {
        var paths = [];
        for (var i in boundaries) {
            paths.push(new google.maps.LatLng(boundaries[i].lat, boundaries[i].lng));
        }

        return new google.maps.Polygon({
            map: map,
            paths: paths
        });
    };

    this.getMode = function() {
        return mode;
    };

    /**
     * Set the editor mode
     * @param   string      Mode (Boundaries.modes.X)
     * @param   integer     ID if applicable
     */
    this.setMode = function(newMode, id) {
        mode = newMode;

        if (draw) {
            draw.setMap(null);
            draw = null;
        }
        for (var i in regions) {
            regions[i].polygon.setEditable(false);
        }

        $('#help h3').text(helpContent[mode].heading);
        $('#help p').text(helpContent[mode].body);

        if (mode == self.modes.AddRegion) {
            addDrawingManager();

        } else if (mode == self.modes.EditRegion) {
            var region = regions[id];
            var polygon = region.polygon;

            var newName = prompt('New Region Name? (leave blank to skip)'); 

            tmpRegion = region;
            if (newName) {
                tmpRegion.name = newName;
                onPolygonChange();
            }
            polygon.setEditable(true);
            map.setCenter(polygon.getCenter());
            google.maps.event.addListener(polygon.getPath(), 'set_at', onPolygonChange);
            google.maps.event.addListener(polygon.getPath(), 'insert_at', onPolygonChange);
            google.maps.event.addListener(polygon.getPath(), 'remove_at', onPolygonChange);
        }
    };

    /**
     * Draws the map on the page inside el
     * Adds each boundary to the map
     *
     * @param   DOMElement
     */
    var drawMap = function(el) {
        map = new google.maps.Map(el, {
            zoom      : 10,
            center    : findCenter(),
            mapTypeId : google.maps.MapTypeId.ROADMAP,
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        });
    };

    /**
     * Enables the drawing manager
     */
    var addDrawingManager = function() {
        draw = new google.maps.drawing.DrawingManager({
            drawingMode: google.maps.drawing.OverlayType.POLYGON,
            drawingControl: false
        });

        draw.setMap(map);
        google.maps.event.addListener(draw, 'polygoncomplete', onPolygonComplete);

    };

    /**
     * Returns a flat, savable region object from the polygon one
     * @param      obj    Region (name, polygon)
     * @return    obj    Region (name, boundaries)
     */
    var getFlatRegion = function(region) {
        var regionInfo = {
            name: region.name,
            boundaries: []
        };

        var coords = region.polygon.getPath().getArray();
        for (var i in coords) {
            regionInfo.boundaries.push({
                order: i,
                location: {
                    latitude: coords[i].lat(),
                    longitude: coords[i].lng()
                }
            });
        }

        return regionInfo;
    };

    /**
     * Once a polygon is created, it's added to the regions array for processing
     * @param   google.maps.Polygon
     */
    var onPolygonComplete = function(polygon) {
        var region = {
            name: prompt('Please enter a name for this Region.'),
            polygon: polygon
        };

        polygon.setRegion(region);

        google.maps.event.addListener(polygon, 'mouseover', onPolygonMouseOver);
        google.maps.event.addListener(polygon, 'mouseout', onPolygonMouseOut);
        google.maps.event.addListener(polygon, 'click', onPolygonClick);
        google.maps.event.addListener(polygon, 'click', onVertexClick);

        regionTmp = region;

        $.post(routes.create, { region: getFlatRegion(region) }, onCreateRegion);
    };


    var onPolygonMouseOver = function() {
        var region = this.getRegion();
        $("#regions tr").removeClass('active');
        $('#region' + region.id).addClass('active');
    };

    var onPolygonMouseOut = function() {
        $("#regions tr").removeClass('active');
    };

    var onPolygonClick = function() {
        self.setMode(self.modes.EditRegion, this.getRegion().id);
    };

    var onVertexClick = function(mev) {
        if (mev.vertex != null) {
            if (confirm('Do you want to remove this node?')) {
                this.getPath().removeAt(mev.vertex);
            }
        }
    };

    /**
     * Called on a successful new region
     * @param    obj   id (ID), table (HTML)
     */
    var onCreateRegion = function(data) {
        regions[data.id] = regionTmp;
        regions[data.id].id = data.id;

        self.setMode(self.modes.Browse);
        $('#regions').html(data.table);
    };

    /**
     * Once a polygon changes, the region update is triggered
     */
    var onPolygonChange = function() {
        var region = tmpRegion;
        $.post(routes.edit + '/' + region.id, { region: getFlatRegion(region) }, onUpdateRegion);
        $('#region' + region.id + ' td.name').text(region.name);
    };

    /**
     * Called on a successful region update
     */
    var onUpdateRegion = function(data) {

    };

    /**
     * Returns the calculated center of the map
     * @return  google.maps.LatLng
     */
    var findCenter = function() {
        var response = JSON.parse(response = $.ajax({
            type: "GET",
            url: "/dashboard/regions/lookup",
            async: false
        }).responseText);

        return new google.maps.LatLng(response.latitude, response.longitude);
    };

    /**
     * Delete a region
     */
    this.removeRegion = function(id) {
        $.post(routes.delete + '/' + id, {}, function(data) {
            regions[id].polygon.setMap(null);
            regions[id] = null;
            delete regions[id];
            $('#regions').html(data);
        });
    };
};

google.maps.Polygon.prototype.setRegion = function(region) {
    this.region = region;
};

google.maps.Polygon.prototype.getRegion = function() {
    return this.region;
};

google.maps.MVCArray.setPolygon = function(polygon) {
    this.polygon = polygon;
};

google.maps.MVCArray.getPolygon = function() {
    return this.polygon;
};

if (!google.maps.Polygon.prototype.getBounds) {
    google.maps.Polygon.prototype.getBounds = function() {
        var bounds = new google.maps.LatLngBounds();
        var paths = this.getPaths();
        var path;

        for (var p = 0; p < paths.getLength(); p++) {
            path = paths.getAt(p);
            for (var i = 0; i < path.getLength(); i++) {
                bounds.extend(path.getAt(i));
            }
        }

        return bounds;
    };
}

if (!google.maps.Polygon.prototype.getCenter) {
    google.maps.Polygon.prototype.getCenter = function() {
        return this.getBounds().getCenter();
    };
}
