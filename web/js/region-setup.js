var Boundaries = function() {
    var map;
    var draw;
    var routes;
    var self = this;
    var region;

    /**
     * Prepares the map for usage
     */
    this.init = function(el, systemRoutes) {
        routes = systemRoutes;
        drawMap(el);
    };

    /**
     * Adds a new City
     */
    this.addCity = function(text) {
        $('#canvas').show();
        google.maps.event.trigger(map, 'resize');

        $('div.modal-body p img').hide();
        
        if (typeof region != 'undefined') {
            region.polygon.setMap(null);
            delete region;
        }
        
        $('#setup p img').hide();

        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'address' : text }, function(results, status) {
           if (status == google.maps.GeocoderStatus.OK) { 
               var position = results[0].geometry.location;

               region = { name: text };

               map.setCenter(position);
               var paths = [];
               var points = pseudoGCircle(position, 10, 6);

               for (var i in points ) {
                   paths.push(new google.maps.LatLng(points[i][0], points[i][1]));
               }

               region.polygon = new google.maps.Polygon({
                   map: map,
                   paths: paths,
                   editable: true
               });
           }
        });
    };

    /**
     * Saves the region
     */
    this.save = function() {
        if (typeof region == 'undefined') {
            alert('Cannot proceed without creating a region first.');
            return false;
        }

        $.post(routes.create, { region: getFlatRegion(region), wizard: true }, function(data) {
           window.location = routes.dashboard; 
        });

        return false;
    };

    /**
     * Draws the map on the page inside el
     * Adds each boundary to the map
     * 
     * @param   DOMElement
     */
    var drawMap = function(el) {
        map = new google.maps.Map(el, {
            zoom      : 10,
            mapTypeId : google.maps.MapTypeId.ROADMAP,
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        });
    };

    /**
     * Returns a flat, savable region object from the polygon one
     * @param      obj    Region (name, polygon)
     * @return    obj    Region (name, boundaries)
     */
    var getFlatRegion = function(region) {
        var regionInfo = {
            name: region.name,
            boundaries: []
        };

        var coords = region.polygon.getPath().getArray();
        for (var i in coords) {
            regionInfo.boundaries.push({
                order: i,
                location: {
                    latitude: coords[i].lat(),
                    longitude: coords[i].lng()
                }
            });
        }

        return regionInfo;
    };
};


/**
 * @param point   Center point in google.maps.LatLng
 * @param radius  Rarius of circle in km
 * @param number  Number of polygon points (optional, default 6)
 * @return array
 */
function pseudoGCircle(point, radius, vertex) {
    var vertex = (vertex) ? vertex : 6;
    var radius = (radius && radius != 0) ? radius : 10;
    var E = 0.00669437999014131699613723354004;
    var latRadians = degreesToRadians(point.lat());
    var TMP = 1 - E * Math.pow(Math.sin(latRadians), 2);
    var A = 110574.27582159444444444444444444; // (PI_ER * (1 - E)) / 180
    var B = 111319.49079327333333333333333333; // PI_ER / 180
    var arc_lat = A / Math.pow(TMP, 3/2);
    var arc_lng = (B * Math.cos(latRadians)) / Math.pow(TMP, 1/2);
    var R = radius * 1000;
    var points = new Array(vertex);
    for (i = 0; i < vertex; i++) {
        var rad = (i / (vertex / 2)) * Math.PI;
        var lat = (R / arc_lat) * Math.sin(rad) + point.lat();
        var lng = (R / arc_lng) * Math.cos(rad) + point.lng();
        points[i] = [lat, lng];
    }
    return points;
}

function degreesToRadians(deg) {
    return deg * (Math.PI / 180);
  }