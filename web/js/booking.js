$(function() {
	/**
	 * When this item's data-target is changed, content is updated via AJAX
	 */
	$('.ajaxFields').each(function() {
	    var $container = $(this);
	    var $target = $($(this).data('target'));
	    
	    $target.change(function() {
	        if ($target.is('select')) {
	            var val = $(this).val();
	        } else {
                var val = false;
                $target.each(function() {
                   if ($(this).val() && $(this).is(':checked')) { 
                       val = true;
                   }
                });
	        }
        
            if (val) {
                var  url = fieldPath + '/' + $(this).val();
                if (typeof bookingId != "undefined" && bookingId) {
                    url += '/' + bookingId;
                }
                
                $.get(url, function(data) {
                    $container.html(data);
                });
            } else {
                $container.html('');
            }	            
	        

	    }).change();
	});
	
	/**
	 * When this item's data-target is toggled, contents visibliity is toggled.
	 * Required elements (HTML 5 required attribute) are toggled as well to prevent
	 * required field errors
	 */
	$('.duplicatable').each(function() {
	    var $container = $(this);
	    var $target = $($(this).data('target'));
	    var $items = $(this).find('input');

	    $items.each(function() {
	        $(this).data('required', $(this).attr('required'));
	    });

	    $target.change(function() {
	        if ($(this).attr('checked')) {
	            $container.hide();
	            $items.attr('required', false);
	        } else {
	            $container.show();
	            $items.each(function() {
	                $(this).attr('required', $(this).data('required'));
	            });    
	        }

	    }).change();
	});
	
	
	
	
});