<?php

if (file_exists(__DIR__ . '/offline')) {
    require __DIR__ . '/maintenance.php';
    exit;
}

if (PHP_SAPI == 'cli-server') {
    $file = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REQUEST_URI'];
    if (!is_dir($file) and file_exists($file)) {
        return false;
    }
}

use Symfony\Component\HttpFoundation\Request;

// If you don't want to setup permissions the proper way, just uncomment the following PHP line
// read http://symfony.com/doc/current/book/installation.html#configuration-and-setup for more information
//umask(0000);

$loader = require_once __DIR__.'/../app/bootstrap.php.cache';
require_once __DIR__.'/../app/AppKernel.php';

$_SERVER['SERVER_NAME'] = preg_replace('#\.\d+\.\d+\.\d+\.\d+\.xip\.io#', '', $_SERVER['SERVER_NAME']);

$kernel = new AppKernel('dev', true);
$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
