<?php

use Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle;

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        date_default_timezone_set('America/Vancouver');
        
        $bundles = array(
            // Symfony Bundles
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new JMS\AopBundle\JMSAopBundle(),
            new JMS\DiExtraBundle\JMSDiExtraBundle($this),
            new JMS\SecurityExtraBundle\JMSSecurityExtraBundle(),
            new Orc\CalendarBundle\OrcCalendarBundle(),
        );
        
        $bundles = array_merge($bundles, array(
            // Vendor Bundles
            new FOS\UserBundle\FOSUserBundle(),
            new Mopa\Bundle\BootstrapBundle\MopaBootstrapBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new Craue\FormFlowBundle\CraueFormFlowBundle(),
            new Elnur\BlowfishPasswordEncoderBundle\ElnurBlowfishPasswordEncoderBundle(),
            new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle(),
            new Vich\GeographicalBundle\VichGeographicalBundle(),
            new Sensio\Bundle\BuzzBundle\SensioBuzzBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
            new Synd\MultiTenantBundle\SyndMultiTenantBundle(),
        ));
        
        $bundles = array_merge($bundles, array(
            // App Bundles
            new Orc\UserBundle\OrcUserBundle(),
            new Orc\BookingBundle\OrcBookingBundle(),
            new Orc\SaasBundle\OrcSaasBundle(),
            new Orc\BootstrapBundle\OrcBootstrapBundle(),
            new Orc\GeographicalBundle\OrcGeographicalBundle(),
            new Orc\BillingBundle\OrcBillingBundle(),
        ));

        if (in_array($this->getEnvironment(), array('dev'))) {
            // Dev and Test Bundles
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/config_'.$this->getEnvironment().'.yml');
    }
}
