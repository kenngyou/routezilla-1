<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20120924223018 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("ALTER TABLE crew_radius ADD location_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE crew_radius ADD CONSTRAINT FK_BF4F1EB564D218E FOREIGN KEY (location_id) REFERENCES location (id) ON DELETE CASCADE");
        $this->addSql("CREATE INDEX IDX_BF4F1EB564D218E ON crew_radius (location_id)");
    
        $this->addSql('ALTER TABLE location ADD old_id INTEGER UNSIGNED NOT NULL');
        
        $this->addSql('
            INSERT INTO location
            SELECT null as id
                 , location.street
                 , location.city
                 , location.code
                 , location.province
                 , location.country
                 , location.latitude
                 , location.longitude
                 , location.id AS old_id
              FROM location
            INNER
              JOIN booking
                ON booking.location_id = location.id
            INNER
              JOIN crew_radius
                ON booking.radius_id = crew_radius.id
        ');

        $this->addSql('
            UPDATE crew_radius radius
            LEFT OUTER
              JOIN booking
                ON booking.radius_id = radius.id
               SET radius.location_id = ( SELECT id
                                            FROM location
                                           WHERE old_id = booking.location_id
                                             AND old_id IS NOT NULL )
         ');
        
        $this->addSql('ALTER TABLE location DROP old_id');
    
    }

    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "mysql");
        
        $this->addSql("ALTER TABLE crew_radius DROP FOREIGN KEY FK_BF4F1EB564D218E");
        $this->addSql("DROP INDEX IDX_BF4F1EB564D218E ON crew_radius");
        $this->addSql("ALTER TABLE crew_radius DROP location_id");
    }
}