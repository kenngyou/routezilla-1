<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your need!
 */
class Version20121022204452 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE client ADD skipWizard TINYINT NOT NULL DEFAULT 0');

    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE client DROP skipWizard');
    }
}
