<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration,
    Doctrine\DBAL\Schema\Schema;

class Version20120925144646 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql("ALTER TABLE user ADD old_username VARCHAR(255)");
        
        $this->addSql("
            UPDATE user
               SET old_username = username
        ");
        
        $this->addSql("UPDATE user SET username = email");
    }

    public function down(Schema $schema)
    {
        $this->addSql("
            UPDATE user
               SET username = old_username
        ");
        
        $this->addSql("ALTER TABLE user DROP old_username");
    }
}