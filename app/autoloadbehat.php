<?php

use Doctrine\Common\Annotations\AnnotationRegistry;

$loader = call_user_func(function() {
    $loader = new \Composer\Autoload\ClassLoader();
    $composerDir = __DIR__ . '/../vendor/composer';

    $map = require $composerDir . '/autoload_namespaces.php';
    foreach ($map as $namespace => $path) {
        $loader->add($namespace, $path);
    }

    $classMap = require $composerDir . '/autoload_classmap.php';
    if ($classMap) {
        $loader->addClassMap($classMap);
    }

    $loader->register();
    return $loader;
});

// intl
if (!function_exists('intl_get_error_code')) {
    require_once __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs/functions.php';

    $loader->add('', __DIR__.'/../vendor/symfony/symfony/src/Symfony/Component/Locale/Resources/stubs');
}

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
