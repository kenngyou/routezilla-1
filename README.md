# Routezilla [![BuildStatus](http://ci.dev.66.209.190.184.xip.io/job/Routezilla/badge/icon)](http://ci.dev.66.209.190.184.xip.io/job/Routezilla)

## Installation

Clone and move into fork of the project.

    curl -s http://getcomposer.org/installer | php
    cp app/config/parameters.yml.dist app/config/parameters.yml
    cp web/.htaccess.dist web/.htaccess
    php composer.phar install
    
Edit `app/config/parameters.yml` with necessary credentials, or change web/.htaccess environment if necessary.  

Change file permissions so `app/cache`, `app/logs`, `app/var`, and `web/uploads` are writable by both the command user and the web user.

To install the application, run the following command.  To specify an environment, pass -e [x] to specify (defaults to dev).

    bin/install
    
You also need to add two scheduled tasks:
   
    10 2 * * * /path/to/app/console orc:mail:send-inactivity-reminders
    30 * * * * /path/to/app/console orc:mail:send-customer-reminders

## Updating

After getting latest code from Git, run the following command which will bring the system data up to date.  Similar to install, it takes the -e option, default to dev.  To update both dev and test (say, while developing), you can alternatively use the `bin/update-dev` command.

    bin/update

## Cleaning / Re-Installing

If you need to wipe an environment (essentially re-installing the system), you can use the following command.  It'll nuke the database, caches, etc. and re-install the system to a clean working state.  To specify an environment, pass -e [x] again.

    bin/clean

## Testing

    bin/test
